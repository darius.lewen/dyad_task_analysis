import numpy as np
from tqdm import tqdm

from load.data_loader import load_single
from load.loading_helper import unique_pairs
from trial_descriptor import load_trial_descriptions, get_dist_p_to_t

np.random.seed(0)


def get_normalized_distances(rec, td):
    trial_start_bins = td['start'].to_numpy()
    p_to_t = get_dist_p_to_t(rec)
    dist_matrix = np.vstack((
        p_to_t['p0_to_comp'],
        p_to_t['p0_to_coop0'],
        p_to_t['p0_to_coop1'],
        p_to_t['p1_to_comp'],
        p_to_t['p1_to_coop0'],
        p_to_t['p1_to_coop1'],
    )).T
    dist_matrix = dist_matrix[trial_start_bins]
    dist_matrix /= dist_matrix.sum(axis=1)[:, None]
    return dist_matrix


def get_normalized_efforts(rec, td):
    trial_start_bins = td['start'].to_numpy()
    p_to_t = get_dist_p_to_t(rec)
    comp_effort = np.min(np.vstack((p_to_t['p0_to_comp'], p_to_t['p1_to_comp'])), axis=0)
    coop0_effort = np.max(np.vstack((p_to_t['p0_to_coop0'], p_to_t['p1_to_coop0'])), axis=0)
    coop1_effort = np.max(np.vstack((p_to_t['p0_to_coop1'], p_to_t['p1_to_coop1'])), axis=0)
    effort_matrix = np.vstack((comp_effort, coop0_effort, coop1_effort)).T
    effort_matrix = effort_matrix[trial_start_bins]
    effort_matrix /= effort_matrix.sum(axis=1)[:, None]
    return effort_matrix


def get_collections(td):
    collections = td['coll_type'].to_numpy()
    collections[collections == 'single_p0'] = 0
    collections[collections == 'single_p1'] = 0
    collections[collections == 'joint0'] = 1
    collections[collections == 'joint1'] = 2
    return collections


def random_split(data, train_frac=.9):
    n = data.shape[1]
    train_n = int(n * train_frac)
    rand_indices = np.arange(n)
    np.random.shuffle(rand_indices)
    train_indices = rand_indices[:train_n]
    test_indices = rand_indices[train_n:]
    train_data = data[:, train_indices]
    test_data = data[:, test_indices]
    return train_data, test_data


def load(dyad_idx, lap):
    rec = load_single(dyad_idx, lap, frames_per_sec=120)
    td = load_trial_descriptions(dyad_idx, lap)
    return rec, td


def cut_first_half(rec, td, t=120*10*60):
    rec = rec[t:]
    for i, start_idx in enumerate(td['start'].to_numpy()):
        if start_idx > t:
            td = td.iloc[i:]
            td['start'] -= t  # to use them as indices later on
            td['end'] -= t
            return rec, td


def get_invites(td):
    # accepted_coop0_invites = (td['trial_class'] == 'accepted_coop0_invite').to_numpy().astype(int)
    # accepted_coop1_invites = (td['trial_class'] == 'accepted_coop1_invite').to_numpy().astype(int)
    # declined_coop0_invites = (td['trial_class'] == 'declined_coop0_invite').to_numpy().astype(int)
    # declined_coop1_invites = (td['trial_class'] == 'declined_coop1_invite').to_numpy().astype(int)
    # coop0_invites = accepted_coop0_invites + declined_coop0_invites
    # coop1_invites = accepted_coop1_invites + declined_coop1_invites
    # return np.vstack([coop0_invites, coop1_invites]).T
    trial_class = td['trial_class'].to_numpy()
    invites = np.zeros_like(trial_class)
    invites[trial_class == 'accepted_coop0_invite'] = 1  # todo here I need to know the agent identity
    invites[trial_class == 'declined_coop0_invite'] = 1
    invites[trial_class == 'accepted_coop1_invite'] = 2
    invites[trial_class == 'declined_coop1_invite'] = 2
    return invites


def get_asym_invites(rec, td):
    trial_start_bins = td['start'].to_numpy()
    coll_type = td['coll_type'].to_numpy()[1:]
    prev_coll_type = td['coll_type'].to_numpy()[:-1]
    invites = np.zeros_like(trial_start_bins)  # todo
    p_to_t = get_dist_p_to_t(rec)[trial_start_bins]
    dist_matrix = np.vstack((
        p_to_t['p0_to_comp'],
        p_to_t['p0_to_coop0'],
        p_to_t['p0_to_coop1'],
        p_to_t['p1_to_comp'],
        p_to_t['p1_to_coop0'],
        p_to_t['p1_to_coop1'],
    )).T
    dist_matrix = dist_matrix[trial_start_bins]


def get_reduced_classes(td):
    classes = td['trial_class'].to_numpy().copy()
    classes[classes == 'concurrent'] = 0
    classes[classes == 'p0_ahead'] = 0
    classes[classes == 'p1_ahead'] = 0
    classes[classes == 'accepted_coop0_invite'] = 1
    classes[classes == 'accepted_coop1_invite'] = 1
    classes[classes == 'declined_coop0_invite'] = 2
    classes[classes == 'declined_coop1_invite'] = 2
    classes[classes == 'anti_corr'] = 0
    classes[classes == 'miscoordination'] = 0
    return classes


def get_classes(td):
    classes = td['trial_class'].to_numpy().copy()
    classes[classes == 'concurrent'] = 0
    classes[classes == 'p0_ahead'] = 1
    classes[classes == 'p1_ahead'] = 1
    classes[classes == 'accepted_coop0_invite'] = 2
    classes[classes == 'accepted_coop1_invite'] = 2
    classes[classes == 'declined_coop0_invite'] = 3
    classes[classes == 'declined_coop1_invite'] = 3
    classes[classes == 'anti_corr'] = 4
    classes[classes == 'miscoordination'] = 5
    return classes


def get_preprocessed(dyad_idx, lap):
    rec, td = load(dyad_idx, lap)
    if lap == 0:
        rec, td = cut_first_half(rec, td)
    collections = get_collections(td)
    # efforts = get_normalized_efforts(rec, td)[1:]
    # prev_prev_collections = collections[:-1]
    # prev_collections = collections[:-1]
    # next_collections = collections[1:]
    # invites = get_invites(td)[1:]
    # prev_classes = get_classes(td)[:-1]
    # prev_reduced_classes = get_reduced_classes(td)[:-1]
    efforts = get_normalized_efforts(rec, td)[4:]
    dists = get_normalized_distances(rec, td)[4:]
    prev_4_collections = collections[:-4]
    prev_3_collections = collections[1:-3]
    prev_2_collections = collections[2:-2]
    prev_1_collections = collections[3:-1]
    next_collections = collections[4:]
    invites = get_invites(td)[4:]
    # asym_invites = get_asym_invites(rec, td)[4:]
    prev_classes = get_classes(td)[:-4]
    prev_reduced_classes = get_reduced_classes(td)[:-4]
    frame0 = np.zeros_like(efforts)
    frame1 = np.zeros_like(efforts)
    frame2 = np.zeros_like(efforts)
    frame3 = np.zeros_like(efforts)
    frame4 = np.zeros_like(efforts)
    frame0[:, 0] = invites
    frame0[:, 1] = prev_2_collections
    frame0[:, 2] = prev_classes
    frame1[:, 0] = prev_1_collections
    frame1[:, 1] = next_collections
    frame1[:, 2] = prev_reduced_classes
    frame2[:, 0] = prev_3_collections
    frame2[:, 1] = prev_4_collections
    frame3 = dists[:, :3]
    frame4 = dists[:, 3:]
    data = np.stack((efforts, frame0, frame1, frame2, frame3, frame4))
    return data


def second_block_n_trial_split(data, length, n=100):
    train_data = np.concatenate([data[:, :length], data[:, length + n:]], axis=1)
    test_data = data[:, length: length + n]
    return train_data, test_data


def gen_all_preprocessed_data():
    for dyad_idx in tqdm(range(len(unique_pairs))):
        first_block_2nd_half = get_preprocessed(dyad_idx, lap=0)
        second_block = get_preprocessed(dyad_idx, lap=1)
        data = np.concatenate([first_block_2nd_half, second_block], axis=1)
        train_data, test_data = second_block_n_trial_split(data, length=first_block_2nd_half.shape[1])
        # train_data, test_data = random_split(data)
        np.save(f'preprocessed_data/train/dyad{dyad_idx}.npy', train_data)
        np.save(f'preprocessed_data/test/dyad{dyad_idx}.npy', test_data)
        np.save(f'preprocessed_data/full/dyad{dyad_idx}.npy', data)


def load_preprocessed_data(dyad_idx, kind):
    efforts, frame0, frame1, frame2, frame3, frame4 = np.load(f'preprocessed_data/{kind}/dyad{dyad_idx}.npy')
    return dict(
        efforts=efforts,
        invites=frame0[:, 0],
        prev_2_collections=frame0[:, 1],
        prev_classes=frame0[:, 2],
        prev_1_collections=frame1[:, 0],
        next_collections=frame1[:, 1],
        prev_reduced_classes=frame1[:, 2],
        prev_3_collections=frame2[:, 0],
        prev_4_collections=frame2[:, 1],
        distances=np.hstack((frame3, frame4))
    )


if __name__ == '__main__':
    gen_all_preprocessed_data()

