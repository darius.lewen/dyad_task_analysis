import numpy as np
# import statsmodels.api as sm
from statsmodels.graphics.tsaplots import plot_acf
from matplotlib import pyplot as plt

from trial_descriptor import load_all_trial_descriptions


def get_single_t_binary(td):
    p0_collections = np.array(td['coll_type'] == 'single_p0').astype(int)
    p1_collections = np.array(td['coll_type'] == 'single_p1').astype(int)
    return p0_collections + p1_collections


def get_invite_binary(td):
    accepted_coop0_invite = np.array(td['trial_class'] == 'accepted_coop0_invite').astype(int)
    accepted_coop1_invite = np.array(td['trial_class'] == 'accepted_coop1_invite').astype(int)
    declined_coop0_invite = np.array(td['trial_class'] == 'declined_coop0_invite').astype(int)
    declined_coop1_invite = np.array(td['trial_class'] == 'declined_coop1_invite').astype(int)
    return accepted_coop0_invite + accepted_coop1_invite + declined_coop0_invite + declined_coop1_invite


def get_invite_after_single_binary(td):
    single_t_binary = get_single_t_binary(td)
    invite_binary = get_invite_binary(td)
    after_single_binary = np.array([False] + list(single_t_binary[:-1].astype(bool)))
    return invite_binary[after_single_binary]


if __name__ == '__main__':
    tds = load_all_trial_descriptions()
    after_single_invite_binaries = [get_invite_after_single_binary(td) for td in tds]
    for after_single_invite_binary in after_single_invite_binaries[20:40]:
        print(after_single_invite_binary)
        if len(after_single_invite_binary) > 10:
            plot_acf(after_single_invite_binary, lags=10)

