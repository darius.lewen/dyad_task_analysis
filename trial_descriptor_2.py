import concurrent
import math
import os
import time

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from tqdm import tqdm

from glm.util import cut_first_half
from load.data_loader import load_single, pair_idx_single_rec_path, recordings_folder
from load.loading_helper import pairs, unique_pairs
from presentation.plotting import scatterplot
from reward_decomposition import get_payoffs
from simulations import get_expected_min_effort_heatmap, get_expected_min_effort
from skill_diff import r_squared
from trial_descriptor import get_trial_class, get_new_ahead_measure, get_opti_pos

from trial_predictor import get_dyad_weighting, effort_based_prediction
from _util import convert, game_width_in_cm, inv_convert

cut_tds = None
uncut_tds = None
debug_flag = False


def get_collection_keys(record):
    return [key for key in record.keys() if 'occupation' in key or 'collected' in key]


def get_collections(record):
    collected_targets = record[get_collection_keys(record)].to_numpy()
    collection_indices = np.where(np.sum(collected_targets, axis=1) == 1)[0]
    return collected_targets[collection_indices], collection_indices


def vec_to_label(coll_vec):
    coll_class_idx = np.where(coll_vec == 1)[0]
    if coll_class_idx == 0:
        return 'single_p0'
    elif coll_class_idx == 1:
        return 'single_p1'
    elif coll_class_idx == 2:
        return 'joint0'
    return 'joint1'


def get_collection_dict(rec):
    return {coll_idx: vec_to_label(hot_encoded_coll)
            for hot_encoded_coll, coll_idx in zip(*get_collections(rec))}


def sits_on_condition(p_idx, t_name, start_idx, p_to_t, sits_on_threshold):
    p_t_dist = p_to_t[f'p{p_idx}_to_{t_name}'][start_idx]
    if p_t_dist < sits_on_threshold:
        return True
    return False


def nearby_condition(p_idx, t_name, start_idx, p_to_t, nearby_threshold):
    p_t_dist = p_to_t[f'p{p_idx}_to_{t_name}'][start_idx]
    other_p_t_dist = p_to_t[f'p{(1 + p_idx) % 2}_to_{t_name}'][start_idx]
    return p_t_dist < other_p_t_dist * nearby_threshold


def inviting(coll_name, condition, *args):
    t_name = convert[coll_name]
    for coop_t_name in ['coop0', 'coop1']:
        for p_idx in [0, 1]:
            if condition(p_idx, coop_t_name, *args):
                if coop_t_name == t_name:
                    return f'accepted_{coop_t_name}_invite'
                elif 'single' in coll_name and f'p{p_idx}' not in coll_name:
                    return f'declined_{coop_t_name}_invite'
    return None


def get_end_indices(trial_start_indices, collection_types, dists_to_targets):
    end_indices = list()
    for start, next_start, coll_type in zip(trial_start_indices[:-1], trial_start_indices[1:], collection_types):
        target_kind = convert[coll_type]
        if 'single' in coll_type:
            coll_p_idx = 0 if 'p0' in coll_type else 1
            dists_coll_p = dists_to_targets[f'p{coll_p_idx}_to_{target_kind}'][start:next_start]
            end_indices.append(start + get_last_entrance_idx(dists_coll_p))
        else:
            dists_p0 = dists_to_targets[f'p0_to_{target_kind}'][start:next_start]
            dists_p1 = dists_to_targets[f'p1_to_{target_kind}'][start:next_start]
            last_entrance_p0 = get_last_entrance_idx(dists_p0)
            last_entrance_p1 = get_last_entrance_idx(dists_p1)
            end_indices.append(start + max([last_entrance_p0, last_entrance_p1]))
    end_indices.append(20 * 60 * 120)
    return end_indices


def get_t_desc_base(rec, lap):
    coll_dict = get_collection_dict(rec)
    coll_indices = list(coll_dict.keys())
    prev_coll_indices = [None] + coll_indices[:-1]
    prev_prev_coll_indices = [None, None] + coll_indices[:-2]
    trial_start_indices = [0] + coll_indices[:-1]
    trial_end_indices = np.array(coll_indices) - 120  # todo does not align with entered timestamps -> its a 50 50 distribution, 50 are in the 'end' bin, and roughly 50 are one bin before
    # collection_types = list(coll_dict.values())
    # trial_end_indices = get_end_indices(trial_start_indices, collection_types, dists_to_targets)
    # get_limiting_agent_idx()
    trial_descriptions = [{'start': start, 'end': end,
                           'start_time': start / 120 if lap == 0 else start / 120 + 20 * 60,
                           'duration': (end - start) / 120,
                           'prev_prev_coll_type': coll_dict[prev_prev_coll_i] if prev_prev_coll_i is not None else 'no_prev',
                           'prev_coll_type': coll_dict[prev_coll_i] if prev_coll_i is not None else 'no_prev',
                           'coll_type': coll_dict[coll_i]}
                          for start, end, coll_i, prev_coll_i, prev_prev_coll_i in
                          zip(trial_start_indices, trial_end_indices, coll_indices,
                              prev_coll_indices, prev_prev_coll_indices)]
    return trial_descriptions


def get_last_entrance_idx(trial_dists_to_targets):
    on_target = trial_dists_to_targets < .05
    was_once_on_target = False
    for i, on_t in enumerate(reversed(on_target)):
        if not was_once_on_target and not on_t:
            print('eroorrrrr!!!')
            raise Exception('errorrr!')
        else:
            was_once_on_target = True
        if not on_t:
            return len(on_target) - i - 1
    return 0


def get_limiting_agent_idx(t_desc, dists_to_targets):
    if 'single' in t_desc['coll_type']:
        return 0 if '0' in t_desc['coll_type'] else 1
    coop_target_kind = convert[t_desc['coll_type']]
    dists_p0 = dists_to_targets[f'p0_to_{coop_target_kind}'][t_desc['start']:t_desc['end'] + 100]  # todo this offset is due to 50 50 problem
    dists_p1 = dists_to_targets[f'p1_to_{coop_target_kind}'][t_desc['start']:t_desc['end'] + 100]
    last_entrance_p0 = get_last_entrance_idx(dists_p0)
    last_entrance_p1 = get_last_entrance_idx(dists_p1)
    if last_entrance_p0 == last_entrance_p1:  # hotfix to solve for the average statistic
        random_agent_idx = int(np.round(np.random.rand(1)))
        limiting_agent_idx = random_agent_idx
    else:
        limiting_agent_idx = np.argmax([last_entrance_p0, last_entrance_p1])
    return limiting_agent_idx


def get_agent_traj_len(rec, t_desc, p_idx):
    trial_rec = rec[t_desc['start']:t_desc['end']+1]
    agent_positions = trial_rec[[f'p{p_idx}_agent_x', f'p{p_idx}_agent_y']].to_numpy()
    traj_len = np.sum(np.linalg.norm(agent_positions[:-1] - agent_positions[1:], axis=1))
    return traj_len


def get_agent_start_end_displacement(rec, t_desc, p_idx):
    trial_rec = rec[t_desc['start']:t_desc['end']+1]
    agent_positions = trial_rec[[f'p{p_idx}_agent_x', f'p{p_idx}_agent_y']].to_numpy()
    start_end_displacement = np.linalg.norm(agent_positions[0] - agent_positions[-1])
    return start_end_displacement


def get_trajectory_distance(rec, t_desc, dists_to_targets):
    if 'single' in t_desc['coll_type']:
        p_idx = 0 if '0' in t_desc['coll_type'] else 1
    else:
        p_idx = get_limiting_agent_idx(t_desc, dists_to_targets)
    # trial_rec = rec[t_desc['start']:t_desc['end']+1]
    # agent_positions = trial_rec[[f'p{p_idx}_agent_x', f'p{p_idx}_agent_y']].to_numpy()
    # traj_dist = np.sum(np.linalg.norm(agent_positions[:-1] - agent_positions[1:], axis=1))
    # if debug_flag:
    #     ax = plt.gca()
    #     ax.set_xlim(0, 1)
    #     ax.set_ylim(0, 1)
    #     ax.plot(agent_positions[:, 0], agent_positions[:, 1])
    traj_dist = get_agent_traj_len(rec, t_desc, p_idx)
    return traj_dist

def get_agent_target_displacements(rec):
    p0 = rec[['p0_agent_x', 'p0_agent_y']].to_numpy()
    p1 = rec[['p1_agent_x', 'p1_agent_y']].to_numpy()
    comp = rec[['comp_x', 'comp_y']].to_numpy()
    coop0 = rec[['coop0_x', 'coop0_y']].to_numpy()
    coop1 = rec[['coop1_x', 'coop1_y']].to_numpy()
    return dict(
        p0_to_comp=np.linalg.norm(p0 - comp, axis=1),
        p0_to_coop0=np.linalg.norm(p0 - coop0, axis=1),
        p0_to_coop1=np.linalg.norm(p0 - coop1, axis=1),
        p1_to_comp=np.linalg.norm(p1 - comp, axis=1),
        p1_to_coop0=np.linalg.norm(p1 - coop0, axis=1),
        p1_to_coop1=np.linalg.norm(p1 - coop1, axis=1),
    )


def get_prev_next_target_displacement(t_desc, rec):
    if t_desc['prev_coll_type'] == 'no_prev':
        next_target = convert[t_desc['coll_type']]
        next_target_pos = rec[[f'{next_target}_x', f'{next_target}_y']].iloc[t_desc['start']].to_numpy()
        return np.linalg.norm(np.ones(2) * .5 - next_target_pos)
    prev_target = convert[t_desc['prev_coll_type']]
    next_target = convert[t_desc['coll_type']]
    prev_target_pos = rec[[f'{prev_target}_x', f'{prev_target}_y']].iloc[t_desc['start'] - 1].to_numpy()
    next_target_pos = rec[[f'{next_target}_x', f'{next_target}_y']].iloc[t_desc['start']].to_numpy()
    return np.linalg.norm(prev_target_pos - next_target_pos)


def get_limiting_agent_next_target_displacement(t_desc, rec, dists_to_targets):
    limiting_agent_idx = get_limiting_agent_idx(t_desc, dists_to_targets)
    next_target = convert[t_desc['coll_type']]
    next_target_pos = rec[[f'{next_target}_x', f'{next_target}_y']].iloc[t_desc['start']].to_numpy()
    limiting_agent_pos = rec[[f'p{limiting_agent_idx}_agent_x', f'p{limiting_agent_idx}_agent_y']].iloc[t_desc['start']].to_numpy()
    return np.linalg.norm(limiting_agent_pos - next_target_pos)


def get_target_dist(t_desc, rec, target):
    trial_start_rec = rec.iloc[t_desc['start']]
    p0_pos = trial_start_rec[['p0_agent_x', 'p0_agent_y']].to_numpy()
    p1_pos = trial_start_rec[['p1_agent_x', 'p1_agent_y']].to_numpy()
    target_pos = trial_start_rec[[f'{target}_x', f'{target}_y']].to_numpy()
    p0_dist = np.linalg.norm(target_pos - p0_pos)
    p1_dist = np.linalg.norm(target_pos - p1_pos)
    criterion = min if target == 'comp' else max
    return criterion([p0_dist, p1_dist])


def get_dists_from_free_agent(prev_coll_type, start_rec):
    dists_from_free_agent = dict(
        from_free_to_other=np.nan,
        from_free_to_coop=np.nan,
        from_free_to_fst_1_opti=np.nan,
        from_free_to_opti=np.nan,
    )
    # t_desc = dict(
    #     asp_dist_to_other=None,
    #     asp_dist_to_coop=None,
    #     asp_dist_to_w10_opti=None,
    #     asp_dist_to_w05_opti=None,
    # )
    if 'single' in prev_coll_type:
        p0 = start_rec[['p0_agent_x', 'p0_agent_y']].to_numpy()
        p1 = start_rec[['p1_agent_x', 'p1_agent_y']].to_numpy()
        collecting_agent = p0 if 'p0' in prev_coll_type else p1
        free_agent = p1 if 'p0' in prev_coll_type else p0
        coop0 = start_rec[['coop0_x', 'coop0_y']].to_numpy()
        coop1 = start_rec[['coop1_x', 'coop1_y']].to_numpy()
        free_to_coop0 = np.linalg.norm(coop0 - free_agent)
        free_to_coop1 = np.linalg.norm(coop1 - free_agent)
        comp = collecting_agent
        targets = np.vstack([comp, coop0, coop1])
        if targets.dtype == np.dtype('O'):
            targets = targets.astype(np.float64)
            # print('cast!')
        opti_pos_w10 = get_opti_pos(targets, w=.99)  # use w=.99 instead of 1
        opti_pos_w05 = get_opti_pos(targets, w=.5)
        dists_from_free_agent['from_free_to_other'] = np.linalg.norm(collecting_agent - free_agent)
        dists_from_free_agent['from_free_to_coop'] = min([free_to_coop0, free_to_coop1])
        dists_from_free_agent['from_free_to_fst_1_opti'] = np.linalg.norm(opti_pos_w10 - free_agent)
        dists_from_free_agent['from_free_to_opti'] = np.linalg.norm(opti_pos_w05 - free_agent)
    return dists_from_free_agent


def gen_trial_descriptions(pair_idx, lap):
    rec = load_single(pair_idx, lap, frames_per_sec=120)
    agent_target_displacements = get_agent_target_displacements(rec)
    trial_descriptions = get_t_desc_base(rec, lap)
    comp_frac = get_comp_frac(pd.DataFrame(trial_descriptions))

    for t_desc in trial_descriptions:
        for target in ['comp', 'coop0', 'coop1']:
            t_desc[f'dist_{inv_convert[target]}'] = get_target_dist(t_desc, rec, target)
        t_desc['prev_next_target_displacement'] = get_prev_next_target_displacement(t_desc, rec) - .05  # remove target radius
        t_desc['path_len'] = get_trajectory_distance(rec, t_desc, dists_to_targets=agent_target_displacements)
        t_desc['limiting_agent_next_target_displacement'] = get_limiting_agent_next_target_displacement(t_desc, rec,
                                                            dists_to_targets=agent_target_displacements) - .05 # remove target radius
        if t_desc['path_len'] < t_desc['limiting_agent_next_target_displacement']:
            path_len_error = t_desc['path_len'] - (t_desc['limiting_agent_next_target_displacement'])
            print(f'corrected_path_len_error: {path_len_error}')
            t_desc['path_len'] = t_desc['limiting_agent_next_target_displacement']
        t_desc['speed'] = t_desc['path_len'] / (t_desc['duration'] + 1e-10)
        if t_desc['speed'] > .007 * 120:  # above speed limit
            print(f'corrected_speed_error: {t_desc["speed"] - .007 * 120}')
            t_desc['speed'] = .007 * 120

        t_desc['path_len_due_to_curvature'] = t_desc['path_len'] - t_desc['limiting_agent_next_target_displacement']
        t_desc['reduction_due_to_adv_placement'] = t_desc['path_len_due_to_curvature'] + t_desc['prev_next_target_displacement'] - t_desc['path_len']

        # t_desc['mis_cord_measure'] = None
        # t_desc['ahead_measure_4'] = None
        t_desc['ahead_measure_4'] = get_new_ahead_measure(dists_to_targets=agent_target_displacements, t_desc=t_desc)
        t_desc['mis_cord_measure'] = t_desc['path_len_due_to_curvature'] / (t_desc['path_len'] + 1e-10)
        t_desc['trial_class'] = get_trial_class(dists_to_targets=agent_target_displacements, rec=rec, t_desc=t_desc, comp_frac=comp_frac)  # todo refactor: this is just a wokring import from the old trial_descriptor.py

        t_desc['p0_traj_len'] = get_agent_traj_len(rec, t_desc, p_idx=0)
        t_desc['p1_traj_len'] = get_agent_traj_len(rec, t_desc, p_idx=1)
        t_desc['p0_traj_len_due_to_curvature'] = t_desc['p0_traj_len'] - get_agent_start_end_displacement(rec, t_desc, p_idx=0)
        t_desc['p1_traj_len_due_to_curvature'] = t_desc['p1_traj_len'] - get_agent_start_end_displacement(rec, t_desc, p_idx=1)
        t_desc['p0_speed'] = t_desc['p0_traj_len'] / (t_desc['duration'] + 1e-10)
        t_desc['p1_speed'] = t_desc['p1_traj_len'] / (t_desc['duration'] + 1e-10)
        if t_desc['p0_speed'] > .007 * 120:  # above speed limit
            t_desc['p0_speed'] = .007 * 120
        if t_desc['p1_speed'] > .007 * 120:  # above speed limit
            t_desc['p1_speed'] = .007 * 120

        t_desc |= get_dists_from_free_agent(t_desc['prev_coll_type'], start_rec=rec.iloc[t_desc['start']])

    dump_trail_descriptions(pair_idx, lap, trial_descriptions)


def get_trial_descriptions_for_dyad(pair_idx):
    pair = unique_pairs[pair_idx]
    lap_amount = pairs.count(pair)
    for lap in range(lap_amount):
        gen_trial_descriptions(pair_idx, lap)
    print(f'finished pair {pair_idx}, {pair}')


def gen_all_trial_descriptions():
    t0 = time.time()
    with concurrent.futures.ProcessPoolExecutor(max_workers=7) as ppe:
        for dyad_idx in range(len(unique_pairs)):
            ppe.submit(get_trial_descriptions_for_dyad, dyad_idx)
    # for pair_idx, pair in enumerate(unique_pairs):
    #     lap_amount = pairs.count(pair)
    #     for lap in range(lap_amount):
    #         gen_trial_descriptions(pair_idx, lap)

    print(f'tds_generation_time: {time.time() - t0}')


def dump_trail_descriptions(pair_idx, lap, trial_descriptions):
    path = pair_idx_single_rec_path(pair_idx, lap)
    trial_descriptions = pd.DataFrame(trial_descriptions)
    trial_descriptions.to_pickle(path + '/trial_descriptions_2.pkl')
    trial_descriptions.to_csv(f'data/tds/td_pair{pair_idx}_lap{lap}.csv')


def load_trial_descriptions(pair_idx, lap):
    path = pair_idx_single_rec_path(pair_idx, lap)
    return pd.read_pickle(path + '/trial_descriptions_2.pkl')


def get_comp_frac(td):
    val_counts = td['coll_type'].value_counts()
    for c_type in ['single_p0', 'single_p1']:
        if c_type not in val_counts.keys():
            val_counts[c_type] = 0
    return val_counts[['single_p0', 'single_p1']].sum() / val_counts.sum()


def unsorted_load_all_trial_descriptions(cut_initial=True):
    all_trial_descriptions = list()
    for pair_idx, pair in enumerate(unique_pairs):
        # lap_amount = pairs.count(pair)
        lap_amount = 2  # we take only the first 2 laps!
        t_descriptions = load_trial_descriptions(pair_idx, lap=0)
        if cut_initial:
            _, t_descriptions = cut_first_half(rec=None, td=t_descriptions)
        for lap in range(1, lap_amount):
            t_descriptions = pd.concat([t_descriptions, load_trial_descriptions(pair_idx, lap)], axis=0)
        all_trial_descriptions.append(t_descriptions)
    return all_trial_descriptions


def print_unsorted_ids_and_comp_fractions():
    unsorted_tds = unsorted_load_all_trial_descriptions()
    comp_fracs = [get_comp_frac(td) for td in unsorted_tds]
    for cf, (i, id) in zip(comp_fracs, enumerate(unique_pairs)):
        print(i, list(id), cf)


def get_p_ids_sorted():
    tds = unsorted_load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    return np.array(unique_pairs)[comp_fractions.argsort()]


def load_all_trial_descriptions(cut_initial=True):
    if cut_initial:
        return load_cut_tds()
    else:
        return load_uncut_tds()


def load_cut_tds():
    global cut_tds
    if cut_tds is None:
        cut_tds = unsorted_load_all_trial_descriptions(cut_initial=True)
        # We calculate the comp fraction sorting always with cut_initial=True!
        unsorted_comp_fractions = [get_comp_frac(td) for td in cut_tds]
        comp_frac_sorting = np.argsort(unsorted_comp_fractions)
        cut_tds = [cut_tds[i] for i in comp_frac_sorting]
    return cut_tds


def load_uncut_tds():
    global uncut_tds
    if uncut_tds is None:
        uncut_tds = unsorted_load_all_trial_descriptions(cut_initial=False)
        # We calculate the comp fraction sorting always with cut_initial=True!
        unsorted_comp_fractions = [get_comp_frac(td) for td in unsorted_load_all_trial_descriptions(cut_initial=True)]
        comp_frac_sorting = np.argsort(unsorted_comp_fractions)
        uncut_tds = [uncut_tds[i] for i in comp_frac_sorting]
    return uncut_tds


def print_error(target, prediction, plot=False):
    error = target - prediction
    print(error.mean(), error.var())
    print(r_squared(target, prediction))
    print()
    if plot:
        plt.figure()
        ax = plt.axes()
        ax.hist(error)


def save_tds_as_csv(path=None):
    if path is None:
        path = f'{recordings_folder}/trial_descriptions'
    if not os.path.exists(path):
        os.makedirs(path)
    tds = load_all_trial_descriptions(cut_initial=False)
    for td, p_ids in zip(tds, get_p_ids_sorted()):
        td.to_csv(f'{path}/{p_ids[0]}_{p_ids[1]}.csv')


if __name__ == '__main__':
    t0 = time.time()

    # gen_trial_descriptions(pair_idx=36, lap=0)  # pair_idx is position in loading_helper.py, just adjust pair_idx
    # gen_trial_descriptions(pair_idx=36, lap=1)
    # gen_trial_descriptions(pair_idx=0, lap=0)  # pair_idx is position in loading_helper.py, just adjust pair_idx
    gen_trial_descriptions(pair_idx=24, lap=1)
    # gen_trial_descriptions(pair_idx=0, lap=1)

    # gen_all_trial_descriptions()  #  classify all trials - roughly 33 min
    #
    # tds = load_all_trial_descriptions(cut_initial=False)
    # # tds = load_all_trial_descriptions()
    # for i, p_ids in enumerate(get_p_ids_sorted()):
    #     print(i, p_ids)
    #
    # save_tds_as_csv()
    #
    # print('Time passed:', (time.time() - t0) / 60)
