import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sympy import bell

from pred_performance import violin_swarm_mean_plot
from trial_descriptor_2 import load_all_trial_descriptions, get_comp_frac


def get_accepted_invite_binary(td):
    accepted_coop0_invite = np.array(td['trial_class'] == 'accepted_coop0_invite').astype(int)
    accepted_coop1_invite = np.array(td['trial_class'] == 'accepted_coop1_invite').astype(int)
    return accepted_coop0_invite + accepted_coop1_invite


def get_invite_binary(td):
    accepted_coop0_invite = np.array(td['trial_class'] == 'accepted_coop0_invite').astype(int)
    accepted_coop1_invite = np.array(td['trial_class'] == 'accepted_coop1_invite').astype(int)
    declined_coop0_invite = np.array(td['trial_class'] == 'declined_coop0_invite').astype(int)
    declined_coop1_invite = np.array(td['trial_class'] == 'declined_coop1_invite').astype(int)
    return accepted_coop0_invite + accepted_coop1_invite + declined_coop0_invite + declined_coop1_invite


def get_single_target_closest_binary(td):
    dist_single = np.array(td['dist_single'])
    dist_joint0 = np.array(td['dist_joint0'])
    dist_joint1 = np.array(td['dist_joint1'])
    return (2 == ((dist_single < dist_joint0).astype(int) + (dist_single < dist_joint1).astype(int))).astype(int)


def get_acc_inv_single_closest_prob(td):
    single_target_closest_binary = get_single_target_closest_binary(td)
    accepted_invite_binary = get_accepted_invite_binary(td)
    invite_binary = get_invite_binary(td)
    single_closest_acc_inv = (2 == (single_target_closest_binary + accepted_invite_binary)).astype(int)
    single_closest_inv = (2 == (single_target_closest_binary + invite_binary)).astype(int)
    return sum(single_closest_acc_inv) / sum(single_closest_inv)


if __name__ == '__main__':
    tds = load_all_trial_descriptions()
    invite_binaries = [get_invite_binary(td) for td in tds]
    accepted_invite_binaries = [get_accepted_invite_binary(td) for td in tds]
    invite_acceptance_probabilities = np.array([np.sum(acc_inv_bin) / np.sum(inv_bin)
                                                if inv_bin.sum() > 30 else np.nan for acc_inv_bin, inv_bin in
                                                zip(accepted_invite_binaries, invite_binaries)])
    inv_acc_prob_single_closest = np.array([get_acc_inv_single_closest_prob(td) for td in tds])
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    thresh_0 = .1
    thresh_1 = .9
    above_thresh_0 = comp_fractions > thresh_0
    below_thresh_1 = comp_fractions < thresh_1
    in_intermediate = (above_thresh_0.astype(int) + below_thresh_1.astype(int)) == 2
    inv_acc_prob_in_intermediate = np.array(invite_acceptance_probabilities)[in_intermediate]
    inv_acc_prob_in_intermediate = inv_acc_prob_in_intermediate[~np.isnan(inv_acc_prob_in_intermediate)]
    print(inv_acc_prob_in_intermediate.mean())
    inv_acc_prob_single_closest = np.array(inv_acc_prob_single_closest)[in_intermediate]
    inv_acc_prob_single_closest = inv_acc_prob_single_closest[~np.isnan(inv_acc_prob_single_closest)]
    print(inv_acc_prob_single_closest.mean())
    # to_plot = pd.DataFrame(dict(invite_acceptance_probabilities=invite_acceptance_probabilities))
    # violin_swarm_mean_plot(data=to_plot, ax=plt.gca())

