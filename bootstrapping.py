import numpy as np
import scipy
import seaborn
from matplotlib import pyplot as plt

from trial_descriptor import get_comp_frac, unsorted_load_all_trial_descriptions

xspace = np.linspace(0, 1, 100)


def get_kde(values, bw_method=.12):
    hist, bins = np.histogram(values, bins=10)
    hist_area = np.sum(1 / 10 * hist)
    kde = scipy.stats.gaussian_kde(values, bw_method=bw_method)(xspace)
    kde_area = np.sum(1 / len(xspace) * kde)
    return kde / kde_area * hist_area


def plot_kde(values, ax, color='gray', alpha=.2, with_hist=True):
    if with_hist:
        ax.hist(values, bins=10)
    ax.plot(xspace, get_kde(values), color=color, alpha=alpha)


def plot_conf_interval(bootstraps, ax):
    kde_s = list()
    for bootstrap in bootstraps:
        kde_s.append(get_kde(bootstrap))
    mean, lower, higher = mean_confidence_interval(np.array(kde_s))
    ax.plot(xspace, mean)
    ax.fill_between(xspace, lower, higher, color='red')


def mean_confidence_interval(data, confidence=0.95):
    mean = np.mean(data, axis=0)
    mean_standard_error = scipy.stats.sem(data, axis=0)
    diff = mean_standard_error * scipy.stats.t.ppf((1 + confidence) / 2., len(data)-1)
    return mean, mean - diff, mean + diff


def binomial(k, n, p):
    return scipy.special.binom(n, k) * p ** k * (1 - p) ** (n - k)


if __name__ == '__main__':
    comp_fractions = np.array([get_comp_frac(td) for td in unsorted_load_all_trial_descriptions()])

    bootstrapped_comp_fractions = list()
    bootstrapped_kde_s = list()
    for _ in range(10**4):
        bootstrap = np.random.choice(comp_fractions, size=len(comp_fractions))
        bootstrapped_comp_fractions.append(bootstrap)
        bootstrapped_kde_s.append(get_kde(bootstrap))

    bootstrapped_kde_s = np.array(bootstrapped_kde_s).T
    confidence_bands = np.quantile(bootstrapped_kde_s, q=[0.025, 0.975], axis=1)
    bootstrap_mean, bootstrap_var = np.mean(bootstrapped_kde_s, axis=1), np.std(bootstrapped_kde_s, axis=1)

    is_competitive = (comp_fractions > .9).astype(int)
    p = is_competitive.mean()
    binomial_distri_mean = p * len(comp_fractions)
    binomial_distri_var = p * len(comp_fractions) * (1 - p)

    fig, axes = plt.subplots(2, 2)
    axes = axes.flatten()

    plot_kde(comp_fractions, axes[0], color='k', alpha=1., with_hist=False)
    plot_kde(comp_fractions, axes[1], color='k', alpha=1.)
    plot_kde(comp_fractions, axes[2], color='k', alpha=1.)

    for bootstrap in bootstrapped_comp_fractions[:50]:
        plot_kde(bootstrap, axes[0], with_hist=False)

    xshade = np.concatenate((xspace, xspace[::-1]))
    yshade = np.concatenate((confidence_bands[1], confidence_bands[0][::-1]))
    axes[1].fill(xshade, yshade, color="lightblue", alpha=.4)

    axes[2].fill_between(xspace, bootstrap_mean - bootstrap_var, bootstrap_mean + bootstrap_var,
                         color='red', alpha=.4)
    axes[2].plot(xspace, bootstrap_mean, color='k')

    x_values = np.arange(0, 50)
    binomial_probs = np.array([binomial(k, len(is_competitive), p) for k in x_values])
    axes[3].scatter(x_values, binomial_probs)
    axes[3].set_xlim((5, 30))

    plt.show()

