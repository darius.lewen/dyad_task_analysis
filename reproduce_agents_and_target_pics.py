import numpy as np
from PIL import Image, ImageDraw, ImageFont, ImageOps


PI = 3.141593


color_players = [
    (87, 117, 180),
    (250, 114, 44)
]

color_competitive_target = [
    (32, 32, 32),
    (255, 255, 255)
]

color_punishing_target = [
    (32, 32, 32),
    (210, 100, 100),
    (255, 255, 255),
]
black = (0, 0, 0)

target_radius = .05  # target and agent sizes determine the game
agent_radius = .02
field_size = 800
font = 'DejaVuSansMono.ttf'


def relative_luminance(col):
    return 0.2126*col[0] + 0.7152*col[1] + 0.0722*col[2]


print(relative_luminance(color_players[0]))
print(relative_luminance(color_players[1]))
print(relative_luminance(color_competitive_target[1]))
print(relative_luminance([1, 1, 1]))


def chance_luminance(color, target_luminance=140.):
    luminance = relative_luminance(color)
    luminance_corrected_color = np.array(color) * target_luminance / luminance
    return tuple([int(c) for c in luminance_corrected_color])


# color_players[0] = chance_luminance(color_players[0])
# color_players[1] = chance_luminance(color_players[1])
# color_competitive_target[1] = chance_luminance(color_competitive_target[1])
print(relative_luminance(color_players[0]))
print(relative_luminance(color_players[1]))
print(relative_luminance(color_competitive_target[1]))

def _radian_to_degree(angle):
    return angle*180/PI


def _fraction_to_degree(fraction):
    rotation_in_radian = fraction * 2 * PI
    offset = PI * 3/2
    return _radian_to_degree(rotation_in_radian + offset)


def get_target_image(fraction=1., is_punishing=False, is_competitive=False,
                     high_p_idx=None, comp_p_idx=None, image_size=256, border_percentage=.05):
    if is_competitive:
        color_0 = color_competitive_target[0]
        if comp_p_idx is not None:
            color_1 = color_players[comp_p_idx]
        else:
            color_1 = color_competitive_target[1]
    elif is_punishing:
        color_0 = color_punishing_target[0]
        color_1 = color_punishing_target[1]
    else:
        color_0 = color_players[(1 + high_p_idx) % 2]
        color_1 = color_players[high_p_idx]
    image = Image.new('RGBA', (image_size, image_size), (0, 0, 0, 0))
    draw = ImageDraw.Draw(image)
    draw.ellipse((0, 0, image_size, image_size),
                 fill=black),
    upper, lower = image_size * (1 - border_percentage), image_size * border_percentage
    draw.ellipse((lower, lower, upper, upper),
                 fill=color_0),
    draw.pieslice((lower, lower, upper, upper),
                  start=_fraction_to_degree(0 + (1-fraction) / 2),
                  end=_fraction_to_degree(fraction + (1-fraction) / 2),
                  fill=color_1)
    if is_punishing:
        image_font = ImageFont.truetype(font, 7 * int(target_radius * field_size))
        draw.text((image_size // 2, image_size // 2), '!', fill=color_punishing_target[2], font=image_font, anchor='mm')
    return image


def get_agent_image(agent_idx=0, image_size=256, border_percentage=0.05):
    image = Image.new('RGBA', (image_size, image_size), (0, 0, 0, 0))
    draw = ImageDraw.Draw(image)
    draw.ellipse((0, 0, image_size, image_size),
                 fill=black),
    # border_percentage = (field_size * agent_radius - 2) / field_size
    upper, lower = image_size * (1 - border_percentage), image_size * border_percentage
    draw.ellipse((lower, lower, upper, upper),
                 fill=color_players[agent_idx]),
    return image


if __name__ == '__main__':
    comp = get_target_image(is_competitive=True)
    coop0 = get_target_image(fraction=5/7, high_p_idx=0)
    coop1 = get_target_image(fraction=5/7, high_p_idx=1)
    p0_agent = get_agent_image()
    p1_agent = get_agent_image(agent_idx=1)
    comp.save('generated_game_objects/comp.png', 'PNG')
    coop0.save('generated_game_objects/coop0.png', 'PNG')
    coop1.save('generated_game_objects/coop1.png', 'PNG')
    p0_agent.save('generated_game_objects/p0_agent.png', 'PNG')
    p1_agent.save('generated_game_objects/p1_agent.png', 'PNG')
    comp.save('generated_game_objects/comp.png', 'PNG')
    coop0.save('generated_game_objects/coop0.png', 'PNG')
    coop1.save('generated_game_objects/coop1.png', 'PNG')
    p0_agent.save('generated_game_objects/p0_agent.png', 'PNG')
    p1_agent.save('generated_game_objects/p1_agent.png', 'PNG')

