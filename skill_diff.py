import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from _util import save_fig
from first_on_target_as_sd import get_only_straight, get_being_first_skill_diff
from inefficiencies_full import comp_frac_x_format
from presentation.plotting import scatterplot
from rew_diff import get_skill_differences
from rew_diff_2 import get_single_target_diff
from reward_decomposition import get_agent_rewards, get_mean_in_cm
from trial_descriptor import load_all_trial_descriptions, get_comp_frac


def get_trial_amount_estimator(tds):
    comp_fractions = [get_comp_frac(td) for td in tds]
    speed = get_mean_in_cm('speed', tds)
    curvature = get_mean_in_cm('distance_ol', tds)
    from_past_target = get_mean_in_cm('chosen_effort', tds) + get_mean_in_cm('path_shortening', tds)
    advantageous_placement = get_mean_in_cm('path_shortening', tds)

    curvature_poly = np.zeros(4)
    curvature_poly[1:] = np.polyfit(comp_fractions, curvature, deg=2)
    from_past_target_poly = np.polyfit(comp_fractions, from_past_target, deg=3)
    advantageous_placement_ploy = np.zeros(4)
    advantageous_placement_ploy[1:] = np.polyfit(comp_fractions, advantageous_placement, deg=2)
    distance_ploy = curvature_poly + from_past_target_poly - advantageous_placement_ploy
    speed_poly = np.polyfit(comp_fractions, speed, deg=1)

    def trial_amount_estimator(x):
        return 20 * 60 / (1 + np.polyval(distance_ploy, x) / np.polyval(speed_poly, x))

    return trial_amount_estimator


def r_squared(org, estimate):
    return 1 - (sum((org - estimate) ** 2) / sum((org - org.mean()) ** 2))


def error_stats(org, estimate):
    e = org - estimate
    print(f'Mean: {e.mean()}')
    print(f'Var: {e.var()}')
    print(f'{r"$R^2$"}: {r_squared(org, estimate)}')


def get_only_straight_single(td):
    towards_p0 = (td['coll_type'] == 'single_p0').to_numpy(int)
    towards_p1 = (td['coll_type'] == 'single_p1').to_numpy(int)
    single = towards_p0 + towards_p1
    p0_ahead = (td['trial_class'] == 'p0_ahead').to_numpy(int)
    p1_ahead = (td['trial_class'] == 'p1_ahead').to_numpy(int)
    concurrent = (td['trial_class'] == 'concurrent').to_numpy(int)
    straight = p0_ahead + p1_ahead + concurrent
    return td.loc[single + straight == 2]


if __name__ == '__main__':
    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)

    single_target_diff = get_single_target_diff(tds)

    payoff_diff = high_rewards - low_rewards
    skill_differences = np.array(get_skill_differences(tds))
    skill_differences[np.isnan(skill_differences)] = 0.

    trial_amount_estimator = get_trial_amount_estimator(tds)

    def get_trial_amounts(tds):  # per block, tds has initial 10 minute cut, divide by 1.5
        return np.array([len(td) / 1.5 for td in tds])

    payoff_diff_calculation = skill_differences * comp_fractions * get_trial_amounts(tds) * .07
    error_stats(payoff_diff, payoff_diff_calculation)

    payoff_diff_estimate = skill_differences * comp_fractions * trial_amount_estimator(comp_fractions) * .07
    error_stats(payoff_diff, payoff_diff_estimate)

    oss_tds = [get_only_straight_single(td) for td in tds]

    oss_skill_difference = np.array(get_skill_differences(oss_tds))
    oss_skill_difference[np.isnan(oss_skill_difference)] = 0.

    oss_payoff_diff_estimate = oss_skill_difference * comp_fractions * trial_amount_estimator(comp_fractions) * .07
    error_stats(payoff_diff, oss_payoff_diff_estimate)

    only_straight_tds = [get_only_straight(td) for td in tds]
    being_first_skill_diff = get_being_first_skill_diff(only_straight_tds)
    bf_payoff_diff_estimate = being_first_skill_diff * comp_fractions * trial_amount_estimator(comp_fractions) * .07
    error_stats(payoff_diff, bf_payoff_diff_estimate)

    scalar = 1.3
    fig, axes = plt.subplots(1, 2, figsize=(1.8 * 2 * scalar, 1.8 * 1.6 * scalar))
    plt.subplots_adjust(bottom=.6, left=.18, top=.98, right=.95, wspace=1.2)

    for ax in axes:
        ax.set_box_aspect(1)
    axes[0].set_ylim([14, 29])
    axes[0].set_xlabel('Fraction of\nsingle targets $\\Phi$')
    axes[0].set_ylabel('Payoff (€)')
    comp_frac_x_format(axes[0])

    xspace = np.linspace(0, 1)
    dyad_mean_payoff = .07 / 2 * trial_amount_estimator(xspace)

    payoff_diff_10 = .1 * xspace * trial_amount_estimator(xspace) * .07
    payoff_diff_30 = .3 * xspace * trial_amount_estimator(xspace) * .07

    # axes[0].plot(xspace, dyad_mean_payoff, c=np.ones(3) * .7)
    # axes[0].plot(xspace, dyad_mean_payoff + payoff_diff_10 / 2, c=np.ones(3) * .5)
    # axes[0].plot(xspace, dyad_mean_payoff - payoff_diff_10 / 2, c=np.ones(3) * .5)
    # axes[0].plot(xspace, dyad_mean_payoff + payoff_diff_30 / 2, c='k')
    # axes[0].plot(xspace, dyad_mean_payoff - payoff_diff_30 / 2, c='k')

    axes[0].plot(xspace, dyad_mean_payoff, c='k')
    axes[0].plot(xspace, dyad_mean_payoff + payoff_diff_10 / 2, c=np.ones(3) * .5)
    axes[0].plot(xspace, dyad_mean_payoff - payoff_diff_10 / 2, c=np.ones(3) * .5)
    axes[0].plot(xspace, dyad_mean_payoff + payoff_diff_30 / 2, c=np.ones(3) * .7)
    axes[0].plot(xspace, dyad_mean_payoff - payoff_diff_30 / 2, c=np.ones(3) * .7)

    high_payoff_curve_10 = dyad_mean_payoff + payoff_diff_10 / 2
    high_payoff_curve_30 = dyad_mean_payoff + payoff_diff_30 / 2

    cost_of_coop_curve_base = dyad_mean_payoff[-1] - dyad_mean_payoff
    cost_of_coop_curve_10 = high_payoff_curve_10[-1] - high_payoff_curve_10
    cost_of_coop_curve_30 = high_payoff_curve_30[-1] - high_payoff_curve_30

    axes[1].plot(xspace, cost_of_coop_curve_base, c='k')
    axes[1].plot(xspace, cost_of_coop_curve_10, c=np.ones(3) * .5)
    axes[1].plot(xspace, cost_of_coop_curve_30, c=np.ones(3) * .7)

    dyad_mean_payoff = .07 / 2 * trial_amount_estimator(comp_fractions)
    comp_mean_payoff = .07 / 2 * trial_amount_estimator(1)
    oss_payoff_diff_estimate = oss_skill_difference * comp_fractions * trial_amount_estimator(comp_fractions) * .07
    oss_comp_payoff_diff_estimate = oss_skill_difference * 1 * trial_amount_estimator(1) * .07
    oss_payoff_estimate = dyad_mean_payoff + oss_payoff_diff_estimate / 2
    oss_comp_payoff_estimate = comp_mean_payoff + oss_comp_payoff_diff_estimate / 2
    cost_of_cooperation = oss_comp_payoff_estimate - oss_payoff_estimate

    bf_payoff_diff_estimate = being_first_skill_diff * comp_fractions * trial_amount_estimator(comp_fractions) * .07
    bf_comp_payoff_diff_estimate = being_first_skill_diff * 1 * trial_amount_estimator(1) * .07
    bf_payoff_estimate = dyad_mean_payoff + bf_payoff_diff_estimate / 2
    bf_comp_payoff_estimate = comp_mean_payoff + bf_comp_payoff_diff_estimate / 2
    cost_of_cooperation_2 = bf_comp_payoff_estimate - bf_payoff_estimate

    to_plot = pd.DataFrame(dict(comp_fractions=comp_fractions, cost_of_cooperation=cost_of_cooperation))
    scatterplot(to_plot, x='comp_fractions', y='cost_of_cooperation')

    axes[1].set_xlabel('Fraction of\nsingle targets $\\Phi$')
    axes[1].set_ylabel('Cost of\ncooperation (€)')
    axes[1].fill_between([-1, 0.08], -1, 12, color='gray', alpha=.3, edgecolor=None, label='not enough data')  # todo find right x value
    axes[1].legend(loc='upper left', bbox_to_anchor=(-.42, -.58), frameon=False)
    axes[1].set_ylim(-.42, 10.02)
    axes[1].set_xlim(-.02, 1.02)
    comp_frac_x_format(axes[1])

    save_fig()


    # def plot_skill_diff_curves(agent_rew_mean_curve, ax, skill_diff=.1, c='k', alpha_base=.3, linewidth_base=2):
    #     rew_diff = skill_diff * xspace * 2 * agent_rew_mean_curve
    #     lower = agent_rew_mean_curve - rew_diff / 2
    #     higher = agent_rew_mean_curve + rew_diff / 2
    #     ax.plot(xspace, lower, label=f'Lower payoff for {int(100 * skill_diff)}% difference in single targets', c=c)
    #     ax.plot(xspace, higher, label=f'Higher payoff for {int(100 * skill_diff)}% difference in single targets', c=c)

