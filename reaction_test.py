import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from load.loading_helper import unique_pairs
from presentation.plotting import scatterplot
from reward_decomposition import get_payoffs
from trial_descriptor import unsorted_load_all_trial_descriptions, get_comp_frac

if __name__ == '__main__':
    reaction_test = pd.read_csv('reaction_test_data/crt.csv')
    dyad_reaction_performance = {pair: [np.nan, np.nan] for pair in unique_pairs}
    for pair_idx, pair in enumerate(dyad_reaction_performance.keys()):
        for i, subject in enumerate(pair):
            if subject in reaction_test['VPNummer'].to_numpy():
                reaction_time = reaction_test['CRT'][reaction_test['VPNummer'] == subject]
                dyad_reaction_performance[pair][i] = float(reaction_time)
    unsorted_tds = unsorted_load_all_trial_descriptions()
    payoffs = [get_payoffs(td) for td in unsorted_tds]
    payoff_diffs = [p0 - p1 for p0, p1 in payoffs]
    reaction_diffs = [p0 - p1 for p0, p1 in dyad_reaction_performance.values()]
    unsorted_comp_fractions = [get_comp_frac(td) for td in unsorted_tds]
    comp_frac_sorting = np.argsort(unsorted_comp_fractions)
    to_plot = pd.DataFrame(dict(
        reaction_diffs=np.array(reaction_diffs)[comp_frac_sorting],
        payoff_diffs=np.array(payoff_diffs)[comp_frac_sorting],
        comp_fractions=np.array(unsorted_comp_fractions)[comp_frac_sorting],
    ))
    fig, axes = plt.subplots(1, 3)
    scatterplot(to_plot, x='comp_fractions', y='reaction_diffs', ax=axes[0])
    scatterplot(to_plot, x='comp_fractions', y='payoff_diffs', ax=axes[1])
    scatterplot(to_plot, x='reaction_diffs', y='payoff_diffs', ax=axes[2])
    # dyad_reaction_performance = pd.DataFrame(dyad_reaction_performance)

