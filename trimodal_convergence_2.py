import random

import numpy as np
import scipy
from matplotlib import pyplot as plt

from trial_descriptor import load_all_trial_descriptions, get_comp_frac


x = np.linspace(0, 1, 100)


def obtain_estimate(samples):
    kde = scipy.stats.gaussian_kde(samples, bw_method=.2)(x)
    return kde / sum(kde)


if __name__ == '__main__':
    comp_fractions = [get_comp_frac(td) for td in load_all_trial_descriptions()]
    conv_measure = list()
    for number_of_samples in range(10, 51, 5):
        conv_measure.append(list())
        for k in range(300):
            samples = random.sample(comp_fractions, number_of_samples)
            next_samples, prev_samples = samples[:5], samples[5:]
            prev = obtain_estimate(prev_samples)
            next = obtain_estimate(prev_samples + next_samples)
            # next = scipy.stats.gaussian_kde(prev_samples + next_samples, bw_method=.2)
            conv_measure[-1].append(sum(abs(prev - next))/100)
    conv_measure = [np.mean(x) for x in conv_measure]
    plt.plot(list(range(len(conv_measure))), conv_measure)

