import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from presentation.plotting import scatterplot
from rew_diff import get_skill_differences
from reward_decomposition import plot_corr_coef
from trial_descriptor import load_all_trial_descriptions, get_comp_frac


def get(df, key):
    return None if key not in df.keys() else df[key][0]


def get_value_counts(td, key):
    return td[[key]].apply(lambda x: x.value_counts()).transpose()


def get_being_first_skill_diff(tds):
    bf_skill_diff = list()
    for td in tds:
        first_count = get_value_counts(td, 'first_entered')
        p0_first = get(first_count, 'p0')
        p1_first = get(first_count, 'p1')
        if p0_first is None or p1_first is None or (p0_first + p1_first) < 50:
            bf_skill_diff.append(np.nan)
        else:
            bf_skill_diff.append(abs(p0_first - p1_first) / (p0_first + p1_first))
    return np.array(bf_skill_diff)


def get_only_straight(td):
    p0_ahead = (td['trial_class'] == 'p0_ahead').to_numpy(int)
    p1_ahead = (td['trial_class'] == 'p1_ahead').to_numpy(int)
    concurrent = (td['trial_class'] == 'concurrent').to_numpy(int)
    straight = p0_ahead + p1_ahead + concurrent
    return td.loc[straight.astype(bool)]


def get_only_straight_joint(td):
    towards_p0 = (td['coll_type'] == 'joint0').to_numpy(int)
    towards_p1 = (td['coll_type'] == 'joint1').to_numpy(int)
    joint = towards_p0 + towards_p1
    p0_ahead = (td['trial_class'] == 'p0_ahead').to_numpy(int)
    p1_ahead = (td['trial_class'] == 'p1_ahead').to_numpy(int)
    concurrent = (td['trial_class'] == 'concurrent').to_numpy(int)
    straight = p0_ahead + p1_ahead + concurrent
    return td.loc[joint + straight == 2]


if __name__ == '__main__':
    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    # skill_differences = np.array(get_skill_differences(tds))
    skill_differences = np.array(get_skill_differences(tds))
    only_straight_joint_tds = [get_only_straight_joint(td) for td in tds]
    # only_straight_tds = [get_only_straight(td) for td in tds]
    being_first_skill_diff = get_being_first_skill_diff(only_straight_joint_tds)
    to_plot = pd.DataFrame(dict(comp_fractions=comp_fractions, skill_differences=skill_differences,
                                being_first_skill_diff=being_first_skill_diff))
    scatterplot(to_plot, x='skill_differences', y='being_first_skill_diff')
    take_out = np.isnan(skill_differences) ^ np.isnan(being_first_skill_diff)
    plot_corr_coef(skill_differences[~take_out], being_first_skill_diff[~take_out], ax=plt.gca())



        # both = get(first_count, 'both')



#
#
# def get_skill_differences(tds):
#     skill_differences = list()
#     for td in tds:
#         coll_count = get_coll_count(td)
#         p0_single = get(coll_count, 'single_p0')
#         p1_single = get(coll_count, 'single_p1')
#         if p0_single is None or p1_single is None or (p0_single + p1_single) < 50:
#             skill_differences.append(None)
#         else:
#             skill_differences.append(abs(p0_single - p1_single) / (p0_single + p1_single))
#     return skill_differences
