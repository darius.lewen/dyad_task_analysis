import pandas as pd
import seaborn as sn

from matplotlib import pyplot as plt

from _util import save_fig
from glm.plotting.performance_plot import get_best_model_count
from glm.util import order

if __name__ == '__main__':
    fig, axes = plt.subplots(1, 2, figsize=(11, 5))
    axes = axes.flatten()
    best_model_count = get_best_model_count()
    all_model_vals = dict(pd.read_pickle('performance.pkl').T['aic'])
    all_model_vals = {key: val for key, val in all_model_vals.items() if len(best_model_count[key]) > 0}
    all_model_vals = pd.DataFrame({key: all_model_vals[key] for key in order})
    performance_heatmap = {key: all_model_vals.T[best_model_count[key]].mean(axis=1) for key in order}
    performance_heatmap = {key: val - min(val) for key, val in performance_heatmap.items()}
    performance_heatmap = pd.DataFrame(performance_heatmap)
    # lim_performance_heatmap = performance_heatmap.copy()
    # lim_performance_heatmap[lim_performance_heatmap > 30] = 30
    # sn.heatmap(lim_performance_heatmap, ax=axes[0], vmin=0, vmax=30, cmap='Reds_r', annot=True, fmt='.0f')
    sn.heatmap(performance_heatmap, ax=axes[0], vmin=0, vmax=30, cmap='Reds_r')
    sn.heatmap(performance_heatmap, ax=axes[1], vmin=0, vmax=300, cmap='Reds_r')
    for ax_label, ax in zip('ABCD', axes):
        ax.text(0, 1.04, ax_label, transform=ax.transAxes, size=14)
        ax.set_xlabel('Dyads with best AICc on Model')
        ax.set_ylabel('Model')
        ax.collections[0].colorbar.set_label('$AICc - AICc_{Dyad\\_model\\_best}$', rotation=-90, labelpad=15)
    plt.subplots_adjust(left=.18, bottom=.40, right=.95, wspace=.8, top=.9, )
    save_fig()
