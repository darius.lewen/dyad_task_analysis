import numpy as np
import pandas as pd
import seaborn
from scipy.stats import pearsonr, entropy
from sklearn.feature_selection import mutual_info_regression

from movement_glm import get_polar_velocity, speed_limit, get_velocity, cartesian_to_polar, pred_target_frame
from load.data_loader import load, pair_idx_to_ids


def get_mutual_info(x, y):
    return mutual_info_regression(x.reshape(-1, 1), y)


if __name__ == '__main__':
    pair_idx = 38
    p_ids, rec_num = pair_idx_to_ids(pair_idx)
    rec = load(pair_idx, frames_per_sec=120)[1]
    # rec = rec.iloc[::pred_target_frame]
    # rec = rec[:5000]
    rec = pd.concat([rec.iloc[i::pred_target_frame] for i in range(pred_target_frame)])
    velocity = get_velocity(rec)
    polar_velocity = cartesian_to_polar(velocity[:, 0], velocity[:, 1])
    eps = 1e-6
    polar_velocity[:, 0] /= speed_limit
    speed_is_zero = polar_velocity[:, 0] == 0
    speed_is_one = polar_velocity[:, 0] == 1
    polar_velocity[:, 0][speed_is_zero] = eps  # for beta distribution, it's defined only for x>0
    polar_velocity[:, 0][speed_is_one] = 1 - eps  # for beta distribution, it's defined only for x<1
    # speed_too_low = polar_velocity[:, 0] == 0

    jump_in_data = polar_velocity[:, 0] > 1
    polar_velocity = polar_velocity[~jump_in_data]
    velocity = velocity[~jump_in_data]

    speed_too_low = polar_velocity[:, 0] < 0.05
    polar_velocity = polar_velocity[~speed_too_low]
    velocity = velocity[~speed_too_low]
    # speed_too_low = polar_velocity[:, 0] < 0.02
    # polar_velocity[:, 1][speed_too_low] = np.pi * np.random.random(size=speed_too_low.astype(int).sum())
    # polar_velocity[:, 1][speed_too_low] *= np.random.choice([-1, 1], size=speed_too_low.astype(int).sum())
    # r_vel, p_vel = pearsonr(velocity[:, 0], velocity[:, 1])
    # r_polar_vel, p_polar_vel = pearsonr(polar_velocity[:, 0], polar_velocity[:, 1])
    # mi = get_mutual_info(velocity[:, 0], velocity[:, 1])
    # mi_polar = get_mutual_info(polar_velocity[:, 0], polar_velocity[:, 1])
    # alpha = 0.006
    alpha = 0.003
    # alpha = 0.01
    # alpha = .1
    seaborn.jointplot(x=velocity[:, 0], y=velocity[:, 1], kind='reg', joint_kws=dict(scatter_kws=dict(alpha=alpha)))
    seaborn.jointplot(x=polar_velocity[:, 0], y=polar_velocity[:, 1], kind='reg', joint_kws=dict(scatter_kws=dict(alpha=alpha)))

    # polar_velocity = get_polar_velocity(rec, speed_limit)
    # seaborn.jointplot(x=polar_velocity[:, 0], y=polar_velocity[:, 1], kind='reg', joint_kws=dict(scatter_kws=dict(alpha=.1)))
    # r_polar_vel_2, p_polar_vel_2 = pearsonr(polar_velocity[:, 0], polar_velocity[:, 1])
    # mi_polar_2 = get_mutual_info(polar_velocity[:, 0], polar_velocity[:, 1])
