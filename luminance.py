import numpy as np


def relative_luminance(col):
    # https://stackoverflow.com/questions/596216/formula-to-determine-perceived-brightness-of-rgb-color
    return 0.2126*col[0] + 0.7152*col[1] + 0.0722*col[2]


def chance_luminance(color, target_luminance=140.):  # target values above 140 cause distortion of original colors
    luminance = relative_luminance(color)
    luminance_corrected_color = np.array(color) * target_luminance / luminance
    return tuple([int(c) for c in luminance_corrected_color])


org_color_players = np.array([
    [87, 117, 180],
    [250, 114, 44]
])
org_color_competitive_target = np.array([255, 255, 255])
print(org_color_players)
print(org_color_players[0])


print(relative_luminance(org_color_players[0]))
print(relative_luminance(org_color_players[1]))
print(relative_luminance(org_color_competitive_target))
print(relative_luminance([1, 1, 1]))


new_col_p0 = chance_luminance(org_color_players[0])
new_col_p1 = chance_luminance(org_color_players[1])
new_col_comp = chance_luminance(org_color_competitive_target)
print(relative_luminance(new_col_p0))
print(relative_luminance(new_col_p1))
print(relative_luminance(new_col_comp))

print(f'Original colors:\nBlue: {org_color_players[0]}\nOrange: {org_color_players[1]}\nSingle_target: {org_color_competitive_target}\n')
print()
print(f'Aquiluminescent colors:\nBlue: {new_col_p0}\nOrange: {new_col_p1}\nSingle_target: {new_col_comp}\n')

