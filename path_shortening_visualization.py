from os import path

import numpy as np
from matplotlib import image, pyplot as plt, animation

from simulations import get_expected_min_effort_heatmap


# img_comp = image.imread('res/images/comp.png')
# img_coop0 = image.imread('res/images/coop0.png')
# img_coop1 = image.imread('res/images/coop1.png')
img_comp = image.imread('generated_game_objects/comp.png')
img_coop0 = image.imread('generated_game_objects/coop0.png')
img_coop1 = image.imread('generated_game_objects/coop1.png')
img_p0_agent = image.imread('generated_game_objects/p0_agent.png')
img_p1_agent = image.imread('generated_game_objects/p1_agent.png')


def get_heatmap_shift(heatmap_number=0, *args, **kwargs):
    file_name = f'heatmap_shifts/heatmap{heatmap_number}.npy'
    if not path.exists(file_name):
        heatmaps = calc_heatmap_shift(*args, **kwargs)
        np.save(file_name, heatmaps)
    return np.load(file_name)


def calc_heatmap_shift(comp, coop0, coop1, n):
    shift_amount = len(comp)
    heatmaps = np.zeros((shift_amount, n, n))
    for i in range(shift_amount):
        targets = np.vstack((comp[i], coop0[i], coop1[i]))
        heatmaps[i] = get_expected_min_effort_heatmap(targets, res=n).T
    return heatmaps


def get_heatmap_shifts(target_positions):
    heatmap_shifts = list()
    for i, (comp_positions, coop0_positions, coop1_positions) in enumerate(zip(*target_positions.values())):
        heatmap_shifts.append(get_heatmap_shift(i, comp_positions, coop0_positions, coop1_positions, heatmap_res))
    return np.array(heatmap_shifts)


def calc_extent(pos, n, size=.1, aspect_ratio=1.):
    base_x, base_y = (pos - size / 2) * n
    return base_x, base_x + size * n * aspect_ratio, base_y, base_y + size * n


def target_included(target_pos):
    return not target_pos[0] > 1.


def plot_heatmap(ax, heatmap, comp, coop0, coop1, n, vmin, vmax, plot_opti=False):
    if plot_opti:
        opti = np.argwhere(heatmap == heatmap.min())[0] / n
    else:
        opti = None
    heat_img = ax.imshow(heatmap.T, cmap='magma_r', vmin=vmin, vmax=vmax)
    plot_targets(ax, comp, coop0, coop1, n, opti=opti)
    return heat_img


def plot_targets(ax, comp, coop0, coop1, n, opti=None, p0=None, p1=None, prev_t=None, zorder=100):
    comp = np.array(comp)
    coop0 = np.array(coop0)
    coop1 = np.array(coop1)
    p1_offset = np.array([-.01, .005] if opti is not None else [0, 0])
    ax.set_box_aspect(1)
    ax.tick_params(left=False, right=False, labelleft=False,
                   labelbottom=False, bottom=False)
    if target_included(comp):
        ax.imshow(img_comp, aspect='auto', extent=calc_extent(comp + p1_offset, n), zorder=zorder)  # because practically the circle is spanned from the other agent not the target...
        # ax.imshow(img_comp, aspect='auto', extent=calc_extent(comp, n))
        # ax.imshow(img_p1_agent, aspect='auto', extent=calc_extent(comp + p1_offset, n, size=.04))
    if target_included(coop0):
        ax.imshow(img_coop0, aspect='auto', extent=calc_extent(coop0, n), zorder=zorder)
    if target_included(coop1):
        ax.imshow(img_coop1, aspect='auto', extent=calc_extent(coop1, n), zorder=zorder)
    if opti is not None:
        ax.imshow(img_p0_agent, aspect='auto', extent=calc_extent(np.array(opti), n, size=.04), zorder=zorder + 20)
        ax.imshow(img_p1_agent, aspect='auto', extent=calc_extent(comp, n, size=.04), zorder=zorder + 20)
    if p0 is not None:
        ax.imshow(img_p0_agent, aspect='auto', extent=calc_extent(p0, n, size=.04), zorder=zorder + 20)
    if p1 is not None:
        ax.imshow(img_p1_agent, aspect='auto', extent=calc_extent(p1, n, size=.04), zorder=zorder + 20)
    if prev_t is not None:
        if prev_t == 'comp':
            prev_t_img = img_comp.copy()
        elif prev_t == 'coop0':
            prev_t_img = img_coop0.copy()
        else:
            prev_t_img = img_coop1.copy()
        prev_t_img[:, :, 3] *= .4  # alpha
        ax.imshow(prev_t_img, aspect='auto', extent=calc_extent(p0, n), zorder=0)
    ax.scatter(*(comp * n), c='k', alpha=0.)  # hotfix todo do this right


if __name__ == '__main__':

    shift_amount = 64
    heatmap_res = 10

    target_positions = dict(
        comp=list(),
        coop0=list(),
        coop1=list(),
    )

    # only comp - heatmap 1
    target_positions['comp'].append(np.vstack(2 * (np.linspace(0, .5, shift_amount),)).T)
    target_positions['coop0'].append(np.ones((shift_amount, 2)) * 2)
    target_positions['coop1'].append(np.ones((shift_amount, 2)) * 2)

    # only comp - heatmap 2
    target_positions['comp'].append(np.vstack([np.ones(shift_amount) * .2, np.linspace(.1, .85, shift_amount)]).T)
    target_positions['coop0'].append(np.ones((shift_amount, 2)) * 2)
    target_positions['coop1'].append(np.ones((shift_amount, 2)) * 2)

    # one coop - heatmap 3
    target_positions['comp'].append(np.array(shift_amount * (np.array([.2, .4]),)))
    target_positions['coop0'].append(np.vstack(2 * (np.linspace(.85, 0, shift_amount),)).T)
    target_positions['coop1'].append(np.ones((shift_amount, 2)) * 2)

    # full config - heatmap 4
    target_positions['comp'].append(np.array(shift_amount * (np.array([.2, .4]),)))
    target_positions['coop0'].append(np.vstack(2 * (np.linspace(0, .85, shift_amount),)).T)
    target_positions['coop1'].append(np.array(shift_amount * (np.array([.63, .2]),)))

    heatmap_shifts = get_heatmap_shifts(target_positions)

    fig, axes = plt.subplots(2, 2)
    fig.suptitle('Expected minimal effort for orange agent placement')
    axes = axes.flatten()
    vmin, vmax = heatmap_shifts.min(), heatmap_shifts.max()
    img = plot_heatmap(axes[0], heatmap_shifts[0][0],
                       target_positions['comp'][0][0],
                       target_positions['coop0'][0][0],
                       target_positions['coop1'][0][0],
                       heatmap_res, vmin=vmin, vmax=vmax)
    fig.colorbar(img, ax=axes.ravel().tolist())


    def animate(frame_idx):
        for heatmap_idx in range(4):
            plot_heatmap(axes[heatmap_idx], heatmap_shifts[heatmap_idx][frame_idx],
                         target_positions['comp'][heatmap_idx][frame_idx],
                         target_positions['coop0'][heatmap_idx][frame_idx],
                         target_positions['coop1'][heatmap_idx][frame_idx],
                         heatmap_res, vmin=vmin, vmax=vmax)

    anim = animation.FuncAnimation(fig, animate,
                                   frames=shift_amount)
    FFwriter = animation.FFMpegWriter(fps=20)
    anim.save('plots/path_shortening_visualization.mp4', writer=FFwriter, dpi=200)

