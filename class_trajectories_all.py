import concurrent

import matplotlib
import numpy as np

matplotlib.use('Agg')
import os

import matplotlib.pyplot as plt

from class_trajectories import plot_trial_sheet#, get_lin_traj
from load.data_loader import load_single, pair_idx_single_rec_path
from load.loading_helper import unique_pairs
from trial_descriptor_2 import load_trial_descriptions, get_comp_frac
from trial_descriptor import is_different_target

fontdict = dict(size=10)
pair_names = [f'replay_idx_{i}_pair_id_{unique_pairs[i][0]}_{unique_pairs[i][1]}'
              for i in range(len(unique_pairs))]
trial_classes = ['anti_corr', 'miscoordination', 'p0_ahead', 'p1_ahead', 'accepted_coop0_invite',
                 'accepted_coop1_invite', 'declined_coop0_invite', 'declined_coop1_invite',
                 'concurrent']


def create_directories():
    for pair_name in pair_names:
        p = f'plots/class_trajectories/per_dyad/{pair_name}'
        if not os.path.exists(p):
            os.makedirs(p)
        for lap in [0, 1]:
            lap_p = f'{p}/block{lap}'
            if not os.path.exists(lap_p):
                os.makedirs(lap_p)
    for trial_class in trial_classes:
        p = f'plots/class_trajectories/per_class/{trial_class}'
        if not os.path.exists(p):
            os.makedirs(p)


def get_format_info(info, key, rjust_width=14, op=None):
    info_sub_str = info[key]
    if isinstance(info_sub_str, (np.float64, np.int64)):
        if op is not None:
            info_sub_str = op(info_sub_str)
        info_sub_str = "%.2f" % info_sub_str
    info_sub_str = info_sub_str.rjust(rjust_width, '_')
    return info_sub_str


def load_predictions(pair_idx, lap):
    path = pair_idx_single_rec_path(pair_idx, lap)
    return np.load(f'{path}/prediction.npy')


def get_entropy(prediction, rjust_width=14):
    entropy = -np.sum(prediction * np.log(prediction))
    entropy_str = '%.2f' % entropy
    entropy_str = entropy_str.rjust(rjust_width, '_')
    return entropy_str


def gen_class_traj(trial_idx):
    # trial_idx = 295
    # trial_idx = 224
    fig = plt.figure(figsize=(5, 2))
    plt.subplots_adjust(right=.5, left=0.)
    ax = plt.axes()
    ax.set_box_aspect(1)
    ax.set_xlim([-.05, 1.05])
    ax.set_ylim([-.05, 1.05])
    ax.tick_params(left=False, right=False, labelleft=False,
                   labelbottom=False, bottom=False)
    line0, _ = plot_trial_sheet(rec, td, trial_idx, ax)

    different_aim = is_different_target(rec, int(td['start'].iloc[trial_idx]), int(td['end'].iloc[trial_idx]), comp_frac)

    info = td.iloc[trial_idx]
    ljust_width = 24
    info_keys = ['start', 'end', 'duration', 'coll_type', 'prev_coll_type', 'prev_next_target_displacement', 'path_len',
                 'limiting_agent_next_target_displacement', 'speed', 'path_len_due_to_curvature',
                 'reduction_due_to_adv_placement', 'ahead_measure_4', 'mis_cord_measure', 'trial_class']
    info_str = [ # todo reimplement when trial descriptor refactoring is finished
        'Classification:'.ljust(ljust_width - 8, '_') + get_format_info(info, 'trial_class', rjust_width=14 + 8),
        'Payoff/time (€/sec):'.ljust(ljust_width, '_') + get_format_info(info, 'duration', op=lambda x: 0.07/x),
        'Movement duration (sec):'.ljust(ljust_width, '_') + get_format_info(info, 'duration'),
        'Trajectory distance:'.ljust(ljust_width, '_') + get_format_info(info, 'path_len'),
        'Speed (dist/sec):'.ljust(ljust_width, '_') + get_format_info(info, 'speed'),
        'Non-straight line dist:'.ljust(ljust_width, '_') + get_format_info(info, 'path_len_due_to_curvature'),
        'Prev to next t dist:'.ljust(ljust_width, '_') + get_format_info(info, 'prev_next_target_displacement'),
        'Agent place dist reduct:'.ljust(ljust_width, '_') + get_format_info(info, 'reduction_due_to_adv_placement'),
        '',
        'All distances are in percentage of',
        'field_width (=800px)',
        '',
        # 'Different aim:'.ljust(ljust_width, '_') + str(different_aim).rjust(14, '_'),
        # 'Curve measure:'.ljust(ljust_width, '_') + get_format_info(info, 'mis_cord_measure'),
        # 'Traj distance:'.ljust(ljust_width, '_') + get_format_info(info, 'distance'),
        # 'Chosen effort:'.ljust(ljust_width, '_') + get_format_info(info, 'chosen_effort'),
        # 'Ahead measure 3:'.ljust(ljust_width, '_') + get_format_info(info, 'ahead_measure_3'),
        'Curve measure:'.ljust(ljust_width, '_') + get_format_info(info, 'mis_cord_measure'),
        'Prediction entropy'.ljust(ljust_width, '_') + get_entropy(predictions[trial_idx])
        ]
    info_str = '\n'.join(info_str)

    ax.text(1.6, 1., info_str, size=5.8, horizontalalignment='left', verticalalignment='top',
            transform=ax.transAxes, linespacing=1.3)
    # info_str = [ # todo reimplement when trial descriptor refactoring is finished
    #     'Classification:'.ljust(ljust_width - 8, '_') + get_format_info(info, 'trial_class', rjust_width=14 + 8),
    #     'Payoff/time (€/sec):'.ljust(ljust_width, '_') + get_format_info(info, 'duration', op=lambda x: 0.07/x),
    #     'Movement duration (sec):'.ljust(ljust_width, '_') + get_format_info(info, 'duration'),
    #     'Trajectory distance:'.ljust(ljust_width, '_') + get_format_info(info, 'distance'),
    #     'Speed (dist/sec):'.ljust(ljust_width, '_') + get_format_info(info, 'speed'),
    #     'Non-straight line dist:'.ljust(ljust_width, '_') + get_format_info(info, 'distance_ol'),
    #     'Prev to next t dist:'.ljust(ljust_width, '_') + get_format_info(info, 'chosen_effort'),
    #     'Agent place dist reduct:'.ljust(ljust_width, '_') + get_format_info(info, 'path_shortening'),
    #     '',
    #     'All distances are in percentage of',
    #     'field_width (=800px)',
    #     '',
    #     'Different aim:'.ljust(ljust_width, '_') + str(different_aim).rjust(14, '_'),
    #     # 'Traj correlation:'.ljust(ljust_width, '_') + get_format_info(info, 'correlation'),
    #     'Curve measure:'.ljust(ljust_width, '_') + get_format_info(info, 'mis_cord_measure'),
    #     'Traj distance:'.ljust(ljust_width, '_') + get_format_info(info, 'distance'),
    #     'Chosen effort:'.ljust(ljust_width, '_') + get_format_info(info, 'chosen_effort'),
    #     # 'P0 ahead percentage:'.ljust(ljust_width, '_') + get_format_info(info, 'p0_ahead_p'),
    #     # 'P1 ahead percentage:'.ljust(ljust_width, '_') + get_format_info(info, 'p1_ahead_p'),
    #     # 'No one ahead percentage:'.ljust(ljust_width, '_') + get_format_info(info, 'no_ahead_p'),
    #     # 'Ahead classification:'.ljust(ljust_width, '_') + get_format_info(info, 'new_ahead'),
    #     # 'True traj correlation:'.ljust(ljust_width, '_') + get_format_info(info, 'cross_corr_2'),
    #     'Ahead measure 3:'.ljust(ljust_width, '_') + get_format_info(info, 'ahead_measure_3'),
    #     'Prediction entropy'.ljust(ljust_width, '_') + get_entropy(predictions[trial_idx])
    #     ]
    # info_str = '\n'.join(info_str)
    #
    # ax.text(1.55, 1., info_str, size=4, horizontalalignment='left', verticalalignment='top',
    #         transform=ax.transAxes)

    cbar = fig.colorbar(line0, ax=ax)
    cbar.set_label('Elapsed time (seconds)', rotation=-90, size=9, labelpad=14)
    trial_idx_str = str(trial_idx).rjust(3, "0")
    file_format = 'pdf'
    plt.savefig(f'plots/class_trajectories/per_dyad/{pair_names[pair_idx]}/block{lap}/{trial_idx_str}.{file_format}', dpi=300)
    plt.savefig(f'plots/class_trajectories/per_class/{info["trial_class"]}/{pair_names[pair_idx]}_block{lap}_trial{trial_idx_str}.{file_format}', dpi=300)
    plt.close()


if __name__ == '__main__':   # idea: new class: different decisions: take normalized traj fit line, sort mse, see the closest target -> target -> targets differ -> its a different target trial!
    create_directories()
    plt.rcParams['font.family'] = 'monospace'

    # pair_idx = 10  # comp
    pair_idx = 36  # comp
    # pair_idx = 37  # 1/3
    # pair_idx = 12  # 1/3
    # pair_idx = 38  # coop
    # pair_idx = 9  # comp

    # pair_idx = 8
    # pair_idx = 10  # comp
    lap = 1
    rec = load_single(pair_idx, lap)
    td = load_trial_descriptions(pair_idx, lap)
    predictions = load_predictions(pair_idx, lap)
    comp_frac = get_comp_frac(td)

    # gen_class_traj(1)
    with concurrent.futures.ProcessPoolExecutor(max_workers=7) as ppe:
        # for trial_idx in list(range(50)) + list(range(200, 300)):
        for trial_idx in list(range(150)):
            ppe.submit(gen_class_traj, trial_idx)
