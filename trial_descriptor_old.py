import numpy as np
import pandas as pd

from load.data_loader import load_single, pair_idx_single_rec_path
from load.loading_helper import pairs, unique_pairs

from trial_predictor import get_dyad_weighting, effort_based_prediction
from _util import convert

tds = None

c_params = dict(ahead=2, correlation=.1, sits_on_target=.05,  # classification parameters
                nearby=.5, miscoordination=1.9)


def get_collection_keys(record):
    return [key for key in record.keys() if 'occupation' in key or 'collected' in key]


def get_collections(record):
    collected_targets = record[get_collection_keys(record)].to_numpy()
    collection_indices = np.where(np.sum(collected_targets, axis=1) == 1)[0]
    return collected_targets[collection_indices], collection_indices


def vec_to_label(coll_vec):
    coll_class_idx = np.where(coll_vec == 1)[0]
    if coll_class_idx == 0:
        return 'single_p0'
    elif coll_class_idx == 1:
        return 'single_p1'
    elif coll_class_idx == 2:
        return 'joint0'
    return 'joint1'


def get_collection_dict(rec):
    return {coll_idx: vec_to_label(hot_encoded_coll)
            for hot_encoded_coll, coll_idx in zip(*get_collections(rec))}


def get_dist_p_to_t(rec):
    p0 = rec[['p0_agent_x', 'p0_agent_y']].to_numpy()
    p1 = rec[['p1_agent_x', 'p1_agent_y']].to_numpy()
    comp = rec[['comp_x', 'comp_y']].to_numpy()
    coop0 = rec[['coop0_x', 'coop0_y']].to_numpy()
    coop1 = rec[['coop1_x', 'coop1_y']].to_numpy()
    return dict(
        p0_to_comp=np.linalg.norm(p0 - comp, axis=1) - 0.05,  # remove target radius
        p0_to_coop0=np.linalg.norm(p0 - coop0, axis=1) - 0.05,
        p0_to_coop1=np.linalg.norm(p0 - coop1, axis=1) - 0.05,
        p1_to_comp=np.linalg.norm(p1 - comp, axis=1) - 0.05,
        p1_to_coop0=np.linalg.norm(p1 - coop0, axis=1) - 0.05,
        p1_to_coop1=np.linalg.norm(p1 - coop1, axis=1) - 0.05,
    )


def sits_on_condition(p_idx, t_name, start_idx, p_to_t, sits_on_threshold):
    p_t_dist = p_to_t[f'p{p_idx}_to_{t_name}'][start_idx]
    if p_t_dist < sits_on_threshold:
        return True
    return False


def nearby_condition(p_idx, t_name, start_idx, p_to_t, nearby_threshold):
    p_t_dist = p_to_t[f'p{p_idx}_to_{t_name}'][start_idx]
    other_p_t_dist = p_to_t[f'p{(1 + p_idx) % 2}_to_{t_name}'][start_idx]
    return p_t_dist < other_p_t_dist * nearby_threshold


def inviting(coll_name, condition, *args):
    t_name = convert[coll_name]
    for coop_t_name in ['coop0', 'coop1']:
        for p_idx in [0, 1]:
            if condition(p_idx, coop_t_name, *args):
                if coop_t_name == t_name:
                    return f'accepted_{coop_t_name}_invite'
                elif 'single' in coll_name and f'p{p_idx}' not in coll_name:
                    return f'declined_{coop_t_name}_invite'
    return None


def get_comp_frac_from_rec(rec):
    comp_collections = rec[['comp_collected_by_p0', 'comp_collected_by_p1']].to_numpy().sum()
    coop_collections = rec[['coop0_collected', 'coop1_collected']].to_numpy().sum()
    return comp_collections / (comp_collections + coop_collections)


def get_t_desc_base(rec):
    coll_dict = get_collection_dict(rec)
    coll_indices = list(coll_dict.keys())
    trial_start_indices = [0] + coll_indices[:-1]
    trial_end_indices = np.array(coll_indices) - 120
    trial_descriptions = [{'start': start, 'end': end,
                           'duration': (end - start) / 120,
                           'coll_type': coll_dict[coll_i]}
                          for start, end, coll_i in zip(trial_start_indices, trial_end_indices, coll_indices)]
    return trial_descriptions


def get_efforts(dists_to_targets, t_desc):
    start = t_desc['start']
    return dict(
        comp=min(dists_to_targets['p0_to_comp'][start], dists_to_targets['p1_to_comp'][start]),
        coop0=max(dists_to_targets['p0_to_coop0'][start], dists_to_targets['p1_to_coop0'][start]),
        coop1=max(dists_to_targets['p0_to_coop1'][start], dists_to_targets['p1_to_coop1'][start]),
    )


def get_cross_corr(dists_to_targets, t_desc, eps=1e-6):
    collected_target_name = convert[t_desc["coll_type"]]
    if t_desc['start'] == t_desc['end']:
        return np.zeros(200)  # Todo this is a hotfix
    p0_trial = dists_to_targets[f'p0_to_{collected_target_name}'][t_desc['start']:t_desc['end']]
    p1_trial = dists_to_targets[f'p1_to_{collected_target_name}'][t_desc['start']:t_desc['end']]
    p0_trial = (p0_trial - p0_trial.mean()) / (p0_trial.std() + eps)
    p1_trial = (p1_trial - p1_trial.mean()) / (p1_trial.std() + eps)
    return np.correlate(p0_trial, p1_trial, 'full') / len(p0_trial)


def get_correlation(t_desc):
    return t_desc['cross_corr'][(len(t_desc['cross_corr']) - 1) // 2]


def get_path_shortening(dists_to_targets, prev_t_desc, t_desc):
    if prev_t_desc is not None:
        collecting_agent_idx = 0 if 'p0' in t_desc['coll_type'] else 1
        prev_collecting_agent_idx = 0 if 'p0' in prev_t_desc['coll_type'] else 1
        if 'single' in t_desc['coll_type'] and 'single' in prev_t_desc['coll_type'] and collecting_agent_idx != prev_collecting_agent_idx:
            collected_t_name = convert[t_desc['coll_type']]
            dist_non_collecting_agent = dists_to_targets[f'p{prev_collecting_agent_idx}_to_{collected_t_name}'][t_desc['start']]
            dist_collecting_agent = dists_to_targets[f'p{collecting_agent_idx}_to_{collected_t_name}'][t_desc['start']]
            if dist_collecting_agent < dist_non_collecting_agent:
                return dist_non_collecting_agent - dist_collecting_agent
    return 0.


def get_chosen_effort(dists_to_targets, t_desc):
    collected_target = convert[t_desc['coll_type']]
    efforts = get_efforts(dists_to_targets, t_desc)
    return efforts[collected_target]


def get_first_on_joint_target(dists_to_targets, t_desc):
    t_name = convert[t_desc['coll_type']]
    start = max(t_desc['start'] - 1, 0)  # avoid negative start idx
    end = t_desc['end'] + 119
    p0_to_t = dists_to_targets[f'p0_to_{t_name}'][end: start: -1]  # iterate distance function reversely
    p1_to_t = dists_to_targets[f'p1_to_{t_name}'][end: start: -1]
    p0_on_t = p0_to_t < .05
    p1_on_t = p1_to_t < .05
    for p0_on, p1_on in zip(p0_on_t, p1_on_t):
        if not p1_on and p0_on:
            return 'p0'
        elif not p0_on and p1_on:
            return 'p1'
        elif not p0_on and not p1_on:
            return 'both'
    print('Error in get_first_on_joint')
    return None


def get_trajectory_distance(rec, t_desc):
    trial_rec = rec[t_desc['start']:t_desc['end']]
    p0_positions = trial_rec[['p0_agent_x', 'p0_agent_y']].to_numpy()
    p1_positions = trial_rec[['p1_agent_x', 'p1_agent_y']].to_numpy()
    p0_dist = np.sum(np.linalg.norm(p0_positions[:-1] - p0_positions[1:], axis=1))
    p1_dist = np.sum(np.linalg.norm(p1_positions[:-1] - p1_positions[1:], axis=1))
    coll_type = t_desc['coll_type']
    if 'single_p0' == coll_type:
        return p0_dist
    elif 'single_p1' == coll_type:
        return p1_dist
    else:
        return max(p0_dist, p1_dist)


def get_ahead_measure(t_desc, width=30):
    cross_corr = t_desc['cross_corr']
    middle_idx = (len(cross_corr) - 1) // 2
    p0_score = cross_corr[middle_idx - width:middle_idx].sum()
    p1_score = cross_corr[middle_idx + 1:middle_idx + width + 1].sum()
    return p0_score - p1_score


def get_trial_class(dists_to_targets, t_desc):
    coll_name = t_desc['coll_type']
    start_idx = t_desc['start']
    sits_on_invite = inviting(coll_name, sits_on_condition, start_idx, dists_to_targets, c_params['sits_on_target'])
    if sits_on_invite is not None:
        return sits_on_invite
    nearby_invite = inviting(coll_name, nearby_condition, start_idx, dists_to_targets, c_params['nearby'])
    if nearby_invite is not None:
        return nearby_invite
    if t_desc['coll_type'] == 'single_p0' and t_desc['correlation'] < c_params['correlation']:
        return 'anti_corr'
    if t_desc['coll_type'] == 'single_p1' and t_desc['correlation'] < c_params['correlation']:
        return 'anti_corr'
    if t_desc['mis_cord_measure'] > c_params['miscoordination'] and t_desc['distance'] > .1:
        return 'miscoordination'
    if t_desc['ahead_measure'] > c_params['ahead']:
        return 'p0_ahead'
    if t_desc['ahead_measure'] < -1 * c_params['ahead']:
        return 'p1_ahead'
    return 'concurrent'


def get_min_effort(dists_to_targets, t_desc):
    return min(list(get_efforts(dists_to_targets, t_desc).values()))


def gen_trial_descriptions(pair_idx, lap):
    rec = load_single(pair_idx, lap, frames_per_sec=120)
    trial_descriptions = get_t_desc_base(rec)
    dists_to_targets = get_dist_p_to_t(rec)
    prev_trial_descriptions = [None] + trial_descriptions[:-1]
    dyad_weighting = get_dyad_weighting(get_comp_frac_from_rec(rec))

    for prev_t_desc, t_desc in zip(prev_trial_descriptions, trial_descriptions):
        t_desc['min_effort'] = get_min_effort(dists_to_targets, t_desc)
        t_desc['chosen_effort'] = get_chosen_effort(dists_to_targets, t_desc)
        t_desc['effort_ol'] = t_desc['chosen_effort'] - t_desc['min_effort']
        t_desc['distance'] = get_trajectory_distance(rec, t_desc)
        t_desc['speed'] = t_desc['distance'] / t_desc['duration']
        t_desc['distance_ol'] = t_desc['distance'] - t_desc['chosen_effort']
        t_desc['cross_corr'] = get_cross_corr(dists_to_targets, t_desc)
        t_desc['correlation'] = get_correlation(t_desc)
        t_desc['path_shortening'] = get_path_shortening(dists_to_targets, prev_t_desc, t_desc)
        t_desc['mis_cord_measure'] = t_desc['distance'] / t_desc['chosen_effort']
        t_desc['ahead_measure'] = get_ahead_measure(t_desc)
        t_desc['trial_class'] = get_trial_class(dists_to_targets, t_desc)
        efforts = get_efforts(dists_to_targets, t_desc)
        for i, weighting in enumerate([.001, 1., .5, dyad_weighting]):
            t_desc[f'prediction{i}'], t_desc[f'prediction{i}_prob'] = effort_based_prediction(efforts, weighting)

    dump_trail_descriptions(pair_idx, lap, trial_descriptions)


def gen_all_trial_descriptions():
    for pair_idx, pair in enumerate(unique_pairs):
        lap_amount = pairs.count(pair)
        for lap in range(lap_amount):
            gen_trial_descriptions(pair_idx, lap)


def dump_trail_descriptions(pair_idx, lap, trial_descriptions):
    path = pair_idx_single_rec_path(pair_idx, lap)
    trial_descriptions = pd.DataFrame(trial_descriptions)
    trial_descriptions.to_pickle(path + '/trial_descriptions.pkl')


def load_trial_descriptions(pair_idx, lap):
    path = pair_idx_single_rec_path(pair_idx, lap)
    return pd.read_pickle(path + '/trial_descriptions.pkl')


def get_comp_frac(td):
    val_counts = td['coll_type'].value_counts()
    for c_type in ['single_p0', 'single_p1']:
        if c_type not in val_counts.keys():
            val_counts[c_type] = 0
    return val_counts[['single_p0', 'single_p1']].sum() / val_counts.sum()


def unsorted_load_all_trial_descriptions():
    all_trial_descriptions = list()
    for pair_idx, pair in enumerate(unique_pairs):
        # lap_amount = pairs.count(pair)
        lap_amount = 2  # we take only the first 2 laps!
        t_descriptions = load_trial_descriptions(pair_idx, lap=0)
        for lap in range(1, lap_amount):
            t_descriptions = pd.concat([t_descriptions, load_trial_descriptions(pair_idx, lap)], axis=0)
        all_trial_descriptions.append(t_descriptions)
    return all_trial_descriptions


def print_unsorted_ids_and_comp_fractions():
    unsorted_tds = unsorted_load_all_trial_descriptions()
    comp_fracs = [get_comp_frac(td) for td in unsorted_tds]
    for cf, (i, id) in zip(comp_fracs, enumerate(unique_pairs)):
        print(i, list(id), cf)

def get_p_ids_sorted():
    tds = unsorted_load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    return np.array(unique_pairs)[comp_fractions.argsort()]


def load_all_trial_descriptions():
    global tds
    if tds is None:
        tds = unsorted_load_all_trial_descriptions()
        unsorted_comp_fractions = [get_comp_frac(td) for td in unsorted_load_all_trial_descriptions()]
        comp_frac_sorting = np.argsort(unsorted_comp_fractions)
        tds = [tds[i] for i in comp_frac_sorting]
        # tds.sort(key=get_comp_frac)
    return tds


if __name__ == '__main__':
    print_unsorted_ids_and_comp_fractions()
    # gen_all_trial_descriptions()
    tds = load_all_trial_descriptions()
    for i, p_ids in enumerate(get_p_ids_sorted()):
        print(i, p_ids)


