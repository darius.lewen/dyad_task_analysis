import numpy as np
import pandas as pd
import seaborn as sn
from matplotlib import pyplot as plt, ticker

from _util import save_fig
from glm.likelihood import entropy
from glm.plotting.entropy_corr import get_cut_tds, get_simple_entropy_ts
from per_class_entropy import reduced_classes, get_reduced_class_binary
from presentation.plotting import scatterplot
from trial_descriptor import unsorted_load_all_trial_descriptions, get_comp_frac, load_all_trial_descriptions

if __name__ == '__main__':
    tds = unsorted_load_all_trial_descriptions()
    unsorted_comp_fractions = [get_comp_frac(td) for td in tds]
    comp_frac_sorting = np.argsort(unsorted_comp_fractions)
    comp_fractions = [unsorted_comp_fractions[i] for i in comp_frac_sorting]

    tds = get_cut_tds()
    mis_cord_measure = [td['mis_cord_measure'].to_numpy() for td in tds]
    trial_classes = [td['trial_class'].to_numpy() for td in tds]
    entropies_per_dyad = get_simple_entropy_ts(filename='glm/unsorted_performance.pkl')
    entropies_per_dyad = [entropies_per_dyad[i] for i in comp_frac_sorting]
    entropies_per_dyad_exc_comp = entropies_per_dyad[:-15]

    def get_per_class_entropy_and_curvature(tds, entropy_ts, key=None):
        if key is None:
            key = 'mis_cord_measure'
        per_class_entropies = {c: list() for c in reduced_classes}
        per_class_curvature = {c: list() for c in reduced_classes}
        for i, (entropy, td) in enumerate(zip(entropy_ts, tds)):
            mis_cord_measure = td[key]
            for c in reduced_classes:
                reduced_class_binary = get_reduced_class_binary(td, c)
                per_class_entropies[c].extend(list(entropy[reduced_class_binary]))
                per_class_curvature[c].extend(list(mis_cord_measure[reduced_class_binary].to_numpy()))
        return per_class_entropies, per_class_curvature  # todo this needs refactoring!

    per_class_entropies, per_class_curvatures = get_per_class_entropy_and_curvature([tds[i] for i in comp_frac_sorting][:-15], entropies_per_dyad_exc_comp)
    new_per_class_entropies = dict(trial_class=list(), entropy=list(), curvature=list())

    invite_entropies = per_class_entropies['accepted_invite'] + per_class_entropies['declined_invite']
    invite_curvatures = per_class_curvatures['accepted_invite'] + per_class_curvatures['declined_invite']

    new_per_class_entropies['trial_class'].extend(['Invite' for _ in invite_entropies])
    new_per_class_entropies['entropy'].extend(list(invite_entropies))
    new_per_class_entropies['curvature'].extend(list(invite_curvatures))

    other_entropies = per_class_entropies['one_ahead'] + per_class_entropies['concurrent'] + per_class_entropies['miscoordination'] + per_class_entropies['anti_corr']
    other_curvatures = per_class_curvatures['one_ahead'] + per_class_curvatures['concurrent'] + per_class_curvatures['miscoordination'] + per_class_curvatures['anti_corr']

    new_per_class_entropies['trial_class'].extend(['Other' for _ in other_entropies])
    new_per_class_entropies['entropy'].extend(list(other_entropies))
    new_per_class_entropies['curvature'].extend(list(other_curvatures))

    new_per_class_entropies = pd.DataFrame(new_per_class_entropies)

    fig, axes = plt.subplots(1, 2)
    axes = axes.flatten()
    axes[1].set_ylim(0, .6)
    sn.boxplot(new_per_class_entropies, x='trial_class', y='entropy', ax=axes[0])
    sn.boxplot(new_per_class_entropies, x='trial_class', y='curvature', ax=axes[1])

