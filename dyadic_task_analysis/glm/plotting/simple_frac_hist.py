import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sn
from matplotlib import ticker

from matplotlib.colors import ListedColormap

from _util import save_fig
from glm.plotting.class_comparison import fontdict
from glm.plotting.type_hist import get_hist_plot_frame
from inefficiencies_full import comp_frac_x_format
from trial_descriptor import get_comp_frac, load_all_trial_descriptions

if __name__ == '__main__':
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.style.use('default')

    # to_plot = get_hist_plot_frame()
    comp_fractions = [get_comp_frac(td) for td in load_all_trial_descriptions()]
    fig, axes = plt.subplots(2, 2, figsize=(5, 5))
    axes = axes.flatten()
    for ax in axes[[1, 2]]:
        ax.axis('off')
    plt.subplots_adjust(left=.15, bottom=.15, right=.9, top=.9, wspace=.6, hspace=.7)
    for ax in axes:
        ax.set_box_aspect(1)
    axes[0].text(-0.06, 1.04, 'C', transform=axes[0].transAxes, size=10)
    axes[3].text(-0.06, 1.04, 'D', transform=axes[3].transAxes, size=10)
    axes[0].set_xlim(0, 1)
    axes[0].set_xlabel('Fraction of\nsingle targets collected $\\Phi$', fontdict=fontdict)
    axes[0].set_ylabel('Number of dyads', fontdict=fontdict)
    comp_frac_x_format(axes[0])
    sn.histplot(x=comp_fractions, bins=10, ax=axes[0], legend=False)
    save_fig()
