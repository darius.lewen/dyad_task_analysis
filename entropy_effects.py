import numpy as np
import pandas as pd
import seaborn as sn
from matplotlib import pyplot as plt, ticker

from _util import save_fig
from glm.likelihood import entropy
from glm.plotting.entropy_corr import get_cut_tds, get_simple_entropy_ts
from per_class_entropy import reduced_classes, get_reduced_class_binary
from presentation.plotting import scatterplot
from trial_descriptor import unsorted_load_all_trial_descriptions, get_comp_frac, load_all_trial_descriptions

if __name__ == '__main__':
    fig = plt.figure(figsize=(3, 3.3))
    ax = plt.gca()
    ax.set_box_aspect(1)
    ax.set_ylabel('Probability of\ntrial class')
    ax.set_xlabel('Uncertainty of\ntarget choice (bits)')
    tds = unsorted_load_all_trial_descriptions()
    unsorted_comp_fractions = [get_comp_frac(td) for td in tds]
    comp_frac_sorting = np.argsort(unsorted_comp_fractions)
    comp_fractions = [unsorted_comp_fractions[i] for i in comp_frac_sorting]

    tds = get_cut_tds()
    mis_cord_measure = [td['mis_cord_measure'].to_numpy() for td in tds]
    trial_classes = [td['trial_class'].to_numpy() for td in tds]
    entropies_per_dyad = get_simple_entropy_ts(filename='glm/unsorted_performance.pkl')
    entropies_per_dyad = [entropies_per_dyad[i] for i in comp_frac_sorting]
    entropies_per_dyad_exc_comp = entropies_per_dyad[:-15]

    def get_per_class_entropies(tds, entropy_ts):
        per_class_entropies = {c: list() for c in reduced_classes}
        for i, (entropy, td) in enumerate(zip(entropy_ts, tds)):
            for c in reduced_classes:
                reduced_class_binary = get_reduced_class_binary(td, c)
                per_class_entropies[c].append(entropy[reduced_class_binary])
        return per_class_entropies

    per_class_entropies = get_per_class_entropies([tds[i] for i in comp_frac_sorting][:-15], entropies_per_dyad_exc_comp)
    new_per_class_entropies = dict()
    # new_per_class_entropies['invite'] = np.concatenate(
    #     [np.concatenate(per_class_entropies['declined_invite']),
    #      np.concatenate(per_class_entropies['accepted_invite'])]
    # )
    new_per_class_entropies['Failed Invitation'] = np.concatenate(per_class_entropies['declined_invite'])
    new_per_class_entropies['Invitation'] = np.concatenate(per_class_entropies['accepted_invite'])
    # new_per_class_entropies['Straight movement to same target'] = np.concatenate(per_class_entropies['straight'])
    new_per_class_entropies['Concurrent to same target'] = np.concatenate(per_class_entropies['concurrent'])
    new_per_class_entropies['One ahead to same target'] = np.concatenate(per_class_entropies['one_ahead'])
    new_per_class_entropies['Strongly curved'] = np.concatenate(per_class_entropies['miscoordination'])
    new_per_class_entropies['Towards different targets'] = np.concatenate(per_class_entropies['anti_corr'])
    to_plot = list()
    for key, val in new_per_class_entropies.items():
        for e in val:
            to_plot.append((key, e))
    to_plot = pd.DataFrame(to_plot)
    to_plot.rename(columns={0: 'trial_class', 1: 'entropy'}, inplace=True)
    plt.subplots_adjust(left=.2, right=.98, bottom=.54, top=.9, wspace=1, hspace=1)

    # colors = ['#164a64', '#fb4f2f', '#e4ae38', '#e277c1', '#9367bc', '#d62628', ]
    # colors = list(reversed(['#164a64', '#e4ae38', '#e277c1', '#9367bc', '#fb4f2f', '#d62628', ]))
    # colors = list(reversed(['#164a64', '#d62628', '#fb4f2f', '#e4ae38', '#e277c1', '#9367bc', ]))
    colors = ['#9367bc', '#e277c1', '#e4ae38', '#fb4f2f', '#d62628', '#164a64']
    sn.kdeplot(new_per_class_entropies, multiple='fill', ax=ax, palette=colors, alpha=1.)
    # sn.kdeplot(new_per_class_entropies, multiple='stack', ax=ax, palette=colors, alpha=.5)
    # sn.kdeplot(new_per_class_entropies, ax=ax, palette=colors, alpha=.5)
    # sn.histplot(new_per_class_entropies, multiple='fill', ax=ax, bins=8, palette=colors, alpha=1.)
    ax.set_xlim(0, np.log2(3))
    sn.move_legend(ax, loc='upper left', bbox_to_anchor=(-1, -.5), frameon=False)
    ax.xaxis.set_major_locator(ticker.FixedLocator([0.000001, .5, 1, 1.5]))
    save_fig()

