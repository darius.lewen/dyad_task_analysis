import matplotlib.pyplot as plt

from _util import save_fig
from class_trajectories import plot_trial_sheet
from load.data_loader import load_single
from trial_descriptor_2 import load_trial_descriptions

if __name__ == '__main__':
    fig, axes = plt.subplots(2, 2)
    axes = axes.flatten()

    pair_idx = 37
    lap = 1
    trial_idx = 26  # 2 could be one option, 5 also, 34

    rec = load_single(pair_idx, lap, frames_per_sec=120)
    td = load_trial_descriptions(pair_idx, lap)

    # for ax_idx, ax in enumerate(axes):
    #     plot_trial_sheet(rec, td, trial_idx + ax_idx, ax)
    plot_trial_sheet(rec, td, trial_idx, axes[0], collection_preiod=True)
    plot_trial_sheet(rec, td, trial_idx, axes[1])
    plot_trial_sheet(rec, td, trial_idx + 1, axes[2], collection_preiod=True)
    plot_trial_sheet(rec, td, trial_idx + 1, axes[3])

    for i, ax in enumerate(axes):
        # ax.set_title(title, fontsize=fontdict['size'], pad=3.6)
        ax.set_box_aspect(1)
        ax.set_xlim([-.05, 1.05])
        ax.set_ylim([-.05, 1.05])
        ax.tick_params(left=False, right=False, labelleft=False,
                       labelbottom=False, bottom=False)

    save_fig(sub_folder='presentation/', format='svg')

