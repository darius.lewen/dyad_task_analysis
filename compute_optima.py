import time

import numpy as np

from simulations import multiprocess_sim

if __name__ == '__main__':
    t0 = time.time()
    weigthings = np.array([.5])
    process_amount = 2
    iterations = 10 ** 4 // process_amount
    non_opti_frac = 0.
    ps = True
    result = multiprocess_sim(simulation_args=(weigthings, iterations, non_opti_frac, ps),
                              process_amount=process_amount)
    print(time.time() - t0)

    # without ps:
    # n=10**6 16sec / 2 fst:0.333945
    # n=10**7 38sec / 2fst:0.3333265
    # n=10**8 ...

    # with ps:
    # n=10**3 21sec / 2 fst:0.406
    # n=10**4 93sec / 2 fst:0.3956
    # n=10**4 100sec / 2 fst:0.4183 - another run
    # n=10**4 100sec / 2 fst:0.4034 - another run
