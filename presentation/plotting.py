import numpy as np
import scipy
import seaborn as sn
import cmasher
from matplotlib import pyplot as plt, rcParams
from scipy.stats import pearsonr, linregress

from cmap_generator import get_cmap
from pred_performance import get_comp_frac_colors
from trial_descriptor import load_all_trial_descriptions, get_comp_frac

# cmap = get_cmap()
cmap = cmasher.gem

# rcParams['axes.linewidth'] = 1.2
# fontdict = dict(size=12)


def get_comp_frac_color(comp_frac):
    return cmap.colors[int(comp_frac * 255)]


def reg_plot(x, y, ax, bootstrap_size=1000, xspace_res=50):
    res = linregress(x, y)
    lin_func = lambda x: res.slope * x + res.intercept
    add_x_space = (max(x) - min(x)) * .2
    xspace = np.linspace(min(x) - add_x_space, max(x) + add_x_space, xspace_res)
    ax.plot(xspace, lin_func(xspace), color='k', linewidth=1, alpha=.9)
    bootstrapped_y_values = np.zeros((bootstrap_size, xspace_res))
    value_pairs = np.stack([x, y])
    for i in range(bootstrap_size):
        sample_indices = np.random.randint(len(x), size=len(x))
        x_sample, y_sample = value_pairs[:, sample_indices]
        sample_res = linregress(x_sample, y_sample)
        sample_lin_func = lambda x: sample_res.slope * x + sample_res.intercept
        bootstrapped_y_values[i] = sample_lin_func(xspace)
    ax.fill_between(x=xspace, alpha=.4, linewidth=0, zorder=0,
                    y1=np.quantile(bootstrapped_y_values, 0.025, axis=0),
                    y2=np.quantile(bootstrapped_y_values, 0.975, axis=0), color='gray')


def diagonal_r_and_p_text(x, y, ax, slope=1, text_pos=(.985, .015), ha='right', va='bottom', include_p=True):
    # sst = (len(x)-1) * np.var(y)  # total sum of squares
    sst = sum((y - np.mean(y)) ** 2)
    print(sst)
    diag_pred = slope * x + 0  # diagonal linear line
    sse = sum((y - diag_pred) ** 2)  # sum of squared errors
    print(sse)
    r_squared = 1 - sse / sst
    r = r_squared ** .5
    print(r)
    test_statistic = r * (len(x) - 2) ** .5 / (1 - r_squared) ** .5
    p_value = 2 * scipy.stats.t.sf(abs(test_statistic), df=len(x) - 2)  # sf = suvival function = cummulative density function (cdf)
    r_str = r'r$\approx$'
    print(f'r={r}, p={p_value}')
    r_str = r_str + '%.2f' % round(r, 2)
    p_str = get_p_value_str(p_value)
    if include_p:
        ax.text(*text_pos, f'{r_str}\n{p_str}', ha=ha, va=va, transform=ax.transAxes)
    else:
        ax.text(*text_pos, f'{r_str}', ha=ha, va=va, transform=ax.transAxes)



def r_and_p_text(x, y, ax, text_pos=(.985, .015), ha='right', va='bottom'):
    res = linregress(x, y)
    r_str = r'r$\approx$'
    print(f'r={res.rvalue}, p={res.pvalue}')
    r_str = r_str + '%.2f' % round(res.rvalue, 2)
    p_str = get_p_value_str(res.pvalue)
    ax.text(*text_pos, f'{r_str}\n{p_str}', ha=ha, va=va, transform=ax.transAxes)


def get_p_value_str(p_value):
    if p_value < 10 ** -6:
        return 'p<$10^{-6}$'
    elif p_value < 10 ** -5:
        return 'p<$10^{-5}$'
    elif p_value < 10 ** -4:
        return 'p<$10^{-4}$'
    elif p_value < 10 ** -3:
        return 'p<$10^{-3}$'
    elif p_value < 10 ** -2:
        return 'p<$10^{-2}$'
    elif p_value < .05:
        return 'p$\\approx$' + '%.2f' % p_value
    return 'ns'


def scatterplot(data, x=None, y=None, hue=None, ax=None, size=None, markers=None, legend=False, zorder=None,
                dyad_plot=True):
    # if size is None:  todo?
    #     size = 4.5
    if markers is None:
        # old python version idx: 2, 4, 7, 53
        # new python version idx: 1, 5, 6, 56
        markers = {0: 'o', 1: 'd', 5: 'd', 6: 'd', 56: 'P'}
        # markers = {0: 'o', 2: 'd', 4: 'd', 7: 'd', 53: 'P'}
    # markers = {0: 'o', 2: 'd', 4: 'd', 7: 'd', 53: 'P', 3: '.', 33: '.', 43: '.', } todo this is for identifying the examples
    if not dyad_plot:
        markers = {0: 's'}
    if zorder is None:
        zorder = 1  # todo?
    if hue is None:
        hue = 'comp_fractions'
    to_plot = data.assign(style=[i if i in markers.keys() else 0 for i in range(len(data))])
    # if 'style' in data.keys():
    #     style = 'style'
    to_plot.sort_values(by=['style'], inplace=True)
    sn.scatterplot(to_plot, x=x, y=y, hue=hue, style='style', #style_order=style_order,
                   ax=ax, palette=cmap, legend=legend, size=size,
                   markers=markers, vmax=1, zorder=zorder, alpha=.8)
    # sn.scatterplot(data, x=x, y=y, hue=hue, style=style, #style_order=style_order,
    #                ax=ax, palette=cmap, legend=legend, size=size,
    #                markers=markers, vmax=1, zorder=zorder, alpha=.8)


def violin_swarm(data, ax, x=None, y=None, show_mean=False, marker_size=None):
    if y is None:
        y = 'comp_fractions'
    if marker_size is None:
        marker_size = 3.5
    swarm_data = data.copy()
    swarm_data['index'] = swarm_data.index
    swarm_data = swarm_data.melt(id_vars='index')
    comp_frac_colors = get_comp_frac_colors()
    marker_encoding_colors = np.array([.5 * np.ones(3), .8 * np.ones(3)])
    marker_path_objects = get_marker_path_objects(['d', 'P'])
    # markers = {0: 'o', 2: 'd', 4: 'd', 7: 'd', 53: 'P'}
    default_marker = get_marker_path_objects(['o'])[0]
    comp_frac_colors[1] = marker_encoding_colors[0]
    comp_frac_colors[5] = marker_encoding_colors[0]
    comp_frac_colors[6] = marker_encoding_colors[0]
    comp_frac_colors[56] = marker_encoding_colors[1]
    # old python version idx: 2, 4, 7, 53
    # new python version idx: 1, 5, 6, 56
    # non-todo this is for identifying the examples
    # comp_frac_colors[3] = marker_encoding_colors[1]
    # comp_frac_colors[33] = marker_encoding_colors[1]
    # comp_frac_colors[43] = marker_encoding_colors[1]
    ax = sn.swarmplot(data=swarm_data, x='variable', y='value', ax=ax, hue='index', legend=False,
                      palette=comp_frac_colors, size=marker_size, alpha=.8)
    collections = ax.collections
    for collection in collections:
        paths = []
        for current_color in collection.get_facecolors():
            current_color = current_color[:-1]
            found_non_default_maker = False
            for m_enc_c, corresponding_marker in zip(marker_encoding_colors, marker_path_objects):
                if np.array_equal(current_color, m_enc_c):
                    paths.append(corresponding_marker)
                    found_non_default_maker = True
                    break
            if not found_non_default_maker:
                current_c_in_colorset = any((current_color == comp_frac_colors).astype(int).sum(axis=1) == 3)
                if current_c_in_colorset:
                    paths.append(default_marker)
        collection.set_paths(paths)
        facecolors = collection.get_facecolors()
        orig_colors = np.array(get_comp_frac_colors())[len(data) - len(facecolors):]  # restore original colors, we assume only low comp_frac cut-off
        # if len(orig_colors) > len(data):
        #     orig_colors = orig_colors[: len(data)]
        facecolors[:, :-1] = orig_colors
        collection.set_facecolor(facecolors)
    sn.violinplot(data=data, bw=.2, cut=0, ax=ax, inner=None, scale='area', color='w')
    plt.setp(ax.collections, edgecolor='k')
    if show_mean:
        sn.barplot(data=data, ax=ax, color='gray', alpha=.3, errorbar=None)


def get_marker_path_objects(markers):
    ax = plt.gca()
    # import matplotlib.pyplot as plt
    # fig, ax = plt.subplots(1, 1)
    path_objects = list()
    for m in markers:
        collection = ax.scatter([0, 0], [1, 1], marker=m)
        p_obj, = collection.get_paths()
        path_objects.append(p_obj)
        collection.remove()
    # plt.close()
    # plt.close()
    return path_objects
