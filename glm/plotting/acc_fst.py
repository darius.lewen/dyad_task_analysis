import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn

from glm.plotting.performance_plot import get_best_model_for_dyad
from trial_descriptor import get_comp_frac, load_all_trial_descriptions


def get_best_model_acc(dyad_amount=58, model_sel='aic', performance_measure='acc', filename='performance.pkl',
                       model_selection=None):
    all_model_vals = pd.read_pickle(filename)
    best_acc = list()
    for dyad_idx in range(dyad_amount):
        best_model = get_best_model_for_dyad(dyad_idx, key=model_sel, filename=filename,
                                             model_selection=model_selection)
        best_acc.append(float(all_model_vals[best_model][performance_measure][dyad_idx]))
    return np.array(best_acc)


if __name__ == '__main__':
    best_model_acc = get_best_model_acc()
    best_model_aic = get_best_model_acc(performance_measure='aic')
    comp_fractions = [get_comp_frac(td) for td in load_all_trial_descriptions()]
    dyad_amount = 58
    best_model = [get_best_model_for_dyad(i) for i in range(dyad_amount)]
    fig, axes = plt.subplots(1, 2)
    sn.scatterplot(x=comp_fractions, y=best_model_acc, hue=best_model, palette='colorblind', ax=axes[0], legend=False)
    sn.scatterplot(x=comp_fractions, y=best_model_aic, hue=best_model, palette='colorblind', ax=axes[1], legend=False)


