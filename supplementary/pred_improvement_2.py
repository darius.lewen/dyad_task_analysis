import itertools

import matplotlib
import numpy as np
import pandas as pd
import seaborn as sn
from scipy.interpolate import splrep, splev

# old scipy version 1.9.3  --- also updated matplotlib

import glm.linearities_sim as lin
from scipy.stats import bootstrap, mannwhitneyu, false_discovery_control
from matplotlib import pyplot as plt, ticker

from _util import save_fig
from glm.likelihood import gen_glm
from glm.likelihood_2 import load_per_dyad_stats, get_per_dyad_stats
from pred_performance import generate_fst_bar
from presentation.fig22_predicting_dyads import get_sorted_effort_glm_stats, get_true_fst_binary, get_pred_fst_binary, \
    get_closest_pred_acc
from presentation.plotting import violin_swarm, scatterplot, get_comp_frac_color
from reward_decomposition import get_mean_in_cm
from trial_descriptor_2 import load_all_trial_descriptions, get_comp_frac, unsorted_load_all_trial_descriptions



group_names = ['Coop', 'Inter', 'Comp']


# def get_sorted_effort_1hist_glm_stats(invites=False):
#     comp_frac_sorting = np.argsort([get_comp_frac(td) for td in unsorted_load_all_trial_descriptions()])
#     if invites:
#         glm = gen_glm([lin.effort_linearity, lin.prev_t_inv_linearity])
#     else:
#         glm = gen_glm([lin.effort_linearity, lin.prev_t_linearity])
#     per_dyad_stats_effort_glm = get_per_dyad_stats(glm, save=False)
#     return [per_dyad_stats_effort_glm[dyad_idx] for dyad_idx in comp_frac_sorting]


def get_aic_glm_stats(linearities):
    unsorted_comp_fractions = [get_comp_frac(td) for td in unsorted_load_all_trial_descriptions(cut_initial=True)]
    comp_frac_sorting = np.argsort(unsorted_comp_fractions)
    per_dyad_stats = get_per_dyad_stats(gen_glm(linearities), save=False)
    return [per_dyad_stats[i] for i in comp_frac_sorting]


def gen_aic_values():
    # sorted_effort_glm_stats = get_sorted_effort_glm_stats()
    # dist_1hist_glm_stats = get_sorted_effort_1hist_glm_stats()
    # dist_inv_glm_stats = get_sorted_effort_1hist_glm_stats()
    # dist_1hist_inv_glm_stats = get_sorted_effort_1hist_glm_stats(invites=True)
    # full_glm_stats = load_per_dyad_stats()
    # comp_frac_sorting = np.argsort([get_comp_frac(td) for td in unsorted_load_all_trial_descriptions()])
    # sorted_full_glm_stats = [full_glm_stats[i] for i in comp_frac_sorting]
    # dist_glm_aic = np.array([summary['aic'] for summary in sorted_effort_glm_stats])
    # dist_1hist_glm_aic = np.array([summary['aic'] for summary in dist_1hist_glm_stats])
    # dist_1hist_inv_glm_aic = np.array([summary['aic'] for summary in dist_1hist_inv_glm_stats])
    # full_glm_aic = np.array([summary['aic'] for summary in sorted_full_glm_stats])
    dist_glm = get_aic_glm_stats([lin.effort_linearity])
    # dist_inv_glm = get_sorted_aic_glm_stats([lin.effort_linearity, lin.invite_linearity])
    dist_1hist_glm = get_aic_glm_stats([lin.effort_linearity, lin.prev_t_linearity])
    dist_1hist_inv_glm = get_aic_glm_stats([lin.effort_linearity, lin.prev_t_inv_linearity])
    full_glm = get_aic_glm_stats([lin.effort_linearity, lin.prev_t_inv_linearity, lin.prev_prev_t_linearity])
    np.save(f'supplementary/aic_values/dist_glm.npy', [summary['aic'] for summary in dist_glm])
    np.save(f'supplementary/aic_values/dist_1hist_glm.npy', [summary['aic'] for summary in dist_1hist_glm])
    np.save(f'supplementary/aic_values/dist_1hist_inv_glm.npy', [summary['aic'] for summary in dist_1hist_inv_glm])
    np.save(f'supplementary/aic_values/full_glm.npy', [summary['aic'] for summary in full_glm])


def load_aic_values():
    dist_glm_aic = np.load(f'supplementary/aic_values/dist_glm.npy')
    dist_1hist_glm_aic = np.load(f'supplementary/aic_values/dist_1hist_glm.npy')
    dist_1hist_inv_glm_aic = np.load(f'supplementary/aic_values/dist_1hist_inv_glm.npy')
    full_glm_aic = np.load(f'supplementary/aic_values/full_glm.npy')
    return dist_glm_aic, dist_1hist_glm_aic, dist_1hist_inv_glm_aic, full_glm_aic


def get_grouped_df(comp_fractions, aic_improvements):
    groups = list()
    for comp_frac in comp_fractions:
        if comp_frac < .1:
            groups.append(group_names[0])
        elif .05 <= comp_frac <= .9:
            groups.append(group_names[1])
        else:
            groups.append(group_names[2])
    return pd.DataFrame(dict(group=groups, improvement=aic_improvements))


def get_benjamini_hochberg_p_values(improvement):
    combinations = list(itertools.combinations(group_names, 2))
    p_values = list()
    index_pairs = list()
    for group0, group1 in combinations:
        res = mannwhitneyu(
            improvement[improvement['group'] == group0]['improvement'],
            improvement[improvement['group'] == group1]['improvement'],
        )
        print(group0, group1, res[0], f'non-adjusted-p-value: {res[1]}')
        p_values.append(res[1])
        index_pairs.append((group_names.index(group0), group_names.index(group1)))
    adjusted_p_values = false_discovery_control(p_values)
    return dict(zip(index_pairs, adjusted_p_values))


def get_significance_identifier(p_value):
    if p_value > .05:
        return None
    elif p_value > .01:
        return '*'
    elif p_value > .001:
        return '**'
    return '***'


def get_upper_ci_s(improvement):
    upper_ci_s = list()
    for group in group_names:
        boot = bootstrap((improvement[improvement['group'] == group]['improvement'],), statistic=estimator,
                         method='percentile')
        upper_ci_s.append(boot.confidence_interval.high)
    return upper_ci_s


def custom_bar_plot(improvement, estimator, ax):
    # sn.violinplot(data=data, bw=.2, cut=0, ax=ax, inner=None, scale='area', color='w')
    # sn.violinplot(improvement, x='group', y='improvement', ax=ax, bw=.4, cut=0,
    #               palette=dict(coop=get_comp_frac_color(.05), inter=get_comp_frac_color(.5), comp=get_comp_frac_color(.95)))
    sn.boxplot(improvement, x='group', y='improvement', ax=ax,# flierprops={"facecolor": "x"},
               palette=dict(zip(group_names, [get_comp_frac_color(comp_frac) for comp_frac in [.05, .5, .95]])))
    # sn.barplot(improvement, x='group', y='improvement', ax=ax, estimator=estimator, errorbar=('ci', 95), alpha=.8,
    #            palette=dict(coop=get_comp_frac_color(.05), inter=get_comp_frac_color(.5), comp=get_comp_frac_color(.95)))
    p_values = get_benjamini_hochberg_p_values(improvement)
    print(p_values)
    upper_ci_s = get_upper_ci_s(improvement)
    ax_height = ax.get_ylim()
    ax_height = ax_height[1] - ax_height[0]
    rel_offset = .15
    offset = 0
    for key, value in p_values.items():
        significance_identifier = get_significance_identifier(value)
        if significance_identifier is not None:
            offset += ax_height * rel_offset
            y = max([upper_ci_s[key[0]], upper_ci_s[key[1]]]) + offset
            little = ax_height * rel_offset / 2
            ax.plot([key[0], key[1]], [y, y], color='k')
            # ax.plot([key[0], key[0], key[1], key[1]], [y - little, y, y, y - little], color='k')
            mid = sum(key) / 2
            ax.text(mid, y - little, significance_identifier, ha='center', va='bottom', weight='bold')


if __name__ == '__main__':
    matplotlib.rcParams.update({'axes.titlesize': matplotlib.rcParams['axes.labelsize']})
    size_scaler = 4
    fig, axes = plt.subplots(2, 3, figsize=(2 * size_scaler, 2 * size_scaler))
    plt.subplots_adjust(left=.16, top=.9, bottom=.25, right=.98, wspace=1, hspace=1)
    axes = axes.flatten()

    for ax_label, ax in zip('   be ', axes):
        ax.set_box_aspect(1)
        ax.set_xlabel('Group')
        # ax.xaxis.set_major_locator(comp_frac_locator)
        # ax.xaxis.set_major_formatter(comp_frac_formatter)
        # ax.set_xlim(0, 1)
        ax.text(-0.26, 1.04, ax_label, transform=ax.transAxes, weight='bold')

    axes[3].set_ylabel('Model\nimprovement (AIC)')
    axes[4].set_ylabel('Mean distance\nto target (cm)')
    # axes[3].set_ylabel('Model improvement (AIC)\ndue to secondary predictors')
    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])

    # outliers to sort out: 438_439 after comp_frac sorting this is dyad 44

    # gen_aic_values()
    aic_values = list(load_aic_values())
    aic_improvements = [b - a for a, b in zip(aic_values[1:], aic_values[:-1])]

    estimator = np.median
    # estimator = np.mean

    # sorting = np.delete(np.arange(len(tds)), [44])

    # sorting = np.delete(np.arange(len(tds)), [2, 4, 7, 44])
    # sorting = np.delete(np.arange(len(tds)), [1, 5, 6])

    # potential_suppl = True
    potential_suppl = False
    if potential_suppl:
        sorting = np.delete(np.arange(len(tds)), [1, 5, 6])
        hist_improvement = get_grouped_df(comp_fractions[sorting], aic_improvements[0][sorting])
        inv_improvement = get_grouped_df(comp_fractions[sorting], aic_improvements[1][sorting])
        full_improvement = get_grouped_df(comp_fractions[sorting], aic_improvements[2][sorting])
        to_plot = pd.DataFrame(dict(comp_fractions=comp_fractions[sorting], aic_improvements=aic_improvements[0][sorting]))
        scatterplot(to_plot, x='comp_fractions', y='aic_improvements', ax=axes[3])
        to_plot = pd.DataFrame(dict(comp_fractions=comp_fractions[sorting], aic_improvements=aic_improvements[1][sorting]))
        scatterplot(to_plot, x='comp_fractions', y='aic_improvements', ax=axes[4])
        to_plot = pd.DataFrame(dict(comp_fractions=comp_fractions[sorting], aic_improvements=aic_improvements[2][sorting]))
        scatterplot(to_plot, x='comp_fractions', y='aic_improvements', ax=axes[5])
        custom_bar_plot(hist_improvement, estimator, axes[0])
        custom_bar_plot(inv_improvement, estimator, axes[1])
        custom_bar_plot(full_improvement, estimator, axes[2])
    else:
        sorting = np.arange(len(tds))
        sec_improvement = get_grouped_df(comp_fractions[sorting], sum(aic_improvements)[sorting])
        custom_bar_plot(sec_improvement, estimator, axes[3])
        tds = load_all_trial_descriptions()
        dist_prev_to_next = get_mean_in_cm('limiting_agent_next_target_displacement', tds) + get_mean_in_cm('reduction_due_to_adv_placement', tds)
        dist_reduction_adv_placement = get_mean_in_cm('reduction_due_to_adv_placement', tds)
        distance = dist_prev_to_next - dist_reduction_adv_placement
        distance = get_grouped_df(comp_fractions[sorting], distance[sorting])
        custom_bar_plot(distance, None, axes[4])
        axes[4].set_ylim(14.251807142428937, 25.67291744950062)

    save_fig(sub_folder='supplementary/', format='svg')

    # sorting = np.delete(np.arange(len(tds)), [2, 4, 7, 44])
    # inv_improvement = get_grouped_df(comp_fractions[sorting], aic_improvements[0][sorting])
    # hist_improvement = get_grouped_df(comp_fractions[sorting], aic_improvements[1][sorting])
    # full_improvement = get_grouped_df(comp_fractions[sorting], aic_improvements[2][sorting])
    # sn.barplot(inv_improvement, x='group', y='improvement', ax=axes[3], estimator=np.median, errorbar=('ci', 95))
    # sn.barplot(hist_improvement, x='group', y='improvement', ax=axes[4], estimator=np.median, errorbar=('ci', 95))
    # sn.barplot(full_improvement, x='group', y='improvement', ax=axes[5], estimator=np.median, errorbar=('ci', 95))



    # for combi in combinations:
    #     res = mannwhitneyu(values[bin_binaries[combi[0]]], values[bin_binaries[combi[1]]])
    #     p_values.append(res[1])
    # print(p_values)
    # adj_p_values = false_discovery_control(p_values)
    # print(adj_p_values)

#     # comp_fractions = np.array([get_comp_frac(td) for td in tds])
#     # aic_improvement_1hist = dist_glm_aic - dist_1hist_glm_aic
#     # aic_improvement_inv = dist_1hist_glm_aic - dist_1hist_inv_glm_aic
#     # aic_improvement_2hist = dist_1hist_inv_glm_aic - full_glm_aic
#
#     size_scaler = 2.1 * 1.65
#     # size_scaler = 2.1 * 3 / 2
#     fig, axes = plt.subplots(2, 3, figsize=(2 * size_scaler, 2 * size_scaler))
#     plt.subplots_adjust(left=.16, top=.9, bottom=.25, right=.98, wspace=1, hspace=1)
#     axes = axes.flatten()
#
#     comp_frac_locator = ticker.FixedLocator([0, 1 / 3, 2 / 3, 1])
#     comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1'])
#
#     for ax_label, ax in zip('   cde', axes):
#         ax.set_box_aspect(1)
#         ax.set_xlabel('Stable FST')
#         ax.xaxis.set_major_locator(comp_frac_locator)
#         ax.xaxis.set_major_formatter(comp_frac_formatter)
#         ax.set_xlim(0, 1)
#         ax.text(-0.26, 1.04, ax_label, transform=ax.transAxes, weight='bold')
#
#     # axes[3].set_ylim(-30, 470)
#     # axes[4].set_ylim(-30, 470)
#     # axes[5].set_ylim(-5, 23)
#
#
#     # axes[0].set_ylabel('Increase in accuracy\ndue to adding 1hist')
#     # axes[1].set_ylabel('Increase in accuracy\ndue to adding 2hist')
#     # axes[0].set_ylabel('Increase in accuracy\ndue to adding 1hist')
#     # axes[1].set_ylabel('Increase in accuracy\ndue to adding 2hist')
#
#     axes[0].set_ylabel('Model improvement\n(AIC) when including...')
#     axes[3].set_ylabel('Model improvement\n(AIC) when including...')
#     # axes[4].set_ylabel('')
#     # axes[5].set_ylabel('')
#
#     # axes[3].set_title('...secondary placements (mean)')
#     axes[0].set_title('...secondary predictors')
#     axes[3].set_title('...invite placements')
#     axes[4].set_title('...target history i-1')
#     axes[5].set_title('...target history i-2')
#     # axes[5].set_title('...target history i-2')
#
#     sorting = np.delete(np.arange(len(tds)), [44])  # todo overall plot with median
#
#     sorting = np.delete(np.arange(len(tds)), [2, 4, 7, 44])
#
#     comp_fractions = comp_fractions[sorting]
#     aic_improvement_1hist = aic_improvement_1hist[sorting]
#     aic_improvement_inv = aic_improvement_inv[sorting]
#     aic_improvement_2hist = aic_improvement_2hist[sorting]
#
#     bin_edges = np.array([0, .1, .9, 1.001])
#
#     bin_binaries = list()
#     bin_indices = list()
#     for i, (prev, post) in enumerate(zip(bin_edges[:-1], bin_edges[1:])):
#         above = (prev <= comp_fractions).astype(int)
#         below = (comp_fractions < post).astype(int)
#         bin_binaries.append((above + below) == 2)
#         for val in bin_binaries[-1]:
#             if val:
#                 bin_indices.append(i)  # exploit that everything is comp_frac sorted
#
#
#
#
#     def custom_hist(bin_edges, bin_binaries, values, ax, estimator=np.mean):
#         means = [estimator(values[bin_binary]) for bin_binary in bin_binaries]
#         # error_bars = [np.std(values[bin_binary]) for bin_binary in bin_binaries]
#         for bin_binary in bin_binaries:
#             print(len(values[bin_binary]))
#         bootstrap_res = list()
#         for bin_binary in bin_binaries:
#             bootstrap_res.append(bootstrap((values[bin_binary], ), estimator))
#         middle_points = [(prev_edge + post_edge) / 2 for prev_edge, post_edge in zip(bin_edges[:-1], bin_edges[1:])]
#         significance_line_height = 400
#         k = len(list(itertools.combinations(range(len(bin_binaries)), 2)))
#         print(k)
#         p_values = list()
#         combinations = list(itertools.combinations(range(len(bin_binaries)), 2))
#         for combi in combinations:
#             res = mannwhitneyu(values[bin_binaries[combi[0]]], values[bin_binaries[combi[1]]])
#             p_values.append(res[1])
#         print(p_values)
#         adj_p_values = false_discovery_control(p_values)
#         print(adj_p_values)
#         print(combinations)
#         for adj_p_val, combi in zip(adj_p_values, combinations):
#             print(f'mannwhitneyu for {combi}: p={adj_p_val} U={res[0]}')
#             significance_identifier = get_significance_identifier(p_value=adj_p_val)
#             print(significance_identifier)
#             if significance_identifier is not None:
#                 significance_line_height += 40
#                 ax.plot([middle_points[combi[0]], middle_points[combi[1]]], [significance_line_height, significance_line_height])
#
#             # mannwhitneyu_res.append(mannwhitneyu_res)
#         # bootstrap_res = [bootstrap((values[bin_binary], ), np.mean) for bin_binary in bin_binaries]
#         # u1, p = mannwhitneyu(invite_trials_entropies, non_invite_trials_entropies)
#         for mean, boot_res, prev_edge, post_edge in zip(means, bootstrap_res, bin_edges[:-1], bin_edges[1:]):
#             mid = (prev_edge + post_edge) / 2
#             ax.fill_between([prev_edge, post_edge], [mean, mean], color=get_comp_frac_color(mid), alpha=.8)
#             ax.plot([mid, mid], list(boot_res.confidence_interval), color='#070707', linewidth=2.5, alpha=.4)
#
#
#
#     # imp_1hist_means = [np.mean(aic_improvement_1hist[bin_binary]) for bin_binary in bin_binaries]
#     # imp_1hist_stds = [np.std(aic_improvement_1hist[bin_binary]) for bin_binary in bin_binaries]
#     # custom_hist(bin_edges, values=imp_1hist_means, error_bars=imp_1hist_stds, ax=axes[5])
#     # imp_inv_means = [np.mean(aic_improvement_inv[bin_binary]) for bin_binary in bin_binaries]
#     # imp_inv_stds = [np.std(aic_improvement_inv[bin_binary]) for bin_binary in bin_binaries]
#     # custom_hist(bin_edges, values=imp_inv_means, error_bars=imp_inv_stds, ax=axes[5])
#     custom_hist(bin_edges, bin_binaries, aic_improvement_1hist + aic_improvement_inv + aic_improvement_2hist, ax=axes[0])
#     # custom_hist(bin_edges, bin_binaries, aic_improvement_1hist + aic_improvement_inv, ax=axes[3])
#     custom_hist(bin_edges, bin_binaries, aic_improvement_inv, ax=axes[0])
#     custom_hist(bin_edges, bin_binaries, aic_improvement_1hist, ax=axes[1])
#     custom_hist(bin_edges, bin_binaries, aic_improvement_2hist, ax=axes[2])
#     # custom_hist(bin_edges, bin_binaries, aic_improvement_1hist + aic_improvement_inv + aic_improvement_2hist, ax=axes[4], estimator=np.median)
#     # custom_hist(bin_edges, bin_binaries, aic_improvement_1hist, ax=axes[3])
#     # custom_hist(bin_edges, bin_binaries, aic_improvement_inv, ax=axes[4])
#     # custom_hist(bin_edges, bin_binaries, aic_improvement_2hist, ax=axes[5])
#
#     # axes[0].scatter(comp_fractions, aic_improvement_1hist)
#     # axes[1].scatter(comp_fractions, aic_improvement_inv)
#     # axes[2].scatter(comp_fractions, aic_improvement_2hist)
#
#     # bin_indices = np.array(bin_indices)
#     # to_plot = dict(bin_indice=bin_indices[sorting], aic_improvement_1hist=aic_improvement_1hist[sorting], comp_fractions=comp_fractions[sorting])
#     # sn.barplot(pd.DataFrame(to_plot), x='bin_indice', y='aic_improvement_1hist', ax=axes[3], ci=66)
#     # scatterplot(data=pd.DataFrame(to_plot), x='comp_fractions', y='aic_improvement_1hist', ax=axes[0])
#     # to_plot = dict(bin_indice=bin_indices[sorting], aic_improvement_inv=aic_improvement_inv[sorting], comp_fractions=comp_fractions[sorting])
#     # sn.barplot(pd.DataFrame(to_plot), x='bin_indice', y='aic_improvement_inv', ax=axes[4], ci=66)
#     # scatterplot(data=pd.DataFrame(to_plot), x='comp_fractions', y='aic_improvement_inv', ax=axes[1])
#     # to_plot = dict(bin_indice=bin_indices[sorting], aic_improvement_2hist=aic_improvement_2hist[sorting], comp_fractions=comp_fractions[sorting])
#
#
#     # sn.barplot(pd.DataFrame(to_plot), x='bin_indice', y='aic_improvement_2hist', ax=axes[5], ci=66)
#     # scatterplot(data=pd.DataFrame(to_plot), x='comp_fractions', y='aic_improvement_2hist', ax=axes[2])
#
#     # to_plot = {
#     #     'comp_fractions': comp_fractions,
#     #     'aic_incorporating_1hist': aic_improvement_1hist,
#     #     'aic_incorporating_inv': aic_improvement_inv,
#     #     'aic_incorporating_2hist': aic_improvement_2hist,
#     # }
#     # scatterplot(data=pd.DataFrame(to_plot), x='comp_fractions', y='aic_incorporating_1hist', ax=axes[0])
#     # scatterplot(data=pd.DataFrame(to_plot), x='comp_fractions', y='aic_incorporating_inv', ax=axes[1])
#     # scatterplot(data=pd.DataFrame(to_plot), x='comp_fractions', y='aic_incorporating_2hist', ax=axes[2])
#
#     # sn.barplot(pd.DataFrame(to_plot), x='bin_indice', y='aic_improvement_2hist', ax=axes[3], ci=66)# errorbar=('ci', .66))
#     # sorting1 = np.delete(aic_improvement_2hist.argsort(), [55, 56, 57])
#     # aic_improvement_2hist = aic_improvement_2hist[]
#     # aic_improvement_2hist = aic_improvement_2hist[np.delete(aic_improvement_2hist.argsort(), [55, 56, 57])]
#     # to_plot = dict(bin_indice=bin_indices, aic_improvement_1hist=aic_improvement_1hist, aic_improvement_2hist=aic_improvement_2hist)
#     #
#     # to_plot = dict(
#     #     cooperative=[aic_improvement_2hist[i] if bin == 0 else np.nan for i, bin in enumerate(bin_indices)],
#     #     intermediate=[aic_improvement_2hist[i] if bin == 1 else np.nan for i, bin in enumerate(bin_indices)],
#     #     competitive=[aic_improvement_2hist[i] if bin == 2 else np.nan for i, bin in enumerate(bin_indices)],
#     # )
#     # violin_swarm(pd.DataFrame(to_plot), ax=axes[1])
#
#
#     # bin_indices = np.delete(bin_indices, [2, 4, 7])
#
#     # def outlier_robust_mean(x):
#     #     return np.mean(list(sorted(x))[:-2])
#
#     # sn.barplot(pd.DataFrame(to_plot), x='bin_indice', y='aic_improvement_1hist', ax=axes[2], estimator=outlier_robust_mean, ci=66)# errorbar=('ci', .66))
#     # sn.barplot(pd.DataFrame(to_plot), x='bin_indice', y='aic_improvement_2hist', ax=axes[3], estimator=outlier_robust_mean, ci=66)# errorbar=('ci', .66))
#     # to_plot = dict(bin_indice=bin_indices, aic_improvement_1hist=aic_improvement_1hist, aic_improvement_2hist=aic_improvement_2hist)
#
# # error bars std / sqrt(n)
#
#     # binned_means = [np.median(aic_improvement_1hist[bin_binary]) for bin_binary in bin_binaries]
#     # binned_means_2hist = [np.median(aic_improvement_2hist[bin_binary]) for bin_binary in bin_binaries]
#     # axes[2].bar(np.arange(len(binned_means)), binned_means)
#     # axes[3].bar(np.arange(len(binned_means_2hist)), binned_means_2hist)
#
#     # scatterplot(data=pd.DataFrame(to_plot), x='comp_fractions', y='aic_incorporating_1hist', ax=axes[3])
#     # sn.histplot(data=pd.DataFrame(to_plot), x='comp_fractions', y='aic_incorporating_2hist', ax=axes[3])
#
#     save_fig(sub_folder='supplementary/', format='svg')
#
#
