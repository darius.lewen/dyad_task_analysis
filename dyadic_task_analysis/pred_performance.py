import cmasher
import matplotlib
import numpy as np
import pandas as pd
import seaborn as sn
from matplotlib import pyplot as plt, ticker

from _util import save_fig, convert, fontdict
from trial_descriptor import load_all_trial_descriptions, get_comp_frac


def get_true_predicted_bins(prediction_idx=0):
    tds = load_all_trial_descriptions()
    true_predicted_binaries = list()
    for td in tds:
        c_type, prediction = td.replace(convert)[['coll_type', f'prediction{prediction_idx}']].to_numpy().T
        true_predicted_binaries.append((c_type == prediction).astype(int))
    return true_predicted_binaries


def get_acc_for_prediction(prediction_idx):
    true_predicted_binaries = get_true_predicted_bins(prediction_idx)
    return np.array([true_predicted_bin.mean() for true_predicted_bin in true_predicted_binaries])


def get_nll_for_prediction(prediction_idx):
    likelihoods = list()
    t_name_to_idx = {
        'comp': 0,
        'coop0': 1,
        'coop1': 2,
    }
    for td in load_all_trial_descriptions():
        likelihood = 0
        coll_types, prob_vecs = td[['coll_type', f'prediction{prediction_idx}_prob']].to_numpy().T
        for coll_type, prob_vec in zip(coll_types, prob_vecs):
            choosen_t_idx = t_name_to_idx[convert[coll_type]]
            likelihood += prob_vec[choosen_t_idx]  # todo here is a mistake right? I should take the true not the pred_t_name
        likelihoods.append(likelihood)  # todo normalize trial amounts
    return -1 * np.log(np.array(likelihoods))


def get_aic_for_prediction(prediction_idx):
    p_amount = 0 if not prediction_idx == 3 else 1
    return 2 * p_amount + 2 * get_nll_for_prediction(prediction_idx)   # todo add param amount


def get_hypo_performance(performance_func):
    # model_names = ['$E_{min} | w=0.0$ ',
    #                '$E_{min} | w=1.0$ ',
    #                '$E_{min} | w=0.5$ ',
    #                '$E_{min} | \\overline{w}_{dyad}$']
    model_names = ['$w=0.0$ ',
                   '$w=1.0$ ',
                   '$w=0.5$ ',
                   '$w=w_{dyad}$']
                   # '$w=\\omega(\\Phi)$']
    m_performances = pd.DataFrame({name: performance_func(i) for i, name in enumerate(model_names)})
    return m_performances


def gen_n_save_performance():
    acc_for_dyads = get_hypo_performance(performance_func=get_acc_for_prediction)
    nll_for_dyads = get_hypo_performance(performance_func=get_nll_for_prediction)
    aic_for_dyads = get_hypo_performance(performance_func=get_aic_for_prediction)
    acc_for_dyads.to_pickle('data/acc_for_dyads.pkl')
    nll_for_dyads.to_pickle('data/nll_for_dyads.pkl')
    aic_for_dyads.to_pickle('data/aic_for_dyads.pkl')


def load_model_performances():
    dyad_acc = pd.read_pickle('data/acc_for_dyads.pkl')
    dyad_nll = pd.read_pickle('data/nll_for_dyads.pkl')
    dyad_aic = pd.read_pickle('data/aic_for_dyads.pkl')
    return dyad_acc, dyad_nll, dyad_aic


def get_comp_frac_colors():
    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    palette = list()
    for comp_frac in comp_fractions:
        palette.append(cmasher.gem.colors[int(comp_frac / max(comp_fractions) * 255)])
    return palette


def swarm_plot(data, ax):
    data = data.copy()
    data['index'] = data.index
    data = data.melt(id_vars='index')
    sn.swarmplot(x='variable', y='value', ax=ax, hue='index', data=data, legend=False,
                 palette=get_comp_frac_colors(), alpha=.6, size=6)


def violin_swarm_mean_plot(data, ax):
    sn.violinplot(data=data, bw=.3, cut=0, ax=ax, color='w', inner=None, scale='area')#, cut=0.)
    plt.setp(ax.collections, edgecolor="k")
    sn.pointplot(data=data, ax=ax, estimator=np.mean, errorbar=None, join=False, color='k')
    swarm_plot(data, ax)


def generate_fst_bar(ax):
    dummy = np.linspace(0, 1, 100).reshape((10, 10))
    comp_frac_locator = ticker.FixedLocator([0, 1/3, 2/3, 1])
    comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1'])
    cbar_kws = dict(label='Fraction of\nsingle targets collected $\\Phi$',
                    fraction=.3, ticks=comp_frac_locator, format=comp_frac_formatter)
    sn.heatmap(dummy, ax=ax, cmap=cmasher.gem, alpha=.75, cbar_kws=cbar_kws)
    # ax.images[-1].colorbar.set_label('$\\mathbb{E}[E_{\\mathrm{min}}| \\vec x, \\vec y, \\vec j^A, \\vec j^B, w]$ (cm)',
    #                rotation=-90, size=10, labelpad=20)

if __name__ == '__main__':
    matplotlib.rcParams['font.size'] = fontdict['size']
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['text.latex.preamble'] = r'\usepackage{amsfonts}'

    gen_n_save_performance()
    performance_measures = load_model_performances()
    matplotlib.use('TkAgg')
    fig, axes = plt.subplots(2, 1, figsize=(5.6, 5))
    axes = axes.flatten()

    violin_swarm_mean_plot(data=performance_measures[0], ax=axes[0])
    axes[0].set_xlabel('')
    axes[0].xaxis.set_ticks_position('none')
    axes[0].xaxis.set_tick_params(labelsize=fontdict['size'])
    axes[0].yaxis.set_tick_params(labelsize=fontdict['size'])
    axes[0].set_xticklabels(axes[0].get_xticklabels(), fontdict=fontdict)
    axes[0].set_ylabel('Accuracy', fontdict=fontdict)

    generate_fst_bar(axes[1])



    # ax_width, ratio = 7, 2
    # ax_width *= .7
    # rows, cols = 3, 1
    # fig, axes = plt.subplots(rows, cols, figsize=(ax_width * cols, ax_width * rows / ratio))
    # axes = axes.flatten()
    # # fig = plt.figure(figsize=(ax_width, ax_width / ratio))
    # # ax = plt.gca()
    #
    # for ax, measure in zip(axes, performance_measures):
    #     violin_swarm_mean_plot(data=measure, ax=ax)
    #     ax.set_xlabel('')
    #     ax.xaxis.set_ticks_position('none')
    #     ax.xaxis.set_tick_params(labelsize=10)
    #     ax.yaxis.set_tick_params(labelsize=10)
    #     # ax.set_xticklabels(ax.get_xticklabels(), rotation=-30, horizontalalignment='left', fontdict=fontdict)
    #     ax.set_xticklabels(ax.get_xticklabels(), fontdict=fontdict)
    #     ax.set_ylabel('Accuracy', fontdict=fontdict)

    fig.tight_layout()
    save_fig()

