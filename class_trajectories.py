import matplotlib
from euclid import Line2, Point2

from markov_traj import simplify_trial_class
from trial_descriptor import get_lin_traj

matplotlib.use('Agg')

import matplotlib.colors
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib import image

from _util import save_fig, convert_trial_classes_to_colors
from load.data_loader import load_single
from trial_descriptor_2 import load_trial_descriptions

img_comp = image.imread('generated_game_objects/comp.png')
img_coop0 = image.imread('generated_game_objects/coop0.png')
img_coop1 = image.imread('generated_game_objects/coop1.png')
img_p0_agent = image.imread('generated_game_objects/p0_agent.png')
img_p1_agent = image.imread('generated_game_objects/p1_agent.png')


last_line = None
normalize = matplotlib.colors.Normalize(vmin=0, vmax=.8)
fontdict = dict(size=8)


def fit_linear(traj, ax):
    # coefficients, residuals, _, _, _ = np.polyfit(*traj.T, 1, full=True)
    # fit_line = np.poly1d(coefficients)
    # x_space = np.linspace(0, 1, 2)
    lin_traj = get_lin_traj(traj)
    if lin_traj is not None:
        ax.plot(*lin_traj)


def plot_traj(traj, timestamps, ax, cmap=None):
    if cmap is None:
        cmap = 'viridis'
    # fit_linear(traj, ax)

    points = traj.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    global normalize
    if __name__ != '__main__':
        normalize = None
    lc = LineCollection(segments, cmap=cmap, norm=normalize)
    lc.set_array(timestamps)
    lc.set_linewidth(1.3)
    global last_line
    last_line = ax.add_collection(lc)
    return last_line  # for class_trajectories_all.py


def get_extent(pos, target=True):
    radius = .05 if target else .02
    left = pos[0] - radius
    right = pos[0] + radius
    bot = pos[1] - radius
    top = pos[1] + radius
    return left, right, bot, top


def get_ith_class_occurrence_idx(td, trial_class, i):
    count = 0
    for k, row in enumerate(td.iloc):
        if row['trial_class'] == trial_class:
            if count == i:
                return k
            count += 1
    print('ith_class_occurrence not found')


def get_prev_t(td, rec, trial_idx):
    prev_coll_type = td['coll_type'].iloc[trial_idx-1]
    prev_frame = td['start'].iloc[trial_idx-1]
    if 'single' in prev_coll_type:
        prev_t_pos = rec[['comp_x', 'comp_y']].iloc[prev_frame].to_numpy()
        prev_t_img = img_comp.copy()
    elif '0' in prev_coll_type:
        prev_t_pos = rec[['coop0_x', 'coop0_y']].iloc[prev_frame].to_numpy()
        prev_t_img = img_coop0.copy()
    else:
        prev_t_pos = rec[['coop1_x', 'coop1_y']].iloc[prev_frame].to_numpy()
        prev_t_img = img_coop1.copy()
    prev_t_img[:, :, 3] *= .4  # alpha
    return prev_t_pos, prev_t_img


def plot_ith_class_occurrence(rec, td, trial_class, i, ax, prev_traj=False):
    trial_idx = get_ith_class_occurrence_idx(td, trial_class, i)
    ax.set_title(trial_class, fontsize=fontdict['size'])
    plot_trial_sheet(rec, td, trial_idx, ax, prev_traj=prev_traj)
    for spine in ax.spines.values():
        spine.set_edgecolor(convert_trial_classes_to_colors[simplify_trial_class(trial_class)])



def plot_trial_sheet(rec, td, trial_idx, ax, prev_traj=False, collection_preiod=False):
    prev_t_pos, prev_t_img = get_prev_t(td, rec, trial_idx)
    start, end = td[['start', 'end']].iloc[trial_idx]
    if collection_preiod:
        start = td['start'].iloc[trial_idx]
        start, end = start - 120, start
    trial_rec = rec.iloc[start: end]

    if prev_traj:
        plot_prior_traj(rec, td, trial_idx, ax)

    p0_traj = trial_rec[['p0_agent_x', 'p0_agent_y']].to_numpy()
    p1_traj = trial_rec[['p1_agent_x', 'p1_agent_y']].to_numpy()
    timestamps = trial_rec['timestamp'].to_numpy() - trial_rec['timestamp'].iloc[0]
    comp = trial_rec[['comp_x', 'comp_y']].iloc[0].to_numpy()
    coop0 = trial_rec[['coop0_x', 'coop0_y']].iloc[0].to_numpy()
    coop1 = trial_rec[['coop1_x', 'coop1_y']].iloc[0].to_numpy()

    ax.imshow(img_comp, aspect='auto', extent=get_extent(comp))
    ax.imshow(img_coop0, aspect='auto', extent=get_extent(coop0))
    ax.imshow(img_coop1, aspect='auto', extent=get_extent(coop1))
    ax.imshow(prev_t_img, aspect='auto', extent=get_extent(prev_t_pos))
    ax.imshow(img_p0_agent, aspect='auto', extent=get_extent(p0_traj[-1], target=False), zorder=10)
    ax.imshow(img_p1_agent, aspect='auto', extent=get_extent(p1_traj[-1], target=False), zorder=10)

    line0 = plot_traj(p0_traj, timestamps, ax)
    line1 = plot_traj(p1_traj, timestamps, ax)

    return line0, line1  # for class_trajectories_all.py


def plot_prior_traj(rec, td, trial_idx, ax):
    prev_start = int(td[['start']].iloc[trial_idx-1])
    start = int(td[['start']].iloc[trial_idx])
    prior_trial_rec = rec.iloc[prev_start: start]
    p0_traj = prior_trial_rec[['p0_agent_x', 'p0_agent_y']].to_numpy()
    p1_traj = prior_trial_rec[['p1_agent_x', 'p1_agent_y']].to_numpy()
    timestamps = prior_trial_rec['timestamp'].to_numpy() - prior_trial_rec['timestamp'].iloc[0]
    _ = plot_traj(p0_traj, timestamps, ax, cmap='Greys')
    _ = plot_traj(p1_traj, timestamps, ax, cmap='Greys')


def plot_ith_example(pair_idx, lap, i):
    plot_multiple_ith_occurrences(pair_idx, lap, [i, i, i, i, i+1, i+1, i+1, i+1])


def plot_multiple_ith_occurrences(pair_idx, lap, i_s, safe_plot=True, dec_inv_pair_idx=None, dec_inv_lap=None):
    size_scalar = 1.3
    fig, axes = plt.subplots(2, 4, figsize=(6.2 * size_scalar, 2.2 * size_scalar))
    plt.subplots_adjust(bottom=.05, top=.9, wspace=.2, hspace=.2)
    axes = axes.flatten()

    rec = load_single(pair_idx, lap, frames_per_sec=120)
    td = load_trial_descriptions(pair_idx, lap)

    dec_inv_rec = rec
    dec_inv_td = td

    if dec_inv_pair_idx is not None:
        dec_inv_rec = load_single(dec_inv_pair_idx, dec_inv_lap, frames_per_sec=120)
        dec_inv_td = load_trial_descriptions(dec_inv_pair_idx, dec_inv_lap)

    plot_ith_class_occurrence(rec, td, trial_class='concurrent', i=i_s[0], ax=axes[0])
    plot_ith_class_occurrence(rec, td, trial_class='p1_ahead', i=i_s[1], ax=axes[1])
    plot_ith_class_occurrence(rec, td, trial_class='accepted_coop0_invite', i=i_s[2], ax=axes[2])
    plot_ith_class_occurrence(rec, td, trial_class='miscoordination', i=i_s[3], ax=axes[3])

    plot_ith_class_occurrence(rec, td, trial_class='concurrent', i=i_s[4]+1, ax=axes[4])
    plot_ith_class_occurrence(rec, td, trial_class='p0_ahead', i=i_s[5], ax=axes[5])
    plot_ith_class_occurrence(dec_inv_rec, dec_inv_td, trial_class='declined_coop0_invite', i=i_s[6], ax=axes[6])
    plot_ith_class_occurrence(rec, td, trial_class='anti_corr', i=i_s[7], ax=axes[7])

    cbar_stuff = True
    if cbar_stuff:
        dummy_img = axes[-1].imshow(np.array([[0, 1]]), cmap='Greys')
        # dummy_img.set_visible(False)

        # plt.colorbar(orientation="vertical")
        dummy_cbar = fig.colorbar(dummy_img, ax=axes[-1])
        # dummy_cbar.set_ticklabels([])
        dummy_cbar.set_ticks([0, 1])
        dummy_cbar.ax.tick_params(labelsize=fontdict['size'])
        dummy_cbar.set_label('Transition\nfrequency', rotation=90, fontdict=fontdict)
        # cbar = fig.colorbar(last_line, ax=axes.ravel().tolist())
        cbar = fig.colorbar(last_line, ax=axes[3], extend='max')
        cbar.set_label('Elapsed time\n(seconds)', rotation=-90, labelpad=19, fontdict=fontdict)
        cbar.ax.tick_params(labelsize=fontdict['size'])
        cbar.set_ticks([0, .6])

    titles = [
        'Concurrent to the\nsame target',
        'One ahead to the\nsame target',
        'Invitation',
        'Strongly curved',
        'Concurrent to the\nsame target',
        'One ahead to the\nsame target',
        'Failed invitation',
        'Different targets'
    ]

    for i, (title, ax) in enumerate(zip(titles, axes)):
        ax.set_title(title, fontsize=fontdict['size'], pad=3.6)
        ax.set_box_aspect(1)
        ax.set_xlim([-.05, 1.05])
        ax.set_ylim([-.05, 1.05])
        ax.tick_params(left=False, right=False, labelleft=False,
                       labelbottom=False, bottom=False)
    if safe_plot:
        plt.savefig(f'plots/class_trajectories/{i}.svg', dpi=300)


if __name__ == '__main__':   # idea: new class: different decisions: take normalized traj fit line, sort mse, see the closest target -> target -> targets differ -> its a different target trial!
    pair_idx = 37
    lap = 1
    i_s = [11,  # 4  # todo continue with new classes!?
           5,
           28,  # 1 so lala: 3 sieht sehr cool aus: 9 könnte gut sein: 20 auch ganz gut: 27 mehhh... 28, 32
           # 32,  # 1 so lala: 3 sieht sehr cool aus: 9 könnte gut sein: 20 auch ganz gut: 27 mehhh... 28, 32
           1,  #
           0,  # 8,
           4,  # 1, 4
           5,  # 1 for coop0: 0, 4, 5, pair 12 coop0: 0 coop1: 0, 2 (very big quite good), coop1 pair 12 lap0: 1
           # 2,  # 1 for coop0: 0, 4, 5, pair 12 coop0: 0 coop1: 0, 2 (very big quite good), coop1 pair 12 lap0: 1
           1]  #
    plot_multiple_ith_occurrences(pair_idx, lap, i_s, safe_plot=False, dec_inv_pair_idx=37, dec_inv_lap=1)
    save_fig(format='svg')

