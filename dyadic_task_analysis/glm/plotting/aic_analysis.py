import numpy as np
import pandas as pd
import seaborn as sb
from matplotlib import pyplot as plt

from glm.likelihood import read_weights_nml
from pred_performance import get_comp_frac_colors
from trial_descriptor import load_all_trial_descriptions, get_comp_frac
from _util import save_fig

if __name__ == '__main__':
    aic = pd.read_pickle('performance/AIC.pkl')
    # sb.scatterplot(data=aic, x='effort_glm', y='markov_glm')
    aic_measure_0 = aic['effort_glm'].to_numpy() - aic['markov_glm'].to_numpy()
    aic_measure_1 = aic['effort_glm'].to_numpy() - aic['effort_markov_glm'].to_numpy()
    aic_measure_2 = aic['effort_markov_glm'].to_numpy() - aic['invite_effort_markov_glm'].to_numpy()
    aic_measures = [aic_measure_0, aic_measure_1, aic_measure_2]
    comp_fractions = [get_comp_frac(td) for td in load_all_trial_descriptions()]
    fig, axes = plt.subplots(1, 3)
    for ax, aic_measure in zip(axes, aic_measures):
        sb.scatterplot(x=comp_fractions, y=aic_measure, hue=np.arange(len(comp_fractions)), ax=ax,
                       palette=get_comp_frac_colors(), alpha=.6, legend=False)
    save_fig()

    # sb.scatterplot(x=comp_fractions, y=aic_measure_0, ax=axes1)
    # sb.scatterplot(x=comp_fractions, y=aic_measure_1)
    # effort_weights, markov_weights = weights[:2], weights[2:]

