import os
import sys

from matplotlib import pyplot as plt

game_width_in_cm = 800 * 139.7 / (1920 ** 2 + 1080 ** 2) ** .5
fontdict = dict(size=14)

convert = dict(
    single_p0='comp',
    single_p1='comp',
    joint0='coop0',
    joint1='coop1',
)

inv_convert = dict(
    comp='single',
    coop0='joint0',
    coop1='joint1',
)

convert_trial_classes = dict(
    accepted_coop0_invite='Invitation',
    accepted_coop1_invite='Invitation',
    declined_coop0_invite='Failed invitation',
    declined_coop1_invite='Failed invitation',
    miscoordination='Strongly curved',
    anti_corr='Different targets',
    p1_ahead='One ahead to the\nsame target',
    p0_ahead='One ahead to the\nsame target',
    concurrent='Concurrent to the\nsame target',
)

convert_trial_classes_to_colors = dict(  # do not change order
    declined_invite='#9367bc',
    accepted_invite='#e277c1',
    concurrent='#e4ae38',
    ahead='#fb4f2f',
    miscoordination='#d62628',
    anti_corr='#164a64',
)


def save_fig(sub_folder=None, format=None, name=None, dpi=300):
    if sub_folder is None:
        sub_folder = ''
    if format is None:
        format = 'png'
    if name is None:
        script_name = os.path.basename(sys.argv[0])
        name = script_name[:-3]
    path = f'{sub_folder}plots/{name}.{format}'
    plt.savefig(path, dpi=dpi)
    print(path)

