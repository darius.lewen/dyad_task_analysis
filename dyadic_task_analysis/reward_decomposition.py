import cmasher
import matplotlib
import numpy as np
import scipy.optimize
import seaborn as sn
from matplotlib import pyplot as plt, ticker

from inefficiencies_full import fontdict
from _util import game_width_in_cm, save_fig
from simulations import load_results
from trial_descriptor import load_all_trial_descriptions, get_comp_frac

reward_per_target = 0.07
block_time = 20 * 60


def y_ax_scaling(y_values, ax, margin_percentage=.2):
    y_min, y_max = min(y_values), max(y_values)
    margin = (y_max - y_min) * margin_percentage
    ax.set_ylim(y_min - margin, y_max + margin)


def scatter(x, y, ax):
    sn.scatterplot(x=x, y=y, hue=list(range(len(x))), ax=ax, palette=cmasher.gem, alpha=.6, legend=False)


def vertical_lines(x, y_min, y_max, ax):
    for _x, _y_min, _y_max in zip(x, y_min, y_max):
        sn.lineplot(x=[_x, _x], y=[_y_min, _y_max], ax=ax, estimator=None, alpha=.2, color='k')


def get_payoffs(td, block_amount=2):
    coll_count = td[['coll_type']].apply(lambda x: x.value_counts()).transpose()

    def get(df, key):
        return 0 if key not in df.keys() else df[key][0]
    p0_payoff = get(coll_count, 'single_p0') * .07 + get(coll_count, 'joint0') * .05 + get(coll_count, 'joint1') * .02
    p1_payoff = get(coll_count, 'single_p1') * .07 + get(coll_count, 'joint1') * .05 + get(coll_count, 'joint0') * .02
    p0_payoff /= block_amount
    p1_payoff /= block_amount
    return p0_payoff, p1_payoff


def get_payoff_diff(td):
    p0_payoff, p1_payoff = get_payoffs(td)
    return abs(p0_payoff - p1_payoff)


def get_modified_comp_fractions(tds):
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    full_comp_amount = sum(1 if val > .95 else 0 for val in comp_fractions)
    comp_fractions[-full_comp_amount:] += np.arange(full_comp_amount) * .05
    return comp_fractions


def get_agent_rewards(tds):
    agent_mean_reward = np.array([np.mean(get_payoffs(td)) for td in tds])
    reward_diff = np.array([get_payoff_diff(td) for td in tds])
    low_rewards = agent_mean_reward - reward_diff / 2
    high_rewards = agent_mean_reward + reward_diff / 2
    return agent_mean_reward, low_rewards, high_rewards


def plot_trial_durations(ax):
    trial_durations = np.array([(td['end'].to_numpy() - td['start'].to_numpy()).mean() for td in tds])
    trial_durations = trial_durations / 120
    ax.scatter(comp_fractions, trial_durations)


def plot_strategies(ax):
    sim_results = load_results()
    for key in sim_results.keys():
        sim_results[key][1] *= game_width_in_cm

    strategy_colors = ['gray', 'gray', 'gray', 'k']
    strategy_alphas = [2 / 3, 1 / 3, 1, 1]

    for (name, result), color, alpha in zip(sim_results.items(), strategy_colors, strategy_alphas):
        sn.lineplot(x=result[0], y=result[1], ax=ax, color=color, alpha=alpha)

    def horizontal_strategy_line(strategy, color):
        sn.lineplot(x=[1, comp_fractions[-1]], y=[strategy[1, -1], strategy[1, -1]], ax=ax, color=color)

    horizontal_strategy_line(strategy=sim_results['naive_0'], color='gray')
    horizontal_strategy_line(strategy=sim_results['path_shortening'], color='k')


def get_mean_in_cm(measure, tds):
    return np.array([td[measure].mean() for td in tds]) * game_width_in_cm


def print_prediction_errors(R, v, E, E_opt, E_add, t, r=0.07, T=20 * 60):
    error_0 = r * T / (1 + t) - R
    print(f'tmp_error: {v - E / t}')
    # print(f'tmp_error: {1/t - v / E}')
    error_1 = r * T / (1 + E / v) - R
    error_2 = r * T / (1 + (E_opt + E_add) / v) - R
    # print(error_0, error_1, error_2)
    for i, error in enumerate([error_0, error_1, error_2]):
        print(f'Error_{i}:')
        print(f'Mean: {error.mean()}')
        print(f'Var: {error.var()}')
        print()
    return error_2.mean()


def print_strat_diff_corresponding_quantities(e2, E_add, v, r=0.07, T=20 * 60):
    E_add = E_add.mean()
    v = v.mean()
    sim_results = load_results()
    for key in sim_results.keys():
        sim_results[key][1] *= game_width_in_cm
    naive_0_e_opt = sim_results['naive_0'][1].min()
    naive_10_e_opt = sim_results['naive_10'][1].min()
    naive_30_e_opt = sim_results['naive_30'][1].min()

    def get_rew(E_opt, E_add, v):
        return r * T / (1 + (E_opt + E_add) / v) - e2

    # print(get_rew(naive_10_e_opt, E_add, v))
    # print(get_rew(naive_30_e_opt, E_add, v))

    # note: its total reward emitted
    strat_rew_diff = get_rew(naive_0_e_opt, E_add, v) - get_rew(naive_30_e_opt, E_add, v)

    def get_v_diff():
        to_minimize = lambda v_param: abs(get_rew(naive_0_e_opt, E_add, v) - get_rew(naive_0_e_opt, E_add, v_param) - strat_rew_diff)
        res = scipy.optimize.minimize(to_minimize, x0=v)
        return v - res['x'][0]

    def get_E_add_diff():
        to_minimize = lambda e_add_param: abs(get_rew(naive_0_e_opt, E_add, v) - get_rew(naive_0_e_opt, e_add_param, v) - strat_rew_diff)
        res = scipy.optimize.minimize(to_minimize, x0=v)
        return res['x'][0] - E_add

    v_diff = get_v_diff()
    E_add_diff = get_E_add_diff()
    print(strat_rew_diff)
    print(v_diff)
    print(E_add_diff)

    print('v_diff percentage:', v_diff / v)


def print_group_reward(agent_mean_rewards, first_bount=1/3/2, second_bound=2/3):
    comp_fractions = [get_comp_frac(td) for td in load_all_trial_descriptions()]
    coop_group, inter_group, comp_group = list(), list(), list()
    for c_frac, rew in zip(comp_fractions, agent_mean_rewards):
        if c_frac <= first_bount:
            coop_group.append(rew)
        elif first_bount < c_frac <= second_bound:
            inter_group.append(rew)
        else:
            comp_group.append(rew)
    print(f'coop_group\nmean:\t{np.mean(coop_group)}\nvar:\t{np.var(coop_group)}')
    print(f'inter_group\nmean:\t{np.mean(inter_group)}\nvar:\t{np.var(inter_group)}')
    print(f'comp_group\nmean:\t{np.mean(comp_group)}\nvar:\t{np.var(comp_group)}')


if __name__ == '__main__':
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['text.latex.preamble'] = r'\usepackage{amsfonts}'
    ax_width = 4
    ax_height = ax_width * .67
    rows, cols = 2, 2
    fig, axes = plt.subplots(rows, cols, figsize=(ax_width * cols, ax_height * rows))
    axes = axes.flatten()
    trial_duration_axes = axes[0].twinx()

    tds = load_all_trial_descriptions()
    comp_fractions = get_modified_comp_fractions(tds)
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)
    # y_reward_min, y_reward_max = min(low_rewards) + 2.8, max(high_rewards) - 2.8
    y_reward_min, y_reward_max = min(low_rewards), max(high_rewards)
    path_shortening = get_mean_in_cm('path_shortening', tds)
    speed = get_mean_in_cm('speed', tds)
    chosen_effort = get_mean_in_cm('chosen_effort', tds)
    distance_ol = get_mean_in_cm('distance_ol', tds)

    distance = get_mean_in_cm('distance', tds)
    trial_durations = np.array([td['duration'].to_numpy().mean() for td in tds])

    print_group_reward(agent_mean_reward)

    e2 = print_prediction_errors(R=2 * agent_mean_reward, v=speed, E=distance, E_opt=chosen_effort, E_add=distance_ol,
                                 t=trial_durations)

    print_strat_diff_corresponding_quantities(e2=e2, E_add=distance_ol, v=speed)

    scatter(x=comp_fractions, y=agent_mean_reward, ax=axes[0])
    scatter(x=comp_fractions, y=speed, ax=axes[1])
    scatter(x=comp_fractions, y=chosen_effort, ax=axes[2])
    scatter(x=comp_fractions, y=distance_ol, ax=axes[3])
    vertical_lines(x=comp_fractions, y_min=low_rewards, y_max=high_rewards, ax=axes[0])
    # plot_trial_durations(trial_duration_axes)  # to show precision
    vertical_lines(x=comp_fractions, y_min=chosen_effort, y_max=(chosen_effort + path_shortening), ax=axes[2])
    plot_strategies(axes[2])

    axes[0].set_ylim(y_reward_min, y_reward_max)
    y_ax_scaling(speed, ax=axes[1])
    axes[2].set_ylim(12, 26)
    y_ax_scaling(distance_ol, ax=axes[3])


    # axes[0].set_ylabel(r'Reward per block $R/2$ (\texteuro)', fontdict=fontdict)
    # axes[1].set_ylabel(r'Mean speed $\langle s \rangle$ (cm/s)', fontdict=fontdict)
    # axes[2].set_ylabel(r'Mean initial\newline effort to target $\langle E \rangle$ (cm)', fontdict=fontdict)
    # axes[3].set_ylabel(r'Mean distance\newline overlength $\langle D_{ol} \rangle$ (cm)', fontdict=fontdict)
    # trial_duration_axes.set_ylabel(r'Mean trial duration $\langle t \rangle$ (s)', fontdict=fontdict, rotation=270)
    axes[0].set_ylabel(r'Agent mean\newline payoff $R/2$ (\texteuro)', fontdict=fontdict)
    axes[1].set_ylabel(r'Mean speed $\langle s \rangle$ (cm/s)', fontdict=fontdict)
    axes[2].set_ylabel(r'Mean optimal\newline effort $\langle E_\mathrm{opt} \rangle$ (cm)', fontdict=fontdict)
    axes[3].set_ylabel(r'Mean additional\newline effort $\langle E_\mathrm{add} \rangle$ (cm)', fontdict=fontdict)
    trial_duration_axes.set_ylabel(r'Mean trial duration $\langle t \rangle$ (s)', fontdict=fontdict, rotation=270)

    correction_term = .2
    trial_duration_axes.set_ylim(reward_per_target * block_time / (y_reward_max * 2) - 1 - correction_term,
                                 reward_per_target * block_time / (y_reward_min * 2) - 1 - correction_term)
    trial_duration_axes.yaxis.labelpad = 17
    trial_duration_axes.invert_yaxis()

    comp_frac_locator = ticker.FixedLocator([0, 1/3, 2/3, 1, max(comp_fractions)])
    comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1', '1'])

    for ax_label, ax in zip('ABCD', axes):
        ax.set_xlabel('Fraction of single targets collected $\\Phi$', fontdict=fontdict)
        # ax.get_legend().remove()
        ax.xaxis.set_major_locator(comp_frac_locator)
        ax.xaxis.set_major_formatter(comp_frac_formatter)
        ax.vlines(1, ymin=0, ymax=100, color='k', linewidth=1)
        ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes, size=14, weight='bold')
        ax.xaxis.set_tick_params(labelsize=14)
        ax.yaxis.set_tick_params(labelsize=14)
    trial_duration_axes.yaxis.set_tick_params(labelsize=14)

    fig.tight_layout()
    save_fig()

