import numpy as np
from matplotlib import pyplot as plt

from glm.util import cut_first_half
from presentation.plotting import get_comp_frac_color
from rew_diff import get_skill_differences, get_skill_diff
from trial_descriptor import load_all_trial_descriptions, get_comp_frac


def cut_td(td, t):
    for i, start_idx in enumerate(td['start'].to_numpy()):
        if start_idx > t:
            return td.iloc[:i]
    return None


if __name__ == '__main__':
    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    timings = np.arange(1 * 60 * 120, 10 * 60 * 120, 10 * 120)
    skill_diff_timeseries = [np.zeros_like(timings, dtype=float) for _ in tds]
    for i, td in enumerate(tds):
        for k, t in enumerate(timings):
            skill_diff_timeseries[i][k] = get_skill_diff(cut_td(td=td, t=t))
    for sd_timeseries, comp_frac in zip(skill_diff_timeseries, comp_fractions):
        if comp_frac > .1:
            plt.plot(timings, sd_timeseries, c=get_comp_frac_color(comp_frac))
