import numpy as np
import seaborn as sn
from matplotlib import pyplot as plt

from glm.copy_best_over import get_best_models


def unfold_weight_matrix(w):
    return np.array([
        [w[0], w[1], w[1]],
        [w[2], w[3], w[4]],
        [w[2], w[4], w[3]],
    ]).T


def unfold_effort(w):
    return np.array([w[0], w[1], w[1]])[:, None]


if __name__ == '__main__':
    best_models = get_best_models()

    weights = best_models[1][1]

    to_plot = dict(
        distances=unfold_effort(weights['effort']),
        invite=unfold_weight_matrix(weights['invite']),
        hist=unfold_weight_matrix(weights['1_prev_t']),
    )

    fig, axes = plt.subplots(3, 1)
    axes = axes.flatten()

    for ax, (modality_name, weights) in zip(axes, to_plot.items()):
        sn.heatmap(ax=ax, data=weights)

