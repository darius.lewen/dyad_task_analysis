import pickle

import numpy as np
import pandas as pd
import scipy
from tqdm import tqdm

import glm.linearities_sim as lin
from glm.likelihood import entropy, gen_glm
from glm.preprocess import load_preprocessed_data
from glm.util import to_hot_encoded
from load.data_loader import pair_idx_single_rec_path
from load.loading_helper import unique_pairs
from trial_descriptor_2 import get_comp_frac, unsorted_load_all_trial_descriptions, load_trial_descriptions, \
    save_tds_as_csv


def nll(w, glm_func, glm_kwargs):
    expected_values = glm_func(w, glm_kwargs)
    local_likelihoods = expected_values[to_hot_encoded(glm_kwargs['next_collections']).astype(bool)]
    return np.sum(-1 * np.log(local_likelihoods))


def get_train_test_separation(trial_amount, test_fraction=.2):
    test_size = int(test_fraction * trial_amount)
    test_indices, train_indices = list(), list()
    for i in range(test_size, trial_amount + 1, test_size):
        test_indices.append(np.arange(i - test_size, i))
        indices_before_test_indices = np.arange(0, i - test_size)
        indices_after_test_indices = np.arange(i, trial_amount)
        if len(indices_after_test_indices) >= test_size:
            train_indices.append(np.concatenate([indices_before_test_indices, indices_after_test_indices]))
        else:
            train_indices.append(indices_before_test_indices)  # to include everything in a test set once
            test_indices[-1] = np.concatenate([test_indices[-1], indices_after_test_indices])
    return test_indices, train_indices


def get_model_summary(glm, glm_kwargs, train_i, test_i, start_time):
    args = (glm, {key: val[train_i] for key, val in glm_kwargs.items()})
    res = scipy.optimize.minimize(nll, np.random.normal(0, 0.001, size=glm.param_amount), args)
    # print()
    # print(res['hess_inv'])
    # print()
    standard_errors = np.diagonal(res['hess_inv']) ** .5
    test_glm_kwargs = {key: val[test_i] for key, val in glm_kwargs.items()}
    expected_values = glm(res['x'], test_glm_kwargs)
    predictions = np.argmax(expected_values, axis=1)
    collections = test_glm_kwargs['next_collections']
    aic = 2 * glm.param_amount + 2 * res['fun']  # plus because we use nll
    return dict(
        coefficients=res['x'],
        standard_errors=standard_errors,
        p_values=2 * scipy.stats.norm.cdf(-1 * abs(res['x'] / standard_errors)),
        test_indices=test_i,
        train_indices=train_i,
        start_time=start_time[test_i],
        expected_values=expected_values,
        prediction_entropies=entropy(expected_values),
        predictions=predictions,
        collections=collections,
        right_predicted_binary=(predictions == collections).astype(int),
        aic=aic,
    )


def get_prev_t_encoding(td):
    """
    0: prev single
    1: prev joint0
    2: prev joint1
    """
    trial_class = td['trial_class'].to_numpy()
    prev_t = td['prev_coll_type'].to_numpy()
    prev_t_inv = np.zeros_like(trial_class)
    prev_t_inv[prev_t == 'joint0'] = 1
    prev_t_inv[prev_t == 'joint1'] = 2
    return prev_t_inv


def get_prev_t_inv_encoding(td):
    """
    0: prev single and no invite
    1: prev joint0
    2: prev joint1
    3: prev single and invite towards joint0
    4: prev single and invite towards joint1
    """
    trial_class = td['trial_class'].to_numpy()
    prev_t = td['prev_coll_type'].to_numpy()
    acc_coop0_inv = ('accepted_coop0_invite' == trial_class).astype(int)
    acc_coop1_inv = ('accepted_coop1_invite' == trial_class).astype(int)
    dec_coop0_inv = ('declined_coop0_invite' == trial_class).astype(int)
    dec_coop1_inv = ('declined_coop1_invite' == trial_class).astype(int)
    coop0_inv = 1 == (acc_coop0_inv + dec_coop0_inv)
    coop1_inv = 1 == (acc_coop1_inv + dec_coop1_inv)
    prev_t_inv = np.zeros_like(trial_class)
    prev_t_inv[prev_t == 'joint0'] = 1
    prev_t_inv[prev_t == 'joint1'] = 2
    prev_t_inv[coop0_inv] = 3
    prev_t_inv[coop1_inv] = 4
    return prev_t_inv


def get_collection_encoding(td, key):
    prev_2_collections = td[key].to_numpy().copy()
    prev_2_collections[prev_2_collections == 'single_p0'] = 0
    prev_2_collections[prev_2_collections == 'single_p1'] = 0
    prev_2_collections[prev_2_collections == 'joint0'] = 1
    prev_2_collections[prev_2_collections == 'joint1'] = 2
    prev_2_collections[prev_2_collections == 'no_prev'] = 0  # this is zero padding...
    return prev_2_collections


def get_inv_encoding(td):
    trial_class = td['trial_class'].to_numpy()
    acc_coop0_inv = ('accepted_coop0_invite' == trial_class).astype(int)
    acc_coop1_inv = ('accepted_coop1_invite' == trial_class).astype(int)
    dec_coop0_inv = ('declined_coop0_invite' == trial_class).astype(int)
    dec_coop1_inv = ('declined_coop1_invite' == trial_class).astype(int)
    coop0_inv = 1 == (acc_coop0_inv + dec_coop0_inv)
    coop1_inv = 1 == (acc_coop1_inv + dec_coop1_inv)
    invites = np.zeros_like(trial_class)
    invites[coop0_inv] = 1
    invites[coop1_inv] = 2
    return invites


def get_model_summaries(td, glm):
    # glm_kwargs_old = load_preprocessed_data(0, 'full')
    # efforts, prev_t_inv [0, 1, 2, 3, 4], prev_2_collections [0, 1, 2]
    glm_kwargs = dict(
        efforts=td[['dist_single', 'dist_joint0', 'dist_joint1']].to_numpy(),
        prev_1_collections=get_prev_t_encoding(td),
        prev_t_inv=get_prev_t_inv_encoding(td),
        invites=get_inv_encoding(td),
        prev_2_collections=get_collection_encoding(td, key='prev_prev_coll_type'),
        next_collections=get_collection_encoding(td, key='coll_type'),
    )
    test_indices, train_indices = get_train_test_separation(trial_amount=len(glm_kwargs['next_collections']))
    start_time = td['start_time'].to_numpy()
    model_summaries = list()
    for test_i, train_i in zip(test_indices, train_indices):
        model_summaries.append(get_model_summary(glm, glm_kwargs, train_i, test_i, start_time))
    return model_summaries



def get_per_dyad_stats(glm, save=False):
    per_dyad_stats = list()
    tds = unsorted_load_all_trial_descriptions()
    for pair, td in tqdm(zip(unique_pairs, tds)):
        comp_frac = get_comp_frac(td)
        model_summaries = get_model_summaries(td, glm)
        right_predicted_binary = np.concatenate([summary['right_predicted_binary'] for summary in model_summaries])
        start_time = np.concatenate([summary['start_time'] for summary in model_summaries])
        expected_values = np.concatenate([summary['expected_values'] for summary in model_summaries])
        # print(len(td), len(expected_values))
        entropies = np.concatenate([summary['prediction_entropies'] for summary in model_summaries])
        collection_indices = np.concatenate([summary['collections'] for summary in model_summaries])
        aic = np.mean([summary['aic'] for summary in model_summaries])
        # aic = model_summaries[0]['aic']  # todo keep it like that?
        per_dyad_stats.append(dict(
            model_summaries=model_summaries,
            right_predicted_binary=right_predicted_binary,
            accuracy=np.mean(right_predicted_binary),
            start_time=start_time,
            expected_values=expected_values,
            entropies=entropies,
            collection_indices=collection_indices,
            aic=aic,
            dyad_idx=pair,
            comp_frac=comp_frac,
        ))
    if save:
        with open('per_dyad_stats.pkl', 'wb') as f:
            pickle.dump(per_dyad_stats, f)
    return per_dyad_stats


def load_per_dyad_stats():
    with open('/home/brain/work/dyadic_task_analysis/glm/per_dyad_stats.pkl', 'rb') as f:
        per_dyad_stats = pickle.load(f)
    return per_dyad_stats


def append_prediction_values_to_tds(per_dyad_stats):
    for dyad_idx, dyad_model_stats in enumerate(per_dyad_stats):
        block0_path = pair_idx_single_rec_path(dyad_idx, 0) + '/trial_descriptions_2.pkl'
        block1_path = pair_idx_single_rec_path(dyad_idx, 1) + '/trial_descriptions_2.pkl'
        block0_td = pd.read_pickle(block0_path)
        block1_td = pd.read_pickle(block1_path)
        if 'entropy' in block0_td.keys():
            block0_td = block0_td.drop(columns=['pred_prob_single', 'pred_prob_joint0', 'pred_prob_joint1', 'entropy'])
            block1_td = block1_td.drop(columns=['pred_prob_single', 'pred_prob_joint0', 'pred_prob_joint1', 'entropy'])
        expected_values = dyad_model_stats['expected_values']
        to_add = dict(
            pred_prob_single=expected_values[:, 0],
            pred_prob_joint0=expected_values[:, 1],
            pred_prob_joint1=expected_values[:, 2],
            entropy=dyad_model_stats['entropies'],
        )
        trial_amount = len(block0_td) + len(block1_td)
        no_pred_amount = trial_amount - len(dyad_model_stats['entropies'])
        to_add = {key: np.concatenate([[np.nan for _ in range(no_pred_amount)], val]) for key, val in to_add.items()}
        to_add_block0 = pd.DataFrame({key: val[:len(block0_td)] for key, val in to_add.items()})
        to_add_block1 = pd.DataFrame({key: val[len(block0_td):] for key, val in to_add.items()})
        new_block0_td = block0_td.join(to_add_block0)
        new_block1_td = block1_td.join(to_add_block1)
        new_block0_td.to_pickle(block0_path)
        new_block1_td.to_pickle(block1_path)

def avoidance_of_prev_significance_check():
    per_dyad_stats = load_per_dyad_stats()
    comp_frac_below_90 = [.9 > get_comp_frac(td) for td in unsorted_load_all_trial_descriptions()]
    significant = list()
    significant_coef_means_per_dyad = list()
    for stat, below in zip(per_dyad_stats, comp_frac_below_90):
        if below:
            significant_in_k_fold_model = list()
            significant_coefficients_in_this_dyad = list()
            for m_summary in stat['model_summaries']:
                prev_t_single_p, prev_t_joint_p = m_summary['p_values'][2:4]
                single_significant = prev_t_single_p < .05 and not np.isnan(m_summary['coefficients'][2])
                joint_significant = prev_t_joint_p < .05 and not np.isnan(m_summary['coefficients'][3])
                significant_in_k_fold_model.append(single_significant or joint_significant)
                if significant_in_k_fold_model[-1]:
                    if single_significant:
                        significant_coefficients_in_this_dyad.append(m_summary['coefficients'][2])
                    if joint_significant:
                        significant_coefficients_in_this_dyad.append(m_summary['coefficients'][3])
            print('a', significant_in_k_fold_model)
            print('b', all(significant_in_k_fold_model))
            significant.append(all(significant_in_k_fold_model))
            if significant[-1]:
                print(significant_coefficients_in_this_dyad)
                significant_coef_means_per_dyad.append(np.mean(significant_coefficients_in_this_dyad))
    print(f'identity of the previous target was significant in {np.sum(significant)} out of {len(significant)} dyads with FST below 0.9')
    print(f'mean regression coefficient {np.mean(abs(np.array(significant_coef_means_per_dyad)))}')


def coefficient_analysis():
    # for pair_idx, pair in enumerate(unique_pairs):
    coefficient_names = ['single_distance_weight',
                         'joint_distance_weight ',
                         'prev_single_and_no_inv',
                         'prev_joint            ',
                         'invite                ',
                         'prev_prev             ']
    prev_joint_neg_and_sig_counter = 0
    prev_joint_sig_counter = 0
    invite_pos_and_sig_counter = 0
    invite_sig_counter = 0

    prev_single_neg_and_sig_counter = 0
    prev_single_sig_counter = 0
    prev_prev_pos_and_sig_counter = 0
    prev_prev_sig_counter = 0

    p_values = dict(
        prev_single=list(),
        prev_joint=list(),
        invite=list(),
        prev_prev=list(),
    )
    coefficients = dict(
        prev_single=list(),
        prev_joint=list(),
        invite=list(),
        prev_prev=list(),
    )
    for dyad_stat, td, (pair_idx, pair) in zip(per_dyad_stats, unsorted_load_all_trial_descriptions(),
                                               enumerate(unique_pairs)):
        comp_frac = get_comp_frac(td)
        model_summary = dyad_stat['model_summaries'][0]
        print(f'dyad: {pair}')
        print(f'FST: {"% 1.2f" % comp_frac}')
        for i, coef_name in enumerate(coefficient_names):
            coef = '% 4.2f' % model_summary['coefficients'][i]
            std_err = '% 4.2f' % model_summary['standard_errors'][i]
            p_val = '% 4.2f' % model_summary['p_values'][i]
            print(f'{coef_name}\t\t{coef}\t{std_err}\t{p_val}')
        prev_single_coef = model_summary['coefficients'][2]
        prev_joint_coef = model_summary['coefficients'][3]
        invite_coef = model_summary['coefficients'][4]
        prev_prev_coef = model_summary['coefficients'][5]
        prev_single_p_val = model_summary['p_values'][2]
        prev_joint_p_val = model_summary['p_values'][3]
        invite_p_val = model_summary['p_values'][4]
        prev_prev_p_val = model_summary['p_values'][5]
        p_values['prev_single'].append(prev_single_p_val)
        p_values['prev_joint'].append(prev_joint_p_val)
        p_values['invite'].append(invite_p_val)
        p_values['prev_prev'].append(prev_prev_p_val)
        coefficients['prev_single'].append(prev_single_coef)
        coefficients['prev_joint'].append(prev_joint_coef)
        coefficients['invite'].append(invite_coef)
        coefficients['prev_prev'].append(prev_prev_coef)
        # if prev_single_p_val < .05:
        #     prev_single_sig_counter += 1
        #     if prev_single_coef < 0:
        #         prev_single_neg_and_sig_counter += 1
        # if prev_joint_p_val < .05:
        #     prev_joint_sig_counter += 1
        #     if prev_joint_coef < 0:
        #         prev_joint_neg_and_sig_counter += 1
        # if invite_p_val < .05:
        #     invite_sig_counter += 1
        #     if invite_coef > 0:
        #         invite_pos_and_sig_counter += 1
        # if prev_prev_p_val < .05:
        #     prev_prev_sig_counter += 1
        #     if prev_prev_coef > 0:
        #         prev_prev_pos_and_sig_counter += 1

    adjusted_p_values = scipy.stats.false_discovery_control(np.concatenate(list(p_values.values())))
    n = len(list(p_values.values())[0])
    p_values = {key: adjusted_p_values[i * n: (i+1) * n] for i, key in enumerate(p_values.keys())}
    for key in p_values.keys():
        sig_counter = 0
        negative_and_sig_counter = 0
        for p_val, coef in zip(p_values[key], coefficients[key]):
            if p_val < .05:
                sig_counter += 1
                if coef < 0:
                    negative_and_sig_counter += 1
        print(f'{key} significant {sig_counter}/{n} thereby negative {negative_and_sig_counter}/{sig_counter}')
    #
    #
    #
    #
    #
    # print(0)
    # print(f'prev_single_and_no_inv neg: {prev_single_neg_and_sig_counter}/{prev_single_sig_counter}')
    # print(f'prev_joint neg: {prev_joint_neg_and_sig_counter}/{prev_joint_sig_counter}')
    # print(f'invite pos: {invite_pos_and_sig_counter}/{invite_sig_counter}')
    # print(f'prev_prev pos: {prev_prev_pos_and_sig_counter}/{prev_prev_sig_counter}')


if __name__ == '__main__':
    # glm = gen_glm([lin.effort_linearity, lin.prev_t_inv_linearity])
    glm = gen_glm([lin.effort_linearity, lin.prev_t_inv_linearity, lin.prev_prev_t_linearity])
    # per_dyad_stats = get_per_dyad_stats(glm, save=True)
    per_dyad_stats = load_per_dyad_stats()
    # avoidance_of_prev_significance_check()
    coefficient_analysis()
    append_prediction_values_to_tds(per_dyad_stats)
    # tds = unsorted_load_all_trial_descriptions()

    save_tds_as_csv()