import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats

from fst_convergence import split_tds_at_minute
from glm.util import add_properties
from presentation.plotting import scatterplot, reg_plot, r_and_p_text
from trial_descriptor_2 import load_all_trial_descriptions, get_comp_frac


def trunc_exp_distri(x, rate, upper=1):
    return 1 / rate * np.exp(-x / rate) / (1 - np.exp(-upper / rate))


@add_properties(param_amount=1, name='intercept_model')
def intercept_model(w, covariates):
    return w * np.ones(covariates['single_binary'].shape[0])


@add_properties(param_amount=1, name='initial_comp_frac_model')
def initial_comp_frac_model(w, covariates):
    single_binary = covariates['single_binary']
    initial_comp_frac = np.mean(single_binary)
    return w * initial_comp_frac * np.ones(covariates['single_binary'].shape[0])


@add_properties(param_amount=2, name='model_1')
def model_1(w, covariates):
    rate, w0 = w
    single_binary = covariates['single_binary']
    single_dists = covariates['single_dists']
    joint_dists = covariates['joint_dists']
    exp_weights = trunc_exp_distri(np.linspace(0, 1, single_dists.shape[1]), rate=rate)
    weighted_single_dists = single_dists * exp_weights
    weighted_joint_dists = joint_dists * exp_weights
    initial_comp_frac = np.mean(single_binary)
    return initial_comp_frac + w0 * np.sum(weighted_single_dists - weighted_joint_dists, axis=1)


@add_properties(param_amount=2, name='model_1_1')
def model_1_1(w, covariates):
    rate, w0 = w
    single_binary = covariates['single_binary']
    exp_weights = trunc_exp_distri(np.linspace(0, 1, single_binary.shape[1]), rate=rate)
    weighted_single_binary = np.flip(single_binary, axis=1) * exp_weights
    return np.sum(w0 * weighted_single_binary, axis=1)


@add_properties(param_amount=3, name='model_1_1_1')
def model_1_1_1(w, covariates):
    rate, w0, w1 = w
    single_binary = covariates['single_binary']
    invite_binary = covariates['invite_binary']
    exp_weights = trunc_exp_distri(np.linspace(0, 1, single_binary.shape[1]), rate=rate)
    weighted_single_binary = np.flip(single_binary, axis=1) * exp_weights
    return np.sum(w0 * weighted_single_binary + w1 * invite_binary, axis=1)


@add_properties(param_amount=3, name='model_1_1_2')
def model_1_1_2(w, covariates):
    rate, w0, w1 = w
    single_binary = covariates['single_binary']
    invite_binary = covariates['invite_binary']
    exp_weights = trunc_exp_distri(np.linspace(0, 1, single_binary.shape[1]), rate=rate)
    weighted_single_binary = np.flip(single_binary, axis=1) * exp_weights
    weighted_invite_binary = np.flip(invite_binary, axis=1) * exp_weights
    return np.sum(w0 * weighted_single_binary + w1 * weighted_invite_binary, axis=1)


@add_properties(param_amount=4, name='model_1_1_3')
def model_1_1_3(w, covariates):
    rate0, rate1, w0, w1 = w
    single_binary = covariates['single_binary']
    invite_binary = covariates['invite_binary']
    exp_weights0 = trunc_exp_distri(np.linspace(0, 1, single_binary.shape[1]), rate=rate0)
    exp_weights1 = trunc_exp_distri(np.linspace(0, 1, single_binary.shape[1]), rate=rate1)
    weighted_single_binary = np.flip(single_binary, axis=1) * exp_weights0
    weighted_invite_binary = np.flip(invite_binary, axis=1) * exp_weights1
    return np.sum(w0 * weighted_single_binary + w1 * weighted_invite_binary, axis=1)


@add_properties(param_amount=2, name='model_1_2')
def model_1_2(w, covariates):
    rate, w0 = w
    single_dists = covariates['single_dists']
    joint_dists = covariates['joint_dists']
    exp_weights = trunc_exp_distri(np.linspace(0, 1, single_dists.shape[1]), rate=rate)
    weighted_single_dists = single_dists * exp_weights
    weighted_joint_dists = joint_dists * exp_weights
    return w0 * np.sum(weighted_single_dists - weighted_joint_dists, axis=1)


@add_properties(param_amount=3, name='model_2')
def model_2(w, covariates):
    rate, w0, w1 = w
    single_binary = covariates['single_binary']
    single_dists = covariates['single_dists']
    joint_dists = covariates['joint_dists']
    exp_weights = trunc_exp_distri(np.linspace(0, 1, single_dists.shape[1]), rate=rate)
    weighted_single_dists = single_dists * exp_weights
    weighted_joint_dists = joint_dists * exp_weights
    initial_comp_frac = np.mean(single_binary)
    return w0 * initial_comp_frac + w1 * np.sum(weighted_single_dists - weighted_joint_dists, axis=1)


@add_properties(param_amount=3, name='model_2_1')  # todo add invites
def model_2_1(w, covariates):
    rate, w0, w1 = w
    single_binary = covariates['single_binary']
    single_dists = covariates['single_dists']
    joint_dists = covariates['joint_dists']
    exp_weights = trunc_exp_distri(np.linspace(0, 1, single_dists.shape[1]), rate=rate)
    weighted_single_dists = single_dists * exp_weights
    weighted_joint_dists = joint_dists * exp_weights
    weighted_single_binary = np.flip(single_binary, axis=1) * exp_weights
    return np.sum(w0 * weighted_single_binary + w1 * (weighted_single_dists - weighted_joint_dists), axis=1)


@add_properties(param_amount=3, name='model_2_1_1')
def model_2_1_1(w, covariates):
    rate, w0, w1 = w
    single_binary = covariates['single_binary']
    single_dists = covariates['single_dists']
    joint_dists = covariates['joint_dists']
    exp_weights = trunc_exp_distri(np.linspace(0, 1, single_dists.shape[1]), rate=rate)
    weighted_single_dists = 1 / single_dists * exp_weights
    weighted_joint_dists = 1 / joint_dists * exp_weights
    weighted_single_binary = np.flip(single_binary, axis=1) * exp_weights
    return np.sum(w0 * weighted_single_binary + w1 * (weighted_single_dists - weighted_joint_dists), axis=1)


@add_properties(param_amount=4, name='model_2_2')
def model_2_2(w, covariates):
    rate0, rate1, w0, w1 = w
    single_binary = covariates['single_binary']
    single_dists = covariates['single_dists']
    joint_dists = covariates['joint_dists']
    exp_weights0 = trunc_exp_distri(np.linspace(0, 1, single_dists.shape[1]), rate=rate0)
    exp_weights1 = trunc_exp_distri(np.linspace(0, 1, single_dists.shape[1]), rate=rate1)
    weighted_single_dists = single_dists * exp_weights0
    weighted_joint_dists = joint_dists * exp_weights0
    weighted_single_binary = np.flip(single_binary, axis=1) * exp_weights1
    return np.sum(w0 * weighted_single_binary + w1 * (weighted_single_dists - weighted_joint_dists), axis=1)


@add_properties(param_amount=4, name='model_2_2_1')
def model_2_2_1(w, covariates):
    rate0, rate1, w0, w1 = w
    single_binary = covariates['single_binary']
    single_dists = covariates['single_dists']
    joint_dists = covariates['joint_dists']
    exp_weights0 = trunc_exp_distri(np.linspace(0, 1, single_dists.shape[1]), rate=rate0)
    exp_weights1 = trunc_exp_distri(np.linspace(0, 1, single_dists.shape[1]), rate=rate1)
    weighted_single_dists = 1 / single_dists * exp_weights0
    weighted_joint_dists = 1 / joint_dists * exp_weights0
    weighted_single_binary = np.flip(single_binary, axis=1) * exp_weights1
    return np.sum(w0 * weighted_single_binary + w1 * (weighted_single_dists - weighted_joint_dists), axis=1)


@add_properties(param_amount=5, name='model_2_2_2')
def model_2_2_2(w, covariates):
    rate0, rate1, w0, w1, w2 = w
    single_binary = covariates['single_binary']
    single_dists = covariates['single_dists']
    joint_dists = covariates['joint_dists']
    exp_weights0 = trunc_exp_distri(np.linspace(0, 1, single_dists.shape[1]), rate=rate0)
    exp_weights1 = trunc_exp_distri(np.linspace(0, 1, single_dists.shape[1]), rate=rate1)
    weighted_single_dists = single_dists * exp_weights0
    weighted_joint_dists = joint_dists * exp_weights0
    weighted_single_binary = np.flip(single_binary, axis=1) * exp_weights1
    return np.sum(w0 * weighted_single_binary + w1 * weighted_single_dists - w2 * weighted_joint_dists, axis=1)


@add_properties(param_amount=4, name='model_3')
def model_3(w, covariates):
    rate, w0, w1, w2 = w
    single_binary = covariates['single_binary']
    single_dists = covariates['single_dists']
    joint_dists = covariates['joint_dists']
    exp_weights = trunc_exp_distri(np.linspace(0, 1, single_dists.shape[1]), rate=rate)
    weighted_single_dists = single_dists * exp_weights
    weighted_joint_dists = joint_dists * exp_weights
    initial_comp_frac = np.mean(single_binary)
    return w2 * initial_comp_frac + np.sum(w0 * weighted_single_dists - w1 * weighted_joint_dists, axis=1)


def mse(w, y, model_func, covariates):
    y_pred = model_func(w, covariates)
    mse = np.mean((y - y_pred) ** 2)
    return mse


def aic(w, args):  # we assume normally distributed errors
    n = len(args[0])  # should be comp_fractions
    small_sample_correction = 2 * len(w) ** 2 + 2 * len(w) / (n - len(w) - 1)
    return n * np.log(mse(w, *args)) + 2 * len(w) + small_sample_correction


def get_covariates(tds, n=40):
    single_binary = np.zeros((len(tds), n))
    invite_binary = np.zeros((len(tds), n))
    single_dists = np.zeros((len(tds), n))
    joint_dists = np.zeros((len(tds), n))
    for td_idx, td in enumerate(tds):
        single_binary[td_idx] += (td.iloc[:n]['coll_type'] == 'single_p0').to_numpy(dtype=int)
        single_binary[td_idx] += (td.iloc[:n]['coll_type'] == 'single_p1').to_numpy(dtype=int)
        invite_binary[td_idx] += (td.iloc[:n]['coll_type'] == 'accepted_coop0_invite').to_numpy(dtype=int)
        invite_binary[td_idx] += (td.iloc[:n]['coll_type'] == 'accepted_coop1_invite').to_numpy(dtype=int)
        invite_binary[td_idx] += (td.iloc[:n]['coll_type'] == 'declined_coop0_invite').to_numpy(dtype=int)
        invite_binary[td_idx] += (td.iloc[:n]['coll_type'] == 'declined_coop1_invite').to_numpy(dtype=int)
        single_dists[td_idx] = td.iloc[:n]['dist_single'].to_numpy()
        joint_dists[td_idx] = np.min(td.iloc[:n][['dist_joint0', 'dist_joint1']], axis=1)
    return dict(
        single_binary=single_binary,
        invite_binary=invite_binary,
        single_dists=single_dists,
        joint_dists=joint_dists
    )


def get_model_results(models, comp_fractions, covariates):
    model_results = list()
    for model in models:
        args = (comp_fractions, model, covariates)
        res = scipy.optimize.minimize(fun=mse, x0=np.ones(model.param_amount), args=args)
        predicted_comp_fractions = model(res['x'], covariates)
        nan_indices = np.isnan(predicted_comp_fractions)
        r, p = scipy.stats.pearsonr(comp_fractions[~nan_indices], predicted_comp_fractions[~nan_indices])
        model_results.append(dict(
            model_name=model.name,
            res=res,
            r=r,
            p=p,
            predicted_comp_fractions=predicted_comp_fractions,
            aic_val=aic(res['x'], args),
        ))
    return model_results


def get_performance_per_time_per_model(models, comp_fractions, tds, span):
    aic_per_time_per_model = {model.name: list() for model in models}
    r_per_time_per_model = {model.name: list() for model in models}
    for i in range(*span):
        covariates = get_covariates(tds, n=i)
        model_results = get_model_results(models, comp_fractions, covariates)
        for m_res in model_results:
            aic_per_time_per_model[m_res['model_name']].append(m_res['aic_val'])
            r_per_time_per_model[m_res['model_name']].append(m_res['r'])
    return aic_per_time_per_model, r_per_time_per_model


def mean_time_after_n_collections(tds, span):
    mean_times = np.zeros(span[1] - span[0])
    for i in range(*span):
        mean_times[i - span[0]] = np.mean([td.iloc[i]['start_time'] for td in tds])
    return mean_times


if __name__ == '__main__':
    xspace = np.linspace(0, 1)

    tds = load_all_trial_descriptions(cut_initial=False)
    comp_fractions = np.array([get_comp_frac(td) for td in load_all_trial_descriptions(cut_initial=True)])

    all_models = [
        intercept_model, initial_comp_frac_model, model_1, model_1_1, model_1_1_1, model_1_1_2, model_1_1_3,
        model_1_2, model_2, model_2_1, model_2_2, model_2_2_2, model_3
    ]
    models = [model_1_1, model_2_1, model_2_1_1, model_2_2_1,  model_2_2_2]
    covariates = get_covariates(tds)
    model_results = get_model_results(models, comp_fractions, covariates)

    max_n = 100
    span = (3, max_n)
    aic_per_time_per_model, r_per_time_per_model = get_performance_per_time_per_model(models, comp_fractions, tds, span)
    mean_times = mean_time_after_n_collections(tds, span)
    fig, axes = plt.subplots(1, 2)
    axes = axes.flatten()
    for m in models:
        axes[0].plot(mean_times, aic_per_time_per_model[m.name], label=m.name)  # todo use small sample AICc
        axes[1].plot(mean_times, r_per_time_per_model[m.name], label=m.name)
    for ax in axes:
        ax.legend()
