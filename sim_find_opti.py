import time

import numpy as np
import scipy
from matplotlib import pyplot as plt

from simulations import calc_curves, load_results

opti_search_res_dir = 'sim_results_opti_search'

if __name__ == '__main__':
    t0 = time.time()

    weights = np.linspace(.46, .6, 8)  # first search - find calculation results in corresponding folder
    # weights = np.linspace(.55, .57, 10)  # second search

    # for _ in range(100):  # for path_shortening: one iteration needs 25 minutes.  todo nothing is saved debug! -> done now it should work
    #     calc_curves(process_amount=8, res_dir=opti_search_res_dir, weights=weights)
    res = load_results(opti_search_res_dir)
    comp_frac = res['path_shortening'][0]
    dist = res['path_shortening'][1]

    plt.plot(comp_frac, dist)
    xspace = np.linspace(min(comp_frac), max(comp_frac), int(1e8))
    dist_interpolation = scipy.interpolate.interp1d(comp_frac, dist, kind='cubic')
    plt.plot(xspace, dist_interpolation(xspace))
    print(f'Minimum: {xspace[np.argmin((dist_interpolation(xspace)))]}')
    # plt.plot(weights, dist)

    # for _ in range(100):  # for path_shortening: one iteration needs 25 minutes.
    #     calc_curves(process_amount=8, res_dir=opti_search_res_dir, weights=np.linspace(.46, .6, 8))
    print(f'{time.time() - t0} seconds needed')
