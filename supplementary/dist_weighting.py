import numpy as np
import pandas as pd
import seaborn as sn
from matplotlib import pyplot as plt, ticker

from _util import save_fig, game_width_in_cm
from inefficiencies_full import comp_frac_x_format
from presentation.plotting import scatterplot
from simulations import load_results, get_weightings
from trial_descriptor_2 import load_all_trial_descriptions, get_comp_frac

if __name__ == '__main__':
    tds = load_all_trial_descriptions()
    sim_res = load_results()
    weightings = get_weightings()
    to_plot = pd.DataFrame(dict(
        red_due_to_adv_placement=game_width_in_cm * np.array([np.mean(td['reduction_due_to_adv_placement']) for td in tds]),
        agent_to_target=game_width_in_cm * np.array([np.mean(td['limiting_agent_next_target_displacement']) for td in tds]),
        comp_fractions=np.array([get_comp_frac(td) for td in tds]),
    ))

    size_scaler = 3 * .994
    fig, axes = plt.subplots(1, 2, figsize=(2 * size_scaler, 1 * size_scaler))
    plt.subplots_adjust(top=.75, bottom=.3, right=.9)
    axes = axes.flatten()

    # for ax_label, ax in zip('ab', axes):
    #     ax.set_xlabel('Stable FST')
    #     ax.set_box_aspect(1)
    #     ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes, weight='bold')
    #     comp_frac_x_format(ax)
    axes[0].set_box_aspect(1)
    axes[1].set_box_aspect(1)
    comp_frac_locator = ticker.FixedLocator([0, 1/3, 2/3, 1])
    comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1'])
    axes[0].yaxis.set_major_locator(comp_frac_locator)
    axes[0].yaxis.set_major_formatter(comp_frac_formatter)
    axes[1].xaxis.set_major_locator(comp_frac_locator)
    axes[1].xaxis.set_major_formatter(comp_frac_formatter)
    axes[0].set_ylabel('Stable FST')
    axes[0].set_xlabel('Target-type weighting')
    axes[1].set_ylabel('Mean distance\nto target (cm)')
    axes[1].set_ylim([12.475550740475837, 25.757501087688862])

    colors = ['gray', 'gray', 'gray', 'k']
    alphas = [1, 2 / 3, 1 / 3, 1]

    for c, a, sim_resu in zip(colors, alphas, sim_res.values()):
        sn.lineplot(x=weightings, y=sim_resu[0], ax=axes[0], color=c, alpha=a)
        sn.lineplot(x=sim_resu[0], y=game_width_in_cm * sim_resu[1], ax=axes[1], color=c, alpha=a)

    scatterplot(data=to_plot, x='comp_fractions', y='agent_to_target', ax=axes[1])

    for reduction, a_to_t, comp_frac in to_plot.iloc:
        axes[1].plot([comp_frac, comp_frac], [a_to_t, a_to_t + reduction], c='grey', alpha=.3, linewidth=1.4)
        # axes[1].plot([comp_frac, comp_frac], [a_to_t, a_to_t + reduction], c='green', alpha=.3)

    save_fig(sub_folder='supplementary/', format='pdf')
    save_fig(sub_folder='supplementary/', format='svg')

