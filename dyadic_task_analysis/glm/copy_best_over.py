import numpy as np
import pandas as pd

from glm.plotting.entropy_vs_duration_2 import cut_first_td_half
from glm.plotting.performance_plot import get_best_model_for_dyad
from load.data_loader import pair_idx_to_ids, get_rec_num_path, recordings_folder
from load.loading_helper import unique_pairs
from trial_descriptor import load_trial_descriptions, unsorted_load_all_trial_descriptions, get_comp_frac


def split_for_laps(predictions, dyad_idx):
    td = cut_first_td_half(load_trial_descriptions(dyad_idx, lap=0))
    return predictions[: len(td) - 4], predictions[len(td) - 4:]


def get_best_models(filename='unsorted_performance.pkl'):
    best_models = list()
    all_model_vals = pd.read_pickle(filename)
    for dyad_idx in range(len(unique_pairs)):
        best_model_name = get_best_model_for_dyad(dyad_idx, filename=filename)
        best_models.append((best_model_name, all_model_vals[best_model_name]['weights'][dyad_idx][0]))
    return best_models


if __name__ == '__main__':
    best_models = get_best_models()
    for best_model_name, weights in best_models:
        print(best_model_name)
    # filename = 'unsorted_performance.pkl'
    # all_model_vals = pd.read_pickle(filename)
    # best_models = list()
    # for dyad_idx in range(len(unique_pairs)):
    #     best_model_name = get_best_model_for_dyad(dyad_idx, filename=filename)
    #     print(best_model_name)
    #     comp_frac = get_comp_frac(load_trial_descriptions(dyad_idx, lap=0))
    #     print(comp_frac)
    #     best_models.append((best_model_name, all_model_vals[best_model_name]))
    #     best_predictions = all_model_vals[best_model_name]['expected_vals'][dyad_idx]
    #     player_ids, record_numbers = pair_idx_to_ids(dyad_idx)
    #     print(player_ids)
    #     print()
    #     for lap, prediction in enumerate(split_for_laps(best_predictions, dyad_idx)):
    #         path = get_rec_num_path(player_ids[lap], record_numbers[lap], recordings_folder)
    #         np.save(f'{path}/prediction.npy', prediction)
