import numpy as np
import pandas as pd
import seaborn as sn

from _util import save_fig
from glm.plotting.entropy_corr import get_cut_tds, get_simple_entropy_ts
from trial_descriptor import unsorted_load_all_trial_descriptions, get_comp_frac


def get_invite_effects_df():
    tds = unsorted_load_all_trial_descriptions()
    unsorted_comp_fractions = [get_comp_frac(td) for td in tds]
    comp_frac_sorting = np.argsort(unsorted_comp_fractions)
    comp_fractions = [unsorted_comp_fractions[i] for i in comp_frac_sorting]

    tds = get_cut_tds()
    entropies_per_dyad = get_simple_entropy_ts(filename='glm/unsorted_performance.pkl')
    entropies_per_dyad = [entropies_per_dyad[i] for i in comp_frac_sorting]
    entropies_per_dyad_exc_comp = entropies_per_dyad[:-15]
    no_inv_entropies_per_dyad = get_simple_entropy_ts(filename='glm/unsorted_performance.pkl', ts='no_inv_entropy_ts')
    no_inv_entropies_per_dyad = [no_inv_entropies_per_dyad[i] for i in comp_frac_sorting]
    no_inv_entropies_per_dyad_exc_comp = no_inv_entropies_per_dyad[:-15]
    tds = [tds[i] for i in comp_frac_sorting][:-15]
    trial_classes = [td['trial_class'].to_numpy() for td in tds]

    entropies = list()
    no_inv_entropies = list()
    for dyad_trial_classes, ent, no_inv_ent in zip(trial_classes,
                                                   entropies_per_dyad_exc_comp,
                                                   no_inv_entropies_per_dyad_exc_comp):
        acc_coop0_inv_idx = dyad_trial_classes == 'accepted_coop0_invite'
        acc_coop1_inv_idx = dyad_trial_classes == 'accepted_coop1_invite'
        dec_coop0_inv_idx = dyad_trial_classes == 'declined_coop0_invite'
        dec_coop1_inv_idx = dyad_trial_classes == 'declined_coop1_invite'
        entropies.extend(ent[acc_coop0_inv_idx])
        entropies.extend(ent[acc_coop1_inv_idx])
        entropies.extend(ent[dec_coop0_inv_idx])
        entropies.extend(ent[dec_coop1_inv_idx])
        no_inv_entropies.extend(no_inv_ent[acc_coop0_inv_idx])
        no_inv_entropies.extend(no_inv_ent[acc_coop1_inv_idx])
        no_inv_entropies.extend(no_inv_ent[dec_coop0_inv_idx])
        no_inv_entropies.extend(no_inv_ent[dec_coop1_inv_idx])

    df = dict(entropy=list(), inv=list(), dummy=list())
    df['entropy'].extend(entropies)
    df['inv'].extend(['inv_included' for _ in entropies])
    df['dummy'].extend(['entropy' for _ in entropies])
    df['entropy'].extend(no_inv_entropies)
    df['inv'].extend(['inv_excluded' for _ in no_inv_entropies])
    df['dummy'].extend(['entropy' for _ in no_inv_entropies])
    return pd.DataFrame(df)


if __name__ == '__main__':
    df = get_invite_effects_df()
    sn.violinplot(df, x='dummy', y='entropy', hue='inv', split=True, gap=.1, inner='quart', cut=True)
    save_fig()


