import numpy as np
from matplotlib import pyplot as plt

from invite_autocorr import get_invite_after_single_binary
from reward_decomposition import scatter
from trial_descriptor import load_all_trial_descriptions, get_comp_frac

if __name__ == '__main__':
    tds = load_all_trial_descriptions()
    invite_after_single_binaries = [get_invite_after_single_binary(td) for td in tds]
    invite_after_single_probs = [np.mean(binary) for binary in invite_after_single_binaries if len(binary) > 10]
    comp_fractions = [get_comp_frac(td) for td, binary in zip(tds, invite_after_single_binaries) if len(binary) > 10]
    scatter(comp_fractions, invite_after_single_probs, plt.gca())


