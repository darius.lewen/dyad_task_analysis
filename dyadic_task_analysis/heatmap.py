import matplotlib
import numpy as np
import seaborn as sn
from matplotlib import pyplot as plt

from _util import save_fig, game_width_in_cm
from path_shortening_visualization import plot_heatmap as plot_heatmap_base, plot_targets
from simulations import get_expected_min_effort_heatmap

# vmin, vmax = 100, -100
# vmin, vmax = 5.07, 12.46
vmin, vmax = 6.5, 11
heat_imgs = list()


def get_seperator(pos0, pos1):
    diff = pos0 - pos1
    mid = pos0 - diff / 2
    ortho = np.array([-diff[1], diff[0]])
    ortho /= np.linalg.norm(ortho)
    return (mid + ortho * np.linspace(-2, 2, 100)[:, None]).T


def plot_heatmap(comp, coop0, coop1, ax, res=40):
    comp = np.array(comp)
    coop0 = np.array(coop0)
    coop1 = np.array(coop1)
    targets = np.vstack([comp, coop0, coop1])
    heatmap = get_expected_min_effort_heatmap(targets, res=res)
    heatmap *= game_width_in_cm
    global vmin, vmax
    # if vmin > heatmap.min(): # to read out values
    #     vmin = heatmap.min()
    # if vmax < heatmap.max():
    #     vmax = heatmap.max()
    heat_img = plot_heatmap_base(ax, heatmap, comp, coop0, coop1, n=res, vmin=vmin, vmax=vmax)
    heat_imgs.append(heat_img)
    # plt.colorbar(heat_img, ax=ax)


def plot_comp_heatmap(comp, coop0, coop1, ax, res=40):
    comp = np.array(comp)
    coop0 = np.array(coop0)
    coop1 = np.array(coop1)
    targets = np.vstack([comp, np.array([5, 5]), np.array([5, 5])])
    heatmap = get_expected_min_effort_heatmap(targets, res=res)
    heatmap *= game_width_in_cm
    heat_img = plot_heatmap_base(ax, heatmap, comp, coop0, coop1, n=res, vmin=heatmap.min(), vmax=heatmap.max())
    # plt.colorbar(heat_img, ax=ax)


def plot_scheme(ax):
    comp = np.array([.7, .3])
    # comp = np.array([.25, .25])
    # optimum = np.array([0, 1])
    targets = np.array([comp, *(2 * (np.array([5, 5]), ))])
    res = 40
    heatmap = get_expected_min_effort_heatmap(targets, res=res)
    heatmap *= game_width_in_cm
    optimum = np.argwhere(heatmap == heatmap.min())[0] / res

    optimum[1] = 1 - optimum[1]
    optimum = optimum.sum() / 2  # hotfix to cope low res
    optimum = np.array([optimum, 1 - optimum])

    print(optimum)  # todo
    plot_targets(comp=comp, coop0=[5, 5], coop1=[5, 5], ax=ax, n=1)
    other_placement = np.array([.6, .4])
    seperator0 = get_seperator(comp, other_placement)
    seperator1 = get_seperator(comp, optimum)
    ax.plot(seperator0[0], seperator0[1], c='k')
    ax.plot(seperator1[0], seperator1[1], c='gray')
    ax.scatter(other_placement[0], other_placement[1], c='k', s=2)
    ax.scatter(optimum[0], optimum[1], c='gray', s=2)
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)


if __name__ == '__main__':
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['text.latex.preamble'] = r'\usepackage{amsfonts}'
    width, ratio = 5.4, .58
    fig, axes = plt.subplots(2, 3, figsize=(width, width * ratio))
    axes = axes.flatten()

    plot_heatmap(comp=[.7, .3], coop0=[5, 5], coop1=[5, 5], ax=axes[0])
    plot_heatmap(comp=[.7, .3], coop0=[.8, .5], coop1=[.1, .2], ax=axes[1])
    plot_heatmap(comp=[.7, .3], coop0=[.8, .65], coop1=[.1, .2], ax=axes[2])
    plot_heatmap(comp=[.4, .45], coop0=[.1, .6], coop1=[.4, .1], ax=axes[3])
    plot_comp_heatmap(comp=[.4, .45], coop0=[.1, .6], coop1=[.4, .1], ax=axes[4])

    # plot_heatmap(comp=[.4, .45], coop0=[.1, .6], coop1=[.4, .1], ax=axes[4], w=1.)
    # plot_heatmap(comp=[.5, .5], coop0=[.1, .6], coop1=[.4, .1], ax=axes[4])
    # plot_heatmap(comp=[.7, .2], coop0=[5, 5], coop1=[5, 5], ax=axes[5], w=0.)
    # plot_heatmap(comp=[.7, .2], coop0=[5, 5], coop1=[5, 5], ax=axes[6])
    # plot_heatmap(comp=[.9, .1], coop0=[5, 5], coop1=[5, 5], ax=axes[5])

    # plot_targets(comp=[.7, .2], coop0=[5, 5], coop1=[5, 5], ax=axes[7], n=40)

    plot_scheme(axes[-1]) # todo

    for ax_label, ax in zip('ABCDEFGH', axes):
        ax.text(-0.00, 1.04, ax_label, transform=ax.transAxes, size=10)
        ax.set_box_aspect(1)
    axes[-2].text(.99, .875, '$\Phi=1$', transform=axes[-2].transAxes, size=10, ha='right')

    fig.subplots_adjust(hspace=.45, wspace=.19, left=.03, right=.97)

    cbar = fig.colorbar(heat_imgs[-1], ax=axes.ravel().tolist())
    cbar.set_label('$\\mathbb{E}[E_{\\mathrm{min}}| \\vec x, \\vec y, \\vec j^A, \\vec j^B, w]$ (cm)',
                   rotation=-90, size=10, labelpad=20)
    cbar.ax.invert_yaxis()

    save_fig()


