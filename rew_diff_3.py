import matplotlib
import seaborn as sn
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.gridspec import GridSpec
from scipy.stats import pearsonr

from _util import save_fig, game_width_in_cm
from inefficiencies_full import comp_frac_x_format
from rew_diff import get_coll_count, get_skill_differences, filter_low_x, plot_skill_diff_curve, get
from reward_decomposition import scatter, get_agent_rewards, plot_corr_coef, get_mean_in_cm
from reward_decomposition_2 import get_sim_results
from trial_descriptor import load_all_trial_descriptions, get_comp_frac

fontdict = dict(size=12)


def get_single_target_diff(tds):
    singlet_target_diff = list()
    for td in tds:
        coll_count = get_coll_count(td)
        p0_single = get(coll_count, 'single_p0')
        p1_single = get(coll_count, 'single_p1')
        if p0_single is None or p1_single is None:
            singlet_target_diff.append(0)
        else:
            singlet_target_diff.append(abs(p0_single - p1_single))
    return singlet_target_diff


def plot_skill_diff_curve(curve, skill_differences, ax, legend=True):
    quartiles = np.quantile(skill_differences, [.5, .75, 1])
    ax.plot(curve[0], curve[1], color='k', label='$S_\\Delta = 0$')
    labels = [
        '$Q_{0} < S_\\Delta < Q_{50}$',
        '$Q_{50} < S_\\Delta < Q_{75}$',
        '$Q_{75} < S_\\Delta < Q_{100}$',
    ]
    for label, quartile in zip(labels, quartiles):
        skill_diffs = quartile * curve[1] * curve[0]
        lower = curve[1] - skill_diffs / 2
        higher = curve[1] + skill_diffs / 2
        ax.fill_between(curve[0], lower, higher, color='k', alpha=.1, label=label, edgecolor=None)
    if legend:
        ax.legend(loc='upper left', bbox_to_anchor=(1.20, 1.10), frameon=False, fontsize=fontdict['size'])


def get_agent_rew_curves(fitted_agent_mean_curve, skill_diff):
    x_values = fitted_agent_mean_curve[0]
    rew_diff = skill_diff * x_values * 2 * fitted_agent_mean_curve[1]
    lower = fitted_agent_mean_curve[1] - rew_diff / 2
    higher = fitted_agent_mean_curve[1] + rew_diff / 2
    lower = np.array([x_values, lower])
    higher = np.array([x_values, higher])
    return lower, higher


def plot_skill_diff(fitted_agent_mean_curve, ax, skill_diff=.5, alpha=1.):
    skill_diff = np.round(skill_diff, 1)
    lower_rew_curve, higher_rew_curve = get_agent_rew_curves(fitted_agent_mean_curve, skill_diff=skill_diff)
    sn.lineplot(x=higher_rew_curve[0], y=higher_rew_curve[1], ax=ax, c='gray', alpha=alpha,
                label=f'Estimated higher agent\npayoff for $S_\Delta={skill_diff}$')
    sn.lineplot(x=lower_rew_curve[0], y=lower_rew_curve[1], ax=ax, c='gray', alpha=alpha, linestyle='--',
                label=f'Estimated lower agent\npayoff for $S_\Delta={skill_diff}$')


def estimate_agent_optima(skill_differences, fitted_agent_means):
    lower_skill_opti = list()
    higher_skill_opti = list()
    ls_payoff_loss = list()
    hs_payoff_loss = list()
    for i, skill_diff in enumerate(skill_differences):
        lower_rew_curve, higher_rew_curve = get_agent_rew_curves(fitted_agent_means, skill_diff=skill_diff)
        lower_skill_opti.append(lower_rew_curve[0][np.argmax(lower_rew_curve[1])])
        higher_skill_opti.append(higher_rew_curve[0][np.argmax(higher_rew_curve[1])])
        ls_payoff_loss.append(max(lower_rew_curve[1]) - lower_rew_curve[1][i])
        hs_payoff_loss.append(max(higher_rew_curve[1]) - higher_rew_curve[1][i])
    return lower_skill_opti, higher_skill_opti, ls_payoff_loss, hs_payoff_loss


def print_payoff_diff_pred_error(p_diff, comp_fracs, skill_diffs, n_esti):
    f_func = lambda xs, ys: np.array([x for x, y in zip(xs, ys) if y is not None])
    p_diff = f_func(p_diff, skill_diffs)
    comp_fracs = f_func(comp_fracs, skill_diffs)
    n_esti = f_func(n_esti[1], skill_diffs)
    skill_diffs = f_func(skill_diffs, skill_diffs)
    n_esti = n_esti / .035

    predictions = skill_diffs * comp_fracs * n_esti * .07
    errors = predictions - p_diff
    print('True payoff difference:')
    print(f'Mean: {np.mean(p_diff)}')
    print(f'Variance: {np.var(p_diff)}')
    print()
    print('Absolute error:')
    print(f'Mean: {np.mean(abs(errors))}')
    print(f'Variance: {np.var(abs(errors))}')
    print()
    print('True total payoff difference over all dyads:')
    print(sum(p_diff))
    print('Predicted total payoff difference over all dyads:')
    print(sum(predictions))
    print(np.mean(p_diff))
    return np.mean(abs(errors)), np.var(abs(errors))


if __name__ == '__main__':
    scalar = .8
    fig = plt.figure(figsize=(12 * scalar, 7 * scalar * 2))
    plt.subplots_adjust(top=.9, bottom=.1, right=.98)#, wspace=.7, hspace=.7)
    rows, cols = 4, 4
    grid_spec = GridSpec(rows, cols, width_ratios=[1, 1, 1, 1/10], height_ratios=[1, 1, 1, 1], wspace=1, hspace=1)
    axes = [fig.add_subplot(grid_spec[i, k]) for i in range(rows) for k in range(cols)]
    # for (i, k) in [(i, k) for i in range(2) for k in range(4)]:
    #     axes.append(fig.add_subplot(grid_spec[i, k]))
    trial_amount_axes = axes[4].twinx()   # todo this axis needs x2

    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)
    payoff_diff = high_rewards - low_rewards
    scatter(comp_fractions, payoff_diff, ax=axes[0])
    plot_corr_coef(comp_fractions, payoff_diff, axes[0], ha='left', plot_x=.02)

    single_target_diff = get_single_target_diff(tds)
    scatter(single_target_diff, payoff_diff, ax=axes[1])
    plot_corr_coef(single_target_diff, payoff_diff, ax=axes[1], ha='left', plot_x=.02)

    sim_results = get_sim_results()
    naive_0 = sim_results['naive_0'][1]
    mean_add_effort = get_mean_in_cm('distance_ol', tds).mean()
    mean_effort_reduction_by_ps = get_mean_in_cm('path_shortening', tds).mean()
    mean_speed = get_mean_in_cm('speed', tds).mean()
    estimated_agent_mean_curve = (naive_0 + mean_add_effort - mean_effort_reduction_by_ps) / mean_speed  # trial duration
    estimated_agent_mean_curve = .035 * 20 * 60 / (1 + estimated_agent_mean_curve)  # payoff - +1 due to 1sec coll time
    estimated_agent_mean_curve = np.array([sim_results['naive_0'][0], estimated_agent_mean_curve])
    # sn.lineplot(x=estimated_agent_mean_curve[0], y=estimated_agent_mean_curve[1], ax=axes[4], c='k')

    coll_counts = np.array([get_coll_count(td).to_numpy().sum() for td in tds])
    coll_counts = coll_counts / 1.5  # for the 1 and the half block
    scatter(x=comp_fractions, y=coll_counts * .035, ax=axes[4], alpha=.3)
    scatter(x=comp_fractions, y=coll_counts, ax=trial_amount_axes, alpha=.3)

    aic_for_degrees = list()  # https://vitalflux.com/aic-vs-bic-for-regression-models-formula-examples/
    n = len(comp_fractions)
    degrees_to_test = list(range(2, 10))
    for deg in degrees_to_test:
        _, residuals, _, _, _ = np.polyfit(comp_fractions, coll_counts, deg=deg, full=True)
        aic_for_degrees.append(n * np.log(residuals[0] / n) + 2 * deg)
    best_fitting_deg = degrees_to_test[np.argmin(aic_for_degrees)]

    p = np.polyfit(comp_fractions, coll_counts, deg=best_fitting_deg)
    fitted_agent_mean_curve = np.polyval(p, comp_fractions)
    fitted_agent_mean_curve = np.array([comp_fractions, fitted_agent_mean_curve * .035])
    sn.lineplot(x=fitted_agent_mean_curve[0], y=fitted_agent_mean_curve[1], ax=axes[4], c='k')
    sn.lineplot(x=fitted_agent_mean_curve[0], y=fitted_agent_mean_curve[1], ax=axes[5], c='k',
                label='Estimated agent mean\npayoff $R/2$ $(S_\Delta=0.0)$')

    skill_differences = get_skill_differences(tds)
    non_exclude_start = 0
    for i, sd in enumerate(skill_differences):
        if sd is not None:
            non_exclude_start = i
            break

    filtered_comp_fractions = comp_fractions[non_exclude_start:]
    filtered_skill_difference = skill_differences[non_exclude_start:]
    filtered_low_rewards = low_rewards[non_exclude_start:]
    filtered_high_rewards = high_rewards[non_exclude_start:]
    # filtered_comp_fractions, filtered_skill_difference = filter_low_x(comp_fractions, skill_differences)
    # filtered_low_rewards = np.array(filter_low_x(comp_fractions, low_rewards)[1])
    # filtered_high_rewards = np.array(filter_low_x(comp_fractions, high_rewards)[1])

    quantiles = list(np.quantile(filtered_skill_difference, [.5, .9]))
    plot_skill_diff(fitted_agent_mean_curve, ax=axes[5], skill_diff=quantiles[0])
    plot_skill_diff(fitted_agent_mean_curve, ax=axes[5], skill_diff=quantiles[1], alpha=.6)
    # axes[5].legend(loc='upper left', bbox_to_anchor=(1.20, 1.10), frameon=False, fontsize=fontdict['size'])
    handles, labels = axes[5].get_legend_handles_labels()
    legend_label_oder = [3, 1, 0, 2, 4]
    axes[5].legend([handles[idx] for idx in legend_label_oder], [labels[idx] for idx in legend_label_oder],
                   loc='upper left', bbox_to_anchor=(1.2, 1.1), frameon=False, fontsize=fontdict['size'])

    scatter(filtered_comp_fractions, filtered_skill_difference, ax=axes[2])
    plot_corr_coef(filtered_comp_fractions, filtered_skill_difference, axes[2])

    axes[2].fill_between([-1, 0.05], -1, 1, color='gray', alpha=.3, edgecolor=None, label='not enough data')
    axes[2].legend(loc='upper left', bbox_to_anchor=(.12, 1.33), frameon=False, fontsize=fontdict['size'])

    # skill_differences = [sd for sd in skill_differences if sd is not None]
    sn.boxplot(y=filtered_skill_difference, ax=axes[3], color='gray')
    # plot_skill_diff_curve(fitted_agent_mean_curve, skill_differences, ax=axes[5])
    # plot_skill_diff_curve(estimated_agent_mean_curve, skill_differences, ax=axes[4], legend=False)

    # sn.lineplot(x=[0, 1], y=[0, 0], color='k', ax=axes[0])
    single_target_diff = np.array(single_target_diff)
    x_space = np.linspace(0, 1, 100)
    sorted_comp_fractions = sorted(comp_fractions)
    sorted_s_t_diff = single_target_diff[np.argsort(comp_fractions)]
    x_space_s_t_diff = np.interp(x_space, sorted_comp_fractions, sorted_s_t_diff)
    x_space_fitted_agent_mean = np.interp(x_space, estimated_agent_mean_curve[0], estimated_agent_mean_curve[1])
    # x_space = comp_fractions
    # for quantile in quantiles:
    #     edge = quantile * x_space * 2 * x_space_fitted_agent_mean# * x_space_s_t_diff
    #     axes[0].fill_between(x_space, np.zeros_like(x_space), edge, color='k', alpha=.1, edgecolor=None)

    # filtered_fit_a_m_curve = np.array(list(filter_low_x(fitted_agent_mean_curve[0], fitted_agent_mean_curve[1])))
    filtered_fit_a_m_curve = np.array([fitted_agent_mean_curve[0][non_exclude_start:],
                                       fitted_agent_mean_curve[1][non_exclude_start:]])

    lower_skilled_opti, higher_skilled_opti, ls_payoff_loss, hs_payoff_loss = estimate_agent_optima(filtered_skill_difference,
                                                                                                    filtered_fit_a_m_curve)
    scatter(x=filtered_comp_fractions, y=higher_skilled_opti, ax=axes[8])
    scatter(x=filtered_comp_fractions, y=lower_skilled_opti, ax=axes[9])
    scatter(x=filtered_comp_fractions, y=hs_payoff_loss, ax=axes[12])
    scatter(x=filtered_comp_fractions, y=ls_payoff_loss, ax=axes[13])

    for skill_diff, alpha in zip(quantiles, [1., .6]):
        skill_diff = np.round(skill_diff, 1)
        lower_rew_curve, higher_rew_curve = get_agent_rew_curves(fitted_agent_mean_curve, skill_diff)
        sn.lineplot(x=higher_rew_curve[0], y=max(higher_rew_curve[1]) - higher_rew_curve[1], ax=axes[12], c='gray', alpha=alpha)
        sn.lineplot(x=lower_rew_curve[0], y=max(lower_rew_curve[1]) - lower_rew_curve[1], ax=axes[13], c='gray', alpha=alpha, linestyle='--')
    sn.lineplot(x=fitted_agent_mean_curve[0], y=max(fitted_agent_mean_curve[1]) - fitted_agent_mean_curve[1], ax=axes[12], c='k')
    sn.lineplot(x=fitted_agent_mean_curve[0], y=max(fitted_agent_mean_curve[1]) - fitted_agent_mean_curve[1], ax=axes[13], c='k')

    ylabels = ['Agent payoff\ndifference $R_\Delta$ (€)',
               'Normalized single target\n difference $S_\Delta$',
               'Agent mean\npayoff $R/2$ (€)',
               'Estimated payoff (€)',
               'Estimated $\Phi$-optimum for\nthe higher skilled agent',
               'Estimated $\Phi$-optimum for\nthe lower skilled agent',
               'Estimated payoff loss\nof the higher skilled agent\ndue to non-optimal $\Phi$ (€)',
               'Estimated payoff loss\nof the lower skilled agent\ndue to non-optimal $\Phi$ (€)',
               ]

    trial_amount_axes.set_ylabel('Number of target collections $N$', fontdict=fontdict, rotation=270)
    trial_amount_axes.yaxis.labelpad = 17# + 10

    for key, ylabel, i in zip('ACDEFGHI', ylabels, [0, 2, 4, 5, 8, 9, 12, 13]):
        axes[i].text(-0.1, 1.1, key, transform=axes[i].transAxes, size=fontdict['size'])
        axes[i].set_xlabel('Fraction of single\ntargets collected $\\Phi$', fontdict=fontdict)
        axes[i].set_ylabel(ylabel, fontdict=fontdict)
        axes[i].set_box_aspect(1)
        comp_frac_x_format(axes[i])

    comp_frac_x_format(axes[8], also_y=True)
    comp_frac_x_format(axes[9], also_y=True)

    axes[1].set_box_aspect(1)
    axes[2].set_ylim([-0.04, .7])
    axes[3].set_ylim([-0.04, .7])
    axes[2].set_xlim([-0.04, 1.04])
    axes[8].set_xlim([-0.04, 1.04])
    axes[9].set_xlim([-0.04, 1.04])
    axes[8].set_ylim([-0.04, 1.04])
    axes[9].set_ylim([-0.04, 1.04])
    axes[12].set_ylim([0., 7.])
    axes[13].set_ylim([0., 7.])
    axes[1].text(-0.1, 1.1, 'B', transform=axes[1].transAxes, size=fontdict['size'])
    axes[1].set_xlabel('Single target\ndifference', fontdict=fontdict)
    axes[1].set_ylabel('Agent payoff\ndifference $R_\Delta$ (€)', fontdict=fontdict)
    axes[3].axis('off')
    axes[6].axis('off')
    axes[7].axis('off')
    axes[10].axis('off')
    axes[11].axis('off')
    axes[14].axis('off')
    axes[15].axis('off')

    mean_error, var_error = print_payoff_diff_pred_error(payoff_diff, comp_fractions,
                                                         skill_differences, fitted_agent_mean_curve)

    mean_error, var_error = "%.2f" % mean_error, "%.2f" % var_error
    axes[-2].text(0, 0, f'$R_\\Delta = S_\\Delta\\Phi N r_s + e$\n$r_s=0.07€$\nMean($|e|$)$\\approx{mean_error}€$\nVar($|e|$)$\\approx{var_error}€',
                  transform=axes[-2].transAxes, size=fontdict['size'])
    save_fig()
