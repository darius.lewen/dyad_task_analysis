import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

from _util import save_fig, game_width_in_cm
from pred_performance import violin_swarm_mean_plot, generate_fst_bar
from presentation.plotting import violin_swarm
from trial_descriptor_2 import load_all_trial_descriptions, get_comp_frac

# def is_nan_or_none(x):
#     isnan = x == np.nan
#     isnone = x == None
#     return isnan | isnone


if __name__ == '__main__':
    tds = load_all_trial_descriptions()
    tds_without_nan = [td.iloc[~np.isnan(td['from_free_to_other'].to_numpy(dtype=float))] for td in tds]
    enough_data = np.array([len(td) > 50 for td in tds_without_nan])
    to_plot = {key: list() for key in ['from_free_to_other', 'from_free_to_coop', 'from_free_to_fst_1_opti', 'from_free_to_opti']}
    for td in tds_without_nan:
        for key in to_plot.keys():
            to_plot[key].append(np.mean(td[key]) * game_width_in_cm)
    to_plot = pd.DataFrame(to_plot)
    size_scalar = 3
    fig, axes = plt.subplots(1, 2, figsize=(3.6 * size_scalar, 1 * size_scalar))
    axes = axes.flatten()
    plt.subplots_adjust(left=.08, right=.95, bottom=.2)
    violin_swarm(data=to_plot, ax=axes[0])

    generate_fst_bar(axes[1])
    axes[0].set_xticklabels(['Collecting\nagent', 'Closet joint\ntarget', 'FST=1 adv.\nplacement\nposition', 'Adv.\nplacement\nposition'])
    axes[0].set_xlabel('')
    axes[0].set_ylabel('Mean distance\nfrom free agent (cm)')
    save_fig(sub_folder='supplementary/', format='pdf')
    save_fig(sub_folder='supplementary/', format='svg')
