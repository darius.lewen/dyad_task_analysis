import numpy as np

from glm.util import add_properties, to_hot_encoded


@add_properties(param_amount=3, name='bias')
def bias(glm_kwargs, w):
    return np.ones_like(glm_kwargs['prev_1_collections'])[:, None] * w


@add_properties(param_amount=2)  # todo here I need all the six distances.
def effort_linearity(glm_kwargs, w):
    effort = glm_kwargs['efforts']
    return np.array([effort[:, 0] * w[0], effort[:, 1] * w[1], effort[:, 2] * w[1]]).T


@add_properties(param_amount=9)
def prev_t_linearity(glm_kwargs, w, depth=1):
    prev_collections = to_hot_encoded(glm_kwargs[f'prev_{depth}_collections'])
    return prev_collections @ w.reshape((3, 3))


@add_properties(param_amount=9)
def invite_linearity(glm_kwargs, w):  # todo here I need the info which agent invites
    no_invite_w, coop0_inv_w, coop1_inv_w = w[:3], w[3:6], w[6:9]
    # coop0_w = invite_w
    # coop1_w = invite_w[[0, 2, 1]]
    w = np.vstack([no_invite_w, coop0_inv_w, coop1_inv_w])
    invites = to_hot_encoded(glm_kwargs['invites'])
    return invites @ w
