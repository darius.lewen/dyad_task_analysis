<<<<<<< README.md

Supported package und python versions
--------
| Package      | Version |
|--------------|---------|
| Python       | 3.10    |
| matplotlib   | 3.8.3   |
| msgpack      | 1.0.7   |
| numpy        | 1.26.4  |
| pandas       | 2.2.1   |  <!-- maybe change to 1.4.0? -->
| pyglet       | 1.5.27  |
| pyftdi       | 0.55.0  |
| pyzmq        | 25.1.2  |
| scikit-learn | 1.4.0   |  <!-- maybe change to 1.2.2? -->
| scipy        | 1.12.0  | 
| tables       | 3.9.2   |
| cmasher      | 1.6.3   |
| colorcet     | 3.0.1   |
| euclid       | 1.2     |
| numba        | 0.56.4  |
| tqdm         | 4.65.0  |


<!-- One needs to adjust the recordings folder path.... -->
<!-- Currently debug trials from Anna are included in loading_helper.py ... -->
