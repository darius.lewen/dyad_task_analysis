import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from _util import fontdict
from inefficiencies_full import comp_frac_x_format
from presentation.plotting import scatterplot
from reward_decomposition import get_agent_rewards, scatter, plot_corr_coef, get_payoffs
from trial_descriptor import load_all_trial_descriptions, get_comp_frac


def get_different_mov_subset(td):
    return td[td['trial_class'].isin(['anti_corr'])]


def get_straight_mov_subset(td):
    return td[td['trial_class'].isin(['p0_ahead', 'p1_ahead', 'concurrent'])]


def get_physical_ahead_labels(td, subset_func=None):
    if subset_func is None:
        subset_func = get_straight_mov_subset
    td = subset_func(td)
    if len(td) == 0:
        return list()
    ahead_diffs = td['p0_ahead_p'].to_numpy() - td['p1_ahead_p'].to_numpy()
    out = np.array(['no_one' for _ in range(len(td))])
    out[ahead_diffs > .33] = 'p0'
    out[ahead_diffs < -.33] = 'p1'
    return out


def get_leading_ahead_labels(td, subset_func=None):
    if subset_func is None:
        subset_func = get_straight_mov_subset
    td = subset_func(td)
    if len(td) == 0:
        return list()
    out = np.array(['no_one' for _ in range(len(td))])
    out[td['ahead_measure_3'] > 12] = 'p0'
    out[td['ahead_measure_3'] < -12] = 'p1'
    return out


def get_ahead_diff(label_vec):
    p0_ahead_count = (label_vec == 'p0').astype(int).sum()
    p1_ahead_count = (label_vec == 'p1').astype(int).sum()
    return p0_ahead_count - p1_ahead_count


def get_trial_type_ahead_diff(td, get_ahead_func):
    td = td[td['coll_type'].isin(['single_p0', 'single_p1'])]
    if len(td) == 0:
        return 0
    label_seq = get_ahead_func(td)
    if len(label_seq) == 0:
        return 0
    return get_ahead_diff(label_seq)


def get_different_t_single_t_diff(td):
    td = td[td['coll_type'].isin(['single_p0', 'single_p1'])]
    if len(td) == 0:
        return 0
    td = td[td['trial_class'].isin(['anti_corr'])]
    if len(td) == 0:
        return 0
    p0_st_colls = (td['coll_type'] == 'single_p0').astype(int).sum()
    p1_st_colls = (td['coll_type'] == 'single_p1').astype(int).sum()
    return p0_st_colls - p1_st_colls


if __name__ == '__main__':
    rows, cols = 1, 4
    fig, axes = plt.subplots(rows, cols)
    axes = axes.flatten()
    tds = load_all_trial_descriptions()
    # fst filter
    # tds = [td for td in tds if .0 < get_comp_frac(td) < .7]
    comp_fractions = np.array([get_comp_frac(td) for td in tds])

    # todo: decompose difference in single targets -> one fraction due to one ahead, one fraction due to different target

    rew_diffs = [get_payoffs(td) for td in tds]
    rew_diffs = [p0 - p1 for p0, p1 in rew_diffs]
    leading_ahead_labels = [get_leading_ahead_labels(td, subset_func=get_straight_mov_subset) for td in tds]
    leading_ahead_diff = [get_ahead_diff(label_seq) for label_seq in leading_ahead_labels]
    leading_only_s_ahead_diff = [get_trial_type_ahead_diff(td, get_ahead_func=get_leading_ahead_labels) for td in tds]
    different_t_single_t_diffs = [get_different_t_single_t_diff(td) for td in tds]

    # scatter(comp_fractions, leading_ahead_diff, axes[0])
    # scatter(rew_diffs, leading_ahead_diff, axes[1])
    # scatter(rew_diffs, leading_only_s_ahead_diff, axes[2])
    # scatter(rew_diffs, different_t_single_t_diffs, axes[3])
    to_plot = pd.DataFrame(dict(comp_fractions=comp_fractions, rew_diffs=rew_diffs,
                                leading_ahead_diff=leading_ahead_diff,
                                leading_only_s_ahead_diff=leading_only_s_ahead_diff,
                                different_t_single_t_diffs=different_t_single_t_diffs))
    scatterplot(to_plot, x='comp_fractions', y='leading_ahead_diff', ax=axes[0])
    scatterplot(to_plot, x='rew_diffs', y='leading_ahead_diff', ax=axes[1])
    scatterplot(to_plot, x='rew_diffs', y='leading_only_s_ahead_diff', ax=axes[2])
    scatterplot(to_plot, x='rew_diffs', y='different_t_single_t_diffs', ax=axes[3])
    plot_corr_coef(comp_fractions, leading_ahead_diff, axes[0])
    plot_corr_coef(rew_diffs, leading_ahead_diff, axes[1])
    plot_corr_coef(rew_diffs, leading_only_s_ahead_diff, axes[2])
    plot_corr_coef(rew_diffs, different_t_single_t_diffs, axes[3])

    ylabels = [
        'Difference in \nleading in terms of\ninitiating the movement',
        'Difference in \nleading in terms of\ninitiating the movement',
        'Difference in \nleading in terms of\ninitiating the movement\n(only single targets)',
        '',
    ]

    for key, ylabel, ax in zip('ABCD', ylabels, axes):
        ax.text(-0.1, 1.1, key, transform=ax.transAxes, size=fontdict['size'])
        ax.set_ylabel(ylabel, fontdict=fontdict)
        ax.set_box_aspect(1)

    axes[0].set_xlabel('Fraction of single\ntargets collected $\\Phi$', fontdict=fontdict)
    comp_frac_x_format(axes[0])

    for ax in axes[1:]:
        ax.set_xlabel('Payoff difference [€]', fontdict=fontdict)

    plt.subplots_adjust(wspace=.85)
