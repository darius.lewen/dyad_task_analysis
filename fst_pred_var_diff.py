import matplotlib.pyplot as plt
import numpy as np

from _util import save_fig
from conv_visualized import load_frame_count_corrected_tds, get_fst_ma_series, get_effort_prediction_mas
from inefficiencies_full import comp_frac_x_format
from rew_diff_3 import fontdict
from trial_descriptor import load_all_trial_descriptions, get_comp_frac
from reward_decomposition import scatter, plot_corr_coef

if __name__ == '__main__':

    tds = load_frame_count_corrected_tds()
    comp_fractions = [get_comp_frac(td) for td in load_all_trial_descriptions()]

    window = 30 * 120

    effort_prediction_ma = get_effort_prediction_mas(tds, window)
    best_prediction_ma = get_effort_prediction_mas(tds, window, best_models=True)
    true_fst_ma = [get_fst_ma_series(td, window) for td in tds]

    effort_vars = np.array([vals.var() for vals in effort_prediction_ma])
    best_vars = np.array([vals.var() for vals in best_prediction_ma])
    true_vars = np.array([vals.var() for vals in true_fst_ma])

    fig, axes = plt.subplots(2, 3, figsize=(6, 3))
    axes = axes.flatten()

    y_values = [effort_vars, best_vars, true_vars]

    ylabels = [
        '',
        '',
        '',
        '',
        '',
        '',
    ]

    for key, ylabel, ax, y in zip('ABCDEF', ylabels, axes, y_values):
        scatter(comp_fractions, y, ax=ax)
        ax.text(-0.1, 1.1, key, transform=ax.transAxes, size=fontdict['size'])
        ax.set_xlabel('Fraction of single\ntargets collected $\\Phi$', fontdict=fontdict)
        ax.set_ylabel(ylabel, fontdict=fontdict)
        ax.set_box_aspect(1)
        comp_frac_x_format(ax)

    # scatter(effort_vars, best_vars, ax=axes[3])
    scatter(true_vars, effort_vars, ax=axes[4])
    plot_corr_coef(true_vars, effort_vars, ax=axes[4])
    scatter(true_vars, best_vars, ax=axes[5])
    plot_corr_coef(true_vars, best_vars, ax=axes[5])

    save_fig()

