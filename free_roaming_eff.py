import numpy as np
from matplotlib import pyplot as plt

from _util import fontdict, save_fig, game_width_in_cm
from inefficiencies_full import comp_frac_x_format
from reward_decomposition import scatter
from trial_descriptor import load_all_trial_descriptions, get_comp_frac

if __name__ == '__main__':
    fig, axes = plt.subplots(2, 1)

    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])

    only_single_tds = [td[td['coll_type'].str.contains('single')] for td in tds]
    cm_actu_effort_reduction = np.array([td['actu_effort_reduction'].mean() for td in only_single_tds])
    actu_effort_reduction = np.array([td['actu_effort_reduction'].sum() for td in only_single_tds])
    opti_effort_reduction = np.array([td['opti_effort_reduction'].sum() for td in only_single_tds])
    rel_effort_reduction = actu_effort_reduction / opti_effort_reduction
    scatter(x=comp_fractions, y=cm_actu_effort_reduction * game_width_in_cm, ax=axes[0])
    scatter(x=comp_fractions, y=rel_effort_reduction, ax=axes[1])

    ylabels = [r'Optimal free-roaming placement',
               r'Optimal free-roaming placement given $w_{dyad}$']

    for key, ylabel, ax in zip('ABCD', ylabels, axes.flatten()):
        ax.text(-0.1, 1.1, key, transform=ax.transAxes, size=16, weight='bold')
        ax.set_xlabel('Fraction of\nsingle targets collected $\\Phi$', fontdict=fontdict)
        comp_frac_x_format(ax)
        ax.set_ylabel(ylabel, fontdict=fontdict)

    save_fig()

    # todo del this old stuff
# if __name__ == '__main__':
#     fig, axes = plt.subplots(2, 1)
#
#     tds = load_all_trial_descriptions()
#     comp_fractions = np.array([get_comp_frac(td) for td in tds])
#
#     only_single_tds = [td[td['coll_type'].str.contains('single')] for td in tds]
#     w05_p_efficiency = np.array([td['p_efficiency_w05'].mean() for td in only_single_tds])
#     w_dyad_p_efficiency = np.array([td['p_efficiency_w_dyad'].mean() for td in only_single_tds])
#
#     scatter(x=comp_fractions, y=w05_p_efficiency, ax=axes[0])
#     scatter(x=comp_fractions, y=w_dyad_p_efficiency, ax=axes[1])
#
#     ylabels = [r'Optimal free-roaming placement',
#                r'Optimal free-roaming placement given $w_{dyad}$']
#
#     for key, ylabel, ax in zip('ABCD', ylabels, axes.flatten()):
#         ax.text(-0.1, 1.1, key, transform=ax.transAxes, size=16, weight='bold')
#         ax.set_xlabel('Fraction of\nsingle targets collected $\\Phi$', fontdict=fontdict)
#         comp_frac_x_format(ax)
#         ax.set_ylabel(ylabel, fontdict=fontdict)
#
#     save_fig()
