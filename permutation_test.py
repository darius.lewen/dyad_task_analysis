import numpy as np
from matplotlib import pyplot as plt
from numba import jit
from tqdm import tqdm

from trial_descriptor import get_comp_frac, unsorted_load_all_trial_descriptions


def get_true_p():
    comp_fractions = np.array([get_comp_frac(td) for td in unsorted_load_all_trial_descriptions()])
    is_competitive = (comp_fractions > .9).astype(int)
    return is_competitive.mean()


@jit
def _get_mean_diffs(dyad_amount, toy_p, n=500):
    mean_diffs = list()
    for _ in range(500):
        true_p_sim = np.random.binomial(dyad_amount, true_p, size=n)
        toy_p_sim = np.random.binomial(dyad_amount, toy_p, size=n)
        bucket = np.concatenate((true_p_sim, toy_p_sim))
        random_indices = np.arange(len(bucket))
        np.random.shuffle(random_indices)
        first_bucket_half = bucket[random_indices[:len(bucket) // 2]]
        second_bucket_half = bucket[random_indices[len(bucket) // 2:]]
        mean_diffs.append(first_bucket_half.mean() - second_bucket_half.mean())
    return np.array(mean_diffs)


@jit
def get_null_hypo_rejection_count(dyad_amount, toy_p):
    null_hypo_rejection_count = 0
    for _ in range(100):
        true_mean = true_p * dyad_amount
        toy_mean = toy_p * dyad_amount
        actual_mean_diff = toy_mean - true_mean
        mean_diffs = _get_mean_diffs(dyad_amount, toy_p)
        over_boarder_percentage = (mean_diffs > actual_mean_diff).astype(np.int64).mean()
        enough_dyads = .05 > over_boarder_percentage
        if enough_dyads:
            null_hypo_rejection_count += 1
    return null_hypo_rejection_count


def get_required_dyad_amount(toy_p):
    for dyad_amount in range(1, 100, 2):
        null_hypo_rejection_count = get_null_hypo_rejection_count(dyad_amount, toy_p)
        if null_hypo_rejection_count > 80:
            return dyad_amount
    return np.nan


true_p = get_true_p()


if __name__ == '__main__':
    percentual_increase = np.linspace(0., .5, 6)[1:]
    toy_p_s = true_p * (1 + percentual_increase)
    req_dyad_amounts = list()
    for toy_p in tqdm(toy_p_s):
        req_dyad_amounts.append(get_required_dyad_amount(toy_p))
    plt.plot(percentual_increase, req_dyad_amounts)


    #  todo idea: this is not a true permutation test! make it a true one actualy using permutations
    #  todo why should make this a difference? -> we simulate our experiment and then do a permutation test
    #  todo if we have 'such' test results how likely is it that they come from a different distribution

