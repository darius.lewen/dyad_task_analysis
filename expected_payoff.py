import numpy as np
import pandas as pd
from matplotlib import pyplot as plt, ticker

from _util import save_fig, game_width_in_cm
from reward_decomposition import get_agent_rewards, get_mean_in_cm, plot_strategies
from simulations import load_results
from trial_classes import plot_aic_poly
from trial_descriptor import load_all_trial_descriptions, get_comp_frac

if __name__ == '__main__':
    rows, cols = 2, 2
    scalar = 1.3
    fig, axes = plt.subplots(2, 2, figsize=(1.8 * 2 * scalar, 1.8 * 1.6 * scalar))
    plt.subplots_adjust(bottom=.18, left=.18, top=.95, right=.95, wspace=1.2, hspace=1)
    axes = axes.flatten()

    comp_frac_locator = ticker.FixedLocator([0, 1/3, 2/3, 1])
    comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1'])

    for ax_label, ax in zip('ABCD', axes):
        ax.set_box_aspect(1)
        ax.set_xlabel('Fraction of\nsingle targets')
        ax.set_ylabel('Expected\npayoff (€)')
        ax.xaxis.set_major_locator(comp_frac_locator)
        ax.xaxis.set_major_formatter(comp_frac_formatter)
        ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes)
        ax.set_ylim([14, 29])

    tds = load_all_trial_descriptions()
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)
    y_reward_min, y_reward_max = min(agent_mean_reward), max(agent_mean_reward)
    trial_durations = np.array([td['duration'].to_numpy().mean() for td in tds])

    comp_fractions = [get_comp_frac(td) for td in tds]
    distance = get_mean_in_cm('distance', tds)
    speed = get_mean_in_cm('speed', tds)
    additional_effort = get_mean_in_cm('distance_ol', tds)
    effort_from_past_target = get_mean_in_cm('chosen_effort', tds) + get_mean_in_cm('path_shortening', tds)
    effort_reduction_by_ps = get_mean_in_cm('path_shortening', tds)

    xspace, speed_curve = plot_aic_poly(comp_fractions, speed)
    _, additional_effort_curve = plot_aic_poly(comp_fractions, additional_effort)
    _, effort_from_past_target_curve = plot_aic_poly(comp_fractions, effort_from_past_target)
    _, effort_reduction_by_ps_curve = plot_aic_poly(comp_fractions, effort_reduction_by_ps)
    distance_curve = effort_from_past_target_curve + additional_effort_curve - effort_reduction_by_ps_curve

    def duration_to_rew(dur):
        return .035 * 20 * 60 / (dur + 1)

    full_reward_curve = duration_to_rew(distance_curve / speed_curve)

    axes[1].plot(xspace, full_reward_curve, c='k')

    distance_curve = effort_from_past_target_curve + additional_effort.mean() - effort_reduction_by_ps_curve
    only_dist_reward_curve = duration_to_rew(distance_curve / speed.mean())

    axes[0].plot(xspace, only_dist_reward_curve, c='k')

    sim_results = load_results()
    for key in sim_results.keys():
        sim_results[key][1] *= game_width_in_cm

    same_pos_reward_curve = duration_to_rew((sim_results['naive_0'][1] + additional_effort.mean()) / speed.mean())
    opti_coop_reward_curve = duration_to_rew((sim_results['path_shortening'][1] + additional_effort.mean()) / speed.mean())

    axes[2].plot(sim_results['naive_0'][0], same_pos_reward_curve, c='k')
    axes[2].plot(sim_results['path_shortening'][0], opti_coop_reward_curve, c='k')

    save_fig()

