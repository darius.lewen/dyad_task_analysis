import numpy as np
import matplotlib as mpl
import pandas as pd

from inefficiencies_full import comp_frac_x_format
from presentation.plotting import scatterplot
from rew_diff_2 import get_single_target_diff

mpl.use('Agg')

from matplotlib import pyplot as plt, ticker

from _util import save_fig
from reward_decomposition import get_mean_in_cm, get_agent_rewards
from reward_decomposition_2 import get_optimal_from_past_target_curve, get_optimal_effort_reduction
from trial_classes import plot_aic_poly
from trial_descriptor import get_comp_frac, load_all_trial_descriptions


xspace = np.linspace(0, 1)

if __name__ == '__main__':
    scalar = 1.3
    fig, axes = plt.subplots(1, 2, figsize=(1.8 * 2 * scalar, 1.8 * 1.6 * scalar))
    plt.subplots_adjust(bottom=.6, left=.18, top=.98, right=.95, wspace=1.2)

    for ax in axes:
        ax.set_box_aspect(1)
    axes[0].set_ylim([14, 29])

    axes[1].set_xlabel('Single target\ndifference')
    axes[1].set_ylabel('Payoff\ndifference (€)')
    axes[0].set_xlabel('Fraction of\nsingle targets $\\Phi$')
    axes[0].set_ylabel('Payoff (€)')
    comp_frac_x_format(axes[0])
    axes[1].xaxis.set_major_locator(ticker.FixedLocator([0, 100, 200]))
    axes[1].yaxis.set_major_locator(ticker.FixedLocator([0, 7, 14]))
    axes[1].set_ylim([-10 * .07, 210 * .07])
    axes[1].set_xlim([-10, 210])
    # axes[1].xaxis.set_major_formatter(ticker.FixedFormatter(['0', '15', '30']))

    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)
    single_target_diff = get_single_target_diff(tds)
    # relative_single_target_diff = np.array(single_target_diff) / (comp_fractions * np.array([len(td) for td in tds]))
    to_plot = pd.DataFrame(dict(
        payoff_diff=high_rewards - low_rewards,
        single_target_diff=single_target_diff,
        comp_fractions=comp_fractions,
    ))
    scatterplot(to_plot, x='single_target_diff', y='payoff_diff', ax=axes[1])
    # axes[1].plot([0, 200], [0, 200 * 0.07])

    trial_durations = np.array([td['duration'].to_numpy().mean() for td in tds])
    distance = get_mean_in_cm('distance', tds)
    speed = get_mean_in_cm('speed', tds)
    additional_effort = get_mean_in_cm('distance_ol', tds)
    effort_from_past_target = get_mean_in_cm('chosen_effort', tds) + get_mean_in_cm('path_shortening', tds)
    effort_reduction_by_ps = get_mean_in_cm('path_shortening', tds)

    xspace, speed_curve = plot_aic_poly(comp_fractions, speed)
    _, additional_effort_curve = plot_aic_poly(comp_fractions, additional_effort)
    _, effort_from_past_target_curve = plot_aic_poly(comp_fractions, effort_from_past_target)
    _, effort_reduction_by_ps_curve = plot_aic_poly(comp_fractions, effort_reduction_by_ps)

    optimal_from_past_target_curve = get_optimal_from_past_target_curve()
    optimal_effort_reduction = get_optimal_effort_reduction()

    artificial_opti_distance_curve = optimal_from_past_target_curve + additional_effort.mean() - optimal_effort_reduction
    artificial_opti_duration_curve = artificial_opti_distance_curve / speed.mean()
    artificial_comp_distance_curve = optimal_from_past_target_curve + additional_effort.mean() - effort_reduction_by_ps_curve  # take 0 for epsilon strategy - however in the other cases we do also take the mean, here we use the fit instead because with the mean we would have illegal values for low fst values.
    artificial_comp_duration_curve = artificial_comp_distance_curve / speed.mean()

    opti_distance_curve = effort_from_past_target_curve + additional_effort_curve - optimal_effort_reduction
    opti_duration_curve = opti_distance_curve / speed_curve
    comp_distance_curve = effort_from_past_target_curve + additional_effort_curve - effort_reduction_by_ps_curve
    comp_duration_curve = comp_distance_curve / speed_curve

    def duration_to_rew(dur):
        return .035 * 20 * 60 / (dur + 1)

    artificial_comp_reward_curve = duration_to_rew(artificial_comp_duration_curve)
    artificial_opti_reward_curve = duration_to_rew(artificial_opti_duration_curve)
    # axes[0].plot(xspace, artificial_opti_reward_curve)
    # axes[0].plot(xspace, artificial_comp_reward_curve)

    comp_reward_curve = duration_to_rew(comp_duration_curve)
    opti_reward_curve = duration_to_rew(opti_duration_curve)
    # axes[0].plot(xspace, opti_reward_curve)  # create version with mean coop speed
    axes[0].plot(xspace, comp_reward_curve, label='Both payoffs for 0% difference in single targets',
                 c='k')

    def plot_skill_diff_curves(agent_rew_mean_curve, ax, skill_diff=.1, c='k', alpha_base=.3, linewidth_base=2):
        rew_diff = skill_diff * xspace * 2 * agent_rew_mean_curve
        lower = agent_rew_mean_curve - rew_diff / 2
        higher = agent_rew_mean_curve + rew_diff / 2
        ax.plot(xspace, lower, label=f'Lower payoff for {int(100 * skill_diff)}% difference in single targets', c=c)
        ax.plot(xspace, higher, label=f'Higher payoff for {int(100 * skill_diff)}% difference in single targets', c=c)

    legend_bbox_to_anchor = (-.35, -1.35)
    axes[0].legend()
    axes[0].legend(loc='center left', bbox_to_anchor=legend_bbox_to_anchor, frameon=False)
    save_fig(name='skill_diff_0')
    plot_skill_diff_curves(comp_reward_curve, axes[0], c=np.ones(3) * .5)
    handles, labels = axes[0].get_legend_handles_labels()
    legend_label_oder = [2, 0, 1]
    axes[0].legend([handles[idx] for idx in legend_label_oder], [labels[idx] for idx in legend_label_oder],
                   loc='center left', bbox_to_anchor=legend_bbox_to_anchor, frameon=False)
    save_fig(name='skill_diff_1')

    plot_skill_diff_curves(comp_reward_curve, axes[0], skill_diff=.3, c=np.ones(3) * .7)
    handles, labels = axes[0].get_legend_handles_labels()
    legend_label_oder = [4, 2, 0, 1, 3]
    axes[0].legend([handles[idx] for idx in legend_label_oder], [labels[idx] for idx in legend_label_oder],
                   loc='center left', bbox_to_anchor=legend_bbox_to_anchor, frameon=False)
    save_fig(name='skill_diff_2')

# import numpy as np   Todo create bug report with this
# import matplotlib as mpl
# mpl.use('Agg')
#
# from matplotlib import pyplot as plt
#
# from _util import save_fig
# from reward_decomposition import get_mean_in_cm
# from reward_decomposition_2 import get_optimal_from_past_target_curve, get_optimal_effort_reduction
# from trial_classes import plot_aic_poly
# from trial_descriptor import get_comp_frac, load_all_trial_descriptions
#
# if __name__ == '__main__':
#     fig, axes = plt.subplots(1, 2)
#     time_axes = list()
#     for ax in axes:
#         time_axes.append(ax.twinx())
#         time_axes[-1].invert_yaxis()
#
#     tds = load_all_trial_descriptions()
#     trial_durations = np.array([td['duration'].to_numpy().mean() for td in tds])
#     comp_fractions = [get_comp_frac(td) for td in tds]
#     distance = get_mean_in_cm('distance', tds)
#     speed = get_mean_in_cm('speed', tds)
#     additional_effort = get_mean_in_cm('distance_ol', tds)
#     effort_from_past_target = get_mean_in_cm('chosen_effort', tds) + get_mean_in_cm('path_shortening', tds)
#     effort_reduction_by_ps = get_mean_in_cm('path_shortening', tds)
#
#     xspace, speed_curve = plot_aic_poly(comp_fractions, speed)
#     _, additional_effort_curve = plot_aic_poly(comp_fractions, additional_effort)
#     _, effort_from_past_target_curve = plot_aic_poly(comp_fractions, effort_from_past_target)
#     _, effort_reduction_by_ps_curve = plot_aic_poly(comp_fractions, effort_reduction_by_ps)
#
#     optimal_from_past_target_curve = get_optimal_from_past_target_curve()
#     optimal_effort_reduction = get_optimal_effort_reduction()
#
#     artificial_opti_distance_curve = optimal_from_past_target_curve + additional_effort.min() - optimal_effort_reduction
#     artificial_opti_duration_curve = artificial_opti_distance_curve / speed.max()
#     artificial_comp_distance_curve = optimal_from_past_target_curve + additional_effort.min() - 0
#     artificial_comp_duration_curve = artificial_comp_distance_curve / speed.max()
#
#     time_axes[0].plot(xspace, artificial_opti_duration_curve)
#     time_axes[0].plot(xspace, artificial_comp_duration_curve)
#
#     opti_distance_curve = effort_from_past_target_curve + additional_effort_curve - optimal_effort_reduction
#     opti_duration_curve = opti_distance_curve / speed_curve
#     comp_distance_curve = effort_from_past_target_curve + additional_effort_curve - effort_reduction_by_ps_curve
#     comp_duration_curve = comp_distance_curve / speed_curve
#
#     def duration_to_rew(dur):
#         return .035 * 20 * 60 / dur / 2
#
#     # time_axes1_ylim = np.array([1.4, .6])
#     # time_axes[1].set_ylim(time_axes1_ylim)
#     # axes[1].set_ylim(duration_to_rew(time_axes1_ylim))
#     # correction_term_min = -.068
#     # correction_term_max = -.02
#
#     comp_reward_curve = duration_to_rew(comp_duration_curve)
#     axes[1].plot(xspace, comp_reward_curve)
#
#     # def get_agent_rew_curves(fitted_agent_mean_curve, skill_diff): todo
#     #     x_values = fitted_agent_mean_curve[0]
#     #     rew_diff = skill_diff * x_values * 2 * fitted_agent_mean_curve[1]
#     #     lower = fitted_agent_mean_curve[1] - rew_diff / 2
#     #     higher = fitted_agent_mean_curve[1] + rew_diff / 2
#     #     lower = np.array([x_values, lower])
#     #     higher = np.array([x_values, higher])
#     #     return lower, higher
#
#     # time_axes[1].plot(xspace, opti_duration_curve)
#     time_axes[1].plot(xspace, comp_duration_curve)
#
#     save_fig()
#
