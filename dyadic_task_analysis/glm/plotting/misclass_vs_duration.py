import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn

from glm.plotting.entropy_vs_duration import remove_head, plot_correlation
from glm.plotting.performance_plot import get_best_model_for_dyad
from trial_descriptor import load_all_trial_descriptions, get_comp_frac


def get_best_model_missclass(dyad_amount=58):
    all_model_vals = pd.read_pickle('../performance.pkl')
    best_model_missclass = list()
    for dyad_idx in range(dyad_amount):
        best_model = get_best_model_for_dyad(dyad_idx)
        expected_vals = all_model_vals[best_model]['expected_vals'][dyad_idx]
        next_collection = all_model_vals[best_model]['next_collection'][dyad_idx]
        expected_prob = expected_vals.max(axis=1)
        true_prob = expected_vals[np.arange(expected_vals.shape[0]), next_collection.astype(int)]
        expected_true_diff = expected_prob - true_prob
        best_model_missclass.append(expected_true_diff)
    return best_model_missclass



if __name__ == '__main__':  # todo do this again but with a missprediction meassure
    tds = [remove_head(td) for td in load_all_trial_descriptions()]
    comp_fractions = [get_comp_frac(td) for td in tds]
    trial_durations = [td['duration'].to_numpy() for td in tds]
    best_model_missclass = get_best_model_missclass()
    trial_durations = [durations[missclass_measure != 0]
                       for durations, missclass_measure in zip(trial_durations, best_model_missclass)]
    best_model_missclass = [missclass_measure[missclass_measure != 0]
                            for missclass_measure in best_model_missclass]

    fig, axes = plt.subplots(10, 6, figsize=(7, 12), sharex='all', sharey='all')
    axes = axes.flatten()
    for dyad_idx, (missclass_measure, trial_duration, ax) in enumerate(zip(best_model_missclass, trial_durations, axes)):
        if len(missclass_measure) < 10:
            continue
        ax.text(0.0, 1.1, f'Dyad {dyad_idx}', transform=ax.transAxes, size=10)
        sn.kdeplot(x=missclass_measure, y=trial_duration, ax=ax, fill=True)
        plot_correlation(x=missclass_measure, y=trial_duration, ax=ax)
        ax.set_xlim(0, np.log2(3))
        ax.set_ylim(0, 3.2)
        ax.set_box_aspect(1)
    for ax in axes[-2:]:
        ax.axis('off')
    #
    # plt.subplots_adjust(hspace=.8, wspace=.4, left=.08, right=.92, top=.92, bottom=.08)
    # script_name = os.path.basename(sys.argv[0])
    # plt.savefig(f'../plots/{script_name[:-3]}.png', dpi=300)


# for dyad_idx, ax in enumerate(axes):



# fig, axes = plt.subplots(8, 8)
# axes = axes.flatten()
# for dyad_idx, ax in enumerate(axes):
#     sn.scatterplot(x=best_entropy_ts[dyad_idx], y=trial_durations[dyad_idx], ax=ax)
# del best_entropy_ts[4]
# del trial_durations[4]
# fig, axes = plt.subplots(1, 4)
# axes = axes.flatten()
# dyad_idx = 2
# sn.scatterplot(x=best_entropy_ts[dyad_idx], y=trial_durations[dyad_idx], ax=axes[0])
# sn.kdeplot(x=best_entropy_ts[dyad_idx], y=trial_durations[dyad_idx], ax=axes[1], fill=True)
# plot_correlation(x=best_entropy_ts[dyad_idx], y=trial_durations[dyad_idx], ax=axes[1])
# all_entropy_ts = np.concatenate(best_entropy_ts)
# all_trail_durations = np.concatenate(trial_durations)
# sn.kdeplot(x=all_entropy_ts, y=all_trail_durations, ax=axes[2], fill=True)
# plot_correlation(x=all_entropy_ts, y=all_trail_durations, ax=axes[2])
# corr_coefficients = list()
# for dyad_idx in range(58 - 1):
#     corr_coefficient, p_value = pearsonr(x=best_entropy_ts[dyad_idx], y=trial_durations[dyad_idx])
#     corr_coefficients.append(corr_coefficient)
#     if p_value > .05:
#         print(dyad_idx, p_value)
# sn.histplot(x=corr_coefficients, ax=axes[3])

# fig, axes = plt.subplots(8, 8)
# axes = axes.flatten()
# for dyad_idx, ax in enumerate(axes):
#     sn.scatterplot(x=best_entropy_ts[dyad_idx], y=trial_durations[dyad_idx], ax=ax)
