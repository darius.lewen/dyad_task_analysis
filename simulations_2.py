import cmasher
import matplotlib
import numpy as np
import seaborn as sn
from matplotlib import pyplot as plt, ticker

from inefficiencies_full import fontdict
from _util import game_width_in_cm, save_fig
from simulations import load_results, get_weightings
from trial_descriptor import load_all_trial_descriptions, get_comp_frac

reward_per_target = 0.07
block_time = 20 * 60

strategy_colors = ['gray', 'gray', 'gray', 'k']
strategy_alphas = [1 / 3, 2 / 3, 1, 1]

strategy_name_convert = dict(
    naive_30='70\% weighted effort decisions, agents always sharing the same position',
    naive_10='90\% weighted effort decisions, agents always sharing the same position',
    naive_0='100\% weighted effort decisions, agents always sharing the same position',
    path_shortening='100\% weighted effort decisions, optimal free-roaming agent placement',
)


def y_ax_scaling(y_values, ax, margin_percentage=.2):
    y_min, y_max = min(y_values), max(y_values)
    margin = (y_max - y_min) * margin_percentage
    ax.set_ylim(y_min - margin, y_max + margin)


def scatter(x, y, ax):
    sn.scatterplot(x=x, y=y, hue=list(range(len(x))), ax=ax, palette=cmasher.gem, alpha=.6)


def vertical_lines(x, y_min, y_max, ax):
    for _x, _y_min, _y_max in zip(x, y_min, y_max):
        sn.lineplot(x=[_x, _x], y=[_y_min, _y_max], ax=ax, estimator=None, alpha=.2, color='k')


def get_payoffs(td, block_amount=2):
    coll_count = td[['coll_type']].apply(lambda x: x.value_counts()).transpose()

    def get(df, key):
        return 0 if key not in df.keys() else df[key][0]
    p0_payoff = get(coll_count, 'single_p0') * .07 + get(coll_count, 'joint0') * .05 + get(coll_count, 'joint1') * .02
    p1_payoff = get(coll_count, 'single_p1') * .07 + get(coll_count, 'joint1') * .05 + get(coll_count, 'joint0') * .02
    p0_payoff /= block_amount
    p1_payoff /= block_amount
    return p0_payoff, p1_payoff


def get_payoff_diff(td):
    p0_payoff, p1_payoff = get_payoffs(td)
    return abs(p0_payoff - p1_payoff)


def get_modified_comp_fractions(tds):
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    full_comp_amount = sum(1 if val > .95 else 0 for val in comp_fractions)
    comp_fractions[-full_comp_amount:] += np.arange(full_comp_amount) * .05
    return comp_fractions


def get_agent_rewards(tds):
    agent_mean_reward = np.array([np.mean(get_payoffs(td)) for td in tds])
    reward_diff = np.array([get_payoff_diff(td) for td in tds])
    low_rewards = agent_mean_reward - reward_diff / 2
    high_rewards = agent_mean_reward + reward_diff / 2
    return agent_mean_reward, low_rewards, high_rewards


def plot_trial_durations(ax):
    trial_durations = np.array([(td['end'].to_numpy() - td['start'].to_numpy()).mean() for td in tds])
    trial_durations = trial_durations / 120
    ax.scatter(comp_fractions, trial_durations)


def plot_strategies(ax):
    sim_results = load_results()
    for key in sim_results.keys():
        sim_results[key][1] *= game_width_in_cm

    for name, color, alpha in zip(strategy_name_convert.keys(), strategy_colors, strategy_alphas):
        sn.lineplot(x=sim_results[name][0], y=sim_results[name][1], ax=ax, color=color, alpha=alpha)

    def horizontal_strategy_line(strategy, color):
        sn.lineplot(x=[1, comp_fractions[-1]], y=[strategy[1, -1], strategy[1, -1]], ax=ax, color=color)

    horizontal_strategy_line(strategy=sim_results['naive_0'], color='gray')
    horizontal_strategy_line(strategy=sim_results['path_shortening'], color='k')


def get_mean_in_cm(measure, tds):
    return np.array([td[measure].mean() for td in tds]) * game_width_in_cm


if __name__ == '__main__':
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['text.latex.preamble'] = r'\usepackage{amsfonts}'
    ax_width = 7
    ax_height = ax_width * 450/730
    rows, cols = 1, 2
    fig, axes = plt.subplots(rows, cols, figsize=(ax_width, ax_height))
    axes = axes.flatten()

    tds = load_all_trial_descriptions()
    comp_fractions = get_modified_comp_fractions(tds)
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)
    y_reward_min, y_reward_max = min(low_rewards) - 3, max(high_rewards) + 3
    path_shortening = get_mean_in_cm('path_shortening', tds)
    chosen_effort = get_mean_in_cm('chosen_effort', tds)

    scatter(x=comp_fractions, y=chosen_effort, ax=axes[1])
    vertical_lines(x=comp_fractions, y_min=chosen_effort, y_max=(chosen_effort + path_shortening), ax=axes[1])
    plot_strategies(axes[1])

    axes[1].set_ylim(12, 26)
    # axes[1].set_ylabel(r'Mean initial\newline effort to target $\langle E \rangle$ (cm)', fontdict=fontdict)
    axes[1].set_ylabel(r'Expected minimal effort\newline to target $\mathbb{E}[E_\mathrm{min}|w]$ (cm)', fontdict=fontdict)
    axes[0].set_ylabel(r'Target weighting\newline parameter $w$', fontdict=fontdict)

    comp_frac_locator = ticker.FixedLocator([0, 1/3, 2/3, 1, max(comp_fractions)])
    comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1', '1'])

    results = load_results()
    for name, c, a in zip(strategy_name_convert.keys(), strategy_colors, strategy_alphas):
        sn.lineplot(x=results[name][0], y=get_weightings(weight_amount=len(results[name][0])), ax=axes[0],
                    label=strategy_name_convert[name], color=c, alpha=a)

    for ax_label, ax in zip('AB', axes):
        ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes, size=14, weight='bold')
        ax.set_xlabel('Fraction of single targets collected $\\Phi$', fontdict=fontdict)
        ax.xaxis.set_major_locator(comp_frac_locator)
        ax.xaxis.set_major_formatter(comp_frac_formatter)
        ax.xaxis.set_tick_params(labelsize=14)
        ax.yaxis.set_tick_params(labelsize=14)

    axes[0].set_aspect(1)
    axes[1].vlines(1, ymin=0, ymax=100, color='k', linewidth=1)
    axes[1].get_legend().remove()

    axes[0].legend(loc='upper left', bbox_to_anchor=(-.55, -.4), frameon=False, fontsize=fontdict['size'])
    plt.subplots_adjust(left=.05, bottom=.5, right=.95, top=.9, wspace=.2, hspace=.2)

    save_fig()
