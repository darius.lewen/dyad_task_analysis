import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sn
from matplotlib import ticker

from matplotlib.colors import ListedColormap

from _util import save_fig
from glm.plotting.class_comparison import fontdict
from glm.plotting.type_hist import get_hist_plot_frame
from inefficiencies_full import comp_frac_x_format


def gen_color_dict(to_plot):
    cmap = plt.get_cmap('coolwarm')
    # cmap = plt.get_cmap('seismic')
    norm_max = 0
    c_dict = dict()
    for val in to_plot['entropy_correlation']:
        if not isinstance(val, str) and abs(val) > norm_max:
            norm_max = abs(val)
    min_color, max_color = 1, 0
    for val in to_plot['entropy_correlation']:
        if val == 'Constant entropy':
            c_dict[val] = (.001, .001, .001, 1)
            c_dict[.6] = (.001, .001, .001, 1)
        elif val == 'No correlation':
            c_dict[val] = (.2, .2, .2, 1)
            c_dict[.5] = (.2, .2, .2, 1)
        else:
            color_val = 0.5 + val / norm_max / 2
            if color_val > max_color:
                max_color = color_val
            if color_val < min_color:
                min_color = color_val
            c_dict[val] = cmap(color_val)
    # new_cmap = ListedColormap(cmap(0.5 + np.linspace(-norm_max, norm_max, 100) / norm_max / 2))
    print(min_color, max_color)
    new_cmap = ListedColormap(cmap(np.linspace(min_color, max_color, 100)))
    return c_dict, new_cmap


def get_min_max(to_plot):
    values = [val for val in to_plot['entropy_correlation'] if not isinstance(val, str)]
    return min(values), max(values)


if __name__ == '__main__':
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.style.use('default')

    to_plot = get_hist_plot_frame(filename='/home/brain/work/dyadic_task_analysis/glm/unsorted_performance.pkl')
    fig, axes = plt.subplots(2, 2, figsize=(5, 5))
    axes = axes.flatten()
    for ax in axes[[1, 2]]:
        ax.axis('off')
    plt.subplots_adjust(left=.15, bottom=.15, right=.9, top=.9, wspace=.6, hspace=.7)
    for ax in axes:
        ax.set_box_aspect(1)
    axes[0].text(-0.06, 1.04, 'C', transform=axes[0].transAxes, size=10)
    axes[3].text(-0.06, 1.04, 'D', transform=axes[3].transAxes, size=10)
    axes[0].set_xlim(0, 1)
    axes[0].set_xlabel('Fraction of\nsingle targets collected $\\Phi$', fontdict=fontdict)
    axes[0].set_ylabel('Number of dyads', fontdict=fontdict)
    comp_frac_x_format(axes[0])
    palette, newcmap = gen_color_dict(to_plot)
    sn.histplot(data=to_plot, x='comp_frac', hue='entropy_correlation', multiple='stack',
                palette=palette, bins=10, ax=axes[0], legend=False)
    entropy_corr = to_plot['entropy_correlation'].to_numpy()

    neg_entropies, pos_entropies = list(), list()
    for val in entropy_corr:
        if not isinstance(val, str):
            if val < 0:
                neg_entropies.append(val)
            else:
                pos_entropies.append(val)
    print(np.mean(neg_entropies))
    print(np.var(neg_entropies))
    print(np.mean(pos_entropies))
    print(np.var(pos_entropies))

    entropy_corr[[val == 'Constant entropy' for val in entropy_corr]] = .5
    entropy_corr[[val == 'No correlation' for val in entropy_corr]] = .6
    sn.histplot(y=entropy_corr, hue=entropy_corr, bins=10, ax=axes[3], palette=palette, legend=False, multiple='stack')

    locator = ticker.FixedLocator([0, .2, .4, .495, .565])
    formatter = ticker.FixedFormatter(['0.0', '0.2', '0.4', 'No correlation', 'Constant entropy'])
    axes[3].yaxis.set_major_locator(locator)
    axes[3].yaxis.set_major_formatter(formatter)
    axes[3].set_ylabel('person r', fontdict=fontdict)
    axes[3].set_xlabel('Number of dyads', fontdict=fontdict)
    save_fig()

