import cmasher
import matplotlib
import pandas as pd
import seaborn as sn
import numpy as np
from matplotlib import pyplot as plt, ticker

from _util import save_fig, game_width_in_cm
from cmap_generator import get_cmap
from reward_decomposition import get_modified_comp_fractions, get_agent_rewards, get_mean_in_cm, scatter, \
    plot_corr_coef, y_ax_scaling, reward_per_target, block_time, plot_strategies
from simulations import load_results
from trial_classes import plot_aic_poly
from trial_descriptor import load_all_trial_descriptions, get_comp_frac

fontdict = dict(size=12)
xspace = np.linspace(0, 1)


def get_sim_results():
    sim_results = load_results()
    for key in sim_results.keys():
        sim_results[key][1] *= game_width_in_cm
    return sim_results


def get_strat_diff():
    sim_results = get_sim_results()
    naive_0 = sim_results['naive_0']
    path_shortening = sim_results['path_shortening']
    x_space = np.linspace(0, 1, 100)
    naive_0 = np.interp(x_space, naive_0[0], naive_0[1])
    path_shortening = np.interp(x_space, path_shortening[0], path_shortening[1])
    strat_diff = naive_0 - path_shortening
    return x_space, strat_diff


def scatter(data, x, y, ax, style=None, markers=None, legend=False, hue=None):
    cmap = get_cmap()
    # sn.scatterplot(data=data, x=x, y=y, hue=list(range(len(data))), ax=ax, palette=cmasher.gem, alpha=.6, legend=legend,
    #                style=style,
    #                markers=markers)
    if hue is None:
        hue = x
    sn.scatterplot(data=data, x=x, y=y, hue=hue, ax=ax, palette=cmap, alpha=1, legend=legend,
                   style=style,  # todo correct hue parameter
                   markers=markers, linewidth=.3, edgecolor='k')


def get_optimal_effort_reduction():
    sim_results = get_sim_results()
    path_shortening = np.interp(xspace, sim_results['path_shortening'][0], sim_results['path_shortening'][1])
    naive_0 = np.interp(xspace, sim_results['naive_0'][0], sim_results['naive_0'][1])
    return naive_0 - path_shortening


def get_optimal_from_past_target_curve():
    sim_results = get_sim_results()
    naive_0 = np.interp(xspace, sim_results['naive_0'][0], sim_results['naive_0'][1])
    return naive_0


if __name__ == '__main__':
    # matplotlib.rcParams['text.usetex'] = True
    # matplotlib.rcParams['text.latex.preamble'] = r'\usepackage{cmbright, amsfonts}'
    # matplotlib.rcParams['font.sans-serif'] = 'Arial'
    # matplotlib.rcParams['font.family'] = 'sans-serif'
    rows, cols = 2, 3
    # ax_width = 4
    # ax_height = ax_width * .67
    # figsize = (ax_width * cols, ax_height * rows)
    fig, axes = plt.subplots(rows, cols, figsize=(10.4, 9))
    plt.subplots_adjust(left=.092, right=.987, top=.99, bottom=.4, wspace=1.9, hspace=1.2)
    axes = axes.flatten()
    trial_duration_axes = axes[0].twinx()

    tds = load_all_trial_descriptions()
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)
    # y_reward_min, y_reward_max = min(low_rewards) + 2.8, max(high_rewards) - 2.8
    # y_reward_min, y_reward_max = min(low_rewards), max(high_rewards)
    y_reward_min, y_reward_max = min(agent_mean_reward), max(agent_mean_reward)

    # t_desc['distance_ol'] + t_desc['effort_ol'] + t_desc['effort_due_to_non_opti_p'] + t_desc['effort_best_placement']


    # effort_due_to_non_opti_p = get_mean_in_cm('effort_due_to_non_opti_p', tds)
    # effort_best_placement = get_mean_in_cm('effort_best_placement', tds)
    # path_shortening = get_mean_in_cm('path_shortening', tds)
    # speed = get_mean_in_cm('speed', tds)
    # chosen_effort = get_mean_in_cm('chosen_effort', tds)
    # distance_ol = get_mean_in_cm('distance_ol', tds)
    # distance = get_mean_in_cm('distance', tds)

    trial_durations = np.array([td['duration'].to_numpy().mean() for td in tds])

    comp_fractions = [get_comp_frac(td) for td in tds]
    distance = get_mean_in_cm('distance', tds)
    speed = get_mean_in_cm('speed', tds)
    additional_effort = get_mean_in_cm('distance_ol', tds)
    effort_from_past_target = get_mean_in_cm('chosen_effort', tds) + get_mean_in_cm('path_shortening', tds)
    effort_reduction_by_ps = get_mean_in_cm('path_shortening', tds)

    dyad_types = ['Weighted distance minimizer',
                  'Joint target turn taking',
                  'Only single target with near optimal free-roaming placement',
                  'Only single target']

    markers = dict(zip(dyad_types, ['o', 'd', 'P', 'X']))
    style = ['Weighted distance minimizer' for _ in range(len(comp_fractions))]
    # style[30] = 1
    # style[40] = 1
    # style[55] = 1
    style[2] = 'Joint target turn taking'
    style[4] = 'Joint target turn taking'
    style[7] = 'Joint target turn taking'

    style[53] = 'Only single target with near optimal free-roaming placement'

    style[57] = 'Only single target'
    style[54] = 'Only single target'
    style[51] = 'Only single target'
    style[52] = 'Only single target'
    style[50] = 'Only single target'
    style[56] = 'Only single target'
    style[55] = 'Only single target'
    style[48] = 'Only single target'
    style[49] = 'Only single target'

    to_plot = pd.DataFrame(dict(
        comp_fractions=comp_fractions,
        agent_mean_reward=agent_mean_reward,
        distance=distance,
        speed=speed,
        additional_effort=additional_effort,
        effort_from_past_target=effort_from_past_target,
        effort_reduction_by_ps=effort_reduction_by_ps,
        style=style,
    ))

    scatter(data=to_plot, x='comp_fractions', y='agent_mean_reward', ax=axes[0], style='style', markers=markers, legend=True)
    scatter(data=to_plot, x='comp_fractions', y='distance', ax=axes[1], style='style', markers=markers)
    scatter(data=to_plot, x='comp_fractions', y='speed', ax=axes[2], style='style', markers=markers)
    scatter(data=to_plot, x='comp_fractions', y='additional_effort', ax=axes[3], style='style', markers=markers)
    scatter(data=to_plot, x='comp_fractions', y='effort_from_past_target', ax=axes[4], style='style', markers=markers)
    scatter(data=to_plot, x='comp_fractions', y='effort_reduction_by_ps', ax=axes[5], style='style', markers=markers)
    axes[0].legend(loc='upper left', bbox_to_anchor=(-.55, -3.4), frameon=False, fontsize=fontdict['size'])

    # coefficients = list()
    # residuals = list()
    # for degree in range(1, 8):
    #     coef, resi, _, _, _ = np.polyfit(comp_fractions, speed, deg=degree, full=True)
    #     coefficients.append(coef)
    #     residuals.append(resi)
    # best_coefficients = coefficients[np.argmin(residuals)]
    # curve_x = np.linspace(0, 1, 100)
    # curve_y = np.polyval(best_coefficients, curve_x)
    # axes[2].plot(curve_x, curve_y)

    plot_fits = True
    if plot_fits:
        xspace, speed_curve = plot_aic_poly(comp_fractions, speed, axes[2])
        _, additional_effort_curve = plot_aic_poly(comp_fractions, additional_effort, axes[3])
        _, effort_from_past_target_curve = plot_aic_poly(comp_fractions, effort_from_past_target, axes[4])
        _, effort_reduction_by_ps_curve = plot_aic_poly(comp_fractions, effort_reduction_by_ps, axes[5])
        distance_curve = effort_from_past_target_curve + additional_effort_curve - effort_reduction_by_ps_curve
        # optimal_effort_reduction = np.interp(xspace, sim_results['path_shortening'][0], sim_results['path_shortening'][1])
        duration_curve = distance_curve / speed_curve
        axes[1].plot(xspace, distance_curve)
        trial_duration_axes.plot(xspace, duration_curve)

        optimal_effort_reduction = get_optimal_effort_reduction()
        opti_p_distance_curve = effort_from_past_target_curve + additional_effort_curve - optimal_effort_reduction
        opti_p_duration_curve = opti_p_distance_curve / speed_curve
        trial_duration_axes.plot(xspace, opti_p_duration_curve)

        optimal_from_past_target_curve = get_optimal_from_past_target_curve()
        artificial_distance_curve = optimal_from_past_target_curve + additional_effort_curve - optimal_effort_reduction
        artificial_duration_curve = artificial_distance_curve / speed_curve
        trial_duration_axes.plot(xspace, artificial_duration_curve)

        optimal_from_past_target_curve = get_optimal_from_past_target_curve()
        artificial_distance_curve = optimal_from_past_target_curve + additional_effort_curve.mean() - optimal_effort_reduction
        artificial_duration_curve = artificial_distance_curve / speed_curve.mean()
        trial_duration_axes.plot(xspace, artificial_duration_curve)

        optimal_from_past_target_curve = get_optimal_from_past_target_curve()
        artificial_distance_curve = optimal_from_past_target_curve + additional_effort.mean() - optimal_effort_reduction
        artificial_duration_curve = artificial_distance_curve / speed.mean()
        trial_duration_axes.plot(xspace, artificial_duration_curve)

        optimal_from_past_target_curve = get_optimal_from_past_target_curve()
        artificial_distance_curve = optimal_from_past_target_curve + additional_effort.min() - optimal_effort_reduction
        artificial_duration_curve = artificial_distance_curve / speed.max()
        trial_duration_axes.plot(xspace, artificial_duration_curve)


    # plot_aic_poly(comp_fractions, agent_mean_reward, axes[0])

    # scatter(data=to_plot, x='comp_fractions', y='additional_effort', ax=axes[3], style='style', markers=markers)
    # scatter(data=to_plot, x='comp_fractions', y='effort_from_past_target', ax=axes[4], style='style', markers=markers)
    # scatter(data=to_plot, x='comp_fractions', y='effort_reduction_by_ps', ax=axes[5], style='style', markers=markers)
    # naive_strat = get_sim_results()['naive_0']
    # axes[4].legend(fontsize=fontdict['size'], loc='upper left', bbox_toWhat constitues the reward_anchor=(0, -10))

    plot_strategies(axes[4], skip_ps=True)
    axes[4].legend(loc='upper left', bbox_to_anchor=(-.55, -.8), frameon=False, fontsize=fontdict['size'])

    x_strat_diff, y_strat_diff = get_strat_diff()
    sn.lineplot(x=x_strat_diff, y=y_strat_diff, ax=axes[5], color='k', label='Maximal distance reduction possible')

    axes[5].legend(loc='upper left', bbox_to_anchor=(-3.45, -1.475), frameon=False, fontsize=fontdict['size'])
    # scatter(x=comp_fractions, y=effort_best_placement, ax=axes[6])
    plot_corr_coef(comp_fractions, speed, axes[2], plot_y=.03)
    plot_corr_coef(comp_fractions, additional_effort, axes[3])
    plot_corr_coef(comp_fractions, effort_reduction_by_ps, axes[5], plot_x=.02, ha='left')
    # plot_corr_coef(org_comp_fractions, distance_ol, axes[3])

    # y_ax_scaling(speed, ax=axes[1])
    # axes[2].set_ylim(12, 26)
    # y_ax_scaling(distance_ol, ax=axes[3])

    axes[0].set_ylabel('Agent mean\n payoff $R/2$ (€)', fontdict=fontdict)
    trial_duration_axes.set_ylabel('Mean trial\n duration (s)', fontdict=fontdict, rotation=270)
    axes[1].set_ylabel('Mean trajectory\n distance (cm)', fontdict=fontdict)
    axes[2].set_ylabel('Mean speed (cm/s)', fontdict=fontdict)
    axes[3].set_ylabel('Mean distance due to\n not moving in a straight\n line (cm)', fontdict=fontdict)
    axes[4].set_ylabel('Mean distance from \n previous to next target (cm)', fontdict=fontdict)
    axes[5].set_ylabel('Mean distance reduction\n due to free-roaming\n agent placement (cm)', fontdict=fontdict)

    correction_term_min = -.068
    correction_term_max = -.02
    y_reward_min += -.5
    y_reward_max += 3
    trial_duration_axes.set_ylim(reward_per_target * block_time / (y_reward_max * 2) - 1 + correction_term_min,
                                 reward_per_target * block_time / (y_reward_min * 2) - 1 + correction_term_max)
    axes[0].set_ylim(y_reward_min, y_reward_max)
    trial_duration_axes.yaxis.labelpad = 17 + 10
    trial_duration_axes.invert_yaxis()

    # scatter(x=comp_fractions, y=trial_durations, ax=trial_duration_axes)
    # trial_duration_axes.scatter(comp_fractions, trial_durations)

    comp_frac_locator = ticker.FixedLocator([0, 1/3, 2/3, 1])
    comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1'])

    for ax_label, ax in zip('ABCDEF', axes):
        ax.set_box_aspect(1)
        ax.set_xlabel('Fraction of single\n targets collected $\\Phi$', fontdict=fontdict)
        # ax.get_legend().remove()
        ax.xaxis.set_major_locator(comp_frac_locator)
        ax.xaxis.set_major_formatter(comp_frac_formatter)
        # ax.vlines(1, ymin=0, ymax=100, color='k', linewidth=1)
        ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes, size=fontdict['size']) #, weight='bold')
        ax.xaxis.set_tick_params(labelsize=fontdict['size'])
        ax.yaxis.set_tick_params(labelsize=fontdict['size'])
    trial_duration_axes.yaxis.set_tick_params(labelsize=fontdict['size'])

    # fig.tight_layout()
    save_fig()

