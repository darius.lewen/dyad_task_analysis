import matplotlib.pyplot as plt
import numpy as np
import scipy.stats
from matplotlib import ticker

from _util import save_fig
from conv_visualized import load_frame_count_corrected_tds, get_fst_ma_series, get_effort_prediction_mas, fontdict
from reward_decomposition import scatter, plot_corr_coef
from trial_descriptor import get_comp_frac, load_all_trial_descriptions


def get_means_stds(ma_series, fst=False):
    means, stds = list(), list()  # stds = standard deviations
    for dyad_idx in range(58):
        start = 0 if not fst else 10 * 60
        ma_s = ma_series[dyad_idx][start:]
        means.append(ma_s.mean())
        stds.append(ma_s.std())
    return means, stds


if __name__ == '__main__':

    tds = load_frame_count_corrected_tds()
    # tds = load_all_trial_descriptions()
    comp_fractions = [get_comp_frac(td) for td in load_all_trial_descriptions()]

    # window = 30 * 120
    # fst_ma = [get_fst_ma_series(td, window) for td in tds]
    # effort_prediction_ma = get_effort_prediction_mas(tds, window)
    # best_prediction_ma = get_effort_prediction_mas(tds, window, best_models=True)
    fst_ma = [get_fst_ma_series(td) for td in tds]
    effort_prediction_ma = get_effort_prediction_mas(tds)
    best_prediction_ma = get_effort_prediction_mas(tds, best_models=True)

    fst_means, fst_stds = get_means_stds(fst_ma, fst=True)
    effort_means, effort_stds = get_means_stds(effort_prediction_ma)
    best_means, best_stds = get_means_stds(best_prediction_ma)

    scalar = 1.28
    fig, axes = plt.subplots(5, 2, figsize=(5 * scalar, 2 * 5 * scalar))
    axes = axes.flatten()
    scatter(comp_fractions, fst_means, axes[0])
    scatter(comp_fractions, fst_stds, axes[1])
    scatter(comp_fractions, effort_means, axes[2])
    scatter(comp_fractions, effort_stds, axes[3])
    scatter(comp_fractions, best_means, axes[4])
    scatter(comp_fractions, best_stds, axes[5])
    scatter(fst_means, effort_means, axes[6])
    scatter(fst_stds, effort_stds, axes[7])
    scatter(fst_means, best_means, axes[8])
    scatter(fst_stds, best_stds, axes[9])

    # performing wilcoxon signed-rank test to prov that full model captures the variance
    res = scipy.stats.wilcoxon(effort_stds, best_stds)
    print('Comparing the distance GLM with the full GLM:')
    print('With all data:')
    print(f'Wilcoxon signed-rank test: {res.statistic}, p-value: {res.pvalue}')
    print('With 0.1 < FST < .9 data:')
    above_threshold = 0.1 < np.array(comp_fractions)
    below_threshold = 0.9 > np.array(comp_fractions)
    between = 2 == above_threshold.astype(int) + below_threshold.astype(int)
    res = scipy.stats.wilcoxon(np.array(effort_stds)[between], np.array(best_stds)[between])
    print(f'Wilcoxon signed-rank test: {res.statistic}, p-value: {res.pvalue}')

    print()
    print('Better perspective: Comparing the true data with the distance GLM than with the full GLM')

    print('All data:')
    print('Distance GLM:')
    res = scipy.stats.wilcoxon(fst_stds, effort_stds)
    print(f'Wilcoxon signed-rank test: {res.statistic}, p-value: {res.pvalue}')

    print('Full GLM:')
    res = scipy.stats.wilcoxon(fst_stds, best_stds)
    print(f'Wilcoxon signed-rank test: {res.statistic}, p-value: {res.pvalue}')

    print('With 0.1 < FST < .9 data:')
    print('Distance GLM:')
    res = scipy.stats.wilcoxon(np.array(fst_stds)[between], np.array(effort_stds)[between])
    print(f'Wilcoxon signed-rank test: {res.statistic}, p-value: {res.pvalue}')

    print('Full GLM:')
    res = scipy.stats.wilcoxon(np.array(fst_stds)[between], np.array(best_stds)[between])
    print(f'Wilcoxon signed-rank test: {res.statistic}, p-value: {res.pvalue}')


    plot_corr_coef(fst_stds, effort_stds, axes[7], plot_x=.02, ha='left')
    plot_corr_coef(fst_stds, best_stds, axes[9], plot_x=.02, ha='left')
    axes[7].plot([-1, 1], [-1, 1], color='k', linewidth=1)
    axes[9].plot([-1, 1], [-1, 1], color='k', linewidth=1)

    # y_labels = [
    #     'Means of true\n$\\Phi$ moving averages',
    #     'Standard deviations of true\n$\\Phi$ moving averages',
    #     'Means of\ndistance-predicted\n$\\Phi$ moving averages',
    #     'Standard deviations of\ndistance-predicted\n$\\Phi$ moving averages',
    #     'Means of\nbest-fit-predicted\n$\\Phi$ moving averages',
    #     'Standard deviations of\nbest-fit-predicted\n$\\Phi$ moving averages',
    #     'Means of\ndistance-predicted\n$\\Phi$ moving averages',
    #     'Standard deviations of\ndistance-predicted\n$\\Phi$ moving averages',
    #     'Means of\nbest-fit-predicted\n$\\Phi$ moving averages',
    #     'Standard deviations of\nbest-fit-predicted\n$\\Phi$ moving averages',
    # ]
    y_labels = [
        'Means of \n moving averages of\n true $\\Phi$',
        'Standard deviations of\n moving averages of \n true $\\Phi$',
        'Means of \n moving averages of\n distance-predicted $\\Phi$',
        'Standard deviations of\n moving averages of \n distance-predicted $\\Phi$',
        'Means of \n moving averages of\n best-fit-predicted $\\Phi$',
        'Standard deviations of\n moving averages of \n best-fit-predicted $\\Phi$',
        'Means of \n moving averages of\n distance-predicted $\\Phi$',
        'Standard deviations of\n moving averages of \n distance-predicted $\\Phi$',
        'Means of \n moving averages of\n best-fit-predicted $\\Phi$',
        'Standard deviations of\n moving averages of \n best-fit-predicted $\\Phi$',
    ]

    comp_frac_locator = ticker.FixedLocator([0, 1 / 3, 2 / 3, 1])
    comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1'])

    for ax_label, y_label, ax in zip('ABCDEFGHIJ', y_labels, axes):
        ax.set_box_aspect(1)
        ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes, size=fontdict['size'])
        ax.set_ylabel(y_label, fontdict=fontdict)
        if ax_label not in 'GHIJ':
            ax.set_xlabel('Fraction of single\n targets collected $\\Phi$', fontdict=fontdict)
            ax.xaxis.set_major_locator(comp_frac_locator)
            ax.xaxis.set_major_formatter(comp_frac_formatter)

    for ax in axes[[6, 8]]:
        ax.set_xlabel(y_labels[0], fontdict=fontdict)
        ax.xaxis.set_major_locator(comp_frac_locator)
        ax.xaxis.set_major_formatter(comp_frac_formatter)
        ax.yaxis.set_major_locator(comp_frac_locator)
        ax.yaxis.set_major_formatter(comp_frac_formatter)

    for ax in axes[[7, 9]]:
        ax.set_xlabel(y_labels[1], fontdict=fontdict)

    for ax in axes[[1, 3, 5, 7, 9]]:
        ax.set_ylim([-.002, .172])

    for ax in axes[[7, 9]]:
        ax.set_xlim([-.002, .172])
        ax.yaxis.set_major_locator(ticker.FixedLocator([0, .1]))

    plt.subplots_adjust(wspace=.8, hspace=.8, top=.95, bottom=.06, left=.15, right=.92)

    save_fig()

