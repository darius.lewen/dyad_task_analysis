import itertools

import numpy as np
import pandas as pd
import scipy
from matplotlib import pyplot as plt
from tqdm import tqdm
import scipy.special
import glm.linearities_sim as lin
import glm.linearities_asim as asim_lin

from glm.preprocess import load_preprocessed_data
from glm.util import to_hot_encoded, add_properties
from load.loading_helper import unique_pairs
from trial_descriptor import get_comp_frac, unsorted_load_all_trial_descriptions

# np.random.seed(0)
# np.random.seed(1)
np.random.seed(42)


mean_trial_amount = 0


def gen_n_prev_t_linearity(n, prev_t_linearity):
    p_amount = prev_t_linearity.param_amount

    @add_properties(param_amount=n * p_amount, name=f'{n}_prev_t')
    def n_prev_t_linearity(glm_kwargs, weights):
        weights = [weights[i * p_amount: (i + 1) * p_amount] for i in range(n)]
        depths = list(range(1, n + 1))
        return sum(prev_t_linearity(glm_kwargs, w, d) for w, d in zip(weights, depths))
    return n_prev_t_linearity


def neg_log_likelihood(w, glm_func, glm_kwargs, next_collections):
    expected_values = glm_func(w, glm_kwargs)
    local_likelihoods = expected_values[to_hot_encoded(next_collections).astype(bool)]
    return mean_trial_amount * np.sum(-1 * np.log(local_likelihoods)) / len(local_likelihoods)


def accuracy(w, glm, glm_kwargs, next_collections):
    expected_values = glm(w, glm_kwargs)  # todo this can be removed later on...
    predictions = np.argmax(expected_values, axis=1)
    collections = np.argmax(to_hot_encoded(next_collections), axis=1)  # todo remove hot encoding...
    correct_prediction = np.zeros_like(predictions)
    correct_prediction[predictions == collections] = 1
    return correct_prediction.mean()


def entropy(x, eps=1e-10):
    x *= -1 * np.log2(x + eps)
    return x.sum(axis=1)


def entropy_time_series(w, glm, glm_kwargs):
    expected_values = glm(w, glm_kwargs)
    return entropy(expected_values)


def akaike(parameter_amount, nll, trial_amount):
    small_sample_correction = (2 * parameter_amount ** 2 + 2 * parameter_amount) / (trial_amount - parameter_amount - 1)
    return 2 * parameter_amount + 2 * nll + small_sample_correction


# def akaike(parameter_amount, nll, trial_amount):
#     return 2 * parameter_amount + 2 * nll


# def akaike(parameter_amount, nll, trial_amount):  # BIC
#     # small_sample_correction = (2 * parameter_amount ** 2 + 2 * parameter_amount) / (trial_amount - parameter_amount - 1)
#     return np.log(800) * parameter_amount + 2 * nll


def softmax(x):
    return scipy.special.softmax(x, axis=1)


def gen_glm(linearities):
    glm_name = ''.join([linearity.name + '_' for linearity in linearities])[:-1]
    param_amounts = [linearity.param_amount for linearity in linearities]
    linearity_names = [linearity.name for linearity in linearities]
    param_boundaries = np.cumsum([0] + param_amounts)
    glm_param_amount = sum(param_amounts)

    @add_properties(name=glm_name, param_amount=glm_param_amount, param_boundaries=param_boundaries,
                    linearity_names=linearity_names)
    def glm(weights, glm_kwargs):
        return softmax(sum([linearity(glm_kwargs, weights[i:k]) for linearity, i, k in
                            zip(linearities, param_boundaries[:-1], param_boundaries[1:])]))
    return glm


def resort(measures):
    unsorted_comp_fractions = [get_comp_frac(td) for td in unsorted_load_all_trial_descriptions()]
    comp_frac_sorting = np.argsort(unsorted_comp_fractions)
    return {key: np.array(val)[comp_frac_sorting] for key, val in measures.items()}


def get_data(dyad_idx, kind):
    glm_kwargs = load_preprocessed_data(dyad_idx, kind)
    return glm_kwargs, glm_kwargs['next_collections']


def get_weight_frame(weigths, glm, length):
    frame = [dict()] + [None for _ in range(length - 1)]
    for w_name, lower, upper in zip(glm.linearity_names, glm.param_boundaries[:-1], glm.param_boundaries[1:]):
        frame[0][w_name] = weigths[lower: upper]
    return frame


def set_mean_trial_amount():
    trial_amount = 0
    for dyad_idx in range(len(unique_pairs)):
        _, next_collections = get_data(dyad_idx, 'train')
        trial_amount += len(next_collections)
    global mean_trial_amount
    mean_trial_amount = trial_amount // len(unique_pairs)


def get_acc_measures(glm):
    measures = dict(weights=list(), entropy_ts=list(), expected_vals=list(), next_collection=list(),
                    acc=list(), nll=list(), aic=list())
    set_mean_trial_amount()
    for dyad_idx in range(len(unique_pairs)):
        glm_kwargs, next_collections = get_data(dyad_idx, 'train')
        trial_amount = len(next_collections)
        args = (glm, glm_kwargs, next_collections)
        res = scipy.optimize.minimize(neg_log_likelihood, np.random.rand(glm.param_amount), args)
        # np.save(f'weights/{glm.name}/dyad{dyad_idx}.npy', res['x'])
        full_data = get_data(dyad_idx, 'full')
        # measures['weights'].append(res['x'])
        measures['weights'].append(get_weight_frame(res['x'], glm, length=len(full_data[1])))
        measures['entropy_ts'].append(entropy_time_series(res['x'], glm, full_data[0]))
        measures['expected_vals'].append(glm(res['x'], full_data[0]))
        measures['next_collection'].append(full_data[1])  # todo refactor!
        measures['acc'].append(accuracy(res['x'], glm, *get_data(dyad_idx, 'test')))
        measures['nll'].append(res['fun'])
        measures['aic'].append(akaike(glm.param_amount, res['fun'], mean_trial_amount))
    return measures
    # return resort(measures)


if __name__ == '__main__':
    ax_width, ratio = 3, 2
    rows, cols = 3, 1
    fig, axes = plt.subplots(rows, cols, figsize=(ax_width * cols, ax_width * rows / ratio))
    axes = axes.flatten()

    linearities = [lin.effort_linearity,
                   # gen_n_prev_t_linearity(1, asim_lin.prev_t_linearity),
                   # gen_n_prev_t_linearity(2, asim_lin.prev_t_linearity),
                   # gen_n_prev_t_linearity(3, asim_lin.prev_t_linearity),
                   # gen_n_prev_t_linearity(4, asim_lin.prev_t_linearity),
                   gen_n_prev_t_linearity(1, lin.prev_t_linearity),
                   gen_n_prev_t_linearity(2, lin.prev_t_linearity),
                   gen_n_prev_t_linearity(3, lin.prev_t_linearity),
                   gen_n_prev_t_linearity(4, lin.prev_t_linearity),
                   # gen_n_prev_t_linearity(5, lin.prev_t_linearity),
                   # gen_n_prev_t_linearity(4, lin.prev_t_linearity),
                   # gen_n_pref_t_linearity(5),
                   # gen_n_pref_t_linearity(6),
                   lin.invite_linearity,]
                   # lin.bias]

    linearity_combinations = sum([list(itertools.combinations(linearities, i))
                                  for i in range(1, len(linearities) + 1)], list())

    def multi_pref_filter(combinations):
        pref_count = 0
        for func in combinations:
            if 'prev_t' in func.name:
                pref_count += 1
        return pref_count < 2

    linearity_combinations = list(filter(multi_pref_filter, linearity_combinations))
    linearity_combinations.append((lin.bias, ))
    print(len(linearity_combinations))
    print(linearity_combinations)
    glm_s = list(reversed([gen_glm(lin) for lin in linearity_combinations]))
    # glm_s = [gen_glm(lin) for lin in linearity_combinations][:2]
    # all_model_vals = pd.DataFrame()
    unsorted_all_model_vals = {glm.name: get_acc_measures(glm) for glm in tqdm(glm_s)}
    sorted_all_model_vals = {key: resort(val) for key, val in unsorted_all_model_vals.items()}
    unsorted_performance = pd.DataFrame(unsorted_all_model_vals)
    performance = pd.DataFrame(sorted_all_model_vals)
    unsorted_performance.to_pickle('unsorted_performance.pkl')
    performance.to_pickle('performance.pkl')

    # def get_correction_term_for_glm(glm, n=800):
    #     return (glm.param_amount ** 2 + 2 * glm.param_amount) / (n - glm.param_amount - 1)
    #
    # glm_correction_terms = [get_correction_term_for_glm(glm) for glm in glm_s]

