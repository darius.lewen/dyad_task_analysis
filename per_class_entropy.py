import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from glm.plotting.entropy_corr import get_cut_tds
from pred_performance import violin_swarm_mean_plot
from trial_descriptor import get_comp_frac, unsorted_load_all_trial_descriptions

classes = ['miscoordination', 'p0_ahead', 'p1_ahead', 'concurrent', 'anti_corr',
           'declined_coop0_invite', 'accepted_coop0_invite',
           'declined_coop1_invite', 'accepted_coop1_invite']

# reduced_classes = ['miscoordination', 'anti_corr', 'straight',
#                    'declined_invite', 'accepted_invite']

reduced_classes = ['miscoordination', 'anti_corr', 'one_ahead', 'concurrent',
                   'declined_invite', 'accepted_invite']


def get_reduced_class_binary(td, c):
    if 'declined' in c:
        coop0 = (td['trial_class'] == 'declined_coop0_invite').to_numpy().astype(int)
        coop1 = (td['trial_class'] == 'declined_coop1_invite').to_numpy().astype(int)
        return (coop0 + coop1).astype(bool)
    elif 'accepted' in c:
        coop0 = (td['trial_class'] == 'accepted_coop0_invite').to_numpy().astype(int)
        coop1 = (td['trial_class'] == 'accepted_coop1_invite').to_numpy().astype(int)
        return (coop0 + coop1).astype(bool)
    elif c == 'one_ahead':
        p0 = (td['trial_class'] == 'p0_ahead').to_numpy().astype(int)
        p1 = (td['trial_class'] == 'p1_ahead').to_numpy().astype(int)
        return (p0 + p1).astype(bool)
    elif c == 'concurrent':
        return (td['trial_class'] == 'concurrent').to_numpy().astype(bool)
    elif c == 'miscoordination':
        return (td['trial_class'] == 'miscoordination').to_numpy().astype(bool)
    elif c == 'anti_corr':
        return (td['trial_class'] == 'anti_corr').to_numpy().astype(bool)
    else:
        print('error')
        return None


def get_simple_entropy_ts(dyad_amount=58, filename='../performance.pkl'):
    all_model_vals = pd.read_pickle(filename)
    best_entropy_ts = list()
    for dyad_idx in range(dyad_amount):
        best_entropy_ts.append(all_model_vals['effort_1_prev_t_invite']['entropy_ts'][dyad_idx])
    return best_entropy_ts


def get_mean_per_class_entropies(tds, entropy_ts):
    per_class_entropies = {c: list() for c in reduced_classes}
    for i, (entropy, td) in enumerate(zip(entropy_ts, tds)):
        for c in reduced_classes:
            reduced_class_binary = get_reduced_class_binary(td, c)
            per_class_entropies[c].append(entropy[reduced_class_binary].mean())
    return per_class_entropies


if __name__ == '__main__':
    # entropy_ts = get_best_entropy_ts(filename='glm/performance.pkl')
    tds = unsorted_load_all_trial_descriptions()
    unsorted_comp_fractions = [get_comp_frac(td) for td in tds]
    comp_frac_sorting = np.argsort(unsorted_comp_fractions)
    tds = get_cut_tds()
    entropy_ts = get_simple_entropy_ts(filename='glm/unsorted_performance.pkl')
    tds = [tds[i] for i in comp_frac_sorting][:-15]
    entropy_ts = [entropy_ts[i] for i in comp_frac_sorting][:-15]
    per_class_entropies = get_mean_per_class_entropies(tds, entropy_ts)
    comp_fractions = [get_comp_frac(td) for td in tds]
    violin_swarm_mean_plot(pd.DataFrame(per_class_entropies), ax=plt.gca())

