import numpy as np
import pandas as pd
import seaborn as sn
from matplotlib import pyplot as plt
from scipy.stats import pearsonr, linregress

from _util import save_fig
from glm.plotting.entropy_ts import get_best_entropy_ts
from glm.plotting.entropy_vs_duration_2 import get_concat_td, remove_head
from pred_performance import fontdict
from reward_decomposition import plot_corr_coef
from trial_descriptor import unsorted_load_all_trial_descriptions, get_comp_frac


def get_cut_tds():
    return [remove_head(get_concat_td(dyad_idx)) for dyad_idx in range(58)]


def get_simple_entropy_ts(dyad_amount=58, filename='../performance.pkl', model=None, ts=None):
    if model is None:
        model = 'effort_prev_t_inv'
    if ts is None:
        ts = 'entropy_ts'
    all_model_vals = pd.read_pickle(filename)
    best_entropy_ts = list()
    for dyad_idx in range(dyad_amount):
        best_entropy_ts.append(all_model_vals[model][ts][dyad_idx])
    return best_entropy_ts


if __name__ == '__main__':
    tds = unsorted_load_all_trial_descriptions()
    unsorted_comp_fractions = [get_comp_frac(td) for td in tds]
    comp_frac_sorting = np.argsort(unsorted_comp_fractions)
    comp_fractions = [unsorted_comp_fractions[i] for i in comp_frac_sorting]

    tds = get_cut_tds()
    durations = [td['duration'].to_numpy() for td in tds]
    distance_ol = [td['distance_ol'].to_numpy() for td in tds]  # todo make all negative values to zero
    mis_cord_measure = [td['mis_cord_measure'].to_numpy() for td in tds]
    distance = [td['distance'].to_numpy() for td in tds]
    effort = [td['chosen_effort'].to_numpy() for td in tds]
    speed = [td['speed'].to_numpy() for td in tds]
    # best_entropy_ts = get_best_entropy_ts(filename='../unsorted_performance.pkl')
    best_entropy_ts = get_simple_entropy_ts(filename='../unsorted_performance.pkl')

    # curvatures = list()
    # for dist_ol, dist in zip(distance_ol, distance):
    #     dist_ol[dist_ol <= 0] = 1e-6
    #     dist[dist <= 0] = 1e-6
    #     dist_ol[np.isnan(dist_ol)] = 1e-6
    #     dist[np.isnan(dist)] = 1e-6
    #     dist_ol[np.isinf(dist_ol)] = 1e-6
    #     dist[np.isinf(dist)] = 1e-6
    #     out = dist_ol / dist
    #     out[out <= 0] = 1e-6
    #     out[out >= 3] = 3
    #     out[np.isnan(out)] = 1e-6
    #     out[np.isinf(out)] = 1e-6
    #     curvatures.append(out)

    # best_entropy_ts = [best_entropy_ts[i] for i in comp_frac_sorting][15:-15]
    # curvatures = [curvatures[i] for i in comp_frac_sorting][15:-15]
    curvatures = mis_cord_measure

    for i, curvature in enumerate(curvatures):
        curvatures[i][curvature < 0.] = 0
        # curvatures[i][np.isnan(curvature)] = 0
        curvatures[i][curvature > 1] = 1
        # curvatures[i][np.isinf(curvature)] = 0

    best_entropy_ts = [best_entropy_ts[i] for i in comp_frac_sorting][:-15]
    curvatures = [curvatures[i] for i in comp_frac_sorting][:-15]
    # best_entropy_ts = [best_entropy_ts[i] for i in comp_frac_sorting][22:-17]
    # curvatures = [curvatures[i] for i in comp_frac_sorting][22:-17]
    best_entropy_ts = np.concatenate(best_entropy_ts)
    curvatures = np.concatenate(curvatures)
    # curvatures = np.concatenate(mis_cord_measure)

    joint_grid = sn.JointGrid(x=best_entropy_ts, y=curvatures, height=3)
    # joint_grid.plot_joint(sn.histplot, vmax=175)#, vmax=1)
    joint_grid.plot_joint(sn.kdeplot, fill=True)
    joint_grid.plot_marginals(sn.histplot)
    # joint_grid = sn.jointplot(x=best_entropy_ts, y=curvatures, height=3, kind='hist')
    # joint_grid = sn.jointplot(x=best_entropy_ts, y=curvatures, kind='kde', height=3, alpha=.05)
    # joint_grid = sn.jointplot(x=best_entropy_ts, y=curvatures, kind='kde', fill=True, height=3)
    plt.subplots_adjust(wspace=0., hspace=0., left=.2, right=.9, bottom=.2, top=.9)
    axes = joint_grid.ax_joint
    axes.set_xlim([0, np.log2(3)])
    axes.set_ylim([0, .5])
    axes.set_xlabel('Entropy', fontdict=fontdict)
    axes.set_ylabel('Curvature', fontdict=fontdict)
    axes.set_box_aspect(1)
    plot_corr_coef(best_entropy_ts, curvatures, ax=axes, plot_x=.96)
    slope, intercept, r, p, se = linregress(x=best_entropy_ts, y=curvatures)  # todo make this run!
    xlin = np.linspace(0, np.log2(3), 10)
    sn.lineplot(x=xlin, y=intercept+slope*xlin, c='k', ax=axes)
    save_fig()
