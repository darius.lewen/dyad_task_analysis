import numpy as np
import pandas as pd
import seaborn as sn
from matplotlib import pyplot as plt, ticker
from pyparsing import col

from _util import save_fig
from first_on_target_as_sd import get_only_straight
from presentation.fig5_skill import get_trial_amount_estimator, plot_skill_diff_curves
from presentation.plotting import scatterplot, violin_swarm
from rew_diff import get_skill_differences
from rew_diff_2 import get_single_target_diff
from reward_decomposition import get_agent_rewards, get_mean_in_cm
from skill_diff import get_only_straight_single
from trial_classes import plot_aic_poly
from trial_descriptor_2 import load_all_trial_descriptions, get_comp_frac

if __name__ == '__main__':
    tds = load_all_trial_descriptions()
    comp_fractions = [get_comp_frac(td) for td in tds]
    oss_tds = [get_only_straight_single(td) for td in tds]
    oss_skill_difference = np.array(get_skill_differences(oss_tds))
    to_plot = pd.DataFrame(dict(skill_diff=oss_skill_difference, comp_fractions=comp_fractions))
    # size_scaler = 3
    # fig, axes = plt.subplots(2, 2, figsize=(2 * size_scaler, 2 * size_scaler))
    fig_width = 7
    fig_ratio = 9 / 16
    fig, axes = plt.subplots(2, 3, figsize=(fig_width, fig_width * fig_ratio))
    axes = axes.flatten()
    axes[2].axis('off')
    axes[5].axis('off')
    axes = [axes[0], axes[1], axes[3], axes[4]]
    # axes = axes.flatten()
    plt.subplots_adjust(left=.1, right=.987, top=.89, bottom=.14, wspace=.6, hspace=.8)
    # plt.subplots_adjust(top=.75, bottom=.3)
    scatterplot(to_plot, x='comp_fractions', y='skill_diff', ax=axes[0])
    axes[0].fill_between([-1, 0.08], -1, 1, color='gray', alpha=.2, edgecolor=None, label='No skill\ndiff. data')  # todo find right x value
    axes[0].set_xlim(-.02, 1.02)
    axes[0].set_ylim(-.02, .52)
    axes[0].set_xlabel('')
    axes[0].set_ylabel('Skill difference (%)')
    axes[3].set_xlabel('Skill difference (%)')
    axes[3].set_ylabel('Dyad count')
    axes[0].set_xlabel('Stable FST')
    axes[1].set_xlabel('Stable FST')
    axes[2].set_xlabel('Stable FST')
    axes[0].legend(loc='upper right', bbox_to_anchor=(1.07, 1.07), frameon=False)
    comp_frac_locator = ticker.FixedLocator([0, 1 / 3, 2 / 3, 1])
    comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1'])
    axes[0].xaxis.set_major_locator(comp_frac_locator)
    axes[0].xaxis.set_major_formatter(comp_frac_formatter)
    axes[2].xaxis.set_major_locator(comp_frac_locator)
    axes[2].xaxis.set_major_formatter(comp_frac_formatter)
    axes[0].yaxis.set_major_locator(ticker.FixedLocator([0, .1, .3]))
    axes[0].yaxis.set_major_formatter(ticker.FixedFormatter(['0', '10', '30']))
    axes[0].set_box_aspect(1)

    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)

    single_target_diff = get_single_target_diff(tds)

    payoff_diff = high_rewards - low_rewards

    # skill_differences = np.array(get_skill_differences(tds))
    # skill_differences[np.isnan(skill_differences)] = 0.

    trial_amount_estimator = get_trial_amount_estimator(tds)

    def get_trial_amounts(tds):  # per block, tds has initial 10 minute cut, divide by 1.5
        return np.array([len(td) / 1.5 for td in tds])

    oss_tds = [get_only_straight_single(td) for td in tds]

    oss_skill_difference = np.array(get_skill_differences(oss_tds))
    # oss_skill_difference[np.isnan(oss_skill_difference)] = 0.

    only_straight_tds = [get_only_straight(td) for td in tds]

    xspace = np.linspace(0, 1)
    dyad_mean_payoff = .07 / 2 * trial_amount_estimator(xspace)

    payoff_diff_10 = .1 * xspace * trial_amount_estimator(xspace) * .07
    payoff_diff_30 = .3 * xspace * trial_amount_estimator(xspace) * .07

    high_payoff_curve_10 = dyad_mean_payoff - payoff_diff_10 / 2
    high_payoff_curve_30 = dyad_mean_payoff - payoff_diff_30 / 2

    cost_of_coop_curve_base = np.max(dyad_mean_payoff) - dyad_mean_payoff
    cost_of_coop_curve_10 = np.max(high_payoff_curve_10) - high_payoff_curve_10
    cost_of_coop_curve_30 = np.max(high_payoff_curve_30) - high_payoff_curve_30

    axes[1].plot(xspace, cost_of_coop_curve_base, c='k')
    axes[1].plot(xspace, cost_of_coop_curve_10, c=np.ones(3) * .25)
    axes[1].plot(xspace, cost_of_coop_curve_30, c=np.ones(3) * .6)

    dyad_mean_payoff = .07 / 2 * trial_amount_estimator(comp_fractions)
    oss_payoff_diff_estimate = oss_skill_difference * comp_fractions * trial_amount_estimator(comp_fractions) * .07
    oss_payoff_estimate = dyad_mean_payoff - oss_payoff_diff_estimate / 2

    oss_optimal_fst_payoff_estimate = list()
    dyad_mean_payoff_curve = .07 / 2 * trial_amount_estimator(xspace)
    for skill_diff in oss_skill_difference:
        estimated_payoff_differences = skill_diff * xspace * trial_amount_estimator(xspace) * .07
        fst_payoff_estimate = dyad_mean_payoff_curve - estimated_payoff_differences / 2
        oss_optimal_fst_payoff_estimate.append(np.max(fst_payoff_estimate))
    oss_optimal_fst_payoff_estimate = np.array(oss_optimal_fst_payoff_estimate)

    cost_of_cooperation = oss_optimal_fst_payoff_estimate - oss_payoff_estimate

    to_plot = pd.DataFrame(dict(comp_fractions=comp_fractions, cost_of_cooperation=cost_of_cooperation))
    scatterplot(to_plot, x='comp_fractions', y='cost_of_cooperation', ax=axes[1])

    axes[1].fill_between([-1, 0.08], -1, 11, color='gray', alpha=.2, edgecolor=None,
                         label='No skill\ndiff. data')  # todo find right x value
    axes[1].set_xlim(-.02, 1.02)
    axes[1].set_ylim(-.18, 5)
    axes[1].set_xlabel('')
    axes[1].set_ylabel('Lower-skilled participant\'s\npayoff loss due to\nnon optimal FST (€)')
    axes[1].set_xlabel('Stable FST')
    axes[1].legend(loc='upper right', bbox_to_anchor=(1.07, 1.07), frameon=False)
    comp_frac_locator = ticker.FixedLocator([0, 1 / 3, 2 / 3, 1])
    comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1'])
    axes[1].xaxis.set_major_locator(comp_frac_locator)
    axes[1].xaxis.set_major_formatter(comp_frac_formatter)
    axes[1].set_box_aspect(1)

    #####################

    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)
    single_target_diff = get_single_target_diff(tds)
    # relative_single_target_diff = np.array(single_target_diff) / (comp_fractions * np.array([len(td) for td in tds]))
    to_plot = pd.DataFrame(dict(
        payoff_diff=high_rewards - low_rewards,
        single_target_diff=single_target_diff,
        comp_fractions=comp_fractions,
    ))

    # scatterplot(to_plot, x='single_target_diff', y='payoff_diff', ax=axes[1])
    # axes[1].plot([0, 220], [0, 220 * 0.07], c='k', linewidth=1, alpha=.7, zorder=1)

    speed = get_mean_in_cm('speed', tds)
    additional_effort = get_mean_in_cm('path_len_due_to_curvature', tds)
    effort_from_past_target = get_mean_in_cm('limiting_agent_next_target_displacement', tds) + get_mean_in_cm('reduction_due_to_adv_placement', tds)
    effort_reduction_by_ps = get_mean_in_cm('reduction_due_to_adv_placement', tds)
    duration = np.array([td['duration'].to_numpy().mean() for td in tds])

    xspace, speed_curve = plot_aic_poly(comp_fractions, speed)
    _, additional_effort_curve = plot_aic_poly(comp_fractions, additional_effort)
    _, effort_from_past_target_curve = plot_aic_poly(comp_fractions, effort_from_past_target)
    _, effort_reduction_by_ps_curve = plot_aic_poly(comp_fractions, effort_reduction_by_ps)

    comp_distance_curve = effort_from_past_target_curve + additional_effort_curve - effort_reduction_by_ps_curve
    # comp_duration_curve = comp_distance_curve / speed_curve
    comp_duration_curve = comp_distance_curve / speed.mean()  # expected payoff without speed effect

    _, org_duration_curve = plot_aic_poly(comp_fractions, duration)

    def duration_to_rew(dur):
        return .035 * 20 * 60 / (dur + 1)

    comp_reward_curve = duration_to_rew(comp_duration_curve)
    org_reward_curve = duration_to_rew(org_duration_curve)

    axes[2].plot(xspace, comp_reward_curve, label='Both payoffs for 0% difference in single targets', c='k')
    plot_skill_diff_curves(comp_reward_curve, axes[2], c=np.ones(3) * .25)
    plot_skill_diff_curves(comp_reward_curve, axes[2], skill_diff=.3, c=np.ones(3) * .6)

    skill_diff =.21
    xspace = np.linspace(0, 1)
    rew_diff = skill_diff * xspace * 2 * comp_reward_curve
    higher = comp_reward_curve + rew_diff / 2
    axes[2].plot(xspace, higher, label=f'Lower payoff for {int(100 * skill_diff)}% difference in single targets', linestyle='dashed', c='r')

    # sn.histplot(x=skill_differences, ax=axes[2], color='gray', binwidth=.1, alpha=.5, kde=True)
    axes[3].hist(x=oss_skill_difference, color='gray', alpha=.5, bins=6)
    # to_plot = pd.DataFrame(dict(skill_difference=oss_skill_difference, comp_fractions=comp_fractions))
    # violin_swarm(to_plot, ax=axes[1], marker_size=4.4)
    # violin_swarm(data=to_plot, y='skill_difference', ax=axes[3])
    axes[3].plot([.06, .06], [0, 50], c='k')
    axes[3].plot([skill_diff, skill_diff], [0, 50], linestyle='dashed', c='r')
    axes[3].set_xticklabels(axes[3].get_xticks(), rotation=-90)
    axes[3].xaxis.set_major_locator(ticker.FixedLocator([0, .1, .3]))
    axes[3].xaxis.set_major_formatter(ticker.FixedFormatter(['0', '10', '30']))
    axes[3].set_ylim(0, 20)
    axes[2].set_box_aspect(1)
    axes[3].set_box_aspect(1)
    axes[2].set_ylabel('Expected payoff (€)\nwithout movement\nspeed effect')

    for ax_label, ax in zip('abcd', axes):
        ax.set_box_aspect(1)
        ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes, weight='bold')

    save_fig(sub_folder='supplementary/', format='svg')

