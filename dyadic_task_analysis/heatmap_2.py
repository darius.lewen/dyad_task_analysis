import matplotlib
import numpy as np
from matplotlib import pyplot as plt

from _util import save_fig, game_width_in_cm
from path_shortening_visualization import plot_heatmap as plot_heatmap_base, plot_targets
from simulations import get_expected_min_effort_heatmap


def plot_heatmap(comp, coop0, coop1, ax, res=40):
    comp = np.array(comp)
    coop0 = np.array(coop0)
    coop1 = np.array(coop1)
    targets = np.vstack([comp, coop0, coop1])
    heatmap = get_expected_min_effort_heatmap(targets, res=res)
    heatmap *= game_width_in_cm
    heat_img = plot_heatmap_base(ax, heatmap, comp, coop0, coop1, n=res, vmin=heatmap.min(), vmax=heatmap.max())
    plt.colorbar(heat_img, ax=ax)


def plot_comp_heatmap(comp, coop0, coop1, ax, res=40):
    comp = np.array(comp)
    coop0 = np.array(coop0)
    coop1 = np.array(coop1)
    targets = np.vstack([comp, np.array([5, 5]), np.array([5, 5])])
    heatmap = get_expected_min_effort_heatmap(targets, res=res)
    heatmap *= game_width_in_cm
    heat_img = plot_heatmap_base(ax, heatmap, comp, coop0, coop1, n=res, vmin=heatmap.min(), vmax=heatmap.max())
    # plt.colorbar(heat_img, ax=ax)


if __name__ == '__main__':
    matplotlib.rcParams['text.usetex'] = True
    fig, axes = plt.subplots(2, 3)
    axes = axes.flatten()

    plot_heatmap(comp=[.7, .2], coop0=[5, 5], coop1=[5, 5], ax=axes[0])
    plot_heatmap(comp=[.7, .2], coop0=[.8, .4], coop1=[.1, .2], ax=axes[1])
    plot_heatmap(comp=[.7, .2], coop0=[.8, .55], coop1=[.1, .2], ax=axes[2])
    plot_heatmap(comp=[.4, .45], coop0=[.1, .6], coop1=[.4, .1], ax=axes[3])
    # plot_heatmap(comp=[.4, .45], coop0=[.1, .6], coop1=[.4, .1], ax=axes[4], w=1.)
    plot_comp_heatmap(comp=[.4, .45], coop0=[.1, .6], coop1=[.4, .1], ax=axes[4])
    # plot_heatmap(comp=[.5, .5], coop0=[.1, .6], coop1=[.4, .1], ax=axes[4])
    # plot_heatmap(comp=[.7, .2], coop0=[5, 5], coop1=[5, 5], ax=axes[5], w=0.)
    # plot_heatmap(comp=[.7, .2], coop0=[5, 5], coop1=[5, 5], ax=axes[6])
    # plot_heatmap(comp=[.9, .1], coop0=[5, 5], coop1=[5, 5], ax=axes[5])

    # plot_targets(comp=[.7, .2], coop0=[5, 5], coop1=[5, 5], ax=axes[7], n=40)
    plot_targets(comp=[.7, .2], coop0=[5, 5], coop1=[5, 5], ax=axes[-1], n=40)

    for ax_label, ax in zip('ABCDEFGH', axes):
        ax.text(-0.00, 1.04, ax_label, transform=ax.transAxes, size=14)
    axes[-2].text(1.00, .8, '$\\Phi = 1$', transform=axes[-2].transAxes, size=14, ha='right')

    save_fig()


