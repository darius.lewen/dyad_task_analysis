import numpy as np

# order = ['1_prev_t',
#          'effort_3_prev_t',
#          'effort_2_prev_t',
#          'effort_1_prev_t',
#          'effort_4_prev_t_invite',
#          'effort_3_prev_t_invite',
#          'effort_2_prev_t_invite',
#          'effort_1_prev_t_invite',
#          'effort_invite',
#          'bias']


order = [
    'effort_3_prev_t_invite',
    'effort_2_prev_t_invite',
    'effort_1_prev_t_invite',
    'effort_invite',
    'effort_3_prev_t',
    'effort_2_prev_t',
    'effort_1_prev_t',
    'bias',
    '1_prev_t',
]

# order = ['1_prev_t',
#          # '2_prev_t',
#          # 'effort_3_prev_t',
#          'effort_2_prev_t',
#          'effort_1_prev_t',
#          'effort_4_prev_t_invite',
#          'effort_3_prev_t_invite',
#          'effort_2_prev_t_invite',
#          'effort_1_prev_t_invite',
#          'effort_invite',
#          'bias']


def add_properties(param_amount=0, param_boundaries=None, linearity_names=None, name=None):
    def wrapper(f):
        f.name = name if name is not None else f.__name__[:-10]
        f.param_amount = param_amount
        f.param_boundaries = param_boundaries
        f.linearity_names = linearity_names
        return f
    return wrapper


def to_hot_encoded(x, class_amount=3):
    x = x.astype(int)
    hot_encoded = np.zeros((x.size, class_amount))
    hot_encoded[np.arange(x.size), x] = 1
    return hot_encoded


