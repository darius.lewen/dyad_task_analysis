import numpy as np

from simulations import get_weighted_efforts, get_sim_result_for_name


def get_naive_dyad_weighting(comp_frac):
    naive_comp_fractions, _naive_weights = get_sim_result_for_name('naive_0')
    for naive_w, prev_cf, next_cf in zip(_naive_weights, naive_comp_fractions[:-1], naive_comp_fractions[1:]):
        if prev_cf <= comp_frac <= next_cf:
            return naive_w


def effort_based_prediction(*args, **kwargs):
    weighted_dists = get_weighted_dists(*args, **kwargs)
    return predict(weighted_dists)


def get_weighted_dists(efforts, weighting):
    weighted_efforts = get_weighted_efforts(np.array(list(efforts.values())), weighting)
    return dict(zip(efforts.keys(), weighted_efforts))


def neg_logit(w_distances):
    return 1 / (1 + np.exp(w_distances))


def softmin(w_distances):
    return np.exp(-1 * w_distances) / np.sum(np.exp(-1 * w_distances))


def predict(dists):
    probabilities = softmin(np.array(list(dists.values())))
    # probabilities = neg_logit(np.array(list(dists.values())))
    target_names = list(dists.keys())
    predicted_t_name = target_names[int(np.argmin(list(dists.values())))]
    return predicted_t_name, probabilities

