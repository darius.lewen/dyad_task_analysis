import pandas as pd
from matplotlib import pyplot as plt

from _util import save_fig
# from glm.plotting.class_comparison import fontdict
from plotting.type_hist import get_hist_plot_frame
from pred_performance import violin_swarm_mean_plot, generate_fst_bar, load_model_performances
from presentation.plotting import violin_swarm

model_selection = [  # full
    'effort',
    # '1_prev_t',
    # 'invite',
    'effort_invite',
    'effort_1_prev_t',
    # 'effort_1_prev_t_invite',
    'effort_prev_t_inv',
]

#
# xticklables = [
#     'Predict\nnearest\njoint\n$(w=0.0)$',
#     'Predict\nalways\nsingle\n$(w=1.0)$',
#     'Predict\nnearest\ntarget\n$(w=0.5)$',
#     'with\nmatching\nweighting\n$(w=w(\\Phi_{dyad}))$',
#     'Distance\nGLM',
#     'Distance\ninvite\nGLM',
#     'Distance\nprevious target\nGLM',
#     'Distance\ninvite\nprevious target\nGLM',
# ]

xticklables = [
    'Predict\nnearest\njoint',
    'Predict\nalways\nsingle',
    'Predict\nnearest\ntarget',
    'Predict\nwith $\\Phi$\nmatching\nweighting',
    'Distance\nGLM',
    'Distance +\ninvite\nGLM',
    'Distance +\nprevious target\nGLM',
    'Distance +\ninvite +\nprevious target\nGLM',
]

if __name__ == '__main__':
    data = pd.read_pickle('performance.pkl')
    selected_model_accuracies = pd.DataFrame(data[model_selection].T['acc'].T.to_dict())
    size_scalar = 1.1
    fig, axes = plt.subplots(3, 1, figsize=(11 * size_scalar / 2 * .9, 5 * size_scalar / 2 * 3))
    plt.subplots_adjust(hspace=.8, top=.98, bottom=.02, right=.98)

    w_dyad_acc = pd.read_pickle('../data/acc_for_dyads.pkl')

    unsorted_best_model = get_hist_plot_frame('unsorted_performance.pkl')
    best_model = unsorted_best_model.sort_values(by=['comp_frac']).set_index(pd.Series(list(range(len(unsorted_best_model)))))

    # selected_model_accuracies = pd.concat([w_dyad_acc['$w=0.0$ '],
    #                                        w_dyad_acc['$w=1.0$ '],
    #                                        w_dyad_acc['$w=0.5$ '],
    #                                        w_dyad_acc['$w=w_{dyad}$'],
    #                                        selected_model_accuracies], axis=1)

    # violin_swarm_mean_plot(data=w_dyad_acc, ax=axes[0])
    violin_swarm(data=w_dyad_acc, ax=axes[0], show_mean=True)
    violin_swarm(data=selected_model_accuracies, ax=axes[1], show_mean=True)
    for ax in axes[:2]:
        ax.set_xlabel('')
        ax.xaxis.set_ticks_position('none')
        # ax.xaxis.set_tick_params(labelsize=fontdict['size'])
        # ax.yaxis.set_tick_params(labelsize=fontdict['size'])
    axes[0].set_xticklabels(xticklables[:4])#, fontdict=fontdict)
    axes[1].set_xticklabels(xticklables[4:])#, fontdict=fontdict)
    axes[0].set_ylabel('Accuracy')#, fontdict=fontdict)
    axes[1].set_ylabel('Test set accuracy')#, fontdict=fontdict)
    axes[0].set_ylim([-.02, 1.02])
    axes[1].set_ylim([.48, 1.02])
    generate_fst_bar(axes[2])
    # fig.tight_layout()
    save_fig()
