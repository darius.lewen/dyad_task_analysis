import matplotlib
import seaborn as sn
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.gridspec import GridSpec
from scipy.stats import pearsonr

from _util import save_fig, game_width_in_cm
from inefficiencies_full import comp_frac_x_format
from rew_diff import get_coll_count, get_skill_differences, filter_low_x, plot_skill_diff_curve, get
from reward_decomposition import scatter, get_agent_rewards, plot_corr_coef, get_mean_in_cm
from reward_decomposition_2 import get_sim_results
from trial_descriptor import load_all_trial_descriptions, get_comp_frac

fontdict = dict(size=12)


def get_single_target_diff(tds, block_amount=1.5):
    singlet_target_diff = list()
    for td in tds:
        coll_count = get_coll_count(td)
        p0_single = get(coll_count, 'single_p0')
        p1_single = get(coll_count, 'single_p1')
        if p0_single is None or p1_single is None:
            singlet_target_diff.append(0)
        else:
            singlet_target_diff.append(abs(p0_single - p1_single))
    return np.array(singlet_target_diff) / block_amount


def plot_skill_diff_curve(curve, skill_differences, ax, legend=True):
    quartiles = np.quantile(skill_differences, [.5, .75, 1])
    ax.plot(curve[0], curve[1], color='k', label='$S_\\Delta = 0$')
    labels = [
        '$Q_{0} < S_\\Delta < Q_{50}$',
        '$Q_{50} < S_\\Delta < Q_{75}$',
        '$Q_{75} < S_\\Delta < Q_{100}$',
    ]
    for label, quartile in zip(labels, quartiles):
        skill_diffs = quartile * curve[1] * curve[0]
        lower = curve[1] - skill_diffs / 2
        higher = curve[1] + skill_diffs / 2
        ax.fill_between(curve[0], lower, higher, color='k', alpha=.1, label=label, edgecolor=None)
    if legend:
        ax.legend(loc='upper left', bbox_to_anchor=(1.20, 1.10), frameon=False, fontsize=fontdict['size'])


if __name__ == '__main__':
    scalar = .8
    fig = plt.figure(figsize=(12 * scalar, 7 * scalar))
    plt.subplots_adjust(top=.9, bottom=.2, right=.98, wspace=.7, hspace=.7)
    grid_spec = GridSpec(2, 4, width_ratios=[1, 1, 1, 1/10], height_ratios=[1, 1])
    axes = [fig.add_subplot(grid_spec[i, k]) for i in range(2) for k in range(4)]
    # for (i, k) in [(i, k) for i in range(2) for k in range(4)]:
    #     axes.append(fig.add_subplot(grid_spec[i, k]))

    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)
    payoff_diff = high_rewards - low_rewards
    scatter(comp_fractions, payoff_diff, ax=axes[0])
    plot_corr_coef(comp_fractions, payoff_diff, axes[0], ha='left', plot_x=.02)

    single_target_diff = get_single_target_diff(tds)
    scatter(single_target_diff, payoff_diff, ax=axes[1])
    plot_corr_coef(single_target_diff, payoff_diff, ax=axes[1], ha='left', plot_x=.02)

    sim_results = get_sim_results()
    naive_0 = sim_results['naive_0'][1]
    mean_add_effort = get_mean_in_cm('distance_ol', tds).mean()
    mean_effort_reduction_by_ps = get_mean_in_cm('path_shortening', tds).mean()
    mean_speed = get_mean_in_cm('speed', tds).mean()
    estimated_agent_mean_curve = (naive_0 + mean_add_effort - mean_effort_reduction_by_ps) / mean_speed  # trial duration
    estimated_agent_mean_curve = .035 * 20 * 60 / (1 + estimated_agent_mean_curve)  # payoff - +1 due to 1sec coll time
    estimated_agent_mean_curve = np.array([sim_results['naive_0'][0], estimated_agent_mean_curve])
    sn.lineplot(x=estimated_agent_mean_curve[0], y=estimated_agent_mean_curve[1], ax=axes[4], c='k')

    coll_counts = np.array([get_coll_count(td).to_numpy().sum() for td in tds])
    coll_counts = coll_counts / 2  # for the two blocks
    scatter(x=comp_fractions, y=coll_counts * .035, ax=axes[5])

    p, residuals, _, _, _ = np.polyfit(comp_fractions, coll_counts, deg=7, full=True)
    fitted_agent_mean_curve = np.polyval(p, comp_fractions)
    fitted_agent_mean_curve = np.array([comp_fractions, fitted_agent_mean_curve * .035])
    sn.lineplot(x=fitted_agent_mean_curve[0], y=fitted_agent_mean_curve[1], ax=axes[5], c='k')

    skill_differences = get_skill_differences(tds)
    filtered_comp_fractions, filtered_skill_difference = filter_low_x(comp_fractions, skill_differences)
    scatter(filtered_comp_fractions, filtered_skill_difference, ax=axes[2])
    plot_corr_coef(filtered_comp_fractions, filtered_skill_difference, axes[2])

    axes[2].fill_between([-1, 0.05], -1, 1, color='gray', alpha=.3, edgecolor=None, label='missing data')
    axes[2].legend(loc='upper left', bbox_to_anchor=(.12, 1.33), frameon=False, fontsize=fontdict['size'])

    skill_differences = [sd for sd in skill_differences if sd is not None]
    sn.boxplot(y=filtered_skill_difference, ax=axes[3], color='gray')
    plot_skill_diff_curve(fitted_agent_mean_curve, skill_differences, ax=axes[5])
    plot_skill_diff_curve(estimated_agent_mean_curve, skill_differences, ax=axes[4], legend=False)

    sn.lineplot(x=[0, 1], y=[0, 0], color='k', ax=axes[0])
    quartiles = np.quantile(skill_differences, [.5, .75, 1])
    single_target_diff = np.array(single_target_diff)
    x_space = np.linspace(0, 1, 100)
    sorted_comp_fractions = sorted(comp_fractions)
    sorted_s_t_diff = single_target_diff[np.argsort(comp_fractions)]
    x_space_s_t_diff = np.interp(x_space, sorted_comp_fractions, sorted_s_t_diff)
    x_space_fitted_agent_mean = np.interp(x_space, estimated_agent_mean_curve[0], estimated_agent_mean_curve[1])
    # x_space = comp_fractions
    for quantile in quartiles:
        edge = quantile * x_space * x_space_fitted_agent_mean# * x_space_s_t_diff
        axes[0].fill_between(x_space, np.zeros_like(x_space), edge, color='k', alpha=.1, edgecolor=None)

    ylabels = ['Agent payoff\ndifference $R_\Delta$ (€)',
               'Agent payoffs $R^X$ (€)',
               'Normalized single target\n difference $S_\Delta$',
               'Agent payoffs $R^X$ (€)']

    for key, ylabel, ax in zip('ACDE', ylabels, [axes[0], axes[4], axes[2], axes[5]]):
        ax.text(-0.1, 1.1, key, transform=ax.transAxes, size=fontdict['size'])
        ax.set_xlabel('Fraction of single\ntargets collected $\\Phi$', fontdict=fontdict)
        comp_frac_x_format(ax)
        ax.set_ylabel(ylabel, fontdict=fontdict)

    for i in [0, 1, 2, 4, 5]:
        axes[i].set_box_aspect(1)

    axes[2].set_ylim([-0.04, .7])
    axes[3].set_ylim([-0.04, .7])
    axes[2].set_xlim([-0.04, 1.04])
    axes[1].text(-0.1, 1.1, 'B', transform=axes[1].transAxes, size=fontdict['size'])
    axes[1].set_xlabel('Single target\ndifference', fontdict=fontdict)
    axes[1].set_ylabel('Agent payoff\ndifference $R_\Delta$ (€)', fontdict=fontdict)
    axes[3].axis('off')
    axes[6].axis('off')
    axes[7].axis('off')

    save_fig()
