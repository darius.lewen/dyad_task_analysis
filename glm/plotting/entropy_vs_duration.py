import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sn
from scipy.stats import pearsonr

from glm.plotting.entropy_ts import get_best_entropy_ts
from trial_descriptor import load_all_trial_descriptions, get_comp_frac


def remove_head(td, n=4):   # todo del this and use version of second implementation
    return td.drop(list(range(n)))


def super_scriptinate(val: int) -> str:
    chars = [str(i) for i in range(10)] + ['-']
    super_chars = ['⁰', '¹', '²', '³', '⁴', '⁵', '⁶', '⁷', '⁸', '⁹', '⁻']
    val = str(val)
    for char, super_char in zip(chars, super_chars):
        val = val.replace(char, super_char)
    return val


def p_smaller_than(p: float, m=10) -> str:
    for i in range(m):
        if 10**-i > p > 10**(-i-1) or i == m-1:
            return f'$p<$10{super_scriptinate(-i)}'


def plot_correlation(x, y, ax):
    corr_coefficient, p_value = pearsonr(x, y)
    print(corr_coefficient, p_value)
    if p_value < .05:
        r_val_approx = r'r$\approx$'
        corr_coefficient_str = "%.2f" % corr_coefficient
        ax.text(.05, .9, f'{r_val_approx}{corr_coefficient_str}',
                transform=ax.transAxes, fontsize=10, verticalalignment='top', horizontalalignment='left')
    # r_val_approx = r'$r\approx$'
    # ax.text(.95, .05, f'{r_val_approx}{corr_coefficient_str}\n{p_smaller_than(p_value)}',
    #         transform=ax.transAxes, fontsize=10, verticalalignment='bottom', horizontalalignment='right')


if __name__ == '__main__':  # todo do this again but with a missprediction meassure
    tds = [remove_head(td) for td in load_all_trial_descriptions()]
    comp_fractions = [get_comp_frac(td) for td in tds]
    trial_durations = [td['duration'].to_numpy() for td in tds]
    best_entropy_ts = get_best_entropy_ts()
    # take_out = [i for i in range(58) if not 1.0 > comp_fractions[i] > 0.0 or any(np.isnan(best_entropy_ts[i]))]
    # trial_durations = [trial_duration for i, trial_duration in enumerate(trial_durations) if i not in take_out]
    # best_entropy_ts = [entropy_ts for i, entropy_ts in enumerate(best_entropy_ts) if i not in take_out]
    fig, axes = plt.subplots(10, 6, figsize=(7, 12), sharex='all', sharey='all')
    axes = axes.flatten()
    for dyad_idx, (entropy_ts, trial_duration, ax) in enumerate(zip(best_entropy_ts, trial_durations, axes)):
        ax.text(0.0, 1.1, f'Dyad {dyad_idx}', transform=ax.transAxes, size=10)
        sn.kdeplot(x=entropy_ts, y=trial_duration, ax=ax, fill=True)
        # sn.jointplot(x=entropy_ts, y=trial_duration, ax=ax, fill=True, kind='kde')
        plot_correlation(x=entropy_ts, y=trial_duration, ax=ax)
        ax.set_xlim(0, np.log2(3))
        ax.set_ylim(0, 3.2)
        ax.set_box_aspect(1)
    for ax in axes[-2:]:
        ax.axis('off')

    plt.subplots_adjust(hspace=.8, wspace=.4, left=.08, right=.92, top=.92, bottom=.08)
    script_name = os.path.basename(sys.argv[0])
    plt.savefig(f'../plots/{script_name[:-3]}.png', dpi=300)


# for dyad_idx, ax in enumerate(axes):



    # fig, axes = plt.subplots(8, 8)
    # axes = axes.flatten()
    # for dyad_idx, ax in enumerate(axes):
    #     sn.scatterplot(x=best_entropy_ts[dyad_idx], y=trial_durations[dyad_idx], ax=ax)
    # del best_entropy_ts[4]
    # del trial_durations[4]
    # fig, axes = plt.subplots(1, 4)
    # axes = axes.flatten()
    # dyad_idx = 2
    # sn.scatterplot(x=best_entropy_ts[dyad_idx], y=trial_durations[dyad_idx], ax=axes[0])
    # sn.kdeplot(x=best_entropy_ts[dyad_idx], y=trial_durations[dyad_idx], ax=axes[1], fill=True)
    # plot_correlation(x=best_entropy_ts[dyad_idx], y=trial_durations[dyad_idx], ax=axes[1])
    # all_entropy_ts = np.concatenate(best_entropy_ts)
    # all_trail_durations = np.concatenate(trial_durations)
    # sn.kdeplot(x=all_entropy_ts, y=all_trail_durations, ax=axes[2], fill=True)
    # plot_correlation(x=all_entropy_ts, y=all_trail_durations, ax=axes[2])
    # corr_coefficients = list()
    # for dyad_idx in range(58 - 1):
    #     corr_coefficient, p_value = pearsonr(x=best_entropy_ts[dyad_idx], y=trial_durations[dyad_idx])
    #     corr_coefficients.append(corr_coefficient)
    #     if p_value > .05:
    #         print(dyad_idx, p_value)
    # sn.histplot(x=corr_coefficients, ax=axes[3])

    # fig, axes = plt.subplots(8, 8)
    # axes = axes.flatten()
    # for dyad_idx, ax in enumerate(axes):
    #     sn.scatterplot(x=best_entropy_ts[dyad_idx], y=trial_durations[dyad_idx], ax=ax)