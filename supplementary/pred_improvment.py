import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from _util import save_fig
from glm.likelihood_2 import load_per_dyad_stats
from pred_performance import generate_fst_bar
from presentation.fig22_predicting_dyads import get_sorted_effort_glm_stats, get_true_fst_binary, get_pred_fst_binary, \
    get_closest_pred_acc
from presentation.plotting import violin_swarm
from trial_descriptor_2 import load_all_trial_descriptions, get_comp_frac, unsorted_load_all_trial_descriptions

if __name__ == '__main__':
    tds = load_all_trial_descriptions()
    dyad_idx = 28
    td = tds[dyad_idx]
    full_td = load_all_trial_descriptions(cut_initial=False)[dyad_idx]
    sorted_effort_glm_stats = get_sorted_effort_glm_stats()

    dist_glm_acc = np.array([summary['accuracy'] for summary in sorted_effort_glm_stats])
    comp_frac_sorting = np.argsort([get_comp_frac(td) for td in unsorted_load_all_trial_descriptions()])
    full_glm_stats = load_per_dyad_stats()
    sorted_full_glm_stats = [full_glm_stats[i] for i in comp_frac_sorting]
    full_glm_acc = np.array([summary['accuracy'] for summary in sorted_full_glm_stats])

    prediction_accuracies = {
        'Chance\nlevel': np.ones(len(tds)) / 3,
        'Predict\ncloset\ntarget': get_closest_pred_acc(tds),
        'Distance\nweighting\nGLM': dist_glm_acc,
        'Full\nGLM': full_glm_acc,
    }
    prediction_improvement_names = ['Predict\ncloset\ntarget', 'Weighting\ndistance', 'Incorporating\nsecondaries']
    prediction_improvements = {name: post - pre for name, pre, post in zip(prediction_improvement_names,
                                                                           list(prediction_accuracies.values())[:-1],
                                                                           list(prediction_accuracies.values())[1:])}
    size_scaler = 2.1
    fig, axes = plt.subplots(1, 2, figsize=(4 * size_scaler, 1 * size_scaler))
    plt.subplots_adjust(top=.95, bottom=.28, right=.95)
    axes = axes.flatten()
    generate_fst_bar(axes[1])
    violin_swarm(data=pd.DataFrame(prediction_improvements), ax=axes[0], marker_size=3)

    axes[0].set_xlabel('')
    axes[0].set_ylabel('Increase in accuracy')
    axes[1].set_box_aspect(3)
    axes[1].axis('off')

    save_fig(sub_folder='supplementary/', format='svg')
