import matplotlib
import numpy as np
from matplotlib import pyplot as plt

from _util import save_fig
from simulations import get_weightings, simulate_target_chain
from trial_descriptor import load_all_trial_descriptions


def moving_average(a, n=3) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


def target_seq_to_single_t_collected_seq(target_sequence):
    single_t_collected = np.zeros_like(target_sequence)
    single_t_collected[target_sequence == 0] = 1
    return single_t_collected


def get_comp_frac_seq(td):
    coll_types = td['coll_type'].to_numpy()
    coll_types[coll_types == 'single_p0'] = 1
    coll_types[coll_types == 'single_p1'] = 1
    coll_types[coll_types == 'joint0'] = 0
    coll_types[coll_types == 'joint1'] = 0
    return coll_types


if __name__ == '__main__':
    matplotlib.use('TkAgg')
    ma_window = 20

    s_fractions, s_fractions_var = list(), list()
    for w in get_weightings(40):
        target_sequence = simulate_target_chain(w, 0., int(1e5), False)
        single_t_collected = target_seq_to_single_t_collected_seq(target_sequence)
        single_t_collected = moving_average(single_t_collected, ma_window)
        s_fractions.append(single_t_collected.mean())
        s_fractions_var.append(single_t_collected.var())

    tds = load_all_trial_descriptions()
    comp_frac_seqs = [get_comp_frac_seq(td) for td in tds]
    dyad_s_fractions, dyad_s_fractions_var = list(), list()
    for comp_frac_seq in comp_frac_seqs:
        comp_frac_seq = moving_average(comp_frac_seq, ma_window)
        dyad_s_fractions.append(comp_frac_seq.mean())
        dyad_s_fractions_var.append(comp_frac_seq.var())

    plt.plot(s_fractions, s_fractions_var)
    plt.scatter(dyad_s_fractions, dyad_s_fractions_var)
    plt.show()
    save_fig()
