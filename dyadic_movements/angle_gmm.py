import numpy as np
import pandas as pd
import scipy.optimize

from dyadic_movements.movement_glm import cartesian_to_polar, get_von_mises_pdf
from load.data_loader import load, pair_idx_to_ids, get_rec_num_path, recordings_folder


def get_direction_helper():
    direction_helper = np.stack([[0, 1, 1, 1, 0, -1, -1, -1], [1, 1, 0, -1, -1, -1, 0, 1]]).T
    direction_helper_angles = cartesian_to_polar(direction_helper[:, 0], direction_helper[:, 1])[:, 1]
    return pd.DataFrame(dict(x=direction_helper[:, 0], y=direction_helper[:, 1], angle=direction_helper_angles))


direction_helper = get_direction_helper()


def get_target_positions(rec):
    return [rec[[f'{target}_x', f'{target}_y']].to_numpy() for target in ['comp', 'coop0', 'coop1']]


def get_agent_positions(rec, p_idx):
    return rec[[f'p{p_idx}_agent_x', f'p{p_idx}_agent_y']].to_numpy()


def get_on_target(rec, p_idx):
    agent_positions = get_agent_positions(rec, p_idx)
    target_positions = get_target_positions(rec)
    on_single = (0.1 > np.linalg.norm(agent_positions - target_positions[0], axis=1)).astype(int)
    on_joint0 = (0.1 > np.linalg.norm(agent_positions - target_positions[1], axis=1)).astype(int)
    on_joint1 = (0.1 > np.linalg.norm(agent_positions - target_positions[2], axis=1)).astype(int)
    return 0 < np.sum([on_single, on_joint0, on_joint1], axis=0)


def get_movement_angles(rec, p_idx):
    agent_positions = get_agent_positions(rec, p_idx)
    velocity = np.diff(agent_positions, axis=0)
    polar_velocity = cartesian_to_polar(velocity[:, 0], velocity[:, 1])
    speed = polar_velocity[:, 0] / 0.007
    angle = polar_velocity[:, 1]
    speed_too_low = speed < 0.02  # exclude no or low movements
    on_target = get_on_target(rec.iloc[:-1], p_idx)
    interpretable_movement_angles = 0 == on_target.astype(int) + speed_too_low.astype(int)
    initial_conditions = rec.iloc[:-1][interpretable_movement_angles].reset_index()#drop=True)
    return initial_conditions, angle[interpretable_movement_angles]


def get_angles_to_targets(initial_conditions, p_idx):
    angles_to_targets = list()
    for target in get_target_positions(initial_conditions):
        agent_positions = get_agent_positions(initial_conditions, p_idx)
        # target = initial_conditions[[f'{target}_x', f'{target}_y']].to_numpy()
        # agent_target_diff = agent - target
        agent_target_diff = target - agent_positions
        angles_to_targets.append(cartesian_to_polar(agent_target_diff[:, 0], agent_target_diff[:, 1])[:, 1])
    return np.array(angles_to_targets)


def pdf(w, pi, movement_angles, angles_to_targets):
    out = pi[None, :] @ get_von_mises_pdf(x=movement_angles, mu=angles_to_targets, kappa=w)
    print(out.shape)
    return out


def nll(*args):
    return -1 * np.sum(np.log(pdf(*args)))


def em_algo(org_rec, p_idx):
    initial_conditions, movement_angles = get_movement_angles(org_rec, p_idx)
    angles_to_targets = get_angles_to_targets(initial_conditions, p_idx)

    pi = np.ones(3) / 3
    w = 1e-6 * np.ones(1)
    movement_angles = np.ones(3)[:, None] @ movement_angles[None, :]  # (3, n)
    prev_pi = np.zeros(3)  # todo safe and load preprocessed data
    prev_w = np.zeros(1)
    while sum(abs(pi - prev_pi)) > 0.0001 or abs(w - prev_w) > 0.0001:
        print(pi)
        print(w)
        prev_pi = pi.copy()
        prev_w = w.copy()

        # M-step
        args = (pi, movement_angles, angles_to_targets)
        res = scipy.optimize.minimize(fun=nll, x0=w, args=args)
        w = res['x']

        # E-step
        Z = pi[:, None] * get_von_mises_pdf(x=movement_angles, mu=angles_to_targets, kappa=w)
        Z /= np.sum(Z, axis=0)
        pi = np.mean(Z, axis=1)
    return dict(
        pi=pi,
        Z=Z,
        w=w,
        res=res,
        predictions=get_predictions(Z, initial_conditions),
    )


def get_predictions(Z, initial_conditions):
    frame_amount = len(org_rec) * 2  # *2 because frames_per_second is 60
    predictions = np.nan * np.ones((frame_amount, 3))
    prev_pred = np.zeros(3)
    for i in range(frame_amount):
        pred = Z.T[initial_conditions['index'] == i // 2]
        if len(pred) == 1:
            prev_pred = pred[0].copy()
        predictions[i] = prev_pred.copy()
    return predictions


if __name__ == '__main__':
    # dyad_idx = 0
    dyad_idx = 24
    # p_idx = 0
    lap = 1
    # pair_idx = 12
    # pair_idx = 24
    org_rec = load(dyad_idx, frames_per_sec=60)[lap]

    predictions = np.array([
        em_algo(org_rec, p_idx=0)['predictions'],
        em_algo(org_rec, p_idx=1)['predictions'],
    ])

    player_ids, record_numbers = pair_idx_to_ids(dyad_idx)
    path = get_rec_num_path(player_ids[lap], record_numbers[lap], recordings_folder)
    np.save(f'{path}/continuous_prediction.npy', predictions)

