import pandas as pd
from matplotlib import pyplot as plt

from _util import save_fig
from glm.plotting.class_comparison import fontdict
from glm.plotting.type_hist import get_hist_plot_frame
from pred_performance import violin_swarm_mean_plot, generate_fst_bar, load_model_performances


model_selection = [  # full
    'effort',
    '1_prev_t',
    'invite',
    'effort_invite',
    'effort_1_prev_t',
    'effort_1_prev_t_invite',
]

# model_selection = [
#     'effort',
#     '1_prev_t',
#     'invite',
#     'effort_invite',
#     'effort_1_prev_t',
#     'effort_1_prev_t_invite',
# ]


xticklables = [
    'Predict nearest\njoint target\n$(w=0.0)$',
    'Predict always\nsingle target\n$(w=1.0)$',
    'Predict nearest\ntarget\n$(w=0.5)$',
    'Predict with\nsimulation derived\ndistance weighting\n$(w=w(\\Phi_{dyad}))$',
    'Distance GLM',
    'Previous target GLM',
    'Invite GLM',
    'Distance +\ninvite GLM',
    'Distance +\nprevious target\nGLM',
    'Distance +\ninvite +\nprevious target\nGLM',
]

if __name__ == '__main__':
    data = pd.read_pickle('performance.pkl')
    selected_model_accuracies = pd.DataFrame(data[model_selection].T['acc'].T.to_dict())
    size_scalar = 1.3
    fig, axes = plt.subplots(2, 1, figsize=(11 * size_scalar, 5 * size_scalar))

    w_dyad_acc = pd.read_pickle('../data/acc_for_dyads.pkl')

    unsorted_best_model = get_hist_plot_frame('unsorted_performance.pkl')
    best_model = unsorted_best_model.sort_values(by=['comp_frac']).set_index(pd.Series(list(range(len(unsorted_best_model)))))

    # todo merge into selected_model_accuracies
    selected_model_accuracies = pd.concat([w_dyad_acc['$w=0.0$ '],
                                           w_dyad_acc['$w=1.0$ '],
                                           w_dyad_acc['$w=0.5$ '],
                                           w_dyad_acc['$w=w_{dyad}$'],
                                           selected_model_accuracies], axis=1)
    violin_swarm_mean_plot(data=selected_model_accuracies, ax=axes[0])
    axes[0].set_xlabel('')
    axes[0].xaxis.set_ticks_position('none')
    axes[0].xaxis.set_tick_params(labelsize=fontdict['size'])
    axes[0].yaxis.set_tick_params(labelsize=fontdict['size'])
    axes[0].set_xticklabels(xticklables, fontdict=fontdict)
    # axes[0].set_xticklabels(axes[0].get_xticklabels(), fontdict=fontdict)
    axes[0].set_ylabel('Accuracy', fontdict=fontdict)
    generate_fst_bar(axes[1])
    fig.tight_layout()
    save_fig()
