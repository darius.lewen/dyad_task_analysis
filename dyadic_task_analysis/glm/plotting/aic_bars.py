import os
import sys

import pandas as pd
import seaborn as sb
import matplotlib as mpl
from matplotlib import rc

from matplotlib import pyplot as plt
from _util import fontdict, save_fig


def label_transform(key):
    effort = 'effort' in key
    hist = 'prev_t' in key
    invite = 'invite' in key
    bias = 'bias' in key
    hist_depth = 'n'
    for i in range(1, 5):
        if str(i) in key:
            hist_depth = str(i)

    def get_textcolor(active):
        if active:
            return r'\textcolor{black}'
        return r'\textcolor{red}'

    def get_connector(prev_active, next_active):
        return get_textcolor(prev_active and next_active) + r'{_}'

    return get_textcolor(effort) + r'{distance}' + get_connector(effort, invite) +\
        get_textcolor(invite) + r'{invite}' + get_connector(invite, hist) + \
        get_textcolor(hist) + f'{r"{"}{hist_depth}hist{r"}"}' + get_connector(hist, bias) + \
        get_textcolor(bias) + r'{bias}'


if __name__ == '__main__':
    rc('text', usetex=True)
    rc('text.latex', preamble='\\usepackage{color}')
    all_model_vals = pd.read_pickle('../performance.pkl')
    aic_vals = pd.DataFrame({key: val for key, val in all_model_vals.T['aic'].T.to_dict().items()})
    fig, axes = plt.subplots(5, 1, figsize=(8, 10), sharex='all')
    for dyad_idx, ax in enumerate(axes):
        to_plot = aic_vals.T[dyad_idx]
        # x_labels = [label_transform(key) for key in to_plot.keys()]
        # y_values = to_plot.values
        sb.barplot(x=list(to_plot.keys()), y=to_plot.values, ax=ax)
        ax.set_xticklabels(ax.get_xticklabels(), rotation=90, fontdict=dict(size=10))
        # ax.set_ylim(max(0, min(y_values) - 10), min(y_values) + 80)
    # axes[0].set_ylabel(x_labels[0])
    script_name = os.path.basename(sys.argv[0])
    plt.subplots_adjust(bottom=.35)
    plt.savefig(f'../plots/{script_name[:-3]}.png', dpi=300)
    # sb.barplot(a)
    # aic_heatmap = {key: aic_heatmap[key] for key in order}
    # for dyad_idx in range(58):
    #     all_model_vals
