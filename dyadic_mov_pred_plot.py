import numpy as np
import scipy.stats
from matplotlib import pyplot as plt

from dyadic_movements import get_mov_pred_dir
from load import data_loader
from load.data_loader import pair_idx_to_ids, get_rec_num_path


def get_normal_img(mean, cov):
    pred = scipy.stats.multivariate_normal(mean, cov)
    xlim = (0, 1)
    ylim = (0, 1)
    xres = 100
    yres = 100
    x = np.linspace(xlim[0], xlim[1], xres)
    y = np.linspace(ylim[0], ylim[1], yres)
    xx, yy = np.meshgrid(x, y)
    xxyy = np.c_[xx.ravel(), yy.ravel()]
    zz = pred.pdf(xxyy)
    img = zz.reshape((xres, yres))
    rgb = np.array((182, 35, 35)) / 255
    rgb = [np.ones_like(img) * c for c in rgb]
    rgba_img = np.stack(rgb + [img]).transpose((1, 2, 0))
    return rgba_img


if __name__ == '__main__':  # 120
    pair_idx = 38
    p_idx = 0
    p_ids, rec_num = pair_idx_to_ids(pair_idx)
    rec_path = get_rec_num_path(p_ids[1], rec_num[1], data_loader.recordings_folder)
    mov_pred_dir = get_mov_pred_dir(rec_path)
    mean_pred = np.load(f'{mov_pred_dir}/p{p_idx}_pred.npy')
    var_pred = np.load(f'{mov_pred_dir}/p{p_idx}_var_pred.npy')
    frame_idx = 300
    mean = mean_pred[frame_idx]
    cov = np.diag(var_pred[frame_idx] / 1000)  # todo variance prediction seam to be not working well - however - work on visuzlization first 0
    # get_normal_img(mean, cov)

    fig, axes = plt.subplots(1, 3)
    axes[0].plot(np.arange(len(var_pred[:, 0])), var_pred[:, 0])
    axes[0].plot(np.arange(len(var_pred[:, 0])), var_pred[:, 1])
    # axes[1].imshow(img)
    axes[2].imshow(get_normal_img(mean, cov))

    # plt.imshow(img)
    # plt.show()

