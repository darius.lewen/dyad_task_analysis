import matplotlib
from matplotlib.dates import DateFormatter, MinuteLocator
from matplotlib.ticker import AutoMinorLocator, FixedLocator, FixedFormatter

from _util import save_fig
from fst_convergence import split_tds_at_minute
from presentation.plotting import get_comp_frac_color, violin_swarm, cmap, scatterplot, diagonal_r_and_p_text, \
    r_and_p_text, reg_plot

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt, ticker
from matplotlib.gridspec import GridSpec
import seaborn as sn

from conv_visualized import load_frame_count_corrected_tds, get_fst_ma_series
from trial_descriptor import get_comp_frac, load_all_trial_descriptions, unsorted_load_all_trial_descriptions

matplotlib.use('TkAgg')


if __name__ == '__main__':
    fig_width = 7
    fig_ratio = 9 / 16
    fig = plt.figure(figsize=(fig_width, fig_width * fig_ratio))
    grid_spec = GridSpec(2, 2, width_ratios=[2.5, 1], height_ratios=[1, 1],
                         right=.96, top=.94, bottom=.18, wspace=.8, hspace=.5)# , bottom=.28, right=.98, left=.12, top=.94)  # , wspace=1, hspace=1)
    axes = np.array([
        fig.add_subplot(grid_spec[0, 0]),
        fig.add_subplot(grid_spec[0, 1]),
        fig.add_subplot(grid_spec[1, 0]),
        fig.add_subplot(grid_spec[1, 1]),
    ])

    # ax1
    frame_count_corrected_tds = load_frame_count_corrected_tds()
    tds = load_all_trial_descriptions()
    fps = 120
    window = 60  # seconds
    # window = 30  # seconds
    x_values = np.arange(window, 2 * 20 * 60)
    # x_values = np.array(list(range(window, 2 * 20 * 60 * fps, fps))) / fps
    # x_values = (x_values + window // 2) / fps  # to shift to mid and to seconds
    x_dates = pd.to_datetime(x_values, unit='s')
    ma_series = [get_fst_ma_series(td, w=window * fps) for td in frame_count_corrected_tds]
    comp_fractions = [get_comp_frac(td) for td in tds]
    for dyad_idx in [3, 33, 43]:
        linewidth = 1.2
        # sn.lineplot(x=x_dates, y=ma_series[dyad_idx],  # todo disabled
        #             color='k', linewidth=linewidth * 1.4,
        #             ax=axes[0])
        print(comp_fractions[dyad_idx])
        sn.lineplot(x=x_dates, y=ma_series[dyad_idx],
                    color=get_comp_frac_color(comp_fractions[dyad_idx]),
                    linewidth=linewidth, alpha=.8,
                    ax=axes[0])

    # ax2
    to_plot = pd.DataFrame(dict(comp_fractions=comp_fractions))
    violin_swarm(to_plot, ax=axes[1], marker_size=4.4)

    # ax3
    plot_ax3 = True
    if plot_ax3:
        ma_conv_series = [abs(ma_s - comp_frac) for ma_s, comp_frac in zip(ma_series, comp_fractions)]
        to_plot = pd.DataFrame(dict(time=np.concatenate([x_dates for _ in range(58)]),
                                    ma_series=np.concatenate(ma_conv_series)))
        sn.lineplot(to_plot, x='time', y='ma_series', ax=axes[2], color='k', alpha=.8, err_kws=dict(edgecolor=None),
                    legend=True, label='Cross-dyad mean')
        axes[2].collections[0].set_label('Confidence interval')
        axes[2].legend(frameon=False, fontsize=10)

    # ax4
    initial_tds = split_tds_at_minute(split_minute=1)[0]
    final_tds = split_tds_at_minute(split_minute=10)[1]
    comp_frac_sorting = np.argsort([get_comp_frac(td) for td in unsorted_load_all_trial_descriptions()])
    to_plot = pd.DataFrame(dict(
        initial_comp_fractions=np.array([get_comp_frac(td) for td in initial_tds])[comp_frac_sorting],
        final_comp_fractions=np.array([get_comp_frac(td) for td in final_tds])[comp_frac_sorting],
    ))
    scatterplot(data=to_plot, x='initial_comp_fractions', y='final_comp_fractions', hue='final_comp_fractions',
                ax=axes[3])
    diagonal_r_and_p_text(x=to_plot['initial_comp_fractions'], y=to_plot['final_comp_fractions'], ax=axes[3], include_p=False)
    # r_and_p_text(x=to_plot['initial_comp_fractions'], y=to_plot['final_comp_fractions'], ax=axes[3],
    #              text_pos=(.015, .985), ha='left', va='top')
    # reg_plot(x=to_plot['initial_comp_fractions'], y=to_plot['final_comp_fractions'], ax=axes[3])
    sn.lineplot(x=[-1, 2], y=[-1, 2], c='k', ax=axes[3], linewidth=matplotlib.rcParams['axes.linewidth'])
    # axes operations
    comp_frac_locator = FixedLocator([0, 1/3, 2/3, 1])
    comp_frac_formatter = FixedFormatter(['0', '1/3', '2/3', '1'])

    for ax in axes[[0, 2]]:
        ax.set_xlim([0, pd.to_datetime(2400, unit='s')])
        ax.xaxis.set_minor_locator(AutoMinorLocator(10))
        ax.xaxis.set_major_locator(MinuteLocator(interval=10))
        ax.xaxis.set_major_formatter(DateFormatter('%M'))

    for ax in axes[[0, 1, 3]]:
        ax.set_ylim([-.05, 1.05])
        ax.yaxis.set_major_locator(comp_frac_locator)
        ax.yaxis.set_major_formatter(comp_frac_formatter)

    axes[0].set_ylim([-.015, 1.015])  # todo adapt to linewidth

    # axes[2].set_ylim([-.05, 1/3 + .05])

    axes[1].set_xticklabels(['All dyads'])

    axes[2].set_ylim([-.015, .33])
    axes[2].yaxis.set_major_locator(FixedLocator([0., .1, .2, .3]))
    axes[2].yaxis.set_major_formatter(FixedFormatter(['0.0', '0.1', '0.2', '0.3']))

    axes[3].set_xlim([-.05, 1.05])
    axes[3].xaxis.set_major_locator(comp_frac_locator)
    axes[3].xaxis.set_major_formatter(comp_frac_formatter)
    axes[3].set_box_aspect(1)

    axes[0].set_xlabel('Time (min)')
    axes[0].set_ylabel('1-Minute moving\naverage FST')
    axes[1].set_xlabel('')
    axes[1].set_ylabel('Stable FST')
    axes[2].set_xlabel('Time (min)')
    axes[2].set_ylabel('Deviation from\nstable FST')
    axes[3].set_xlabel('First-minute FST')
    axes[3].set_ylabel('Stable FST')

    for ax_label, ax in zip('acbd', axes):
        ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes, weight='bold')#, size=fontdict['size']) #, weight='bold')

    save_fig(sub_folder='presentation/', format='pdf')
    save_fig(sub_folder='presentation/', format='svg')

