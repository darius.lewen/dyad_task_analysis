import os
import time

import seaborn as sn
from multiprocessing import Process, Queue

from matplotlib import pyplot as plt
from numba import jit
import numpy as np

import pkg_resources

from _util import game_width_in_cm, save_fig, fontdict

# Use exactly this packages - do not change this!
# Scipy is required because its optimized implementations are implicitly used by numba
pkg_resources.require('numba==0.56.4')
pkg_resources.require('numpy==1.21.6')
pkg_resources.require('scipy==1.9.3')

results_dir = __file__[:-15] + '/sim_results'


@jit
def get_efforts(p0, p1, targets):
    comp, coop0, coop1 = targets
    p0_comp = np.linalg.norm(p0 - comp)
    p1_comp = np.linalg.norm(p1 - comp)
    p0_coop0 = np.linalg.norm(p0 - coop0)
    p1_coop0 = np.linalg.norm(p1 - coop0)
    p0_coop1 = np.linalg.norm(p0 - coop1)
    p1_coop1 = np.linalg.norm(p1 - coop1)
    effort_comp = min(p0_comp, p1_comp)
    effort_coop0 = max(p0_coop0, p1_coop0)
    effort_coop1 = max(p0_coop1, p1_coop1)
    return np.array([effort_comp, effort_coop0, effort_coop1])


@jit
def get_weighted_efforts(efforts, weighting):
    comp_effort, coop0_effort, coop1_effort = efforts
    weighted_comp_effort = comp_effort * (1 - weighting)
    weighted_coop0_effort = coop0_effort * weighting
    weighted_coop1_effort = coop1_effort * weighting
    return np.array([weighted_comp_effort, weighted_coop0_effort, weighted_coop1_effort])


@jit
def get_expected_min_effort(player_pos, targets, weighting, n):
    comp, coop0, coop1 = targets.copy()
    positions = np.array([[x, y] for x in range(n) for y in range(n)]) / n
    expected_min_effort = 0
    for i, new_comp in enumerate(positions):
        targets = np.vstack((new_comp, coop0, coop1))
        efforts = get_efforts(p0=comp, p1=player_pos, targets=targets)
        weighted_efforts = get_weighted_efforts(efforts, weighting)
        expected_min_effort += np.min(weighted_efforts) / len(positions)
    return expected_min_effort


@jit
def get_expected_min_effort_heatmap(targets, weighting=.5, res=10):
    heatmap = np.zeros((res, res))
    positions = np.array([[x, y] for x in range(res) for y in range(res)])
    for player_pos in positions:
        heatmap[player_pos[0], player_pos[1]] = get_expected_min_effort(player_pos / res, targets, weighting, res)
    return heatmap


@jit
def get_min_expected_effort_pos(targets, w_ratio=1, res=10):
    heatmap = get_expected_min_effort_heatmap(targets, w_ratio, res)
    return np.argwhere(heatmap == heatmap.min())[0] / res


@jit
def get_next_target_idx(weighted_efforts, non_opti_frac):
    sorted_indices = np.argsort(weighted_efforts)
    if np.random.random() > non_opti_frac:
        return sorted_indices[0]
    rand_idx = 1 if np.random.random() > .5 else 2
    return sorted_indices[rand_idx]


@jit
def get_next_p_positions(collected_idx, targets, w_ratio, path_shortening):
    p0, p1 = targets[collected_idx].copy(), targets[collected_idx].copy()
    if path_shortening and collected_idx == 0:
        p1 = get_min_expected_effort_pos(targets, w_ratio)
    return p0, p1


@jit
def simulate_target_chain(weighting, sub_optimality, n, path_shortening):
    comp = np.random.rand(2)
    coop0 = np.random.rand(2)
    coop1 = np.random.rand(2)
    targets = np.vstack((comp, coop0, coop1))
    target_sequence = np.zeros(n)
    p0 = np.random.rand(2)
    p1 = np.random.rand(2)
    for i in range(n):
        efforts = get_efforts(p0, p1, targets)
        weighted_efforts = get_weighted_efforts(efforts, weighting)
        next_target_idx = get_next_target_idx(weighted_efforts, sub_optimality)
        target_sequence[i] = next_target_idx
        p0, p1 = get_next_p_positions(next_target_idx, targets, weighting, path_shortening)
        targets[next_target_idx] = np.random.rand(2)
    return target_sequence


@jit
def simulate(weighting, sub_optimality, n, path_shortening):
    comp = np.random.rand(2)
    coop0 = np.random.rand(2)
    coop1 = np.random.rand(2)
    targets = np.vstack((comp, coop0, coop1))
    target_count = np.zeros(3)
    p0 = np.random.rand(2)
    p1 = np.random.rand(2)
    total_min_effort = 0.
    for _ in range(n):
        efforts = get_efforts(p0, p1, targets)
        weighted_efforts = get_weighted_efforts(efforts, weighting)
        next_target_idx = get_next_target_idx(weighted_efforts, sub_optimality)
        target_count[next_target_idx] += 1
        total_min_effort += efforts[next_target_idx]
        p0, p1 = get_next_p_positions(next_target_idx, targets, weighting, path_shortening)
        targets[next_target_idx] = np.random.rand(2)
    target_fraction = target_count / n
    avg_min_effort = total_min_effort / n
    avg_min_effort -= 0.05  # remove target radius
    return target_fraction, avg_min_effort


@jit
def run_simulations(weightings, iterations, non_opti_frac, path_shortening):
    out = np.zeros((2, len(weightings)))
    for i, weigthing in enumerate(weightings):
        target_fraction, avg_distance = simulate(weigthing,
                                                 sub_optimality=non_opti_frac,
                                                 n=iterations,
                                                 path_shortening=path_shortening)
        out[0, i] = target_fraction[0]
        out[1, i] = avg_distance
    return out


def run_simulations_wrapper(queue, simulation_args):
    out = run_simulations(*simulation_args)
    if queue is None:
        return out
    else:
        queue.put(out)


def multiprocess_sim(simulation_args, process_amount=2):
    t0 = time.time()
    queue = Queue()
    processes = list()
    results = list()
    for _ in range(process_amount):
        processes.append(Process(target=run_simulations_wrapper,
                                 args=(queue, simulation_args)))
    for p in processes:
        p.start()
    for p in processes:
        p.join()
    while not queue.empty():
        results.append(queue.get())
    print(f'multiprocess_calc needed {time.time() - t0}')
    return sum(results) / process_amount


def get_weightings(weight_amount, eps=1e-6):
    return np.linspace(eps, 1 - eps, weight_amount)


def calc_curves(process_amount, weight_amount=40, iterations=10**6):
    weightings = get_weightings(weight_amount)
    non_opti_fractions = [.0, .1, .3, .0]
    path_shortening = [False, False, False, True]
    simulation_names = ['naive_0', 'naive_10', 'naive_30', 'path_shortening']
    non_opti_fractions = [.0]  # todo Debug or to recalculate only one curve
    path_shortening = [False]
    simulation_names = ['naive_0']
    for sim_name, non_opti_frac, ps in zip(simulation_names, non_opti_fractions, path_shortening):
        result = multiprocess_sim(simulation_args=(weightings, iterations, non_opti_frac, ps),
                                  process_amount=process_amount)
        np.save(f'{results_dir}/{sim_name}.npy', result)


def load_results():
    return {str(file)[:-4]: np.load(f'{results_dir}/{file}') for file in os.listdir(results_dir)}


def get_sim_result_for_name(name):
    naive_w_comp_fractions = load_results()[name][0]
    return naive_w_comp_fractions, get_weightings(weight_amount=len(naive_w_comp_fractions))


def plot_results(results):
    fig, axes = plt.subplots(1, 2, figsize=(8, 4))
    for name, result in results.items():
        sn.lineplot(x=result[0], y=result[1] * game_width_in_cm, ax=axes[0])
        sn.scatterplot(x=result[0], y=get_weightings(weight_amount=len(result[0])), ax=axes[1], label=name)
    axes[1].legend()
    axes[0].set_ylabel('Average distance', fontdict=fontdict)
    axes[1].set_ylabel('Target weighting ratio', fontdict=fontdict)
    for ax in axes:
        ax.set_xlabel('Fraction of\nsingle targets collected', fontdict=fontdict)
    plt.tight_layout()
    save_fig()


def jit_execution_time(func, args):
    t0 = time.time()
    func(*args)
    t1 = time.time()
    func(*args)
    t2 = time.time()
    print(f'compilation time: {t1-t0}')
    print(f'execution time: {t2-t1}')


def calc_var(iterations=200, n=1000):
    values = list()
    for i in range(iterations):
        target_fraction, avg_min_effort = simulate(weighting=.5,
                                                   sub_optimality=0.,
                                                   n=n, path_shortening=False)
        values.append(avg_min_effort)
    return np.var(values) * game_width_in_cm


if __name__ == '__main__':
    result_variance = calc_var()
    # calc_curves(process_amount=2)

    # results = load_results()
    # plot_results(results)

