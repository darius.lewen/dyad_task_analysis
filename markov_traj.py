from collections import Counter

import matplotlib
import numpy as np
import seaborn as sn
import networkx as nx
from matplotlib import cm
from matplotlib import pyplot as plt

from _util import save_fig, convert_trial_classes_to_colors
from trial_descriptor_2 import load_all_trial_descriptions, get_comp_frac


def simplify_trial_class(trial_class):
    if trial_class == 'accepted_coop0_invite' or trial_class == 'accepted_coop1_invite':
        return 'accepted_invite'
    elif trial_class == 'declined_coop0_invite' or trial_class == 'declined_coop1_invite':
        return 'declined_invite'
    elif trial_class == 'p0_ahead' or trial_class == 'p1_ahead':
        return 'ahead'
    else:
        return trial_class


if __name__ == '__main__':
    tds = load_all_trial_descriptions()
    comp_fractions = [get_comp_frac(td) for td in tds]

    tds = tds[:-15]
    comp_fractions = comp_fractions[:-15]

    trial_classes = [td['trial_class'].to_numpy() for td in tds]
    trial_classes = [simplify_trial_class(tc) for tc in np.concatenate(trial_classes)]

    # nodes = list(set(trial_classes))
    # nodes = ['anti_corr', 'concurrent', 'miscoordination', 'accepted_invite', 'ahead', 'declined_invite']
    nodes = ['miscoordination', 'anti_corr', 'declined_invite', 'accepted_invite', 'concurrent', 'ahead', ]
    transition_counts = {(tc1, tc2): 0 for tc1 in nodes for tc2 in nodes}

    for prev_tc, next_tc in zip(trial_classes[:-1], trial_classes[1:]):
        transition_counts[(prev_tc, next_tc)] += 1

    class_count = Counter(trial_classes)
    transition_probs = {key: val / class_count[key[0]] for key, val in transition_counts.items()}
    class_probs = {key: val / len(trial_classes) for key, val in class_count.items()}

    cond_transition_probs = {key: transition_probs[key] - class_probs[key[1]] for key in transition_probs.keys()}

    G = nx.MultiDiGraph()

    for node_start in nodes:
        print(node_start)
        for node_end in nodes:
            length = None if node_start != node_end else 200
            G.add_edge(node_start, node_end, weight=transition_probs[node_start, node_end], length=length)

    pos = nx.shell_layout(G)
    scalar = 3.2
    fig, ax = plt.subplots(figsize=(1 * scalar, 1 * scalar))
    plt.subplots_adjust(top=1, bottom=.24)
    node_colors = [convert_trial_classes_to_colors[simplify_trial_class(tc)] for tc in nodes]
    nx.draw_networkx_nodes(G, pos, node_size=380, edgecolors='black', node_color=node_colors)
    # nx.draw_networkx_labels(G, pos)

    non_self_loops = [(u, v) for u, v in G.edges() if u != v]
    self_loops = [(u, v) for u, v in G.edges() if u == v]

    edges = nx.draw_networkx_edges(G, pos, ax=ax, connectionstyle=f'arc3, rad = {.22}',
                                   edge_cmap=cm.Greys, width=2,
                                   edge_color=[G[edge[0]][edge[1]][0]['weight'] for edge in G.edges], arrows=True)

    edge_orders = np.array([G[nodes[0]][nodes[1]][0]['weight'] for nodes in G.edges])
    for edge, edge_order in zip(edges, edge_orders):
        edge.set_zorder(edge_order)

    pc = matplotlib.collections.PatchCollection(edges, cmap=cm.Greys)

    transition_to_acc_invite_counts = dict()
    transition_to_dec_invite_counts = dict()
    for key, value in transition_counts.items():
        if key[1] == 'accepted_invite':
            transition_to_acc_invite_counts[key] = value
        if key[1] == 'declined_invite':
            transition_to_dec_invite_counts[key] = value

    transition_to_acc_invite_probs = {key[0]: val / sum(transition_to_acc_invite_counts.values())
                                      for key, val in transition_to_acc_invite_counts.items()}

    print(f'Percentage of passive invites: {transition_to_acc_invite_probs["anti_corr"] + transition_to_acc_invite_probs["declined_invite"]}')
    print(f'Percentage of active invites: {transition_to_acc_invite_probs["concurrent"] + transition_to_acc_invite_probs["ahead"]}')

    ax = plt.gca()
    ax.set_axis_off()
    ax.set_box_aspect(.9)
    cbar = plt.colorbar(pc, ax=ax, fraction=.014, location='bottom')
    cbar.set_label('Transition\nfrequency', labelpad=2)
    plt.show()

    save_fig(format='svg')

