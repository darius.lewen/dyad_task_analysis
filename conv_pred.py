import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy

from _util import save_fig
from trial_classes import classes, colors, fontdict
from trial_descriptor import load_all_trial_descriptions, get_comp_frac
from reward_decomposition import scatter, plot_corr_coef

def get_class_amount(td, c):
    if 'miscoordination' in c:
        return get_class_amount_base(td, 'anti_corr') + get_class_amount_base(td, 'miscoordination')
    elif 'ahead' in c:
        return get_class_amount_base(td, 'p0_ahead') + get_class_amount_base(td, 'p1_ahead')
    elif 'invite' not in c:
        return get_class_amount_base(td, c)
    elif 'accepted' in c:
        return get_class_amount_base(td, 'accepted_coop0_invite') + get_class_amount_base(td, 'accepted_coop1_invite')
    else:
        return get_class_amount_base(td, 'declined_coop0_invite') + get_class_amount_base(td, 'declined_coop1_invite')


def get_class_amount_base(td, c):
    class_counts = td['trial_class'].value_counts()
    if c in class_counts:
        return class_counts[c] / len(td)
    return 0


def get_total_class_counts(tds, classes):
    return {c: np.sum([get_class_amount(td, c) for td in tds]) for c in classes}


def get_class_counts(tds, classes):
    return {c: np.array([get_class_amount(td, c) for td in tds]) for c in classes}


def get_first_half(td, t=120*10*60):
    for i, start_idx in enumerate(td['start'].to_numpy()):
        if start_idx > t:
            return td.iloc[:i]


def get_initial_tds(t=120*10*60):
    init_tds = load_all_trial_descriptions(cut_initial=False)
    init_tds = [get_first_half(td, t=t) for td in init_tds]
    return init_tds


def predict(w, init_comp_frac, init_class_count):
    input = np.array([init_comp_frac] + list(init_class_count.values()))
    pred_comp_frac = w @ input
    return pred_comp_frac


def mse(w, init_comp_frac, init_class_count, comp_frac):
    pred_comp_frac = predict(w, init_comp_frac, init_class_count)
    return np.mean((pred_comp_frac - comp_frac) ** 2)


if __name__ == '__main__':
    init_tds = get_initial_tds(5*120*60)
    init_class_counts = get_total_class_counts(init_tds, classes)
    init_comp_fractions = np.array([get_comp_frac(td) for td in init_tds])

    tds = load_all_trial_descriptions()
    class_counts = get_total_class_counts(tds, classes)
    comp_fractions = np.array([get_comp_frac(td) for td in tds])

    scalar = .7
    fig, axes = plt.subplots(2, 4, figsize=(14 * scalar, 7 * scalar))
    axes = axes.flatten()

    scatter(comp_fractions, init_comp_fractions, axes[0])

    to_plot = pd.DataFrame(dict(inital=init_class_counts, final=class_counts), index=classes)
    to_plot.plot.bar(ax=axes[1])

    init_dyad_class_counts = get_class_counts(init_tds, classes)
    args = (init_comp_fractions, init_dyad_class_counts, comp_fractions)
    res = scipy.optimize.minimize(mse, np.zeros(len(classes) + 1), args)
    w = res['x']

    pred_comp_frac = predict(w, init_comp_fractions, init_dyad_class_counts)#init_comp_fractions - comp_fractions

    axes[-1].axis('off')
    save_fig()
