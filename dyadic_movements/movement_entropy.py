import numpy as np
import scipy
from matplotlib import pyplot as plt

from movement_glm import get_mov_pred_dir, rec_path, cartesian_to_polar

if __name__ == '__main__':
    mov_pred_dir = get_mov_pred_dir(rec_path)
    pair_idx = 38
    predictions = np.load(f'{mov_pred_dir}/p{0}_pred.npy')
    n = 10000
    speed_mean = predictions[3000:n, 0]
    speed_precision = predictions[3000:n, 1]
    angle_mean = predictions[3000:n, 2]
    angle_kappa = predictions[3000:n, 3]

    # speed_mean, speed_precision, angle_mean, angle_kappa = predicitons
    speed_pdf = lambda x: scipy.stats.beta.pdf(x=x, a=speed_mean * speed_precision, b=1 - speed_mean)
    angle_pdf = lambda x: np.exp(angle_kappa * np.cos(x - angle_mean)) / (
                2 * np.pi * scipy.special.i0(angle_kappa))  # scipy implementation throws inf values..
    joint_pdf = lambda x: speed_pdf(x[:, :, 0]) * angle_pdf(x[:, :, 1])  # assuming independence...

    xlim = (-1, 1)
    ylim = (-1, 1)
    xres = 100
    yres = 100
    x = np.linspace(xlim[0], xlim[1], xres)
    y = np.linspace(ylim[0], ylim[1], yres)[::-1]
    xx, yy = np.meshgrid(x, y)
    xxyy = np.c_[xx.ravel(), yy.ravel()]
    polar_xxyy = cartesian_to_polar(xxyy[:, 0], xxyy[:, 1])
    zz = joint_pdf(polar_xxyy[:, None, :])
    rectangle_area = (sum(abs(np.array(xlim))) / xres) ** 2
    pdf = zz * rectangle_area
    entropy = lambda pdf: -1 * np.sum(pdf * np.log2(pdf, where=pdf > 0), axis=0)
    e = entropy(pdf)
    plt.plot(np.arange(len(e)), e)


