import pandas as pd
import seaborn as sn

from matplotlib import pyplot as plt, ticker

from _util import save_fig
from glm.plotting.performance_plot import get_best_model_count
from glm.util import order
from reward_decomposition import scatter, get_modified_comp_fractions, get_agent_rewards, get_mean_in_cm
from trial_descriptor import load_all_trial_descriptions, get_comp_frac

fontdict = dict(size=10)

if __name__ == '__main__':
    ax_width = 5
    ax_height = ax_width * .64
    rows, cols = 2, 2
    fig, axes = plt.subplots(rows, cols, figsize=(ax_width * cols, ax_height * rows))
    axes = axes.flatten()

    tds = load_all_trial_descriptions()
    comp_fractions = [get_comp_frac(td) for td in tds]
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)
    reward_diff = high_rewards - low_rewards

    chosen_effort = get_mean_in_cm('chosen_effort', tds)
    best_model_count = get_best_model_count()
    best_model_row = [None for _ in range(len(comp_fractions))]
    for key, val in best_model_count.items():
        for dyad_idx in val:
            best_model_row[dyad_idx] = key

    df = pd.DataFrame(dict(comp_frac=comp_fractions, reward=agent_mean_reward, reward_diff=reward_diff,
                           model=best_model_row, chosen_effort=chosen_effort))
    df['model'] = pd.Categorical(df['model'], order)

    sn.histplot(df, x='model', ax=axes[0], hue='model', hue_order=order, palette='colorblind', alpha=1, legend=False)
    sn.boxplot(df, x='comp_frac', y='model', ax=axes[1], order=order, palette='colorblind')
    sn.boxplot(df, x='reward', y='model', ax=axes[2], order=order, palette='colorblind')
    sn.boxplot(df, x='reward_diff', y='model', ax=axes[3], order=order, palette='colorblind')

    axes[0].set_xticklabels(axes[0].get_xticklabels(), rotation=-30, horizontalalignment='left', fontdict=fontdict)
    axes[0].set_xlabel('Best fitting model', fontdict=fontdict)
    axes[1].set_xlabel('Fraction of single targets collected $\\Phi$', fontdict=fontdict)
    axes[2].set_xlabel('Agent-mean payoff', fontdict=fontdict)
    axes[3].set_xlabel('Inter-agent payoff difference', fontdict=fontdict)

    axes[1].set_ylabel('Best fitting model', fontdict=fontdict)
    axes[2].set_ylabel('Best fitting model', fontdict=fontdict)
    axes[3].set_ylabel('Best fitting model', fontdict=fontdict)

    comp_frac_locator = ticker.FixedLocator([0, 1/3, 2/3, 1])
    comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1'])
    axes[1].xaxis.set_major_locator(comp_frac_locator)
    axes[1].xaxis.set_major_formatter(comp_frac_formatter)
    axes[3].xaxis.set_major_locator(ticker.FixedLocator([0, 3, 6, 9, 12]))
    axes[3].xaxis.set_major_formatter(ticker.FixedFormatter(['0', '3', '6', '9', '12']))

    for ax_label, ax in zip('ABCD', axes):
        ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes, size=14)

    plt.subplots_adjust(left=.2, wspace=1, hspace=.95, right=.97)

    save_fig()
