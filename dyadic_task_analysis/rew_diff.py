import matplotlib
import numpy as np
import seaborn as sn
from matplotlib import pyplot as plt
from matplotlib.gridspec import GridSpec
from scipy.stats import pearsonr

from _util import save_fig, fontdict
from inefficiencies_full import comp_frac_x_format
from reward_decomposition import scatter
from trial_descriptor import load_all_trial_descriptions, get_comp_frac


def get(df, key):
    return None if key not in df.keys() else df[key][0]


def get_coll_count(td):
    return td[['coll_type']].apply(lambda x: x.value_counts()).transpose()


def get_skill_differences(tds):
    skill_differences = list()
    for td in tds:
        coll_count = get_coll_count(td)
        p0_single = get(coll_count, 'single_p0')
        p1_single = get(coll_count, 'single_p1')
        if p0_single is None or p1_single is None:
            skill_differences.append(None)
        else:
            skill_differences.append(abs(p0_single - p1_single) / (p0_single + p1_single))
    return skill_differences


def plot_skill_diff_curve_old(comp_fractions, coll_counts, mean_skill_diff, deg , ax):
    step = 1/2 - 1/3
    comp_frac_for_s_diff = [1/3, 1/3 + step, 1/3 + step * 2, 1/3 + step * 3, 1/3 + step * 4]
    s_diff_indices = [np.argmin(abs(comp_fractions - diff)) for diff in comp_frac_for_s_diff]
    p, residuals, _, _, _ = np.polyfit(comp_fractions, coll_counts, deg=deg, full=True)
    curve = np.polyval(p, comp_fractions)
    mid_points = [(comp_fractions[i], curve[i]) for i in s_diff_indices]
    skill_diffs = [mean_skill_diff * mid_point[0] * mid_point[1] for mid_point in mid_points]
    lowers = [mid_point[1] - skill_diff / 2 for mid_point, skill_diff in zip(mid_points, skill_diffs)]
    highers = [mid_point[1] + skill_diff / 2 for mid_point, skill_diff in zip(mid_points, skill_diffs)]
    sn.lineplot(x=comp_fractions, y=curve, ax=ax)
    ax.vlines(x=comp_frac_for_s_diff, ymin=lowers, ymax=highers)


def plot_skill_diff_curve(comp_fractions, coll_counts, skill_differences, deg , ax):
    p, residuals, _, _, _ = np.polyfit(comp_fractions, coll_counts, deg=deg, full=True)
    mean_r_curve = np.polyval(p, comp_fractions)
    quartiles = np.quantile(skill_differences, [.25, .5, .75])
    # sn.lineplot(x=comp_fractions, y=mean_r_curve * .035, ax=ax, color='k', label='$S_\\Delta = 0$')
    ax.plot(comp_fractions, mean_r_curve * .035, color='k', label='$S_\\Delta = 0$')
    labels = [
        '$0 < S_\\Delta < Q_{25}$',
        '$Q_{25} < S_\\Delta < Q_{50}$',
        '$Q_{50} < S_\\Delta < Q_{75}$',
    ]
    for label, quartile in zip(labels, quartiles):
        skill_diffs = quartile * mean_r_curve * comp_fractions
        lower = mean_r_curve - skill_diffs / 2
        higher = mean_r_curve + skill_diffs / 2
        ax.fill_between(comp_fractions, lower * .035, higher * .035, color='k', alpha=.1, label=label, edgecolor=None)
    ax.legend(loc='upper left', bbox_to_anchor=(1.08, 1.05), frameon=False, fontsize=12)


def filter_low_x(xs, ys):
    xs_new, ys_new = list(), list()
    for x, y in zip(xs, ys):
        # print(x)
        if x > .05:
            xs_new.append(x)
            ys_new.append(y)
    return xs_new, ys_new


def print_first_error():
    coll_counts = [get_coll_count(td) for td in load_all_trial_descriptions()]
    errors = list()
    jt_shares = list()
    jt_reward_diff = list()
    st_reward_diff = list()
    reward_differences = list()
    for count in coll_counts:
        joint0, joint1 = 0, 0
        single_p0, single_p1 = 0, 0
        if 'joint0' in count.keys():
            joint0 = int(count['joint0']) / 2  # two blocks
        if 'joint1' in count.keys():
            joint1 = int(count['joint1']) / 2
        if 'single_p0' in count.keys():
            single_p0 = int(count['single_p0']) / 2
        if 'single_p1' in count.keys():
            single_p1 = int(count['single_p1']) / 2
        st_impact = (single_p0 - single_p1) * 0.07
        jt_impact = (joint0 - joint1) * 0.03
        jt_share = abs(jt_impact) / (abs(st_impact) + abs(jt_impact) + 0.00001)
        rew_diff = st_impact + jt_impact
        jt_shares.append(jt_share)
        jt_reward_diff.append(abs(jt_impact))
        st_reward_diff.append(abs(st_impact))
        reward_differences.append(abs(rew_diff))
        errors.append(abs(rew_diff) - abs(st_impact))
    # plt.scatter(reward_differences, st_reward_diff)
    # plt.scatter(reward_differences, jt_reward_diff)
    print('error')
    print(f'Mean:\t{np.mean(errors)}')
    print(f'Var:\t{np.var(errors)}')  # todo refactor


def print_second_error():



if __name__ == '__main__':
    matplotlib.rcParams['text.usetex'] = True
    fig = plt.figure(figsize=(11.4, 3))
    grid_spec = GridSpec(1, 4, width_ratios=[1, 1, 1/10, 1], height_ratios=[1])
    axes = list()
    for i in range(4):
        axes.append(fig.add_subplot(grid_spec[i]))

    print_first_error()
    print_second_error()

    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    coll_counts = np.array([get_coll_count(td).to_numpy().sum() for td in tds])
    coll_counts = coll_counts / 2  # for the two blocks
    scatter(x=comp_fractions, y=coll_counts * .035, ax=axes[0])

    p, residuals, _, _, _ = np.polyfit(comp_fractions, coll_counts, deg=7, full=True)
    curve = np.polyval(p, comp_fractions)
    sn.lineplot(x=comp_fractions, y=curve * .035, ax=axes[0], c='k')

    skill_differences = get_skill_differences(tds)
    scatter(*filter_low_x(comp_fractions, skill_differences), ax=axes[1])
    corr_coefficient, p_value = pearsonr(*filter_low_x(comp_fractions, skill_differences))
    corr_coefficient = "%.2f" % corr_coefficient
    p_value = "%.2f" % p_value
    # r_val_approx = r'$r\approx$'
    # ax.text(.05, .95, f'{r_val_approx}{corr_coefficient_str}\n{p_smaller_than(p_value)}',  # x=.05, y=.95 for upper left  -- .71, .16 --- .69, .178,
    #         transform=ax.transAxes, fontsize=14, verticalalignment='top', bbox=dict(boxstyle='round', alpha=.5))
    axes[1].text(.98, .8, f'$r\\approx{corr_coefficient}$\n $p\\approx{p_value}$', transform=axes[1].transAxes, size=10,
                 ha='right')

    axes[1].fill_between([-1, 0.05], -1, 1, color='gray', alpha=.3, edgecolor=None, label='missing data')
    axes[1].legend(loc='upper left', bbox_to_anchor=(.12, 1.33), frameon=False, fontsize=fontdict['size'])
    axes[1].set_ylim([-0.04, .64])
    axes[2].set_ylim([-0.04, .64])
    axes[1].set_xlim([-0.04, 1.04])

    skill_differences = [sd for sd in skill_differences if sd is not None]
    sn.boxplot(y=skill_differences, ax=axes[2], color='gray')
    plot_skill_diff_curve(comp_fractions, coll_counts, skill_differences, deg=7, ax=axes[3])

    ylabels = [r'Agent mean\newline payoff $R/2$ (\texteuro)',
               'Normalized single target\n difference $S_\Delta$',
               r'Agent payoffs $R^X$ (\texteuro)']

    for key, ylabel, ax in zip('ABC', ylabels, [axes[0], axes[1], axes[3]]):
        ax.text(-0.1, 1.1, key, transform=ax.transAxes, size=16, weight='bold')
        ax.set_xlabel('Fraction of\nsingle targets collected $\\Phi$', fontdict=fontdict)
        comp_frac_x_format(ax)
        ax.set_ylabel(ylabel, fontdict=fontdict)

    axes[2].axis('off')

    plt.subplots_adjust(left=.1, top=.83, bottom=.36, right=.76, wspace=1)
    save_fig()
