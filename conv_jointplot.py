import matplotlib as mpl
import pandas as pd
import seaborn as sn
from matplotlib import pyplot as plt
from matplotlib.ticker import FixedLocator, FixedFormatter

from _util import save_fig
from cmap_generator import get_cmap
from fst_convergence import split_tds_at_minute
from presentation.plotting import scatterplot
from trial_descriptor import get_comp_frac

if __name__ == '__main__':
    initial_tds = split_tds_at_minute(split_minute=1)[0]
    final_tds = split_tds_at_minute(split_minute=10)[1]
    to_plot = pd.DataFrame(dict(
        initial_comp_fractions=[get_comp_frac(td) for td in initial_tds],
        final_comp_fractions=[get_comp_frac(td) for td in final_tds],
    ))
    # joint = sn.jointplot(data=to_plot, x='initial_comp_fractions', y='final_comp_fractions', space=0, ratio=3,
    #                      marginal_kws=dict(bins=10),
    #                      # marginal_kws=dict(bins=10, kde=True, #color='gray',
    #                      #                   kde_kws=dict(bw_method=.2), line_kws=dict(c='k')),
    #                      legend=False, xlim=[-.02, 1.02], ylim=[-.02, 1.02])
    joint = sn.JointGrid(data=to_plot, x='initial_comp_fractions', y='final_comp_fractions', height=2.2,
                         xlim=[-.02, 1.02], ylim=[-.02, 1.02], space=0, ratio=3)
    plt.subplots_adjust(bottom=.3, left=.34, right=.98)
    sn.histplot(data=to_plot, y='final_comp_fractions', ax=joint.ax_marg_y, stat='density', bins=10, color='#b3b3b3')
    sn.kdeplot(data=to_plot, y='final_comp_fractions', ax=joint.ax_marg_y, color='k', bw_method=.2, cut=0)
    joint.ax_marg_x.axis('off')
    joint.ax_marg_y.axis('off')
    joint.ax_joint.spines[['right', 'top']].set_visible(True)
    scatterplot(data=to_plot, x='initial_comp_fractions', y='final_comp_fractions', hue='final_comp_fractions',
                ax=joint.ax_joint)
    sn.lineplot(x=[-1, 2], y=[-1, 2], c='k', ax=joint.ax_joint, linewidth=mpl.rcParams['axes.linewidth'])
    joint.ax_joint.set_box_aspect(1)
    joint.ax_joint.set_ylabel('Fraction of\nsingle targets $\\Phi$')
    joint.ax_joint.set_xlabel('First minute $\\Phi$')
    comp_frac_locator = FixedLocator([0, 1/3, 2/3, 1])
    comp_frac_formatter = FixedFormatter(['0', '1/3', '2/3', '1'])

    joint.ax_joint.xaxis.set_major_locator(comp_frac_locator)
    joint.ax_joint.xaxis.set_major_formatter(comp_frac_formatter)
    joint.ax_joint.yaxis.set_major_locator(comp_frac_locator)
    joint.ax_joint.yaxis.set_major_formatter(comp_frac_formatter)
    save_fig()
    # sn.scatterplot(data=to_plot, x='initial_comp_fractions', y='final_comp_fractions', hue='final_comp_fractions',
    #                ax=joint.ax_joint, palette=get_cmap(), legend=False)
    # scatterplot(data=to_plot, x='initial_comp_fractions', y='final_comp_fractions', hue='final_comp_fractions',
    #             ax=plt.gca())
    # joint_grid = sn.JointGrid(data=to_plot, x='final_comp_fractions', y='initial_comp_fractions',
    #                           hue='final_comp_fractions', height=3)
    # joint_grid.plot_joint(sn.scatterplot)
    # joint_grid.plot_marginals(sn.histplot)
    # sn.scatterplot(data=to_plot, x='final_comp_fractions', y='initial_comp_fractions', hue='final_comp_fractions',
    #                ax=axes[3], palette=cmap, legend=False, linewidth=.3, edgecolor='k')
    # sn.lineplot(x=[-1, 2], y=[-1, 2], c='k', ax=axes[3], linewidth=matplotlib.rcParams['axes.linewidth'])
