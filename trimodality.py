import matplotlib.pyplot as plt
import numpy as np
import seaborn
import sklearn.mixture

from trial_descriptor_2 import get_comp_frac, load_all_trial_descriptions

if __name__ == '__main__':
    comp_fractions = np.array([get_comp_frac(td) for td in load_all_trial_descriptions()]).reshape(-1, 1)
    components_to_test = 10
    best_component_count = np.zeros(components_to_test)
    for bootstrap_run in range(100):
        bootstrapped_comp_frac = np.random.choice(comp_fractions[:, 0], size=100 * len(comp_fractions))
        aic_values = list()
        for n_components in range(1, components_to_test + 1):
            gmm = sklearn.mixture.GaussianMixture(n_components)
            gmm.fit(comp_fractions)
            print(gmm.aic(comp_fractions))
            aic_values.append(gmm.aic(comp_fractions))
        print()
        best_component_count[np.argmin(aic_values)] += 1
    for _ in range(4):
        plt.figure()
        seaborn.histplot(np.random.choice(comp_fractions[:, 0], size=100 * len(comp_fractions)), bins=10)
