import concurrent
import time

import numpy as np
import pandas as pd
from euclid import Point2, Line2
from matplotlib import pyplot as plt
from tqdm import tqdm

from glm.util import cut_first_half
from load.data_loader import load_single, pair_idx_single_rec_path
from load.loading_helper import pairs, unique_pairs
from simulations import get_expected_min_effort_heatmap, get_expected_min_effort

from trial_predictor import get_dyad_weighting, effort_based_prediction
from _util import convert

cut_tds = None
uncut_tds = None

# c_params = dict(ahead=2, correlation=.1, sits_on_target=.05,  # classification parameters
#                 nearby=.5, miscoordination=1.9)

c_params = dict(ahead=.067, correlation=.1, sits_on_target=.05,  # classification parameters
                nearby=.5, miscoordination=.32, is_straight=.1)
# c_params = dict(ahead=2, correlation=.1, sits_on_target=.05,  # classification parameters
#                 nearby=.5, miscoordination=.525)
# c_params = dict(ahead=12, correlation=.1, sits_on_target=.05,  # classification parameters
#                 nearby=.5, miscoordination=.525)


def get_collection_keys(record):
    return [key for key in record.keys() if 'occupation' in key or 'collected' in key]


def get_collections(record):
    collected_targets = record[get_collection_keys(record)].to_numpy()
    collection_indices = np.where(np.sum(collected_targets, axis=1) == 1)[0]
    return collected_targets[collection_indices], collection_indices


def vec_to_label(coll_vec):
    coll_class_idx = np.where(coll_vec == 1)[0]
    if coll_class_idx == 0:
        return 'single_p0'
    elif coll_class_idx == 1:
        return 'single_p1'
    elif coll_class_idx == 2:
        return 'joint0'
    return 'joint1'


def get_collection_dict(rec):
    return {coll_idx: vec_to_label(hot_encoded_coll)
            for hot_encoded_coll, coll_idx in zip(*get_collections(rec))}


def get_dist_p_to_t(rec):
    p0 = rec[['p0_agent_x', 'p0_agent_y']].to_numpy()
    p1 = rec[['p1_agent_x', 'p1_agent_y']].to_numpy()
    comp = rec[['comp_x', 'comp_y']].to_numpy()
    coop0 = rec[['coop0_x', 'coop0_y']].to_numpy()
    coop1 = rec[['coop1_x', 'coop1_y']].to_numpy()
    return dict(
        p0_to_comp=np.linalg.norm(p0 - comp, axis=1) - 0.05,  # remove target radius
        p0_to_coop0=np.linalg.norm(p0 - coop0, axis=1) - 0.05,
        p0_to_coop1=np.linalg.norm(p0 - coop1, axis=1) - 0.05,
        p1_to_comp=np.linalg.norm(p1 - comp, axis=1) - 0.05,
        p1_to_coop0=np.linalg.norm(p1 - coop0, axis=1) - 0.05,
        p1_to_coop1=np.linalg.norm(p1 - coop1, axis=1) - 0.05,
    )


def sits_on_condition(p_idx, t_name, start_idx, p_to_t, sits_on_threshold):
    p_t_dist = p_to_t[f'p{p_idx}_to_{t_name}'][start_idx]
    if p_t_dist < sits_on_threshold:
        return True
    return False


def nearby_condition(p_idx, t_name, start_idx, p_to_t, nearby_threshold):
    p_t_dist = p_to_t[f'p{p_idx}_to_{t_name}'][start_idx]
    other_p_t_dist = p_to_t[f'p{(1 + p_idx) % 2}_to_{t_name}'][start_idx]
    return p_t_dist < other_p_t_dist * nearby_threshold


def inviting(coll_name, condition, *args):
    t_name = convert[coll_name]
    for coop_t_name in ['coop0', 'coop1']:
        for p_idx in [0, 1]:
            if condition(p_idx, coop_t_name, *args):
                if coop_t_name == t_name:
                    return f'accepted_{coop_t_name}_invite'
                elif 'single' in coll_name and f'p{p_idx}' not in coll_name:
                    return f'declined_{coop_t_name}_invite'
    return None


def get_comp_frac_from_rec(rec):
    comp_collections = rec[['comp_collected_by_p0', 'comp_collected_by_p1']].to_numpy().sum()
    coop_collections = rec[['coop0_collected', 'coop1_collected']].to_numpy().sum()
    return comp_collections / (comp_collections + coop_collections)


def get_t_desc_base(rec):
    coll_dict = get_collection_dict(rec)
    coll_indices = list(coll_dict.keys())
    trial_start_indices = [0] + coll_indices[:-1]
    trial_end_indices = np.array(coll_indices) - 120
    trial_descriptions = [{'start': start, 'end': end,
                           'duration': (end - start) / 120,
                           'coll_type': coll_dict[coll_i]}
                          for start, end, coll_i in zip(trial_start_indices, trial_end_indices, coll_indices)]
    return trial_descriptions


def get_efforts(dists_to_targets, t_desc):
    start = t_desc['start']
    return dict(
        comp=min(dists_to_targets['p0_to_comp'][start], dists_to_targets['p1_to_comp'][start]),
        coop0=max(dists_to_targets['p0_to_coop0'][start], dists_to_targets['p1_to_coop0'][start]),
        coop1=max(dists_to_targets['p0_to_coop1'][start], dists_to_targets['p1_to_coop1'][start]),
    )
# def get_efforts(dists_to_targets, t_desc):
#     limiting_agent_idx, _ = get_limiting_agent_idx(t_desc, dists_to_targets)
#     return dict(
#         comp=dists_to_targets[f'p{limiting_agent_idx}_to_comp'][t_desc['start']],
#         coop0=dists_to_targets[f'p{limiting_agent_idx}_to_coop0'][t_desc['start']],
#         coop1=dists_to_targets[f'p{limiting_agent_idx}_to_coop1'][t_desc['start']],
#     )


def get_cross_corr(dists_to_targets, t_desc, eps=1e-6):
    collected_target_name = convert[t_desc["coll_type"]]
    if t_desc['start'] == t_desc['end']:
        return np.zeros(200)  # Todo this is a hotfix
    p0_trial = dists_to_targets[f'p0_to_{collected_target_name}'][t_desc['start']:t_desc['end']]
    p1_trial = dists_to_targets[f'p1_to_{collected_target_name}'][t_desc['start']:t_desc['end']]
    p0_trial = (p0_trial - p0_trial.mean()) / (p0_trial.std() + eps)
    p1_trial = (p1_trial - p1_trial.mean()) / (p1_trial.std() + eps)
    return np.correlate(p0_trial, p1_trial, 'full') / len(p0_trial)


def get_correlation(dists_to_targets, t_desc):
    cross_corr = get_cross_corr(dists_to_targets, t_desc)
    return cross_corr[(len(cross_corr) - 1) // 2]


def get_path_shortening(dists_to_targets, prev_t_desc, t_desc):
    if prev_t_desc is not None:
        collecting_agent_idx = 0 if 'p0' in t_desc['coll_type'] else 1
        prev_collecting_agent_idx = 0 if 'p0' in prev_t_desc['coll_type'] else 1
        if 'single' in t_desc['coll_type'] and 'single' in prev_t_desc['coll_type'] and collecting_agent_idx != prev_collecting_agent_idx:
            collected_t_name = convert[t_desc['coll_type']]
            dist_non_collecting_agent = dists_to_targets[f'p{prev_collecting_agent_idx}_to_{collected_t_name}'][t_desc['start']]
            dist_collecting_agent = dists_to_targets[f'p{collecting_agent_idx}_to_{collected_t_name}'][t_desc['start']]
            if dist_collecting_agent < dist_non_collecting_agent:
                return dist_non_collecting_agent - dist_collecting_agent
    return 0.


def get_chosen_effort(dists_to_targets, t_desc):
    collected_target = convert[t_desc['coll_type']]
    efforts = get_efforts(dists_to_targets, t_desc)
    return efforts[collected_target]


def get_first_on_joint_target(dists_to_targets, t_desc):
    t_name = convert[t_desc['coll_type']]
    start = max(t_desc['start'] - 1, 0)  # avoid negative start idx
    end = t_desc['end'] + 119
    p0_to_t = dists_to_targets[f'p0_to_{t_name}'][end: start: -1]  # iterate distance function reversely
    p1_to_t = dists_to_targets[f'p1_to_{t_name}'][end: start: -1]
    p0_on_t = p0_to_t < .05
    p1_on_t = p1_to_t < .05
    for p0_on, p1_on in zip(p0_on_t, p1_on_t):
        if not p1_on and p0_on:
            return 'p0'
        elif not p0_on and p1_on:
            return 'p1'
        elif not p0_on and not p1_on:
            return 'both'
    print('Error in get_first_on_joint')
    return None


def get_last_entrance_idx(trial_dists_to_targets):
    not_on_t_indices = np.argwhere(trial_dists_to_targets > 0.)  # -.05 is already subtracted from dists_to_targets
    if len(not_on_t_indices) == 0:
        return -1
    return not_on_t_indices[-1, 0]  # todo this is not equal to the entrances in the record...


def get_limiting_agent_idx(t_desc, dists_to_targets):
    # if 'single' in t_desc['coll_type']:
    #     return (0, -1) if '0' in t_desc['coll_type'] else (1, -1)
    coop_target_kind = convert[t_desc['coll_type']]
    dists_p0 = dists_to_targets[f'p0_to_{coop_target_kind}'][t_desc['start']:t_desc['end']]
    dists_p1 = dists_to_targets[f'p1_to_{coop_target_kind}'][t_desc['start']:t_desc['end']]
    last_entrance_p0 = get_last_entrance_idx(dists_p0)
    last_entrance_p1 = get_last_entrance_idx(dists_p1)
    if last_entrance_p0 == last_entrance_p1:  # hotfix to solve for the average statistic
        random_agent_idx = int(np.round(np.random.rand(1)))
        limiting_agent_idx = random_agent_idx
        last_entrance = last_entrance_p0 if limiting_agent_idx == 0 else last_entrance_p1
    else:
        limiting_agent_idx = np.argmax([last_entrance_p0, last_entrance_p1])
        last_entrance = max([last_entrance_p0, last_entrance_p1])
    return limiting_agent_idx, last_entrance

# def get_limiting_agent_idx(rec, t_desc):
#     if 'single' in t_desc['coll_type']:
#         return (0, -1) if '0' in t_desc['coll_type'] else (1, -1)
#     coop_target_kind = convert[t_desc['coll_type']]
#     a = rec.iloc[t_desc['end'] - 1]
#     p0_is_limiting = a[f'{coop_target_kind}_entered_by_p0'].astype(bool)
#     p1_is_limiting = a[f'{coop_target_kind}_entered_by_p1'].astype(bool)
#     if not (p0_is_limiting or p1_is_limiting):
#         print('error!!')
#     if p0_is_limiting:
#         return 0, t_desc['end'] - 1
#     else:
#         return 1, t_desc['end'] - 1
#     # limiting_agent_idx = np.argmax([last_entrance_p0, last_entrance_p1])
#     # last_entrance = max([last_entrance_p0, last_entrance_p1])
#     # return limiting_agent_idx, last_entrance
#     # return -1, -1


def get_trajectory_distance(rec, t_desc, dists_to_targets):
    if 'single' in t_desc['coll_type']:
        trial_rec = rec[t_desc['start']:t_desc['end']]
        p_idx = 0 if '0' in t_desc['coll_type'] else 1
    else:
        limiting_agent_idx, last_entrance = get_limiting_agent_idx(t_desc, dists_to_targets)
        trial_rec = rec[t_desc['start']:t_desc['start'] + last_entrance]
        p_idx = limiting_agent_idx
    agent_positions = trial_rec[[f'p{p_idx}_agent_x', f'p{p_idx}_agent_y']].to_numpy()
    traj_dist = np.sum(np.linalg.norm(agent_positions[:-1] - agent_positions[1:], axis=1))
    return traj_dist



    # p0_positions = trial_rec[['p0_agent_x', 'p0_agent_y']].to_numpy()
    # p1_positions = trial_rec[['p1_agent_x', 'p1_agent_y']].to_numpy()
    # p0_dist = np.sum(np.linalg.norm(p0_positions[:-1] - p0_positions[1:], axis=1))
    # p1_dist = np.sum(np.linalg.norm(p1_positions[:-1] - p1_positions[1:], axis=1))
    # coll_type = t_desc['coll_type']
    # # todo cut at target entrance
    # if 'single_p0' == coll_type:
    #     return p0_dist
    # elif 'single_p1' == coll_type:
    #     return p1_dist
    # else:
    #     last_limiting_agent_idx, last_entrance = get_limiting_agent_idx(t_desc, dists_to_targets)
    #
    #
    #     # if get_limiting_agent_idx(t_desc, dists_to_targets) == 0:
    #     #     return p0_dist
    #     # else:
    #     #     return p1_dist
    #     # return max(p0_dist, p1_dist)


def get_ahead_measure(t_desc, width=30):
    cross_corr = t_desc['cross_corr']
    middle_idx = (len(cross_corr) - 1) // 2
    p0_score = cross_corr[middle_idx - width:middle_idx].sum()
    p1_score = cross_corr[middle_idx + 1:middle_idx + width + 1].sum()
    return p0_score - p1_score


def aimed_target_thresh_cond(line, comp, coop0, coop1, aim_threshold=.1):
    def get_first_below_thresh_idx(line, point):
        dists_to_point = np.linalg.norm(line - point[:, None], axis=0)
        dist_below_thresh = list(dists_to_point < aim_threshold)
        # elements of line are in movement direction, we exploit this
        if True not in dist_below_thresh:
            return len(dist_below_thresh)
        return dist_below_thresh.index(True)
    comp = get_first_below_thresh_idx(line, comp)
    coop0 = get_first_below_thresh_idx(line, coop0)
    coop1 = get_first_below_thresh_idx(line, coop1)
    no_aim_nearby = comp == coop0 and comp == coop1  # todo idea for another condition: use sets
    if no_aim_nearby:
        return None
    if comp < coop0 and comp < coop1:
        return 'comp'
    elif coop0 < comp and coop0 < coop1:
        return 'coop0'
    else:
        return 'coop1'


def aimed_target_min_cond(line, comp, coop0, coop1, agent_start, agent_end):
    min_dist_to_comp = np.linalg.norm(line - comp[:, None], axis=0).min()
    min_dist_to_coop0 = np.linalg.norm(line - coop0[:, None], axis=0).min()
    min_dist_to_coop1 = np.linalg.norm(line - coop1[:, None], axis=0).min()
    agent_beyond_comp = np.linalg.norm(agent_start - comp) < np.linalg.norm(agent_start - agent_end)
    agent_beyond_coop0 = np.linalg.norm(agent_start - coop0) < np.linalg.norm(agent_start - agent_end)
    agent_beyond_coop1 = np.linalg.norm(agent_start - coop1) < np.linalg.norm(agent_start - agent_end)
    if min_dist_to_comp < min_dist_to_coop0 and min_dist_to_comp < min_dist_to_coop1:
        return 'comp'
    elif min_dist_to_coop0 < min_dist_to_comp and min_dist_to_coop0 < min_dist_to_coop1:
        return 'coop0'
    elif min_dist_to_coop1 < min_dist_to_comp and min_dist_to_coop1 < min_dist_to_coop0:
        return 'coop1'
    else:
        return None


def aimed_target_thresh_proximal_cond(line, comp, coop0, coop1, agent_start, agent_end, comp_frac, thresh=.2, eps=1e-6):
    comp_min_thresh = np.linalg.norm(line - comp[:, None], axis=0).min() < thresh
    coop0_min_thresh = np.linalg.norm(line - coop0[:, None], axis=0).min() < thresh
    coop1_min_thresh = np.linalg.norm(line - coop1[:, None], axis=0).min() < thresh
    comp_agent_end_dist = np.linalg.norm(comp - agent_end)
    coop0_agent_end_dist = np.linalg.norm(coop0 - agent_end)
    coop1_agent_end_dist = np.linalg.norm(coop1 - agent_end)
    comp_agent_start_dist = np.linalg.norm(comp - agent_start)
    coop0_agent_start_dist = np.linalg.norm(coop0 - agent_start)
    coop1_agent_start_dist = np.linalg.norm(coop1 - agent_start)
    moved_towards_comp = comp_agent_end_dist + .1 < comp_agent_start_dist
    moved_towards_coop0 = coop0_agent_end_dist + .1 < coop0_agent_start_dist
    moved_towards_coop1 = coop1_agent_end_dist + .1 < coop1_agent_start_dist
    dist_sorted = sorted([
        (comp_agent_end_dist * (eps + 1 - comp_frac), moved_towards_comp, comp_min_thresh, 'comp'),
        (coop0_agent_end_dist * (eps + comp_frac), moved_towards_coop0, coop0_min_thresh, 'coop0'),
        (coop1_agent_end_dist * (eps + comp_frac), moved_towards_coop1, coop1_min_thresh, 'coop1'),
    ])
    for _, moved_towards, min_thresh, name in dist_sorted:
        if moved_towards and min_thresh:
            return name
    return None





def get_aimed_target(line, comp, coop0, coop1, agent_start, agent_end, comp_frac):
    return aimed_target_thresh_proximal_cond(line, comp, coop0, coop1, agent_start, agent_end, comp_frac)
    # return aimed_target_min_cond(line, comp, coop0, coop1, agent_start, agent_end)
    # thresh_cond = aimed_target_thresh_cond(line, comp, coop0, coop1)
    # if thresh_cond is None:
    #     return aimed_target_min_cond(line, comp, coop0, coop1)
    # return thresh_cond


border_lines = [
    Line2(Point2(0, 0), Point2(1, 0)),
    Line2(Point2(0, 0), Point2(0, 1)),
    Line2(Point2(1, 1), Point2(0, 1)),
    Line2(Point2(1, 1), Point2(1, 0)),
]


def get_entrance_exit_x_vals(fit_line):
    point_0 = Point2(0, fit_line(0))
    point_1 = Point2(1, fit_line(1))
    line = Line2(point_0, point_1)
    x_values = list()
    for b_line in border_lines:
        intersect = line.intersect(b_line)
        if intersect is not None and 0 <= intersect[0] <= 1 and 0 <= intersect[1] <= 1:
            x_values.append(intersect[0])
    return x_values[0], x_values[1]


def get_lin_traj(traj, n=20):  # fit truncated line to trajectory
    try:
        coefficients, residuals, _, _, _ = np.polyfit(*traj.T, 1, full=True)
        inv_coefficients, inv_residual, _, _, _ = np.polyfit(*traj.T[[1, 0]], 1, full=True)
    except ValueError as e:  # Catch wired and rare error
        print('Excepted ValueError:')
        print(e)
        return None
    if len(inv_residual) == 0 or len(residuals) == 0:
        return None
    invert_xy = residuals[0] > inv_residual[0]
    if invert_xy:  # this avoids mal fits for some trajectories
        coefficients = inv_coefficients
        traj = traj[:, [1, 0]]
    fit_line = np.poly1d(coefficients)  # full line
    x0, x1 = get_entrance_exit_x_vals(fit_line)
    x_space = np.linspace(x0, x1, n)
    line = np.array([x_space, fit_line(x_space)])
    line_start_idx = np.linalg.norm(line - traj[0][:, None], axis=0).argmin()
    line_end_idx = np.linalg.norm(line - traj[-1][:, None], axis=0).argmin()
    entrance_exit0 = line[:, 0]
    entrance_exit1 = line[:, -1]
    start_pos_on_line = line[:, line_start_idx]
    end_pos_on_line = line[:, line_end_idx]
    start_ee0_dist = np.linalg.norm(start_pos_on_line - entrance_exit0)
    end_ee0_dist = np.linalg.norm(end_pos_on_line - entrance_exit0)
    towards_ee0 = start_ee0_dist > end_ee0_dist
    new_x0 = start_pos_on_line[0]
    new_x1 = entrance_exit0[0] if towards_ee0 else entrance_exit1[0]
    # Creating the new truncated line. We truncate from the agents initial position to
    # the game field border in the movement direction
    new_x_space = np.linspace(new_x0, new_x1, n)
    output = np.array([new_x_space, fit_line(new_x_space)])
    if invert_xy:
        output = output[[1, 0]]
    return output


def is_different_target(rec, start, end, comp_frac):
    trial_rec = rec.iloc[start: end]
    p0_traj = trial_rec[['p0_agent_x', 'p0_agent_y']].to_numpy()
    p1_traj = trial_rec[['p1_agent_x', 'p1_agent_y']].to_numpy()
    p0_start = trial_rec[['p0_agent_x', 'p0_agent_y']].iloc[0].to_numpy()
    p1_start = trial_rec[['p1_agent_x', 'p1_agent_y']].iloc[0].to_numpy()
    p0_end = trial_rec[['p0_agent_x', 'p0_agent_y']].iloc[-1].to_numpy()
    p1_end = trial_rec[['p1_agent_x', 'p1_agent_y']].iloc[-1].to_numpy()
    comp = trial_rec[['comp_x', 'comp_y']].iloc[0].to_numpy()
    coop0 = trial_rec[['coop0_x', 'coop0_y']].iloc[0].to_numpy()
    coop1 = trial_rec[['coop1_x', 'coop1_y']].iloc[0].to_numpy()

    p0_lin = get_lin_traj(p0_traj)
    p1_lin = get_lin_traj(p1_traj)
    if p0_lin is None or p1_lin is None:
        return True
    p0_aim_t = get_aimed_target(p0_lin, comp, coop0, coop1, p0_start, p0_end, comp_frac)
    p1_aim_t = get_aimed_target(p1_lin, comp, coop0, coop1, p1_start, p1_end, comp_frac)

    if p0_aim_t is None or p1_aim_t is None:
        return False  # we dont know - edge case
    return p0_aim_t != p1_aim_t


def get_trial_class(dists_to_targets, rec, t_desc, comp_frac):
    coll_name = t_desc['coll_type']
    start_idx = t_desc['start']
    end_idx = t_desc['end']
    sits_on_invite = inviting(coll_name, sits_on_condition, start_idx, dists_to_targets, c_params['sits_on_target'])
    if sits_on_invite is not None:
        return sits_on_invite
    nearby_invite = inviting(coll_name, nearby_condition, start_idx, dists_to_targets, c_params['nearby'])
    if nearby_invite is not None:
        if not ('declined' in nearby_invite and comp_frac > 2/3):
            return nearby_invite
    # if 'joint' in t_desc['coll_type'] and t_desc['mis_cord_measure'] > c_params['miscoordination']:
    #     return 'miscoordination'
    if t_desc['mis_cord_measure'] > c_params['miscoordination']:
        return 'miscoordination'
    if 'single' in t_desc['coll_type'] and is_different_target(rec, start_idx, end_idx, comp_frac):# and t_desc['mis_cord_measure'] < c_params['is_straight']:
        return 'anti_corr'
    if t_desc['ahead_measure_4'] > c_params['ahead']:
        return 'p1_ahead'
    if t_desc['ahead_measure_4'] < -1 * c_params['ahead']:
        return 'p0_ahead'
    return 'concurrent'


def get_min_effort(dists_to_targets, t_desc):
    return min(list(get_efforts(dists_to_targets, t_desc).values()))


def get_p_efficiency(rec, t_desc, weigthing=.5, res=10):  # weighting should not be 1 but .99 instead
    if 'single' not in t_desc['coll_type']:
        return 0, 0
    collection_frame = t_desc['end'] + 120 - 1  # -1 because we take the frame before the target respawn
    p0_free_roaming = 1 == rec['comp_collected_by_p1'][collection_frame]
    collecting_agent_pos = rec[['comp_x', 'comp_y']].to_numpy()[collection_frame]
    free_agent_pos = rec[['p0_agent_x', 'p0_agent_y']].to_numpy()[collection_frame]
    if not p0_free_roaming:
        free_agent_pos = rec[['p1_agent_x', 'p1_agent_y']].to_numpy()[collection_frame]
    comp = collecting_agent_pos
    coop0 = rec[['coop0_x', 'coop0_y']].to_numpy()[collection_frame]
    coop1 = rec[['coop1_x', 'coop1_y']].to_numpy()[collection_frame]
    targets = np.vstack([comp, coop0, coop1])
    heatmap = get_expected_min_effort_heatmap(targets, weighting=weigthing, res=res)
    best_positioning = np.array(np.unravel_index(heatmap.argmin(), heatmap.shape)) / res
    opti_expected_effort = get_expected_min_effort(best_positioning, targets, weighting=.5, n=res)
    actu_expected_effort = get_expected_min_effort(free_agent_pos, targets, weighting=.5, n=res)
    naiv_expected_effort = get_expected_min_effort(collecting_agent_pos, targets, weighting=.5, n=res)
    actu_effort_reduction = naiv_expected_effort - actu_expected_effort
    opti_effort_reduction = naiv_expected_effort - opti_expected_effort
    if actu_effort_reduction < -.05:
        print(actu_effort_reduction)
    return actu_effort_reduction, opti_effort_reduction


def get_targets_for_frame(rec, frame):
    comp = rec[['comp_x', 'comp_y']].to_numpy()[frame]
    coop0 = rec[['coop0_x', 'coop0_y']].to_numpy()[frame]
    coop1 = rec[['coop1_x', 'coop1_y']].to_numpy()[frame]
    targets = np.vstack([comp, coop0, coop1])
    return targets


def get_min_eff_for_p(free_agent, coll_agent, targets):
    comp, coop0, coop1 = targets
    comp_eff = min([np.linalg.norm(free_agent - comp), np.linalg.norm(coll_agent - comp)])
    coop0_eff = max([np.linalg.norm(free_agent - coop0), np.linalg.norm(coll_agent - coop0)])
    coop1_eff = max([np.linalg.norm(free_agent - coop1), np.linalg.norm(coll_agent - coop1)])
    return min([comp_eff, coop0_eff, coop1_eff]) - .05  # remove target radius


def get_min_effort_best_placement(rec, prev_t_desc, t_desc, res=10):  # has to be behind min_effort
    if prev_t_desc is None:
        return 0
    if 'joint' in prev_t_desc['coll_type']:
        return t_desc['min_effort']
    min_effort = t_desc['min_effort']

    collection_frame = t_desc['start']

    p0_collecting = 1 == rec['comp_collected_by_p0'][collection_frame]
    comp = rec[['p0_agent_x', 'p0_agent_y']].to_numpy()[collection_frame]
    free_agent = rec[['p1_agent_x', 'p1_agent_y']].to_numpy()[collection_frame]
    if not p0_collecting:
        comp = rec[['p1_agent_x', 'p1_agent_y']].to_numpy()[collection_frame]
        free_agent = rec[['p0_agent_x', 'p0_agent_y']].to_numpy()[collection_frame]
    coop0 = rec[['coop0_x', 'coop0_y']].to_numpy()[collection_frame]
    coop1 = rec[['coop1_x', 'coop1_y']].to_numpy()[collection_frame]
    prev_targets = np.vstack([comp, coop0, coop1])

    heatmap = get_expected_min_effort_heatmap(prev_targets, weighting=.5, res=res)  # not a heatmap!
    best_placement = np.array(np.unravel_index(heatmap.argmin(), heatmap.shape)) / res
    targets = get_targets_for_frame(rec, t_desc['start'])
    min_effort_best_placement = get_min_eff_for_p(free_agent=best_placement, coll_agent=prev_targets[0], targets=targets)
    # test = get_min_eff_for_p(free_agent=free_agent, coll_agent=prev_targets[0], targets=targets)
    return min_effort_best_placement


# def get_dist_curves(dists_to_targets, t_desc): return p0_dist_curve, p1_dist_curve


def get_new_ahead_measure(dists_to_targets, t_desc, intermediate=.05):
    collected_target_name = convert[t_desc["coll_type"]]
    if t_desc['start'] == t_desc['end']:
        return 0, 0, 0
    p0_dist_curve = dists_to_targets[f'p0_to_{collected_target_name}'][t_desc['start']:t_desc['end']]
    p1_dist_curve = dists_to_targets[f'p1_to_{collected_target_name}'][t_desc['start']:t_desc['end']]
    return (p0_dist_curve - p1_dist_curve).mean()
    # p0_ahead_p = (p0_dist_curve < p1_dist_curve - intermediate).astype(int).mean()
    # p1_ahead_p = (p1_dist_curve < p0_dist_curve - intermediate).astype(int).mean()
    # no_ahead_p = (abs(p1_dist_curve - p0_dist_curve) < intermediate).astype(int).mean()
    # return p0_ahead_p, p1_ahead_p, no_ahead_p


def get_cross_corr_2(rec, t_desc):  # todo delete this trash
    start = t_desc['start']
    end = t_desc['end']
    trial_rec = rec.iloc[start: end]
    p0_traj = trial_rec[['p0_agent_x', 'p0_agent_y']].to_numpy()
    p1_traj = trial_rec[['p1_agent_x', 'p1_agent_y']].to_numpy()
    return normalized_traj_cross_corr(p0_traj, p1_traj)


def normalized_traj_cross_corr(p0_traj, p1_traj):
    p0_mean, p1_mean = p0_traj.mean(axis=0), p1_traj.mean(axis=0)
    p0_var, p1_var = p0_traj.var(axis=0), p1_traj.var(axis=0)
    element_wise_dot_res = element_wise_dot(p0_traj - p0_mean, p1_traj - p1_mean)
    ret = np.mean(element_wise_dot_res) / (p0_var @ p1_var)
    return ret


def element_wise_dot(x, y):
    return np.sum(x * y, axis=1)


def get_min_dist_shift(p0_traj, p1_traj, max_shift=60):
    if len(p0_traj) == 0:
        return 0  # hotfix todo investigate
    max_shift = min([len(p0_traj) - 1, max_shift])  # for those trials with fewer bins than max_shift
    shift_mat = np.ones((max_shift * 2 + 1, *p0_traj.shape)) * np.nan
    for i, shift in enumerate(range(-max_shift, max_shift + 1)):
        target_slice = slice(max([0, shift]), min([len(p0_traj), len(p0_traj) + shift]))
        source_slice = slice(max([0, -shift]), min([len(p0_traj), len(p0_traj) - shift]))
        shift_mat[i, target_slice, :] = p0_traj[source_slice]
    diff_vec_mat = shift_mat - p1_traj
    dist_mat = np.linalg.norm(diff_vec_mat, axis=2)
    nan_positions = np.isnan(dist_mat)
    nan_s_per_row = nan_positions.astype(int).sum(axis=1)
    dist_mat[nan_positions] = 0
    mean_dists = np.sum(dist_mat, axis=1) / (len(p0_traj) - nan_s_per_row)
    return np.argmin(mean_dists) - max_shift


def get_ahead_measure_3(rec, t_desc):
    start = t_desc['start']
    end = t_desc['end']
    trial_rec = rec.iloc[start: end]
    p0_traj = trial_rec[['p0_agent_x', 'p0_agent_y']].to_numpy()
    p1_traj = trial_rec[['p1_agent_x', 'p1_agent_y']].to_numpy()
    return get_min_dist_shift(p0_traj, p1_traj)


def get_opti_pos(targets, w, res=10):
    # if not (isinstance(targets, np.ndarray) and targets.shape == (3, 2)):
    #     print(type(targets), targets.shape)
    # print(type(targets), targets.shape, targets.dtype)
    heatmap = get_expected_min_effort_heatmap(targets, weighting=w, res=res)
    best_positioning = np.array(np.unravel_index(heatmap.argmin(), heatmap.shape)) / res
    # print('success')
    return best_positioning


def agent_target_enter_test(rec):
    actual_comp_entered_by_p1 = rec['comp_entered_by_p1'].to_numpy() == 1
    comp_collected_by_p1 = rec['comp_collected_by_p1'].to_numpy() == 1
    actual_comp_entered_by_p1 = actual_comp_entered_by_p1[:-120]
    desired_comp_entered_by_p1_idx = np.where(comp_collected_by_p1[121:])[0]
    success = actual_comp_entered_by_p1[desired_comp_entered_by_p1_idx]
    return None  # todo result: since the collections ends exatly after 120 frames, half of the entrances are 120 and half are 121 bins in the past


def get_first_entered(dists_to_targets, t_desc):
    if 'single' in t_desc['coll_type']:
        return 'p0' if '0' in t_desc['coll_type'] else 'p1'
    coop_target_kind = convert[t_desc['coll_type']]
    dists_p0 = dists_to_targets[f'p0_to_{coop_target_kind}'][t_desc['start']:t_desc['end']]
    dists_p1 = dists_to_targets[f'p1_to_{coop_target_kind}'][t_desc['start']:t_desc['end']]
    last_entrance_p0 = get_last_entrance_idx(dists_p0)  # todo refactor this namings first/last entrance
    last_entrance_p1 = get_last_entrance_idx(dists_p1)
    if last_entrance_p0 == last_entrance_p1:
        return 'both'
    else:
        first_agent_idx = np.argmin([last_entrance_p0, last_entrance_p1])
        return f'p{first_agent_idx}'


def get_smoothed_acceleration(dist_curve, w=30):
    dist_curve = np.convolve(dist_curve, np.ones(w) / w, 'same')  # not all frames seam to contain position updates, we apply this smoothing to make the data differentiable
    speed = np.diff(dist_curve, n=1)
    speed = np.convolve(speed, np.ones(w) / w, 'same')
    acceleration = np.diff(speed, n=1)
    return np.convolve(acceleration, np.ones(w) / w, 'same')


def get_ahead_measure_5(dists_to_targets, t_desc, t=30):  # t = #bins considered, we take only 1/4sec into account
    collected_target_name = convert[t_desc["coll_type"]]
    if t_desc['start'] == t_desc['end']:
        return 0
    p0_dist_curve = dists_to_targets[f'p0_to_{collected_target_name}'][t_desc['start']:t_desc['end']]
    p1_dist_curve = dists_to_targets[f'p1_to_{collected_target_name}'][t_desc['start']:t_desc['end']]
    p0_acceleration_curve = get_smoothed_acceleration(p0_dist_curve)
    p1_acceleration_curve = get_smoothed_acceleration(p1_dist_curve)
    cross_correlation = np.correlate(p0_acceleration_curve, p1_acceleration_curve, 'full')
    return np.argmax(cross_correlation) - len(cross_correlation) / 2


def gen_trial_descriptions(pair_idx, lap):
    rec = load_single(pair_idx, lap, frames_per_sec=120)
    # agent_target_enter_test(rec)
    trial_descriptions = get_t_desc_base(rec)
    dists_to_targets = get_dist_p_to_t(rec)
    prev_trial_descriptions = [None] + trial_descriptions[:-1]
    naive_dyad_weighting = get_dyad_weighting(get_comp_frac_from_rec(rec))
    # ps_dyad_weighting = get_dyad_weighting(get_comp_frac_from_rec(rec), sim_name='path_shortening')

    for prev_t_desc, t_desc in zip(prev_trial_descriptions, trial_descriptions):
        # t_desc['p_efficiency_w05'] = get_p_efficiency(rec, t_desc)
        # t_desc['p_efficiency_w_dyad'] = get_p_efficiency(rec, t_desc, weigthing=ps_dyad_weighting)
        # actu_effort_reduction, opti_effort_reduction = get_p_efficiency(rec, t_desc, weigthing=ps_dyad_weighting)
        # t_desc['actu_effort_reduction'] = actu_effort_reduction
        # t_desc['opti_effort_reduction'] = opti_effort_reduction  todo del this? how to proceed with placement analysis

        t_desc['first_entered'] = get_first_entered(dists_to_targets, t_desc)

        t_desc['min_effort'] = get_min_effort(dists_to_targets, t_desc)
        # t_desc['min_effort_best_placement'] = get_min_effort_best_placement(rec, prev_t_desc, t_desc)
        # t_desc['effort_due_to_non_opti_p'] = t_desc['min_effort'] - t_desc['min_effort_best_placement']  # effort due to non optimal placement
        t_desc['chosen_effort'] = get_chosen_effort(dists_to_targets, t_desc)  # necessary effort
        t_desc['effort_ol'] = t_desc['chosen_effort'] - t_desc['min_effort']  # effort due to target selection
        t_desc['distance'] = get_trajectory_distance(rec, t_desc, dists_to_targets)
        t_desc['distance_ol'] = t_desc['distance'] - t_desc['chosen_effort']  # additional effort

        # print(t_desc['distance'])
        # print(t_desc['distance_ol'] + t_desc['effort_ol'] + t_desc['effort_due_to_non_opti_p'] + t_desc['effort_best_placement'])
        # print()

        t_desc['speed'] = t_desc['distance'] / t_desc['duration']
        t_desc['ahead_measure_1'] = get_correlation(dists_to_targets, t_desc)
        t_desc['ahead_measure_2'] = get_cross_corr_2(rec, t_desc)
        t_desc['ahead_measure_3'] = get_ahead_measure_3(rec, t_desc)
        t_desc['ahead_measure_4'] = get_new_ahead_measure(dists_to_targets, t_desc)
        t_desc['ahead_measure_5'] = get_ahead_measure_5(dists_to_targets, t_desc)
        # t_desc['correlation'] = get_correlation(t_desc)
        t_desc['path_shortening'] = get_path_shortening(dists_to_targets, prev_t_desc, t_desc)
        # t_desc['mis_cord_measure'] = t_desc['chosen_effort'] / (t_desc['distance'] + 1e-10)
        t_desc['mis_cord_measure'] = t_desc['distance_ol'] / (t_desc['distance'] + 1e-10)
        # t_desc['mis_cord_measure'] = t_desc['distance'] / t_desc['chosen_effort']  # todo idea: incorporate speed!?
        # t_desc['mis_cord_measure'] = t_desc['distance_ol'] / (t_desc['distance'] + 1e-10)  # add .05 because we add the prevouily substracted target radius todo this should be corrected foruther up the pipeline
        # t_desc['ahead_measure'] = get_ahead_measure(t_desc)

        # # todo continue here
        # t_desc['p0_ahead_p'], t_desc['p1_ahead_p'], t_desc['no_ahead_p'] = get_new_ahead_measure(dists_to_targets, t_desc)
        # # todo delete no_ahead_p?
        # ahead_diff = t_desc['p0_ahead_p'] - t_desc['p1_ahead_p']
        # if ahead_diff > .33:
        #     t_desc['new_ahead'] = 'p0'
        # elif ahead_diff < -.33:
        #     t_desc['new_ahead'] = 'p1'
        # else:
        #     t_desc['new_ahead'] = 'no_one'

        t_desc['trial_class'] = get_trial_class(dists_to_targets, rec, t_desc)

        start_rec = rec.iloc[t_desc['start']]

        t_desc['asp_dist_to_other'] = None  # asp = after single placement (distances from the free agent)
        t_desc['asp_dist_to_coop'] = None
        t_desc['asp_dist_to_w10_opti'] = None
        t_desc['asp_dist_to_w05_opti'] = None
        if prev_t_desc is not None and 'single' in prev_t_desc['coll_type']:
            p0 = start_rec[['p0_agent_x', 'p0_agent_y']].to_numpy()
            p1 = start_rec[['p1_agent_x', 'p1_agent_y']].to_numpy()
            collecting_agent = p0 if 'p0' in prev_t_desc['coll_type'] else p1
            free_agent = p1 if 'p0' in prev_t_desc['coll_type'] else p0
            coop0 = start_rec[['coop0_x', 'coop0_y']].to_numpy()
            coop1 = start_rec[['coop1_x', 'coop1_y']].to_numpy()
            free_to_coop0 = np.linalg.norm(coop0 - free_agent)
            free_to_coop1 = np.linalg.norm(coop1 - free_agent)
            comp = collecting_agent
            targets = np.vstack([comp, coop0, coop1])
            if targets.dtype == np.dtype('O'):
                targets = targets.astype(np.float64)
                # print('cast!')
            opti_pos_w10 = get_opti_pos(targets, w=.99)  # use w=.99 instead of 1
            opti_pos_w05 = get_opti_pos(targets, w=.5)
            t_desc['asp_dist_to_other'] = np.linalg.norm(collecting_agent - free_agent)
            t_desc['asp_dist_to_coop'] = min([free_to_coop0, free_to_coop1])
            t_desc['asp_dist_to_w10_opti'] = np.linalg.norm(opti_pos_w10 - free_agent)
            t_desc['asp_dist_to_w05_opti'] = np.linalg.norm(opti_pos_w05 - free_agent)

        efforts = get_efforts(dists_to_targets, t_desc)
        for i, weighting in enumerate([.001, 1., .5, naive_dyad_weighting]):
            t_desc[f'prediction{i}'], t_desc[f'prediction{i}_prob'] = effort_based_prediction(efforts, weighting)

    dump_trail_descriptions(pair_idx, lap, trial_descriptions)


def get_trial_descriptions_for_dyad(pair_idx):
    pair = unique_pairs[pair_idx]
    lap_amount = pairs.count(pair)
    for lap in range(lap_amount):
        gen_trial_descriptions(pair_idx, lap)
    print(f'finished pair {pair_idx}, {pair}')


def gen_all_trial_descriptions():
    t0 = time.time()
    with concurrent.futures.ProcessPoolExecutor(max_workers=7) as ppe:
        for dyad_idx in range(len(unique_pairs)):
            ppe.submit(get_trial_descriptions_for_dyad, dyad_idx)
    # for pair_idx, pair in enumerate(unique_pairs):
    #     lap_amount = pairs.count(pair)
    #     for lap in range(lap_amount):
    #         gen_trial_descriptions(pair_idx, lap)

    print(f'tds_generation_time: {time.time() - t0}')


def dump_trail_descriptions(pair_idx, lap, trial_descriptions):
    path = pair_idx_single_rec_path(pair_idx, lap)
    trial_descriptions = pd.DataFrame(trial_descriptions)
    trial_descriptions.to_pickle(path + '/trial_descriptions.pkl')


def load_trial_descriptions(pair_idx, lap):
    path = pair_idx_single_rec_path(pair_idx, lap)
    return pd.read_pickle(path + '/trial_descriptions.pkl')


def get_comp_frac(td):
    val_counts = td['coll_type'].value_counts()
    for c_type in ['single_p0', 'single_p1']:
        if c_type not in val_counts.keys():
            val_counts[c_type] = 0
    return val_counts[['single_p0', 'single_p1']].sum() / val_counts.sum()


def unsorted_load_all_trial_descriptions(cut_initial=True):
    all_trial_descriptions = list()
    for pair_idx, pair in enumerate(unique_pairs):
        # lap_amount = pairs.count(pair)
        lap_amount = 2  # we take only the first 2 laps!
        t_descriptions = load_trial_descriptions(pair_idx, lap=0)
        if cut_initial:
            _, t_descriptions = cut_first_half(rec=None, td=t_descriptions)
        for lap in range(1, lap_amount):
            t_descriptions = pd.concat([t_descriptions, load_trial_descriptions(pair_idx, lap)], axis=0)
        all_trial_descriptions.append(t_descriptions)
    return all_trial_descriptions


def print_unsorted_ids_and_comp_fractions():
    unsorted_tds = unsorted_load_all_trial_descriptions()
    comp_fracs = [get_comp_frac(td) for td in unsorted_tds]
    for cf, (i, id) in zip(comp_fracs, enumerate(unique_pairs)):
        print(i, list(id), cf)


def get_p_ids_sorted():
    tds = unsorted_load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    return np.array(unique_pairs)[comp_fractions.argsort()]


def load_all_trial_descriptions(cut_initial=True):
    if cut_initial:
        return load_cut_tds()
    else:
        return load_uncut_tds()
    # global tds
    # if tds is None:
    #     if cut_initial:
    #         tds = unsorted_load_all_trial_descriptions(cut_initial=cut_initial)
    #     # We calculate the comp fraction sorting always with cut_initial=True!
    #     unsorted_comp_fractions = [get_comp_frac(td) for td in unsorted_load_all_trial_descriptions(cut_initial=True)]
    #     comp_frac_sorting = np.argsort(unsorted_comp_fractions)
    #     tds = [tds[i] for i in comp_frac_sorting]
    # return tds


def load_cut_tds():
    global cut_tds
    if cut_tds is None:
        cut_tds = unsorted_load_all_trial_descriptions(cut_initial=True)
        # We calculate the comp fraction sorting always with cut_initial=True!
        unsorted_comp_fractions = [get_comp_frac(td) for td in cut_tds]
        comp_frac_sorting = np.argsort(unsorted_comp_fractions)
        cut_tds = [cut_tds[i] for i in comp_frac_sorting]
    return cut_tds


def load_uncut_tds():
    global uncut_tds
    if uncut_tds is None:
        uncut_tds = unsorted_load_all_trial_descriptions(cut_initial=False)
        # We calculate the comp fraction sorting always with cut_initial=True!
        unsorted_comp_fractions = [get_comp_frac(td) for td in load_cut_tds()]
        comp_frac_sorting = np.argsort(unsorted_comp_fractions)
        uncut_tds = [uncut_tds[i] for i in comp_frac_sorting]
    return uncut_tds


if __name__ == '__main__':

    # gen_trial_descriptions(37, 1)
    # td = load_trial_descriptions(37, 1)

    print_unsorted_ids_and_comp_fractions()
    gen_all_trial_descriptions()
    tds = load_all_trial_descriptions()
    for i, p_ids in enumerate(get_p_ids_sorted()):   # 40sec per it / 8sec per it
        print(i, p_ids)

    # Invite acceptation percentage
    def get_inv_acceptance_percentage(tds, cond=None):
        if cond is None:
            cond = lambda x: True
        accepted = 0
        declined = 0
        for td in tds:
            if cond(td):
                accepted += (td['trial_class'] == 'accepted_coop0_invite').to_numpy().astype(int).sum()
                accepted += (td['trial_class'] == 'accepted_coop1_invite').to_numpy().astype(int).sum()
                declined += (td['trial_class'] == 'declined_coop0_invite').to_numpy().astype(int).sum()
                declined += (td['trial_class'] == 'declined_coop1_invite').to_numpy().astype(int).sum()
        return accepted / (accepted + declined)

    print(f'Invite acceptance percentage: {get_inv_acceptance_percentage(tds)}')
    print(f'Invite acceptance percentage, FST < 0.9: {get_inv_acceptance_percentage(tds, cond=lambda td: get_comp_frac(td) < .9)}')


