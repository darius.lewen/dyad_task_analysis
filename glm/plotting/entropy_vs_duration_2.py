import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn
from scipy.stats import pearsonr

from glm.plotting.class_comparison import fontdict
from glm.plotting.entropy_ts import get_best_entropy_ts
from glm.preprocess import load, cut_first_half
from trial_descriptor import load_all_trial_descriptions, get_comp_frac, load_trial_descriptions


def remove_head(td, n=4):
    return td[n:].drop(list(range(n)))  # second part because of second block cutout


def cut_first_td_half(td, t=120 * 10 * 60):
    for i, start_idx in enumerate(td['start'].to_numpy()):
        if start_idx > t:
            td = td.iloc[i:]
            td['start'] -= t  # to use them as indices later on
            td['end'] -= t
            return td


def get_concat_td(dyad_idx):
    # tds = list()
    # for lap in [0, 1]:
    #     td = load_trial_descriptions(dyad_idx, lap)
    #     if lap == 0:
    #         td = cut_frist_td_half(td)
    #     tds.append(td)
    return pd.concat([cut_first_td_half(load_trial_descriptions(dyad_idx, lap=0)),
                      load_trial_descriptions(dyad_idx, lap=1)])


if __name__ == '__main__':  # todo do this again but with a missprediction meassure
    tds = [remove_head(get_concat_td(dyad_idx)) for dyad_idx in range(58)]
    # tds = [remove_head(td) for td in load_all_trial_descriptions()]
    comp_fractions = [get_comp_frac(td) for td in tds]
    trial_durations = [td['duration'].to_numpy() for td in tds]
    best_entropy_ts = get_best_entropy_ts(filename='../unsorted_performance.pkl')

    dyad_collection = list()
    for dyad_idx, (entropy_ts, trial_duration) in enumerate(zip(best_entropy_ts, trial_durations)):
        corr_coefficient, p_value = pearsonr(entropy_ts, trial_duration)
        if p_value < .05 and corr_coefficient > .3:
            dyad_collection.append(dyad_idx)

    trial_duration = np.concatenate(np.array(trial_durations)[dyad_collection])
    best_entropy_ts = np.concatenate(np.array(best_entropy_ts)[dyad_collection])

    sn.jointplot(x=best_entropy_ts, y=trial_duration, kind='kde', fill=True, space=0, height=3)
    ax = plt.gca()
    ax.set_xlabel('Prediction entropy', fontdict=fontdict)
    ax.set_ylabel('Trial duration', fontdict=fontdict)
    ax.set_xlim(0, np.log2(3))
    ax.set_ylim(0, 3.2)
    ax.set_box_aspect(1)
    plt.subplots_adjust(bottom=.2, top=.9, left=.2, right=.9)
    script_name = os.path.basename(sys.argv[0])
    plt.savefig(f'../plots/{script_name[:-3]}.png', dpi=300)
