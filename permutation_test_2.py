import numpy as np
from matplotlib import pyplot as plt
from numba import jit
from tqdm import tqdm
from multiprocessing import Process, Queue

from _util import save_fig
from trial_descriptor import get_comp_frac, unsorted_load_all_trial_descriptions

fontdict = dict(size=12)

def get_true_p():
    comp_fractions = np.array([get_comp_frac(td) for td in unsorted_load_all_trial_descriptions()])
    is_competitive = (comp_fractions > .9).astype(int)
    return is_competitive.mean()


@jit
def _get_mean_diffs(dyad_amount, toy_p, n=1000, true_dyad_amount=58):
    mean_diffs = list()
    true_p_sim = np.random.binomial(1, true_p, size=true_dyad_amount)  # bernoulli distri
    toy_p_sim = np.random.binomial(1, toy_p, size=dyad_amount)
    bucket = np.concatenate((true_p_sim, toy_p_sim))
    for _ in range(n):
        np.random.shuffle(bucket)
        first_bucket_half = bucket[:true_dyad_amount]
        second_bucket_half = bucket[true_dyad_amount:]
        mean_diffs.append(first_bucket_half.mean() - second_bucket_half.mean())
    return np.array(mean_diffs)


@jit
def get_null_hypo_rejection_count(dyad_amount, toy_p, n):
    null_hypo_rejection_count = 0
    for _ in range(n):
        true_mean = true_p# * dyad_amount
        toy_mean = toy_p# * dyad_amount
        actual_mean_diff = toy_mean - true_mean
        mean_diffs = _get_mean_diffs(dyad_amount, toy_p)
        over_boarder_percentage = (mean_diffs > actual_mean_diff).astype(np.int64).mean()
        enough_dyads = .05 > over_boarder_percentage
        if enough_dyads:
            null_hypo_rejection_count += 1
    return null_hypo_rejection_count


def get_required_dyad_amount(toy_p, n=1000):
    for dyad_amount in range(6, 100, 2):
        null_hypo_rejection_count = get_null_hypo_rejection_count(dyad_amount, toy_p, n)
        if null_hypo_rejection_count > .8 * n:
            return dyad_amount
    return np.nan


def calc_curve(q):
    toy_p_s = np.linspace(.65, .35, 20)
    req_dyad_amounts = list()
    for toy_p in tqdm(toy_p_s):
        req_dyad_amounts.append(get_required_dyad_amount(toy_p))
        if np.isnan(req_dyad_amounts[-1]):
            break
    q.put(np.stack((req_dyad_amounts, toy_p_s[:len(req_dyad_amounts)])))


def gen_curve(process_amount=2):
    queue = Queue()
    processes = list()
    results = list()
    for _ in range(process_amount):
        processes.append(Process(target=calc_curve,
                                 args=[queue]))
    for p in processes:
        p.start()
    for p in processes:
        p.join()
    while not queue.empty():
        results.append(queue.get())
    result = sum(results) / process_amount
    np.save('permutation_test_2.npy', result)


def load_curve():
    return np.load('permutation_test_2.npy')


def show_per_test():
    dyad_amount = 50
    toy_p = .5
    actual_mean_diff = toy_p - true_p
    mean_diffs = _get_mean_diffs(dyad_amount, toy_p)
    plt.hist(mean_diffs)
    plt.plot([actual_mean_diff, actual_mean_diff], [0, 2000])


# true_p = get_true_p()
true_p = .24


def to_percentual_increase(x):
    return x / (true_p / 100) - 100


if __name__ == '__main__':
    # gen_curve()

    required_dyads, comp_dyad_frac = load_curve()
    required_dyads[np.isnan(required_dyads)] = 100

    figsize = 4
    fig = plt.figure(figsize=(figsize, figsize))
    ax = plt.gca()
    ax_twin = ax.twinx()
    ax.plot(required_dyads, len(required_dyads) * (true_p, ), color='k')
    ax.fill_between(required_dyads, len(required_dyads) * (0, ), comp_dyad_frac, color='gray', alpha=.2)
    ax.fill_between(required_dyads, comp_dyad_frac, len(required_dyads) * (.6, ), color='tab:blue', alpha=.3)

    ax.set_box_aspect(1)
    ax.set_xlim([10 - .4, 80 + .4])
    ylim = np.array([.2, .52])
    ax.set_ylim(ylim)
    ax_twin.set_ylim(to_percentual_increase(ylim))

    in_plot_font_size = 10
    ax.text(30, .45, '\"Spatial separation\ncauses competition\"', size=in_plot_font_size)
    ax.text(20, .35, '\"Not enough data\"', size=in_plot_font_size)
    ax.text(30, true_p + .008, 'Joint room condition', size=in_plot_font_size)

    ax.set_xticks(list(range(10, 90, 20)))
    ax.set_title('Simulating permutation tests', fontdict=fontdict)
    ax.set_xlabel('Separate room dyads', fontdict=fontdict)
    ax.set_ylabel('Fraction of competitive dyads ($\\Phi>0.9$)\nunder separate room condition', fontdict=fontdict)
    ax_twin.set_ylabel('Percentual increase in competition\ndue to separate room condition', fontdict=fontdict, rotation=270)
    ax_twin.yaxis.labelpad = 17 + 10

    plt.tight_layout()
    save_fig()

    # plt.plot([0, 70], [true_p, true_p])

    # show_per_test()
