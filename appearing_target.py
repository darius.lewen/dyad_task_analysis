import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from pred_performance import violin_swarm_mean_plot
from reward_decomposition_2 import scatter
from trial_descriptor import load_all_trial_descriptions, get_comp_frac


def get_apriori_probs(td):
    coll_types = td['coll_type'].to_numpy()
    comp_count0 = (coll_types == 'single_p0').astype(int)
    comp_count1 = (coll_types == 'single_p1').astype(int)
    comp_count = np.sum(comp_count0 + comp_count1)
    coop0_count = np.sum((coll_types == 'joint0').astype(int))
    coop1_count = np.sum((coll_types == 'joint1').astype(int))
    return np.array([comp_count, coop0_count, coop1_count]) / len(coll_types)


def get_after_themself_probs(td):
    return np.array([
        after_itself_count(td, 'single'),
        after_itself_count(td, 'joint0'),
        after_itself_count(td, 'joint1'),
    ]) / len(td)


def get_same_target_prob(td):
    return sum([
        after_itself_count(td, 'single'),
        after_itself_count(td, 'joint0'),
        after_itself_count(td, 'joint1'),
    ]) / len(td)


def after_itself_count(td, coll_type):
    coll_type_binary = get_coll_type_binary(td, coll_type)
    prev_was_coll_type = np.concatenate([[False], coll_type_binary[:-1].astype(bool)])
    prev_was_coll_type_td = td.iloc[prev_was_coll_type]
    coll_type_after_itself_binary = get_coll_type_binary(prev_was_coll_type_td, coll_type)
    return np.sum(coll_type_after_itself_binary)


def get_coll_type_binary(td, coll_type):
    coll_types = td['coll_type'].to_numpy()
    if coll_type == 'single':
        comp_binary0 = (coll_types == 'single_p0').astype(int)
        comp_binary1 = (coll_types == 'single_p1').astype(int)
        return comp_binary0 + comp_binary1
    else:
        return (coll_types == coll_type).astype(int)


if __name__ == '__main__':
    fig, axes = plt.subplots(1, 2)
    axes = axes.flatten()
    tds = load_all_trial_descriptions()
    comp_fractions = [get_comp_frac(td) for td in tds]
    apriori_same_target_probs = np.array([get_apriori_probs(td) for td in tds]) ** 2
    after_themself_probs = np.array([get_after_themself_probs(td) for td in tds])
    diff = np.sum(apriori_same_target_probs - after_themself_probs, axis=1)
    to_plot = pd.DataFrame(dict(change_in_probability=diff))
    violin_swarm_mean_plot(data=to_plot, ax=axes[0])

    actual_same_target_probabilities = np.array([get_same_target_prob(td) for td in tds])
    same_target_probs_assuming_independence = np.sum(apriori_same_target_probs, axis=1)
    diff_in_same_target_prob = same_target_probs_assuming_independence - actual_same_target_probabilities
    # to_plot = pd.DataFrame(dict(change_in_probability=diff_in_same_target_prob))
    # violin_swarm_mean_plot(data=to_plot, ax=axes[1])

    scatter(data=None, x=same_target_probs_assuming_independence, y=actual_same_target_probabilities,
            ax=axes[1], hue=comp_fractions)
    # axes[1].set_xlim([0, 1])
    # axes[1].set_ylim([0, 1])
    axes[1].plot([0, 1], [0, 1])
    axes[1].set_xlabel('Same target probability assuming independence')
    axes[1].set_ylabel('Actual same target probability')

    # scatter(x=comp_fractions, y=diff_in_same_target_prob, ax=axes[1])


    # mean prob reduction of prev target
    #

