import os
import sys

from matplotlib import pyplot as plt

game_width_in_cm = 800 * 139.7 / (1920 ** 2 + 1080 ** 2) ** .5
fontdict = dict(size=14)

convert = dict(
    single_p0='comp',
    single_p1='comp',
    joint0='coop0',
    joint1='coop1',
)


def save_fig():
    script_name = os.path.basename(sys.argv[0])
    plt.savefig(f'plots/{script_name[:-3]}.png', dpi=300)
    print(f'plots/{script_name[:-3]}.png')

