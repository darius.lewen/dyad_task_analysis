import numpy as np
import pandas as pd
from matplotlib import pyplot as plt, ticker
from scipy.stats import pearsonr
from sklearn.metrics import r2_score

from _util import save_fig, game_width_in_cm
from cmap_generator import get_cmap
from first_on_target_as_sd import get_only_straight, get_being_first_skill_diff
from inefficiencies_full import comp_frac_x_format
from presentation.plotting import scatterplot, diagonal_r_and_p_text
from rew_diff import get_skill_differences
from rew_diff_2 import get_single_target_diff
from reward_decomposition import get_agent_rewards, get_mean_in_cm
from reward_decomposition_2 import get_optimal_from_past_target_curve, get_optimal_effort_reduction
from skill_diff import error_stats, get_only_straight_single
from trial_classes import plot_aic_poly
from trial_descriptor_2 import load_all_trial_descriptions, get_comp_frac  # todo make it work with trial_descriptor_2


def get_only_single(td):
    towards_p0 = (td['coll_type'] == 'single_p0').to_numpy(int)
    towards_p1 = (td['coll_type'] == 'single_p1').to_numpy(int)
    return td[(towards_p0 + towards_p1).astype(bool)]


def get_skill_trial_class_count(td):
    p0_ahead_binary = (td['trial_class'] == 'p0_ahead').astype(int)
    p1_ahead_binary = (td['trial_class'] == 'p1_ahead').astype(int)
    concurrent_binary = (td['trial_class'] == 'concurrent').astype(int)
    return np.sum(p0_ahead_binary + p1_ahead_binary + concurrent_binary)


def get_trial_amount_estimator(tds):
    comp_fractions = [get_comp_frac(td) for td in tds]
    speed = get_mean_in_cm('speed', tds)
    curvature = get_mean_in_cm('path_len_due_to_curvature', tds)
    from_past_target = get_mean_in_cm('limiting_agent_next_target_displacement', tds) + get_mean_in_cm('reduction_due_to_adv_placement', tds)
    from_past_target = get_mean_in_cm('prev_next_target_displacement', tds)  #  todo with trial descriptor_2 the exected payoff is to low...
    advantageous_placement = get_mean_in_cm('reduction_due_to_adv_placement', tds)

    curvature_poly = np.zeros(4)
    curvature_poly[1:] = np.polyfit(comp_fractions, curvature, deg=2)
    from_past_target_poly = np.polyfit(comp_fractions, from_past_target, deg=3)
    advantageous_placement_ploy = np.zeros(4)
    advantageous_placement_ploy[1:] = np.polyfit(comp_fractions, advantageous_placement, deg=2)
    distance_ploy = curvature_poly + from_past_target_poly - advantageous_placement_ploy
    speed_poly = np.polyfit(comp_fractions, speed, deg=1)

    def trial_amount_estimator(x):
        return 20 * 60 / (1 + np.polyval(distance_ploy, x) / np.polyval(speed_poly, x))

    return trial_amount_estimator


def duration_to_rew(dur):
    return .035 * 20 * 60 / (dur + 1)


def plot_skill_diff_curves(agent_rew_mean_curve, ax, skill_diff=.1, c='k', alpha_base=.3, linewidth_base=2, xspace=None):
    if xspace is None:
        xspace = np.linspace(0, 1)
    rew_diff = skill_diff * xspace * 2 * agent_rew_mean_curve
    lower = agent_rew_mean_curve - rew_diff / 2
    higher = agent_rew_mean_curve + rew_diff / 2
    ax.plot(xspace, lower, label=f'Lower payoff for {int(100 * skill_diff)}% difference in single targets', c=c)
    ax.plot(xspace, higher, label=f'Higher payoff for {int(100 * skill_diff)}% difference in single targets', c=c)


if __name__ == '__main__':
    rows, cols = 2, 3
    fig_width = 7
    fig_ratio = 9 / 16
    fig, axes = plt.subplots(rows, cols, figsize=(fig_width, fig_width * fig_ratio))
    axes = axes.flatten()
    plt.subplots_adjust(left=.1, right=.987, top=.95, bottom=.14, wspace=.6, hspace=.8)
    axes[2].remove()
    axes[5].remove()
    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    a, b, c = get_agent_rewards(tds)
    agent_mean_reward, low_rewards, high_rewards = np.array(a), np.array(b), np.array(c)
    width = .05
    arrow_len = .3
    color = 'k'
    alpha = 1.
    linewidth = 1.4
    comp_fractions = np.array(comp_fractions)
    cmap = get_cmap()
    special_markers = {2: 'd', 4: 'd', 7: 'd', 53: 'P'}
    for i, (comp_frac, mean, low, high) in enumerate(zip(comp_fractions, agent_mean_reward, low_rewards, high_rewards)):
        comp_frac_c = cmap.colors[int(min([comp_frac, 1]) * 255)]
        for c, scalar in zip(['w', comp_frac_c], [1.3, 1]):
            if high - low > .30:
                axes[0].plot([i, i], [low, high], c=c, alpha=alpha, linewidth=linewidth * scalar,
                             solid_capstyle='round', zorder=10)
            else:
                marker = 'o'
                s = linewidth * scalar
                if i in special_markers.keys():
                    marker = special_markers[i]
                    s *= 3
                if c == 'w':
                    s *= 1.05
                axes[0].scatter([i], [low + (high - low) / 2], c=c, s=s, marker=marker, zorder=20)

    ####################

    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)
    single_target_diff = get_single_target_diff(tds)
    # relative_single_target_diff = np.array(single_target_diff) / (comp_fractions * np.array([len(td) for td in tds]))
    to_plot = pd.DataFrame(dict(
        payoff_diff=high_rewards - low_rewards,
        single_target_diff=single_target_diff,
        comp_fractions=comp_fractions,
    ))
    # todo in current version the markers are not right...
    scatterplot(to_plot, x='single_target_diff', y='payoff_diff', ax=axes[1])
    axes[1].plot([0, 220], [0, 220 * 0.07], c='k', linewidth=1, alpha=.7, zorder=1)
    diagonal_r_and_p_text(x=to_plot['single_target_diff'], y=to_plot['payoff_diff'], slope=0.07, ax=axes[1])

    # trial_durations = np.array([td['duration'].to_numpy().mean() for td in tds])
    # distance = get_mean_in_cm('distance', tds)
    speed = get_mean_in_cm('speed', tds)
    # additional_effort = get_mean_in_cm('distance_ol', tds)
    additional_effort = get_mean_in_cm('path_len_due_to_curvature', tds)
    # effort_from_past_target = get_mean_in_cm('chosen_effort', tds) + get_mean_in_cm('path_shortening', tds)
    # effort_reduction_by_ps = get_mean_in_cm('path_shortening', tds)
    effort_from_past_target = get_mean_in_cm('limiting_agent_next_target_displacement', tds) + get_mean_in_cm('reduction_due_to_adv_placement', tds)
    effort_reduction_by_ps = get_mean_in_cm('reduction_due_to_adv_placement', tds)

    duration = np.array([td['duration'].to_numpy().mean() for td in tds])
    # duration = get_mean_in_cm('duration', tds)

    xspace, speed_curve = plot_aic_poly(comp_fractions, speed)
    _, additional_effort_curve = plot_aic_poly(comp_fractions, additional_effort)
    _, effort_from_past_target_curve = plot_aic_poly(comp_fractions, effort_from_past_target)
    _, effort_reduction_by_ps_curve = plot_aic_poly(comp_fractions, effort_reduction_by_ps)

    # optimal_from_past_target_curve = get_optimal_from_past_target_curve()
    # optimal_effort_reduction = get_optimal_effort_reduction()

    # artificial_opti_distance_curve = optimal_from_past_target_curve + additional_effort.mean() - optimal_effort_reduction
    # artificial_opti_duration_curve = artificial_opti_distance_curve / speed.mean()
    # artificial_comp_distance_curve = optimal_from_past_target_curve + additional_effort.mean() - effort_reduction_by_ps_curve  # take 0 for epsilon strategy - however in the other cases we do also take the mean, here we use the fit instead because with the mean we would have illegal values for low fst values.
    # artificial_comp_distance_curve = optimal_from_past_target_curve + additional_effort_curve - effort_reduction_by_ps_curve  # take 0 for epsilon strategy - however in the other cases we do also take the mean, here we use the fit instead because with the mean we would have illegal values for low fst values.
    # artificial_comp_duration_curve = artificial_comp_distance_curve / speed_curve

    # opti_distance_curve = effort_from_past_target_curve + additional_effort_curve - optimal_effort_reduction
    # opti_duration_curve = opti_distance_curve / speed_curve
    comp_distance_curve = effort_from_past_target_curve + additional_effort_curve - effort_reduction_by_ps_curve
    comp_duration_curve = comp_distance_curve / speed_curve
    # comp_duration_curve = comp_distance_curve / speed.mean()  # expected payoff without speed effect

    _, org_duration_curve = plot_aic_poly(comp_fractions, duration)

    # artificial_comp_reward_curve = duration_to_rew(artificial_comp_duration_curve)
    # artificial_opti_reward_curve = duration_to_rew(artificial_opti_duration_curve)
    # axes[0].plot(xspace, artificial_opti_reward_curve)
    # axes[0].plot(xspace, artificial_comp_reward_curve)

    comp_reward_curve = duration_to_rew(comp_duration_curve)
    org_reward_curve = duration_to_rew(org_duration_curve)
    # opti_reward_curve = duration_to_rew(opti_duration_curve)

    axes[3].plot(xspace, comp_reward_curve, label='Both payoffs for 0% difference in single targets', c='k')
    plot_skill_diff_curves(comp_reward_curve, axes[3], c=np.ones(3) * .25, xspace=xspace)
    plot_skill_diff_curves(comp_reward_curve, axes[3], skill_diff=.3, c=np.ones(3) * .6, xspace=xspace)

    skill_diff =.06  # for supplementary plot....
    xspace = np.linspace(0, 1)
    rew_diff = skill_diff * xspace * 2 * comp_reward_curve
    higher = comp_reward_curve + rew_diff / 2
    axes[3].plot(xspace, higher, label=f'Lower payoff for {int(100 * skill_diff)}% difference in single targets', linestyle='dotted', c='r')

    # axes[3].plot(xspace, org_reward_curve, label='Both payoffs for 0% difference in single targets', c='k')
    # plot_skill_diff_curves(org_reward_curve, axes[3], c=np.ones(3) * .25)
    # plot_skill_diff_curves(org_reward_curve, axes[3], skill_diff=.3, c=np.ones(3) * .6)

    ###############

    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)

    single_target_diff = get_single_target_diff(tds)

    payoff_diff = high_rewards - low_rewards
    skill_differences = np.array(get_skill_differences(tds))
    # skill_differences[np.isnan(skill_differences)] = 0.

    trial_amount_estimator = get_trial_amount_estimator(tds)

    def get_trial_amounts(tds):  # per block, tds has initial 10 minute cut, divide by 1.5
        return np.array([len(td) / 1.5 for td in tds])

    payoff_diff_calculation = skill_differences * comp_fractions * get_trial_amounts(tds) * .07
    error_stats(payoff_diff, payoff_diff_calculation)

    payoff_diff_estimate = skill_differences * comp_fractions * trial_amount_estimator(comp_fractions) * .07
    error_stats(payoff_diff, payoff_diff_estimate)

    oss_tds = [get_only_straight_single(td) for td in tds]

    oss_skill_difference = np.array(get_skill_differences(oss_tds))
    # oss_skill_difference[np.isnan(oss_skill_difference)] = 0.

    oss_payoff_diff_estimate = oss_skill_difference * comp_fractions * trial_amount_estimator(comp_fractions) * .07
    error_stats(payoff_diff, oss_payoff_diff_estimate)

    only_straight_tds = [get_only_straight(td) for td in tds]
    # being_first_skill_diff = get_being_first_skill_diff(only_straight_tds)
    # bf_payoff_diff_estimate = being_first_skill_diff * comp_fractions * trial_amount_estimator(comp_fractions) * .07
    # error_stats(payoff_diff, bf_payoff_diff_estimate)

    xspace = np.linspace(0, 1)
    dyad_mean_payoff = .07 / 2 * trial_amount_estimator(xspace)

    payoff_diff_10 = .1 * xspace * trial_amount_estimator(xspace) * .07
    payoff_diff_30 = .3 * xspace * trial_amount_estimator(xspace) * .07

    high_payoff_curve_10 = dyad_mean_payoff + payoff_diff_10 / 2
    high_payoff_curve_30 = dyad_mean_payoff + payoff_diff_30 / 2

    cost_of_coop_curve_base = np.max(dyad_mean_payoff) - dyad_mean_payoff
    cost_of_coop_curve_10 = np.max(high_payoff_curve_10) - high_payoff_curve_10
    cost_of_coop_curve_30 = np.max(high_payoff_curve_30) - high_payoff_curve_30

    axes[4].plot(xspace, cost_of_coop_curve_base, c='k')
    axes[4].plot(xspace, cost_of_coop_curve_10, c=np.ones(3) * .25)
    axes[4].plot(xspace, cost_of_coop_curve_30, c=np.ones(3) * .6)

    dyad_mean_payoff = .07 / 2 * trial_amount_estimator(comp_fractions)
    # comp_mean_payoff = .07 / 2 * trial_amount_estimator(1)
    oss_payoff_diff_estimate = oss_skill_difference * comp_fractions * trial_amount_estimator(comp_fractions) * .07
    # oss_comp_payoff_diff_estimate = oss_skill_difference * 1 @ trial_amount_estimator(1) * .07
    oss_payoff_estimate = dyad_mean_payoff + oss_payoff_diff_estimate / 2

    oss_optimal_fst_payoff_estimate = list()
    dyad_mean_payoff_curve = .07 / 2 * trial_amount_estimator(xspace)
    for skill_diff in oss_skill_difference:
        estimated_payoff_differences = skill_diff * xspace * trial_amount_estimator(xspace) * .07
        fst_payoff_estimate = dyad_mean_payoff_curve + estimated_payoff_differences / 2
        oss_optimal_fst_payoff_estimate.append(np.max(fst_payoff_estimate))
    oss_optimal_fst_payoff_estimate = np.array(oss_optimal_fst_payoff_estimate)

    # oss_comp_payoff_estimate = comp_mean_payoff + oss_comp_payoff_diff_estimate / 2
    # cost_of_cooperation = oss_comp_payoff_estimate - oss_payoff_estimate
    cost_of_cooperation = oss_optimal_fst_payoff_estimate - oss_payoff_estimate

    # bf_payoff_diff_estimate = being_first_skill_diff * comp_fractions * trial_amount_estimator(comp_fractions) * .07
    # bf_comp_payoff_diff_estimate = being_first_skill_diff * 1 * trial_amount_estimator(1) * .07
    # bf_payoff_estimate = dyad_mean_payoff + bf_payoff_diff_estimate / 2
    # bf_comp_payoff_estimate = comp_mean_payoff + bf_comp_payoff_diff_estimate / 2
    # cost_of_cooperation_2 = bf_comp_payoff_estimate - bf_payoff_estimate

    to_plot = pd.DataFrame(dict(comp_fractions=comp_fractions, cost_of_cooperation=cost_of_cooperation))
    scatterplot(to_plot, x='comp_fractions', y='cost_of_cooperation')

    axes[4].set_xlabel('Stable FST')
    axes[4].set_ylabel('Cost of\ncooperation (€)')
    axes[4].fill_between([-1, 0.08], -1, 12, color='gray', alpha=.2, edgecolor=None, label='No skill\ndiff. data')  # todo find right x value
    axes[4].legend(loc='upper left', bbox_to_anchor=(-.42, -.58), frameon=False)
    axes[4].set_ylim(-.42, 10.02)
    axes[4].set_xlim(-.02, 1.02)
    comp_frac_x_format(axes[4])

    ###########

    axes[0].set_xlabel('Dyads sorted by FST')
    axes[0].set_ylabel('Payoff (€)')
    axes[0].set_box_aspect(1)
    axes[0].set_xticks([0, 29, 58])

    for ax_label, ax in zip('ab cd', axes):
        ax.set_box_aspect(1)
        ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes, weight='bold')

    axes[3].set_ylim([14, 29])

    axes[1].set_xlabel('Single target\ndifference')
    axes[1].set_ylabel('Payoff\ndifference (€)')
    axes[3].set_xlabel('Stable FST')
    axes[3].set_ylabel('Expected\npayoff (€)')
    comp_frac_x_format(axes[3])
    axes[1].xaxis.set_major_locator(ticker.FixedLocator([0, 100, 200]))
    axes[1].yaxis.set_major_locator(ticker.FixedLocator([0, 7, 14]))
    axes[1].set_ylim([-10 * .07, 210 * .07])
    axes[1].set_xlim([-10, 210])
    save_fig(sub_folder='presentation/', format='svg')

    # correlations
    oss_payoff_diff_estimate[np.isnan(oss_payoff_diff_estimate)] = 0
    low_payoff_estimates = dyad_mean_payoff - oss_payoff_diff_estimate / 2
    high_payoff_estimates = dyad_mean_payoff + oss_payoff_diff_estimate / 2
    individual_payoff_estimates = np.concatenate([low_payoff_estimates, high_payoff_estimates])
    individual_payoffs = np.concatenate([low_rewards, high_rewards])
    r, p = pearsonr(individual_payoff_estimates, individual_payoffs)

    # def coefficient_of_determination(target, prediction):
    #     return 1 - np.sum((target - prediction) ** 2) / np.sum((target - np.mean(target)) ** 2)
    #
    # print('individual payoff - estimated individual payoff - coefficient of determination R^2',
    #       coefficient_of_determination(individual_payoffs, individual_payoff_estimates))
    print('individual payoff - estimated individual payoff', r, p)

    r, p = pearsonr(payoff_diff, comp_fractions)
    print('payoff diff - fst', r, p)
    r, p = pearsonr(payoff_diff, single_target_diff)
    print('payoff diff - single_target_diff', r, p)
    # to say 'large part due to skill diff'
    # print(single_target_diff.mean(), single_target_diff.std())
    # single_target_diff_due_to_skill_diff = oss_payoff_diff_estimate / 0.07
    # print(single_target_diff_due_to_skill_diff.mean(), single_target_diff_due_to_skill_diff.std())
    # todo get single target collection trials, get percentage of one ahead and concurrent
    only_single_tds = [get_only_single(td) for td in tds]
    total_single_coll = np.array([len(td) for td in only_single_tds])
    single_coll_due_to_skill = np.array([get_skill_trial_class_count(td) for td in only_single_tds])
    total_single_coll_due_to_skill = sum(single_coll_due_to_skill) / sum(total_single_coll)
    print(total_single_coll_due_to_skill)
    in_competitive_group = 0.9 <= comp_fractions
    total_single_coll = total_single_coll[in_competitive_group]
    single_coll_due_to_skill = single_coll_due_to_skill[in_competitive_group]
    total_single_coll_due_to_skill = sum(single_coll_due_to_skill) / sum(total_single_coll)
    print(total_single_coll_due_to_skill)



