import os.path

import numpy as np
import pandas as pd
import scipy.optimize
from matplotlib import pyplot as plt

from dyadic_movements import sigmoid
from glm.util import add_properties
from load import data_loader
from load.data_loader import load, pair_idx_to_ids, get_rec_num_path


def cartesian_to_polar(x, y):
    rho = np.sqrt(x ** 2 + y ** 2)
    phi = np.arctan2(y, x)
    return np.stack([rho, phi]).T


def polar_to_cartesian(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return np.stack([x, y]).T


def get_velocity(rec):
    positions = rec[['p0_agent_x', 'p0_agent_y']].to_numpy()
    return np.diff(positions, axis=0)


def get_polar_velocity(rec, speed_limit):
    velocity = get_velocity(rec)
    polar_velocity = cartesian_to_polar(velocity[:, 0], velocity[:, 1])
    polar_velocity[:, 0] /= speed_limit
    polar_velocity[:, 1] %= 2 * np.pi  # such that all values are positive [0, 2pi)
    length_is_zero = polar_velocity[:, 0] == 0
    polar_velocity[:, 0][length_is_zero] = 1e-6  # for beta distribution, it's defined only for x>0
    polar_velocity[:, 1][length_is_zero] = 2 * np.pi * np.random.random(np.sum(length_is_zero.astype(int)))  # remove bias
    length_is_one = polar_velocity[:, 0] == 1
    polar_velocity[:, 0][length_is_one] = 1 - 1e-6  # for beta distribution, it's defined only for x<1
    return polar_velocity


def get_rel_accelerations(rel_velocities):
    return [np.diff(np.vstack([np.zeros((1, 2)), rel_vel]), axis=0) for rel_vel in rel_velocities]


def get_depth(covariate, depth):
    return covariate[kernel_size - depth: -depth]


def zero_padding(covariate, kernel_size):
    return np.concatenate([np.zeros(kernel_size), covariate])


def get_model_aic(w, args):
    model = args[1]
    return 2 * model.nll(w, *args) + 2 * len(w)


def get_velocity_aic(vel_prediction, velocity, param_amount):  # this assumes normal distributed velocity
    residuals = velocity - vel_prediction
    sample_cov_mat = residuals.T @ residuals / len(velocity)
    n, d = velocity.shape  # number of samples and dimensionality
    log_like = -1 * n / 2 * (d * np.log(2 * np.pi) + np.log(np.linalg.det(sample_cov_mat)) + d)
    # mse = np.mean((velocity - vel_prediction) ** 2)
    # nll = len(velocity) * (np.log(mse) + 1 + np.log(2 * np.pi))
    return 2 * param_amount - 2 * log_like


def get_velocity_mse_for_speed_model(model_results, velocity):
    speed = model_results['predictions'] * speed_limit
    polar_vel_prediction = np.stack([speed, polar_velocity[:, 1]]).T
    vel_prediction = polar_to_cartesian(polar_vel_prediction[:, 0], polar_vel_prediction[:, 1])
    return np.mean((velocity - vel_prediction) ** 2)


def get_velocity_mse(vel_prediction, velocity):
    return np.mean((velocity - vel_prediction) ** 2)


def get_covariates(rec, speed_limit):
    polar_velocity = get_polar_velocity(rec, speed_limit)
    polar_acceleration = np.diff(np.vstack([np.zeros((1, 2)), polar_velocity]), axis=0)  # todo check whether it really contains the PRIOR acceleration
    return dict(
        speed=polar_velocity[:, 0],
        speed_squared=polar_velocity[:, 0] ** 2,
        angle=polar_velocity[:, 1],
        acceleration=polar_acceleration[:, 0],
        acceleration_squared=polar_acceleration[:, 0] ** 2,
        angle_change=polar_acceleration[:, 1],
        intercept=np.ones_like(polar_velocity[:, 0]),
    )
    # return dict(
    #     speed=zero_padding(speed, kernel_size),   # we do zero padding only here for simplicity with data handling actually we do no padding!
    #     angle=zero_padding(angle, kernel_size),
    #     acceleration=zero_padding(acceleration, kernel_size),
    #     angle_change=zero_padding(angle_change, kernel_size),
    # )


def get_covariate_matrix(covariates, covariate_keys, depth, kernel_size):
    selection = list()
    covariate_names = list()
    for key in covariate_keys:
        zero_padded_covariate = zero_padding(covariates[key], kernel_size)
        for d in range(depth):
            selection.append(get_depth(zero_padded_covariate, d + 1))
            covariate_names.append(f'{key}_depth{d+1}')
    # selection.append(np.ones_like(selection[-1]))
    # covariate_names.append('intercept')
    return np.stack(selection), covariate_names


def get_model_results(models, pred_targets, rec, speed_limit, dispersion=None, modeling_speed=False, to_include=None,
                      x0=None):
    covariates = get_covariates(rec, speed_limit)
    model_results = list()
    for model in models:
        # print(model.name)
        if x0 is None:
            x0 = 1e-4 * np.ones(model.param_amount)
        covariate_matrix, covariate_names = get_covariate_matrix(covariates, model.covariate_keys,
                                                                 model.depth, kernel_size)
        args = (covariate_matrix, model, pred_targets, to_include, dispersion)
        res = scipy.optimize.minimize(fun=model.nll, x0=x0, args=args,
                                      # options=dict(disp=True),
                                      method='Nelder-Mead')
        predictions = model(res['x'], covariate_matrix)
        pred_target_comparision=pd.DataFrame(dict(prediction=predictions, target=pred_targets))
        if modeling_speed:
            pred_target_comparision = pred_target_comparision[pred_targets < 1]
        model_results.append(dict(
            aic=get_model_aic(res['x'], args),
            # coefficients=pd.DataFrame([res['x']],
            #                           columns=['coefficients'], index=covariate_names),
            coefficients=pd.DataFrame(dict(coefficients=res['x']), index=covariate_names),
            pred_range=[min(predictions), max(predictions)],
            pred_target_range=[min(pred_targets), max(pred_targets)],
            pred_target_comparision=pred_target_comparision,
            squared_residuals=(predictions-pred_targets) ** 2,
            predictions=predictions,
            pred_targets=pred_targets,
            res=res,
            args=args,
        ))
    return model_results


def get_mov_pred_dir(rec_path):
    mov_pred_dir = f'{rec_path}/movement_prediction'
    if not os.path.exists(mov_pred_dir):
        os.makedirs(mov_pred_dir)
    return mov_pred_dir


def save_predictions(predictions, p_idx, rec, cov_glm=False):
    if not cov_glm:
        padded_predictions = np.vstack([np.ones(2), predictions])
    else:
        padded_predictions = np.vstack([np.ones((1, 2, 2)), predictions.reshape((predictions.shape[0], 2, 2))])
    org_sorted_predictions = np.zeros_like(padded_predictions)
    for idx, pred in zip(rec.index.to_numpy(), padded_predictions):
        org_sorted_predictions[idx] = pred
    mov_pred_dir = get_mov_pred_dir(rec_path)
    if not cov_glm:
        np.save(f'{mov_pred_dir}/p{p_idx}_pred.npy', org_sorted_predictions)
    else:
        np.save(f'{mov_pred_dir}/p{p_idx}_var_pred.npy', org_sorted_predictions)


# def sigmoid(x):
#     # x[x < -10] = -10
#     x = 1 / (1 + np.exp(-x))
#     x[x == 1.] = 1 - 1e-6
#     return x
#
#
def beta_nll(w, covariate_matrix, model, pred_target, to_include, dispersion):
    pred = model(w, covariate_matrix)
    pred_target = pred_target[to_include]
    pred = pred[to_include]
    dispersion = dispersion[to_include]
    alpha = pred * dispersion
    beta = (1 - pred) * dispersion
    # alpha[alpha == 0] = 1e-7  # handeling pred = 1 and pred = 0...
    # beta[beta == 0] = 1e-7
    # assert all(alpha > 0)
    # assert all(beta > 0)
    return -1 * np.sum(scipy.stats.beta.logpdf(x=pred_target, a=alpha, b=beta)) #+ 10 * np.sum(w ** 2)


def gamma_nll(w, covariate_matrix, model, pred_target, to_include, unused_dispersion_parameter=None):
    pred = model(w, covariate_matrix)
    pred_target = pred_target[to_include]
    pred = pred[to_include]

    residual_deviance = np.sum(2 * ((pred_target - pred) / pred - np.log(pred_target / pred)))

    # Estimate dispersion parameter phi
    dispersion = residual_deviance / (len(pred_target) - len(w))

    # Estimate scale parameter theta for each observation
    theta_hat = dispersion * pred
    a = pred / theta_hat
    nll = -1 * np.sum(scipy.stats.gamma.logpdf(x=pred_target, a=a, scale=theta_hat))
    return nll


def get_speed_glm(depth, keys):
    @add_properties(param_amount=len(keys)*depth, covariate_keys=keys, name=f'speed_glm_{"_".join(keys)}_depth={depth}',
                    nll=beta_nll, depth=depth)
    def glm(weights, covariate_matrix):
        return sigmoid(weights @ covariate_matrix)
    return glm


def get_var_glm(depth, keys):
    @add_properties(param_amount=len(keys)*depth, covariate_keys=keys, name=f'var_glm_{"_".join(keys)}_depth={depth}',
                    nll=gamma_nll, depth=depth)
    def glm(weights, covariate_matrix):
        nu = weights @ covariate_matrix
        # return 1 / (abs(nu) + 1)
        return np.exp(nu)

    return glm

# not totally horrible response functions
# return 1 / (abs(nu) + 1)
# return 1 / (nu ** 2 + 1)
# return np.exp(weights @ covariate_matrix)


def get_new_beta_dispersion(model_results, var_model_results, damping_factor):
    speed_mean_prediction = model_results[0]['predictions']
    speed_var_prediction = var_model_results[0]['predictions']
    new_beta_dispersion = speed_mean_prediction * (1 - speed_mean_prediction) / speed_var_prediction - 1
    new_beta_dispersion = beta_dispersion + damping_factor * (new_beta_dispersion - beta_dispersion)
    return np.max([1e-6 * np.ones_like(new_beta_dispersion), new_beta_dispersion], axis=0)


if __name__ == '__main__':
    kernel_size = 10

    pair_idx = 38
    p_ids, rec_num = pair_idx_to_ids(pair_idx)
    rec_path = get_rec_num_path(p_ids[1], rec_num[1], data_loader.recordings_folder)
    org_rec = load(pair_idx, frames_per_sec=120)[1]

    speed_limit_per_frame = 0.007
    pred_target_frame = 10
    speed_limit = speed_limit_per_frame * pred_target_frame

    rec = org_rec.iloc[::pred_target_frame]  # todo extend 'to_include' with those which have the jump in their covariate!
    # rec = pd.concat([org_rec.iloc[i::pred_target_frame] for i in range(10)])  # todo this dirty connection causes failing dispersion parameters

    polar_velocity = get_polar_velocity(rec, speed_limit)
    speed = polar_velocity[:, 0]
    velocity = get_velocity(rec)

    speed_models = [
        get_speed_glm(depth=1, keys=['speed', 'acceleration', 'intercept']),
    ]
    speed_var_models = [
        get_var_glm(depth=1, keys=['speed', 'acceleration', 'intercept']),  # covariate idea: time or time_bins since collection start
    ]

    # speed_models = [
    #     get_speed_glm(depth=1, keys=['speed', 'acceleration', 'intercept']),
    # ]
    # speed_var_models = [
    #     get_var_glm(depth=1, keys=['speed', 'acceleration', 'intercept']),  # covariate idea: time or time_bins since collection start
    # ]
    beta_glm_coefficients = 1e-4 * np.ones(speed_models[0].param_amount)
    prev_coefficients = 100 * np.ones(speed_models[0].param_amount + speed_var_models[0].param_amount)
    beta_dispersion = 1e-4 * np.ones_like(speed)
    initial_damping_factor = 1
    max_iterations = 10
    to_include = speed < 1
    model_results = get_model_results(models=speed_models, pred_targets=speed, rec=rec, speed_limit=speed_limit,
                                      dispersion=beta_dispersion, modeling_speed=True, to_include=to_include,
                                      x0=beta_glm_coefficients)
    mse = get_velocity_mse_for_speed_model(model_results[0], velocity)
    print(f'Initial MSE: {mse}')
    for i in range(max_iterations):
        var_model_results = get_model_results(models=speed_var_models, pred_targets=model_results[0]['squared_residuals'],
                                              rec=rec, speed_limit=speed_limit, modeling_speed=True, to_include=to_include)

        model_improved = False
        improvement_attempts = 0
        additional_decay = 0
        while not model_improved:
            improvement_attempts += 1
            if improvement_attempts == 4:
                additional_decay = 0
            damping_factor = initial_damping_factor * np.exp(-(0 + additional_decay) / 10)

            new_beta_dispersion = get_new_beta_dispersion(model_results, var_model_results, damping_factor)
            new_model_results = get_model_results(models=speed_models, pred_targets=speed, rec=rec, speed_limit=speed_limit,
                                                  dispersion=new_beta_dispersion, modeling_speed=True,
                                                  to_include=to_include, x0=beta_glm_coefficients)
            new_mse = get_velocity_mse_for_speed_model(new_model_results[0], velocity)
            print(f'MSE: {new_mse} Damping factor: {damping_factor}')
            if new_mse < mse or improvement_attempts == 4:
                if improvement_attempts == 4:
                    print('continue with damping factor = 1...')
                if new_mse < mse:
                    print('Model improvement!')
                model_improved = True
                mse = new_mse
                model_results = new_model_results
                beta_glm_coefficients = model_results[0]['res']['x']
            else:
                print('Overshoot! Increasing decay...')
                additional_decay += 10

        print(model_results[0]['coefficients']['coefficients'])
        coefficients = np.concatenate([model_results[0]['coefficients'], var_model_results[0]['coefficients']])
        if np.sum(abs(coefficients - prev_coefficients)) < 1e-3:
            print('end due to convergence!')
            break
        prev_coefficients = coefficients

    predictions = model_results[-1]['predictions'].copy()
    pred_targets = model_results[-1]['pred_targets'].copy()
    speed_predictions = predictions
    speed_residuals = pred_targets - speed_predictions
    plt.scatter(speed_predictions, speed_predictions + speed_residuals, alpha=.1)
