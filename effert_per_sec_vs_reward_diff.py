import matplotlib.pyplot as plt
import numpy as np

from _util import game_width_in_cm, save_fig
from glm.plotting.entropy_vs_duration import plot_correlation
from reward_decomposition import get_payoff_diff, scatter
from trial_descriptor import load_all_trial_descriptions, get_comp_frac

# from src.classifier.main import load_all_trial_descriptions
# from src.classifier.trial_correlations import get_payoff_diff, plot_correlation
# from src.classifier.trial_type_shares import save_fig
# from src.main.main_2 import game_width_in_cm

if __name__ == '__main__':
    tds = load_all_trial_descriptions()
    comp_fractions = [get_comp_frac(td) for td in tds]
    avg_dist = np.array([td['distance'].mean() for td in tds]) * game_width_in_cm
    trial_durations = np.array([(td['end'].to_numpy() - td['start'].to_numpy()).mean() for td in tds])
    trial_durations = trial_durations / 120 + 1  # add coll process duration and change unit to seconds
    effort_per_second = avg_dist / trial_durations
    reward_diff = np.array([get_payoff_diff(td) for td in tds])
    fig, axes = plt.subplots(1, 2)
    # axes[1].scatter(abs(reward_diff), effort_per_second)
    scatter(x=effort_per_second, y=abs(reward_diff), ax=axes[0])
    scatter(x=comp_fractions, y=abs(reward_diff), ax=axes[1])
    # plot_correlation(abs(reward_diff), effort_per_second, ax=axes[1])
    plt.tight_layout()
    save_fig()
