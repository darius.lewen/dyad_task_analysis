import numpy as np
import pandas as pd
import seaborn
import seaborn as sn
from matplotlib import pyplot as plt, dates, ticker
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import AutoMinorLocator, FixedLocator
import scipy

import glm.linearities_sim as lin
from _util import save_fig
from conv_visualized import load_frame_count_corrected_tds, get_fst_ma_series, get_effort_prediction_mas, fps
from glm.likelihood import gen_glm
from glm.likelihood_2 import get_per_dyad_stats, load_per_dyad_stats
from glm.plotting.type_hist import get_hist_plot_frame
from glm.swarm_parformance import model_selection
from load.loading_helper import unique_pairs
from pred_performance import generate_fst_bar
from presentation.plotting import violin_swarm, scatterplot
from trial_descriptor_2 import get_comp_frac, unsorted_load_all_trial_descriptions, load_trial_descriptions
from trial_descriptor_2 import load_all_trial_descriptions


window = 30  # second
ma_range = np.arange(2 * 20 * 60 - window)


# def get_full_glm_prediction_mas(tds):
#     prediction_mas = list()
#     for td in tds:
#         expected_values = td[['pred_prob_single', 'pred_prob_joint0', 'pred_prob_joint1']].to_numpy()
#         fst_prediction_binary = (expected_values.argmax(axis=1) == 0).astype(int)
#         prediction_mas.append(ma_base(fst_prediction_binary, td['start_time'].to_numpy(), start_time=10 * 60))
#     return prediction_mas


def timestamp_moving_average(single_t_binary, timings, w=None):
    if w is None:
        global window
        w = window
    ma_series = list()
    timings = timings.to_numpy()
    for i in ma_range:
        above = (timings > i).astype(int)
        below = (timings < i + w).astype(int)
        in_window = above + below == 2
        ma_series.append(np.mean(single_t_binary[in_window]))
    return np.array(ma_series)


def get_true_fst_binary(td):
    coll_type = td['coll_type'].to_numpy()
    fst_binary = np.zeros_like(coll_type)
    fst_binary[coll_type == 'single_p0'] = 1
    fst_binary[coll_type == 'single_p1'] = 1
    return fst_binary


def get_pred_fst_binary(expected_values):
    pred_fst_binary = (0 == expected_values.argmax(axis=1)).astype(int)
    return pred_fst_binary


def get_sorted_effort_glm_stats():
    comp_frac_sorting = np.argsort([get_comp_frac(td) for td in unsorted_load_all_trial_descriptions()])
    per_dyad_stats_effort_glm = get_per_dyad_stats(gen_glm([lin.effort_linearity]), save=False)
    return [per_dyad_stats_effort_glm[dyad_idx] for dyad_idx in comp_frac_sorting]


def get_coll_type_encoding(td):
    coll_types = td['coll_type'].to_numpy().copy()
    coll_types[coll_types == 'single_p0'] = 0
    coll_types[coll_types == 'single_p1'] = 0
    coll_types[coll_types == 'joint0'] = 1
    coll_types[coll_types == 'joint1'] = 2
    return coll_types.astype(int)


def get_closest_pred_acc(tds):
    accuracies = list()
    for td in tds:
        collection_encoding = get_coll_type_encoding(td)
        distances = td[['dist_single', 'dist_joint0', 'dist_joint1']].to_numpy()
        dist_pred = np.argmin(distances, axis=1)
        accuracies.append(np.mean((dist_pred == collection_encoding).astype(int)))
    return accuracies


def get_prediction_encoding(expected_values):
    return np.argmax(expected_values, axis=1)


if __name__ == '__main__':
    fig_width = 7
    fig_ratio = 9 / 16
    fig = plt.figure(figsize=(fig_width, fig_width * fig_ratio))
    # grid_spec = GridSpec(2, 3, width_ratios=[2.4, 1.4, 1.], height_ratios=[1, 1],
    #                      right=.96, top=.94, bottom=None, left=None, wspace=.35, hspace=.38)
    # grid_spec = GridSpec(2, 3, width_ratios=[2.4, 1.5, .7], height_ratios=[1, 1],
    #                      right=.96, top=.94, bottom=None, left=None, wspace=.35, hspace=.46)
    # grid_spec = GridSpec(2, 3, width_ratios=[2.4, 1.5, .7], height_ratios=[1, 1],
    #                      right=.96, top=.94, bottom=None, left=None, wspace=.6, hspace=.46)
    # grid_spec = GridSpec(2, 3, width_ratios=[2.5, 3, 1.], height_ratios=[1, 1],
    #                      right=.96, top=.94, bottom=None, left=None, wspace=.5, hspace=.46)
    grid_spec = GridSpec(2, 3, width_ratios=[2.2, 3, 1], height_ratios=[1, 1],
                         right=.96, top=.94, bottom=None, left=None, wspace=.5, hspace=.46)
    axes = np.array([
        fig.add_subplot(grid_spec[0]),
        fig.add_subplot(grid_spec[1: 3]),
        fig.add_subplot(grid_spec[3: 5]),
        fig.add_subplot(grid_spec[5]),
    ])

    # tds = load_all_trial_descriptions()
    tds = load_all_trial_descriptions()
    dyad_idx = 28
    td = tds[dyad_idx]
    full_td = load_all_trial_descriptions(cut_initial=False)[dyad_idx]
    sorted_effort_glm_stats = get_sorted_effort_glm_stats()

    true_fst_binary = get_true_fst_binary(full_td)
    dist_glm_fst_pred_binary = get_pred_fst_binary(expected_values=sorted_effort_glm_stats[dyad_idx]['expected_values'])
    full_glm_fst_pred_binary = get_pred_fst_binary(expected_values=td[['pred_prob_single', 'pred_prob_joint0', 'pred_prob_joint1']].to_numpy().copy())

    ma_true_fst = timestamp_moving_average(true_fst_binary, full_td['start_time'])
    ma_dist_pred_fst = timestamp_moving_average(dist_glm_fst_pred_binary, td['start_time'])
    ma_full_pred_fst = timestamp_moving_average(full_glm_fst_pred_binary, td['start_time'])

    timings = pd.to_datetime(ma_range, unit='s')

    axes[2].plot(timings, ma_true_fst, linewidth=3, color='#CACACA', label=f'Data')
    axes[2].plot(timings, ma_dist_pred_fst, linewidth=1.1, alpha=1, c='#2AB1AA',
                 label=f'Distance weighing')
    axes[2].plot(timings, ma_full_pred_fst, linewidth=1.1, alpha=1, c='#2D2D83', label=f'Full GLM')

    sn.histplot(y=ma_true_fst, ax=axes[3], stat='density', kde=True, binwidth=.05, color='#CACACA',
                kde_kws=dict(bw_method=.3))  # todo exlude first 10 min
    sn.kdeplot(y=ma_dist_pred_fst, ax=axes[3], bw_method=.3, color='#2AB1AA')
    sn.kdeplot(y=ma_full_pred_fst, ax=axes[3], bw_method=.3, color='#2D2D83')

    dist_glm_acc = np.array([summary['accuracy'] for summary in sorted_effort_glm_stats])

    comp_frac_sorting = np.argsort([get_comp_frac(td) for td in unsorted_load_all_trial_descriptions()])
    full_glm_stats = load_per_dyad_stats()
    sorted_full_glm_stats = [full_glm_stats[i] for i in comp_frac_sorting]
    full_glm_acc = np.array([summary['accuracy'] for summary in sorted_full_glm_stats])

    to_plot = {
        'Predict\ncloset\ntarget': get_closest_pred_acc(tds),
        'Distance\nweighting': dist_glm_acc,
        'Full\nGLM': full_glm_acc,
    }
    violin_swarm(data=pd.DataFrame(to_plot), ax=axes[1], marker_size=3)


    res = scipy.stats.wilcoxon(to_plot['Predict\ncloset\ntarget'], to_plot['Distance\nweighting'])
    print('Accuracy: closest vs distance weighting')
    print(f'Wilcoxon signed-rank test: {res.statistic}, p-value: {res.pvalue}')
    res = scipy.stats.wilcoxon(to_plot['Distance\nweighting'], to_plot['Full\nGLM'])
    print('Accuracy: distance weighting vs glm')
    print(f'Wilcoxon signed-rank test: {res.statistic}, p-value: {res.pvalue}')

    axes[2].set_xlim([0, pd.to_datetime(2400, unit='s')])
    axes[2].set_ylim([-.02, 1.02])
    axes[3].set_ylim([-.02, 1.02])
    axes[2].xaxis.set_minor_locator(AutoMinorLocator(5))
    axes[2].xaxis.set_major_formatter(dates.DateFormatter('%M'))
    comp_frac_locator = ticker.FixedLocator([0, 1 / 3, 2 / 3, 1])
    comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1'])
    axes[2].yaxis.set_major_locator(comp_frac_locator)
    axes[2].yaxis.set_major_formatter(comp_frac_formatter)
    axes[3].yaxis.set_major_locator(comp_frac_locator)
    axes[3].yaxis.set_major_formatter(comp_frac_formatter)
    axes[2].set_xlabel('Time (min)')#, fontdict=fontdict)
    axes[2].set_ylabel('30-Second moving\naverage FST')#, fontdict=fontdict)
    axes[2].legend(loc='upper left', ncol=3, bbox_to_anchor=(-.01, 1.07), frameon=False, columnspacing=.6)#, fontsize=fontdict['size'])
    axes[0].set_xlabel('')
    axes[1].set_xlabel('')
    axes[0].xaxis.set_ticks_position('none')
    axes[1].set_ylabel('Prediction accuracy')
    axes[1].set_xticklabels(axes[1].get_xticklabels(), fontdict=dict(size=10))

    generate_fst_bar(axes[0])
    axes[1].set_ylim([-.02, 1.02])

    for ax_label, ax in zip(' acd', axes):
        ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes, weight='bold')

    axes[-1].xaxis.set_major_locator(FixedLocator([0, 2]))
    save_fig(sub_folder='presentation/', format='svg')
    save_fig(sub_folder='presentation/', format='pdf')

    to_plot = {
        'Predict\ncloset\ntarget': get_closest_pred_acc(tds),
        'Distance\nweighting': dist_glm_acc,
        'Full\nGLM': full_glm_acc,
    }
    res = scipy.stats.wilcoxon(np.array(to_plot['Predict\ncloset\ntarget']),
                               np.array([1/3 for _ in tds]))
    print(f'closest vs. chance - wilcoxon signed-rank test: {res.statistic}, p-value: {res.pvalue}')

    in_intermediate_group = np.array([.1 < get_comp_frac(td) < .9 for td in tds])
    u1, p = scipy.stats.mannwhitneyu(np.array(to_plot['Predict\ncloset\ntarget'])[in_intermediate_group],
                                     np.array(to_plot['Predict\ncloset\ntarget'])[~in_intermediate_group])
    print(f'intermediate group vs non-intermediates mannwhitneyu: {u1}, p-value: {p}')
    # timings = np.array([td['start_time'] for td in tds])
    # true_fst_binaries = np.array([get_true_fst_binary(td) for td in tds])
    # pred_fst_binaries = np.array([get_pred_fst_binary(td) for td in tds])
    # ma_true_fst = [timestamp_moving_average(fst_bin, t) for fst_bin, t in zip(true_fst_binaries, timings)]
    # ma_pred_fst = [timestamp_moving_average(fst_bin, t) for fst_bin, t in zip(pred_fst_binaries, timings)]
    # conv_timings = [pd.to_datetime(t, unit='s') for t in timings]
    # plt.plot(conv_timings[dyad_idx], ma_true_fst[dyad_idx])
    # plt.plot(conv_timings[dyad_idx], ma_pred_fst[dyad_idx])

    # todo reincorporate the wilcoxon test and update it...  -> this is already done in variance_fist supplementary -> delete this...
    # true_ma_var, dist_ma_var, full_ma_var = list(), list(), list()
    # for dyad_idx, td in enumerate(tds):
    #     true_fst_binary = get_true_fst_binary(td)
    #     dist_glm_fst_pred_binary = get_pred_fst_binary(expected_values=sorted_effort_glm_stats[dyad_idx]['expected_values'])
    #     full_glm_fst_pred_binary = get_pred_fst_binary(expected_values=td[['pred_prob_single', 'pred_prob_joint0', 'pred_prob_joint1']].to_numpy().copy())
    #
    #     true_ma_var.append(np.var(timestamp_moving_average(true_fst_binary, td['start_time'])))
    #     dist_ma_var.append(np.var(timestamp_moving_average(dist_glm_fst_pred_binary, td['start_time'])))
    #     full_ma_var.append(np.var(timestamp_moving_average(full_glm_fst_pred_binary, td['start_time'])))
