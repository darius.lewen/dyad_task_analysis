import numpy as np
import pandas as pd
import seaborn as sn
from matplotlib import pyplot as plt

from glm.copy_best_over import get_best_models


def unfold_weight_matrix(w):
    return np.array([
        [w[0], w[1], w[1]],
        [w[2], w[3], w[4]],
        [w[2], w[4], w[3]],
    ]).T


def unfold_effort(w):
    return np.array([w[0], w[1], w[1]])[:, None]


if __name__ == '__main__':
    weights = pd.read_pickle('performance.pkl')['effort_prev_t_inv']['weights']  # todo maybe remove the last list structure? its just Nones and our wegiht dict is the first element...
    weights = [w[0] for w in weights]
    weightings = list()
    for w in weights:
        comp_w, coop_w = w['effort']
        comp_after_comp, coop_after_coop, invite = w['prev_t_inv']
        weightings.append([comp_w, coop_w, comp_after_comp, coop_after_coop, invite])
        # dist_weightings.append(comp_w / (comp_w + coop_w))
        # prev_t_weightings.append(w['prev_t_inv'] / (comp_w + coop_w))
        # if dist_weightings[-1] < 0:
        #     dist_weightings[-1] *= -1
        #     prev_t_weightings[-1] *= -1
    # dist_weightings = np.array(dist_weightings)
    # prev_t_weightings = np.array(prev_t_weightings)
    # weightings = np.concatenate([dist_weightings[:, None], prev_t_weightings], axis=1)
    weightings = np.array(weightings)
    normalized_w = weightings / np.linalg.norm(weightings, axis=1)[:, None]
    sn.heatmap(normalized_w, cmap='vlag')
    # sn.heatmap(weightings, cmap='vlag', vmin=-1, vmax=1)


    # best_models = get_best_models()
    #
    # weights = best_models[1][1]
    #
    # to_plot = dict(
    #     distances=unfold_effort(weights['effort']),
    #     invite=unfold_weight_matrix(weights['invite']),
    #     hist=unfold_weight_matrix(weights['1_prev_t']),
    # )
    #
    # fig, axes = plt.subplots(3, 1)
    # axes = axes.flatten()
    #
    # for ax, (modality_name, weights) in zip(axes, to_plot.items()):
    #     sn.heatmap(ax=ax, data=weights)

