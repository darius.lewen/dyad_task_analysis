import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde


if __name__ == '__main__':
    n = 100
    evalpoints = 100
    B = 2500 * 2
    x = np.random.normal(size=n)
    xspace = np.linspace(-3, 3, num=evalpoints)
    d = gaussian_kde(x)
    estimates = np.empty((evalpoints, B))
    for b in range(B):
        xstar = np.random.choice(x, size=n, replace=True)
        dstar = gaussian_kde(xstar)
        estimates[:, b] = dstar(xspace)
    ConfidenceBands = np.quantile(estimates, q=[0.025, 0.975], axis=1)
    plt.plot(xspace, d(xspace), linewidth=2, color="purple", label="Density")
    plt.plot(xspace, np.exp(-xspace ** 2 / 2) / np.sqrt(2 * np.pi), linewidth=2, color="gold", label="Standard Normal")
    xshade = np.concatenate((xspace, xspace[::-1]))
    yshade = np.concatenate((ConfidenceBands[1], ConfidenceBands[0][::-1]))
    plt.fill(xshade, yshade, color="lightblue", alpha=0.4)
    plt.ylim(0, np.max(ConfidenceBands[1]) * 1.01)
    plt.title("Pointwise bootstrap confidence bands")
    plt.legend()
    plt.show()

