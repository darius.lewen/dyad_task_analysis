import numpy as np
import pandas as pd
import seaborn as sn
from matplotlib import pyplot as plt, ticker

from _util import save_fig
from glm.likelihood import entropy
from glm.plotting.entropy_corr import get_cut_tds, get_simple_entropy_ts
from per_class_entropy import reduced_classes, get_reduced_class_binary
from presentation.plotting import scatterplot
from reward_decomposition import plot_corr_coef
from trial_descriptor import unsorted_load_all_trial_descriptions, get_comp_frac, load_all_trial_descriptions

if __name__ == '__main__':
    tds = unsorted_load_all_trial_descriptions()
    unsorted_comp_fractions = [get_comp_frac(td) for td in tds]
    comp_frac_sorting = np.argsort(unsorted_comp_fractions)
    comp_fractions = [unsorted_comp_fractions[i] for i in comp_frac_sorting]

    tds = get_cut_tds()
    # mis_cord_measure = [td['mis_cord_measure'].to_numpy() for td in tds]
    entropies_per_dyad = get_simple_entropy_ts(filename='glm/unsorted_performance.pkl')
    entropies_per_dyad = [list(entropies_per_dyad[i]) for i in comp_frac_sorting]
    entropies_per_dyad_exc_comp = entropies_per_dyad[:-15]
    tds_exc_comp = [tds[i] for i in comp_frac_sorting][:-15]
    trial_classes = np.concatenate([td['trial_class'].to_numpy() for td in tds_exc_comp])
    p0_ahead = (trial_classes == 'p0_ahead').astype(int)
    p1_ahead = (trial_classes == 'p1_ahead').astype(int)
    concurrent = (trial_classes == 'concurrent').astype(int)
    one_ahead = (p0_ahead + p1_ahead + concurrent).astype(bool)
    to_plot = dict(
        ahead_measure_1=sum([list(abs(td['ahead_measure_1'].to_numpy())) for td in tds_exc_comp], []),
        ahead_measure_2=sum([list(abs(td['ahead_measure_2'].to_numpy())) for td in tds_exc_comp], []),
        ahead_measure_3=sum([list(abs(td['ahead_measure_3'].to_numpy())) for td in tds_exc_comp], []),
        ahead_measure_4=sum([list(abs(td['ahead_measure_4'].to_numpy())) for td in tds_exc_comp], []),
        ahead_measure_5=sum([list(abs(td['ahead_measure_5'].to_numpy())) for td in tds_exc_comp], []),
        curvature=sum([list(td['mis_cord_measure'].to_numpy()) for td in tds_exc_comp], []),
        entropy=sum(entropies_per_dyad_exc_comp, [])
    )
    to_plot = {key: np.array(val)[one_ahead] for key, val in to_plot.items()}
    fig, axes = plt.subplots(2, 3)
    axes = axes.flatten()
    to_plot = pd.DataFrame(to_plot)
    sn.regplot(to_plot, x='entropy', y='ahead_measure_1', x_bins=np.linspace(0, np.log2(3), 8), ax=axes[0])
    sn.regplot(to_plot, x='entropy', y='ahead_measure_2', x_bins=np.linspace(0, np.log2(3), 8), ax=axes[1])
    sn.regplot(to_plot, x='entropy', y='ahead_measure_3', x_bins=np.linspace(0, np.log2(3), 8), ax=axes[2])
    sn.regplot(to_plot, x='entropy', y='ahead_measure_4', x_bins=np.linspace(0, np.log2(3), 8), ax=axes[3])
    sn.regplot(to_plot, x='entropy', y='ahead_measure_5', x_bins=np.linspace(0, np.log2(3), 8), ax=axes[4])
    sn.regplot(to_plot, x='entropy', y='curvature', x_bins=np.linspace(0, np.log2(3), 8), ax=axes[5])
    plot_corr_coef(x=to_plot['entropy'], y=to_plot['ahead_measure_1'], ax=axes[0])
    plot_corr_coef(x=to_plot['entropy'], y=to_plot['ahead_measure_2'], ax=axes[1])
    plot_corr_coef(x=to_plot['entropy'], y=to_plot['ahead_measure_3'], ax=axes[2])
    plot_corr_coef(x=to_plot['entropy'], y=to_plot['ahead_measure_4'], ax=axes[3])
    plot_corr_coef(x=to_plot['entropy'], y=to_plot['ahead_measure_5'], ax=axes[4])
    plot_corr_coef(x=to_plot['entropy'], y=to_plot['curvature'], ax=axes[5])

    # last idea for ahead measure: cross correlation of first 30ms of acceleration test it for [1) on trajectory accelaraiton and 2)] acceleration towards target
