import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn

from load.data_loader import pair_idx_to_ids, get_rec_num_path, recordings_folder


def kl_divergence(p, q):
    p[p == 0] = np.nan
    q[q == 0] = np.nan
    return np.sum(p * np.log(p / q), axis=1)


def get_mixed_model(p, q):
    return (p + q) / np.sum(p + q, axis=1)[:, None]

def js_divergence(p, q):  # Jensen-Shannon divergence
    mixed_model = get_mixed_model(p, q)
    return (kl_divergence(p, mixed_model) + kl_divergence(q, mixed_model)) / 2


def js_distance(p, q):
    return np.sqrt(js_divergence(p, q))


def get_rectified_predictions(predictions):
    p0 = predictions[0, :, :3]
    p1 = predictions[1, :, :3]
    p0_rectified = p0 / p0.sum(axis=1)[:, None]
    p1_rectified = p1 / p1.sum(axis=1)[:, None]
    return np.stack([p0_rectified, p1_rectified])


if __name__ == '__main__':
    # dyad_idx = 0
    dyad_idx = 24
    lap = 1
    player_ids, record_numbers = pair_idx_to_ids(dyad_idx)
    path = get_rec_num_path(player_ids[lap], record_numbers[lap], recordings_folder)
    predictions = np.load(f'{path}/continuous_prediction.npy')
    rectified_predictions = get_rectified_predictions(predictions)
    js_dist = js_distance(rectified_predictions[0], rectified_predictions[1])
    js_dist_diff = np.diff(np.concatenate([[0], js_dist]))
    mixed_model = get_mixed_model(rectified_predictions[0], rectified_predictions[1])

    gmm_measures = pd.DataFrame(dict(
        js_distance=js_dist,
        js_distance_diff=js_dist_diff,
        # js_distance=js_distance(predictions[0], predictions[1]),
        # js_distance_diff=np.diff(np.concatenate([[0], js_distance(predictions[0], predictions[1])])),
        # p0_sta_prob=predictions[0, :, 0],  # todo remove
        # p1_sta_prob=predictions[1, :, 0],
        # mm_sta_prob=get_mixed_model(predictions[0], predictions[1])[:, 0],
        p0_single_prob=predictions[0, :, 0],
        p0_joint0_prob=predictions[0, :, 1],
        p0_joint1_prob=predictions[0, :, 2],
        p0_oversh_prob=predictions[0, :, 3],
        p1_single_prob=predictions[1, :, 0],
        p1_joint0_prob=predictions[1, :, 1],
        p1_joint1_prob=predictions[1, :, 2],
        p1_oversh_prob=predictions[1, :, 3],
        p0_single_prob_rectified=rectified_predictions[0, :, 0],
        p0_joint0_prob_rectified=rectified_predictions[0, :, 1],
        p0_joint1_prob_rectified=rectified_predictions[0, :, 2],
        p1_single_prob_rectified=rectified_predictions[1, :, 0],
        p1_joint0_prob_rectified=rectified_predictions[1, :, 1],
        p1_joint1_prob_rectified=rectified_predictions[1, :, 2],
        mm_single_prob=mixed_model[:, 0],
        mm_joint0_prob=mixed_model[:, 1],
        mm_joint1_prob=mixed_model[:, 2],
    ))

    gmm_measures.to_pickle(f'{path}/gmm_measures.pkl')
    # np.save(f'{path}/divergence.npy', js_div)

