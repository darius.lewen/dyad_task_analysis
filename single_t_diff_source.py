import numpy as np
from matplotlib import pyplot as plt

from conv_pred import get_class_amount_base
from rew_diff import get_skill_differences
from reward_decomposition import scatter, plot_corr_coef
from trial_descriptor import load_all_trial_descriptions, get_comp_frac


if __name__ == '__main__':
    tds = load_all_trial_descriptions()
    comp_fractions = [get_comp_frac(td) for td in tds]
    fig, axes = plt.subplots(1, 3, figsize=(5, 3))
    axes = axes.flatten()

    single_by_p0 = [np.array(td['coll_type'] == 'single_p0') for td in tds]
    single_by_p1 = [np.array(td['coll_type'] == 'single_p1') for td in tds]
    single_t_coll = [(by_p0.astype(int) + by_p1.astype(int)).astype(bool) for by_p0, by_p1 in zip(single_by_p0, single_by_p1)]
    single_t_coll_tds = [td[single_t_c] for td, single_t_c in zip(tds, single_t_coll)]
    # single_t_coll = (single_by_p0 + single_by_p1).astype(bool)
    skill_differences = get_skill_differences(tds)
    p0_ahead = np.array([get_class_amount_base(td, 'p0_ahead') for td in single_t_coll_tds])
    p1_ahead = np.array([get_class_amount_base(td, 'p1_ahead') for td in single_t_coll_tds])
    normalized_ahead_diff = abs(p0_ahead - p1_ahead) / (p0_ahead + p1_ahead)

    single_p0_tds = [td[s_by_p0] for td, s_by_p0 in zip(tds, single_by_p0)]
    single_p1_tds = [td[s_by_p1] for td, s_by_p1 in zip(tds, single_by_p1)]
    single_p0_opportunistic = [np.sum(td['trial_class'] == 'anti_corr') for td in single_p0_tds]
    single_p1_opportunistic = [np.sum(td['trial_class'] == 'anti_corr') for td in single_p1_tds]

    scatter(normalized_ahead_diff, skill_differences, axes[0])
    plot_corr_coef(normalized_ahead_diff[14:], skill_differences[14:], axes[0])
