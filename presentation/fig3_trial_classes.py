import numpy as np
import pandas as pd
import seaborn as sn
from matplotlib import pyplot as plt, ticker
from matplotlib.gridspec import GridSpec
from scipy.interpolate import splev, splrep
from scipy.stats import mannwhitneyu, pearsonr

from _util import save_fig, game_width_in_cm
from class_frac import get_class_scatter
from glm.likelihood import entropy
from glm.plotting.entropy_corr import get_cut_tds, get_simple_entropy_ts
from inefficiencies_full import comp_frac_x_format
from invite_effects_2 import get_invite_effects_df
from markov_traj import simplify_trial_class
from per_class_entropy import reduced_classes, get_reduced_class_binary
from presentation.plotting import scatterplot
from trial_descriptor_2 import unsorted_load_all_trial_descriptions, get_comp_frac, load_all_trial_descriptions


ordered_keys_1 = ['accepted_invite', 'declined_invite', 'concurrent', 'ahead', 'miscoordination', 'anti_corr']
ordered_keys_2 = ['accepted_invite', 'declined_invite', 'concurrent', 'one_ahead', 'miscoordination', 'anti_corr']
# colors = ['#9367bc', '#e277c1', '#e4ae38', '#fb4f2f', '#d62628', '#164a64']
colors = ['#e277c1', '#9367bc', '#e4ae38', '#fb4f2f', '#d62628', '#164a64']

# ordered_keys_1 = ['declined_invite', 'accepted_invite', 'concurrent', 'ahead', 'miscoordination', 'anti_corr']
# ordered_keys_2 = ['declined_invite', 'accepted_invite', 'concurrent', 'one_ahead', 'miscoordination', 'anti_corr']
# colors = ['#e277c1', '#9367bc', '#e4ae38', '#fb4f2f', '#d62628', '#164a64']

linewidth = .8

if __name__ == '__main__':
    fig_width = 7
    fig_ratio = 9 / 16
    # fig, axes = plt.subplots(1, 3, figsize=(fig_width, fig_width * fig_ratio / 2))
    # plt.subplots_adjust(left=.17, bottom=.3, right=.9, wspace=1)
    fig = plt.figure(figsize=(fig_width, fig_width * fig_ratio / 2))
    grid_spec = GridSpec(1, 3, width_ratios=[1, 1.5, 1], height_ratios=[1],
                         left=.1, bottom=.3, right=.95, wspace=1, top=.82)
    axes = np.array([
        fig.add_subplot(grid_spec[0]),
        fig.add_subplot(grid_spec[1]),
        fig.add_subplot(grid_spec[2]),
    ])
    axes = axes.flatten()

    tds = unsorted_load_all_trial_descriptions()
    unsorted_comp_fractions = [get_comp_frac(td) for td in tds]
    comp_frac_sorting = np.argsort(unsorted_comp_fractions)
    comp_fractions = [unsorted_comp_fractions[i] for i in comp_frac_sorting]
    mis_cord_measure = [td['mis_cord_measure'].to_numpy() for td in tds]
    trial_classes = [td['trial_class'].to_numpy() for td in tds]
    entropies_per_dyad = get_simple_entropy_ts(filename='glm/unsorted_performance.pkl')
    entropies_per_dyad = [entropies_per_dyad[i] for i in comp_frac_sorting]
    entropies_per_dyad_exc_comp = entropies_per_dyad[:-15]

    tds = load_all_trial_descriptions()
    trial_classes = [td['trial_class'].to_numpy() for td in tds]
    class_scatter = get_class_scatter(trial_classes)
    classes = list(set([simplify_trial_class(tc) for tc in np.concatenate(trial_classes)]))
    mean_trial_amount = np.mean([len(td) for td in tds])
    new_class_scatter = dict()
    for c, relative_freq in class_scatter.items():
        new_class_scatter[c] = list()
        for comp_frac, rel_freq in zip(comp_fractions, relative_freq):
            for _ in range(int(mean_trial_amount * rel_freq)):
                new_class_scatter[c].append(comp_frac)
    new_class_scatter = {key: new_class_scatter[key] for key in ordered_keys_1}

    # sn.kdeplot(new_class_scatter, multiple='fill', ax=axes[0], palette=colors, alpha=1., legend=False, bw_adjust=4,
    #            linewidth=linewidth)

    # sn.kdeplot(new_class_scatter, multiple='fill', ax=axes[0], palette=colors, alpha=1., legend=False, bw_adjust=4,
    #            linewidth=linewidth, )

    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    trial_classes = [td['trial_class'].to_numpy() for td in tds]
    classes = list(set([simplify_trial_class(tc) for tc in np.concatenate(trial_classes)]))
    class_scatter = get_class_scatter(trial_classes)
    class_scatter = {key: class_scatter[key] for key in ordered_keys_1}

    xspace = np.linspace(0, 1)
    class_curves = {c: splev(xspace, splrep(comp_fractions, class_scatter[c], s=1)) for c in class_scatter.keys()}
    summed_class_curves = sum(class_curves.values())
    class_names = ['accepted_invite', 'declined_invite', 'concurrent', 'ahead', 'miscoordination', 'anti_corr']
    normalized_class_curves = {c_name: class_curves[c_name] / summed_class_curves for c_name in class_names}
    # normalized_class_curves = {c: val / summed_class_curves for c, val in class_curves.items()}
    borders = [
        np.zeros_like(xspace),
        *list(np.cumsum(np.stack(list(reversed(normalized_class_curves.values()))), axis=0)),
        # np.ones_like(xspace),
    ]
    colorblind = sn.color_palette('colorblind')
    for prev_b, next_b, col in zip(borders[:-1], borders[1:], reversed(colors)):
        axes[0].plot(xspace, next_b, c='k', linewidth=linewidth)
        axes[0].fill_between(xspace, prev_b, next_b, color=col, linewidth=linewidth)
    axes[0].plot(xspace, borders[-1], c='k', linewidth=linewidth)

    tds = get_cut_tds()
    def get_per_class_entropies(tds, entropy_ts):
        per_class_entropies = {c: list() for c in reduced_classes}
        for i, (entropy, td) in enumerate(zip(entropy_ts, tds)):
            for c in reduced_classes:
                reduced_class_binary = get_reduced_class_binary(td, c)
                per_class_entropies[c].append(entropy[reduced_class_binary])
        return per_class_entropies

    per_class_entropies = get_per_class_entropies([tds[i] for i in comp_frac_sorting][:-15], entropies_per_dyad_exc_comp)
    new_per_class_entropies = dict()

    per_class_entropies = {key: np.concatenate(per_class_entropies[key]) for key in ordered_keys_2}

    sn.kdeplot(per_class_entropies, multiple='fill', ax=axes[2], palette=colors, alpha=1., legend=False,
               linewidth=linewidth)

    invite_effects = get_invite_effects_df()
    invite_effects['inv'][invite_effects['inv'] == 'inv_included'] = 'With invite'
    invite_effects['inv'][invite_effects['inv'] == 'inv_excluded'] = 'Without invite'
    sn.violinplot(invite_effects, x='dummy', y='entropy', hue='inv', split=True, gap=.1, cut=True, inner='quart',
                  ax=axes[1], linewidth=linewidth, linecolor='k', palette=[colors[0], '#555555'])
    plt.setp(axes[1].collections, edgecolor='k')
    plt.setp(axes[1].lines, c='k')

    handles, labels = axes[1].get_legend_handles_labels()
    a = axes[1].legend(handles=handles, labels=labels, loc='upper left', bbox_to_anchor=(-.05, -.05), frameon=False)
    plt.setp(a.legendPatch, color='k')
    axes[1].spines[['right', 'top']].set_visible(False)

    axes[1].set_ylim(0, np.log2(3))
    # sn.move_legend(axes[1], loc='upper left', bbox_to_anchor=(-.05, .2), frameon=False)
    axes[1].set_xlabel('')
    axes[1].set_ylabel('Uncertainty of\ntarget choice (bits)')
    axes[1].set_xticks([])
    # axes[1].set_box_aspect(.5)

    axes[0].set_box_aspect(1)
    axes[0].set_ylabel('Frequency of\ntrajectory class')
    axes[0].set_xlabel('Stable FST')
    axes[0].set_xlim(0, 1)
    axes[0].set_ylim(0, 1)
    axes[0].yaxis.set_major_locator(ticker.FixedLocator([0, 1]))
    comp_frac_x_format(axes[0])
    axes[2].set_box_aspect(1)
    axes[2].set_ylabel('Frequency of\ntrajectory class')
    axes[2].set_xlabel('Uncertainty of\ntarget choice (bits)')
    axes[2].set_xlim(0, np.log2(3))
    axes[2].xaxis.set_major_locator(ticker.FixedLocator([0.000001, 1]))
    axes[2].yaxis.set_major_locator(ticker.FixedLocator([0, 1]))

    for ax_label, ax in zip('bcd', axes):
        ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes, weight='bold')
    axes[0].text(-0.26, 1.04, 'a', transform=axes[0].transAxes, weight='bold')

    save_fig(sub_folder='presentation/', format='svg')

    # we show that the mean entropy of invite trials is significantly reduced
    invite_trials_entropies = invite_effects['entropy'][invite_effects['inv'] == 'With invite'].to_numpy()
    non_invite_trials_entropies = invite_effects['entropy'][invite_effects['inv'] == 'Without invite'].to_numpy()
    # invite_trials_entropies = invite_trials_entropies[:200]
    # non_invite_trials_entropies = non_invite_trials_entropies[:200]
    u1, p = mannwhitneyu(invite_trials_entropies, non_invite_trials_entropies)
    print(u1, p, len(invite_trials_entropies), len(non_invite_trials_entropies))

    # pearson r for entropy vs class
    entropies = np.concatenate(list(per_class_entropies.values()))
    binary_class_memberships = {key: list() for key in per_class_entropies.keys()}
    for first_key in per_class_entropies.keys():
        for second_key, val in per_class_entropies.items():
            binary_class_memberships[first_key].append(np.zeros_like(val)
                                                       if second_key != first_key
                                                       else np.ones_like(val))
    binary_class_memberships = {key: np.concatenate(val) for key, val in binary_class_memberships.items()}
    for class_name, binary_membership in binary_class_memberships.items():
        r, p = pearsonr(entropies, binary_membership)
        print(f'Pearson r for traj class \'{class_name}\':')
        print(f'r={r}')
        print(f'p={p}')
        print()

    print('max speed', 120 * .007 * game_width_in_cm)

