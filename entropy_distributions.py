import numpy as np
import seaborn as sn
from matplotlib import pyplot as plt
from scipy.stats import pearsonr

from glm.plotting.entropy_corr import get_cut_tds
from glm.plotting.entropy_ts import get_best_entropy_ts
from reward_decomposition import plot_corr_coef, scatter
from trial_descriptor import unsorted_load_all_trial_descriptions, get_comp_frac, load_all_trial_descriptions


def get_correlations(xs, ys):
    out = list()
    for x, y in zip(xs, ys):
        r, p = pearsonr(x, y)
        out.append(r if p < .05 else None)
    return out


if __name__ == '__main__':
    tds = unsorted_load_all_trial_descriptions()
    comp_fractions = [get_comp_frac(td) for td in load_all_trial_descriptions()]
    unsorted_comp_fractions = [get_comp_frac(td) for td in tds]
    comp_frac_sorting = np.argsort(unsorted_comp_fractions)
    tds = get_cut_tds()
    entropy_ts = get_best_entropy_ts(filename='glm/unsorted_performance.pkl')
    tds = [tds[i] for i in comp_frac_sorting]
    entropy_ts = [entropy_ts[i] for i in comp_frac_sorting]
    # plt.scatter(entropy_ts[40], tds[40]['speed'])
    # plot_corr_coef(entropy_ts[40], tds[40]['speed'], ax=plt.gca())
    dyad_speeds = [td['speed'].to_numpy() for td in tds]
    from_prev_to_next_t_per_sec = [td['chosen_effort'].to_numpy() / td['duration'].to_numpy() for td in tds]
    dyad_msm = [td['distance_ol'].to_numpy() / (1e-10 + td['distance'].to_numpy()) for td in tds]
    dyad_distances = [td['distance'].to_numpy() for td in tds]

    speed_correlations = get_correlations(entropy_ts, dyad_speeds)
    from_prev_to_next_t_per_sec_correlations = get_correlations(entropy_ts, from_prev_to_next_t_per_sec)
    distance_correlations = get_correlations(entropy_ts, dyad_distances)
    mcm_correlations = get_correlations(entropy_ts, dyad_msm)
    distance_vs_msm = get_correlations(dyad_distances, dyad_msm)

    fig, axes = plt.subplots(2, 2, figsize=(4, 4))
    axes = axes.flatten()
    # scatter(comp_fractions, from_prev_to_next_t_per_sec_correlations, ax=axes[0])
    scatter(comp_fractions, speed_correlations, ax=axes[0])
    scatter(comp_fractions, distance_correlations, ax=axes[1])
    scatter(comp_fractions, mcm_correlations, ax=axes[2])
    scatter(comp_fractions, distance_vs_msm, ax=axes[3])

    # sn.histplot(x=entropy_ts[40], kde=True)
