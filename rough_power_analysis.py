import numpy as np
import scipy
from matplotlib import pyplot as plt

from trial_descriptor import get_comp_frac, load_all_trial_descriptions

bw_method = .2


if __name__ == '__main__':
    comp_fractions = np.array([get_comp_frac(td) for td in load_all_trial_descriptions()])

    bootstrapped_comp_fractions = list()
    for _ in range(100):
        bootstrapped_comp_fractions.append(np.random.choice(comp_fractions, size=len(comp_fractions)))

    naive_xspace = np.linspace(-.5, 1.5, 200)
    kde = scipy.stats.gaussian_kde(comp_fractions, bw_method=bw_method)(naive_xspace)
    values_below = kde[naive_xspace < 0.]
    values_above = kde[naive_xspace > 1.]
    indices_in_between = 0 == ((naive_xspace < 0.).astype(int) + (naive_xspace > 1.).astype(int))
    to_add = np.zeros(100)
    to_add[:len(values_below)] += np.flip(values_below)
    to_add += np.hstack([np.zeros(len(to_add) - len(values_above)), np.flip(values_above)])

    mirror_kde = kde[indices_in_between] + to_add
    xspace = naive_xspace[indices_in_between]

    fig, axes = plt.subplots(2, 2)
    axes = axes.flatten()

    axes[0].plot(xspace, mirror_kde)
    axes[1].plot(naive_xspace, kde)

    # todo Problem: the left and right picks are way to high with kde mirroring

    # p = (comp_fractions > .9).astype(int).mean()

