import numpy as np

from glm.util import add_properties, to_hot_encoded


@add_properties(param_amount=1, name='bias')
def bias(glm_kwargs, w):
    return np.ones_like(glm_kwargs['prev_1_collections'])[:, None] * np.array([w, -w, -w]).flatten()


@add_properties(param_amount=2)
def effort_linearity(glm_kwargs, w):
    effort = glm_kwargs['efforts']
    return np.array([effort[:, 0] * w[0], effort[:, 1] * w[1], effort[:, 2] * w[1]]).T


@add_properties(param_amount=3)
def prev_t_inv_linearity(glm_kwargs, w):
    prev_collections = to_hot_encoded(glm_kwargs['prev_t_inv'], class_amount=5)
    w_after_comp_and_no_inv = np.array([w[0], 0, 0])
    w_after_coop0 = np.array([0, w[1], 0])
    w_after_coop1 = np.array([0, 0, w[1]])
    w_after_comp_and_coop0_inv = np.array([0, w[2], 0])
    w_after_comp_and_coop1_inv = np.array([0, 0, w[2]])
    # w_after_comp_and_no_inv = np.array([w[0], w[1], w[2]])
    # w_after_coop0 = np.array([w[3], w[4], w[5]])
    # w_after_coop1 = np.array([w[6], w[7], w[8]])
    # w_after_comp_and_coop0_inv = np.array([w[9], w[10], w[11]])
    # w_after_comp_and_coop1_inv = np.array([w[12], w[13], w[14]])
    w = np.array([w_after_comp_and_no_inv, w_after_coop0, w_after_coop1,
                  w_after_comp_and_coop0_inv, w_after_comp_and_coop1_inv])
    return prev_collections @ w


@add_properties(param_amount=1)
def prev_prev_t_linearity(glm_kwargs, w):
    return prev_t_linearity(glm_kwargs, w, depth=2)


@add_properties(param_amount=1)
# @add_properties(param_amount=3)
def prev_t_linearity(glm_kwargs, w, depth=1):
    prev_collections = to_hot_encoded(glm_kwargs[f'prev_{depth}_collections'])
    # w_after_comp = np.array([0, w[0], w[0]])
    # w_after_coop0 = np.array([0, w[1], w[2]])
    # w_after_coop1 = np.array([0, w[2], w[1]])
    w_after_comp = np.array([w[0], 0, 0])
    w_after_coop0 = np.array([0, w[0], 0])
    w_after_coop1 = np.array([0, 0, w[0]])
    w = np.array([w_after_comp, w_after_coop0, w_after_coop1])
    return prev_collections @ w


# @add_properties(param_amount=2)
# def invite_linearity(glm_kwargs, w):
#     invites = to_hot_encoded(glm_kwargs['invites'])
#     no_inv = np.array([0, 0, 0])
#     coop0_inv = np.array([0, w[0], w[1]])
#     coop1_inv = np.array([0, w[1], w[0]])
#     w = np.array([no_inv, coop0_inv, coop1_inv])
#     return invites @ w


# @add_properties(param_amount=3)
@add_properties(param_amount=2)
def invite_linearity(glm_kwargs, w):
    invites = to_hot_encoded(glm_kwargs['invites'])
    # no_inv = np.array([0, w[0], w[0]])
    # coop0_inv = np.array([0, w[1], w[2]])
    # coop1_inv = np.array([0, w[2], w[1]])
    no_inv = np.array([w[1], 0, 0])
    coop0_inv = np.array([0, w[0], 0])
    coop1_inv = np.array([0, 0, w[0]])
    w = np.array([no_inv, coop0_inv, coop1_inv])
    return invites @ w

# @add_properties(param_amount=5)
# def prev_t_linearity(glm_kwargs, w, depth=1):
#     prev_collections = to_hot_encoded(glm_kwargs[f'prev_{depth}_collections'])
#     w_after_comp = np.array([w[0], w[1], w[1]])
#     w_after_coop0 = np.array([w[2], w[3], w[4]])
#     w_after_coop1 = np.array([w[2], w[4], w[3]])
#     w = np.array([w_after_comp, w_after_coop0, w_after_coop1]).T
#     return prev_collections @ w


# @add_properties(param_amount=6)
# def invite_linearity(glm_kwargs, w):
#     no_invite_w, invite_w = w[:3], w[3:]
#     coop0_w = invite_w
#     coop1_w = invite_w[[0, 2, 1]]
#     w = np.vstack([no_invite_w, coop0_w, coop1_w])
#     invites = to_hot_encoded(glm_kwargs['invites'])
#     return invites @ w


# @add_properties(param_amount=5)
# def invite_linearity(glm_kwargs, w):
#     no_invite_w, invite_w = w[:2], w[2:]
#     no_invite_w = np.array([no_invite_w[0], no_invite_w[1], no_invite_w[1]])
#     coop0_w = invite_w
#     coop1_w = invite_w[[0, 2, 1]]
#     w = np.vstack([no_invite_w, coop0_w, coop1_w])
#     invites = to_hot_encoded(glm_kwargs['invites'])
#     return invites @ w
