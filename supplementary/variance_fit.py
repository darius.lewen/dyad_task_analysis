import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy
import seaborn as sn
from matplotlib import ticker

from _util import save_fig
from presentation.fig22_predicting_dyads import get_sorted_effort_glm_stats, get_true_fst_binary, get_pred_fst_binary, \
    timestamp_moving_average
from presentation.plotting import scatterplot, diagonal_r_and_p_text
from trial_descriptor_2 import load_all_trial_descriptions, get_comp_frac

if __name__ == '__main__':
    tds = load_all_trial_descriptions()
    full_tds = load_all_trial_descriptions(cut_initial=False)
    sorted_effort_glm_stats = get_sorted_effort_glm_stats()
    to_plot = dict(
        true_std=list(),
        dist_std=list(),
        full_std=list(),
        comp_fractions=np.array([get_comp_frac(td) for td in tds])
    )
    for td, full_td, effort_glm_stats in zip(tds, full_tds, sorted_effort_glm_stats):
        print(len(td), len(full_td), len(effort_glm_stats['expected_values']))
    for td, full_td, effort_glm_stats in zip(tds, full_tds, sorted_effort_glm_stats):
        print(len(td), len(full_td), len(effort_glm_stats['expected_values']))
        true_fst_binary = get_true_fst_binary(full_td)
        dist_glm_fst_pred_binary = get_pred_fst_binary(expected_values=effort_glm_stats['expected_values'])
        full_glm_fst_pred_binary = get_pred_fst_binary(expected_values=td[['pred_prob_single', 'pred_prob_joint0', 'pred_prob_joint1']].to_numpy().copy())
        ma_true_fst = timestamp_moving_average(true_fst_binary, full_td['start_time'])
        ma_dist_pred_fst = timestamp_moving_average(dist_glm_fst_pred_binary, td['start_time'])
        ma_full_pred_fst = timestamp_moving_average(full_glm_fst_pred_binary, td['start_time'])
        no_nan = ~np.isnan(ma_full_pred_fst)
        to_plot['true_std'].append(ma_true_fst[no_nan].std())
        to_plot['dist_std'].append(ma_dist_pred_fst[no_nan].std())
        to_plot['full_std'].append(ma_full_pred_fst[no_nan].std())
    to_plot = pd.DataFrame(to_plot)
    size_scaler = 3
    fig, axes = plt.subplots(1, 2, figsize=(2 * size_scaler, 1 * size_scaler))
    plt.subplots_adjust(top=.75, bottom=.3, right=.9)
    axes = axes.flatten()
    scatterplot(data=to_plot, x='true_std', y='dist_std', ax=axes[0])
    scatterplot(data=to_plot, x='true_std', y='full_std', ax=axes[1])

    for ax_label, ax in zip('ab', axes):
        ax.plot([-1, 1], [-1, 1], c='k', linewidth=matplotlib.rcParams['axes.linewidth'])
        ax.set_xlim(-.005, .175)
        ax.set_ylim(-.005, .175)
        ax.set_xlabel('Standard deviation of\nactual FST\n30-second moving average')
        ax.set_box_aspect(1)
        ax.xaxis.set_major_locator(ticker.FixedLocator([0., .1]))
        ax.yaxis.set_major_locator(ticker.FixedLocator([0., .1]))
        ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes, weight='bold')
    axes[0].set_ylabel('Standard deviation of\nweighted distance predicted FST\n30-second moving average')
    axes[1].set_ylabel('Standard deviation of\nfull GLM predicted FST\n30-second moving average')
    diagonal_r_and_p_text(x=to_plot['true_std'], y=to_plot['dist_std'], ax=axes[0])
    diagonal_r_and_p_text(x=to_plot['true_std'], y=to_plot['full_std'], ax=axes[1])
    save_fig(sub_folder='supplementary/', format='pdf')
    save_fig(sub_folder='supplementary/', format='svg')

    # wilcoxon signed rank test

    dist_true_diff = to_plot['true_std'].to_numpy() - to_plot['dist_std'].to_numpy()
    full_true_diff = to_plot['true_std'].to_numpy() - to_plot['full_std'].to_numpy()
    fig, axes = plt.subplots(2, 1)
    axes[0].hist(dist_true_diff)
    axes[1].hist(full_true_diff)
    has_variance = to_plot['true_std'] > .01
    res = scipy.stats.wilcoxon(dist_true_diff[has_variance], full_true_diff[has_variance])
    print(f'Wilcoxon signed-rank test: {res.statistic}, p-value: {res.pvalue} for the {np.sum(has_variance)}/{len(has_variance)} dyads that exhibit fluctuations')
