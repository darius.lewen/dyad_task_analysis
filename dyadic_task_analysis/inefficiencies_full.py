import colorcet
import matplotlib
import numpy as np
import pandas as pd
import seaborn as sn
import cmasher
from matplotlib import pyplot as plt, ticker
from numpy import mean

from _util import save_fig, fontdict, game_width_in_cm
from trial_descriptor import load_all_trial_descriptions, get_comp_frac

y_keys = ['ahead', 'concurrent', 'accepted_invite', 'declined_invite', 'miscoordination']
x_key = 'comp_fraction'


def get_poly_fits(measure, deg=2):
    poly_fits = list()
    for y_key in y_keys:
        p, residuals, _, _, _ = np.polyfit(measure[x_key], measure[y_key], deg=deg, full=True)
        poly_fits.append((p, residuals))
    return poly_fits


def plot_poly_fits(poly_fits, axes, scaling=1):
    curve_x = np.linspace(0, 1, 100)
    for (p, residuals), ax in zip(poly_fits, axes):
        curve_y = np.polyval(p, curve_x)
        ax.plot(curve_x, curve_y * scaling, label=f'deg: {len(p) - 1} res: {residuals}')


def poly_fits_for_degrees(measure, axes, scaling=1, degrees=None):
    if degrees is None:
        degrees = [2, 3, 4]
    for deg in degrees:
        plot_poly_fits(get_poly_fits(measure, deg), axes, scaling)


def comp_frac_x_format(ax):
    comp_frac_locator = ticker.FixedLocator([0, 1/3, 2/3, 1])
    comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1'])
    ax.xaxis.set_major_locator(comp_frac_locator)
    ax.xaxis.set_major_formatter(comp_frac_formatter)


def set_tick_spacing(ax, y_max=None):
    ax.set_xlim(-.04, 1.04)
    comp_frac_x_format(ax)
    if y_max is not None:
        ax.yaxis.set_major_locator(ticker.FixedLocator([0, y_max / 2, y_max]))
        ax.set_ylim(-.04, y_max + .04)
    ax.xaxis.set_tick_params(labelsize=14)
    ax.yaxis.set_tick_params(labelsize=14)


def set_tick_spacings(axes, y_max):
    for ax in axes:
        set_tick_spacing(ax, y_max)


def violin_over_len_reasons(over_len_reasons, ax, scaling=1, ylabel=None):
    if ylabel is None:
        ylabel = f'Fraction of overlength (cm)\ndue to motion type'
    sn.violinplot(data=over_len_reasons[y_keys] * scaling, ax=ax, scale='count', inner='quartiles', color='w',
                  cut=0.)

    plt.setp(ax.collections, edgecolor="k")
    ax_children = ax.get_children()
    for i in [1, 2, 3, 5, 6, 7, 9, 10, 11, 13, 14, 15]:
        ax_children[i].set_color('k')
    sn.pointplot(data=over_len_reasons[y_keys] * scaling, ax=ax, estimator=mean, ci=None, join=False, color='k')
    ax.set_xticklabels(y_keys, rotation=-30, horizontalalignment='left', fontdict=fontdict)

    ax.set_ylabel(ylabel, fontdict=fontdict)
    ax.xaxis.set_tick_params(labelsize=fontdict['size'])
    ax.yaxis.set_tick_params(labelsize=fontdict['size'])


def get_colors(comp_fractions):
    colors = list()
    for frac in comp_fractions:
        colors.append(colorcet.bmy[int(frac * 255)])
    return colors


def scatter_over_len_reasons(over_len_reasons, axes, scaling=1, ylabel=None):
    if ylabel is None:
        ylabel = 'Fraction of overlength (cm)'
    ol_matrix = over_len_reasons[y_keys].to_numpy()
    ol_matrix[np.isnan(ol_matrix)] = 0
    max_val = np.max(ol_matrix) * scaling * 1.1
    over_len_reasons['index'] = over_len_reasons.index
    for ax, y_key in zip(axes, y_keys):
        sn.scatterplot(x=over_len_reasons[x_key], y=over_len_reasons[y_key] * scaling,
                       hue=over_len_reasons['index'], ax=ax,
                       palette=cmasher.gem, alpha=.6, legend=False)
        ax.set_xlabel('Fraction of\nsingle targets collected', fontdict=fontdict)
        ax.set_ylabel(f'{ylabel}\ndue to {y_key} motions', fontdict=fontdict)
    set_tick_spacings(axes, y_max=max_val)


def get_class_count(block_amount=2):
    tds = load_all_trial_descriptions()
    class_count = [dict() for _ in tds]
    for count, td in zip(class_count, tds):
        for key, val in td['trial_class'].value_counts().items():
            if 'ahead' in key:
                key = 'ahead'
            elif 'anti_corr' == key:
                key = 'miscoordination'
            elif 'accepted' in key:
                key = 'accepted_invite'
            elif 'declined' in key:
                key = 'declined_invite'
            if key not in count.keys():
                count[key] = 0
            count[key] += val / block_amount
        count['comp_fraction'] = get_comp_frac(td)
        for key, val in count.items():
            if np.isnan(val):
                count[key] = 0.
    class_count = pd.DataFrame(class_count)
    class_count[np.isnan(class_count)] = 0.
    return class_count


def plot_row(measure, axes, scaling=1, ylabel=None):
    axes = axes.flatten()
    violin_over_len_reasons(measure, axes[0], scaling, ylabel)
    scatter_over_len_reasons(measure, axes[1:], scaling, ylabel)
    poly_fits_for_degrees(measure, axes[1:], scaling)


def get_over_len(measure, grouping_keys=None):
    if grouping_keys is None:
        grouping_keys = ['trial_class']
    trial_descriptions = [pd.DataFrame((td.groupby(grouping_keys).sum() / len(td))[measure]).transpose()
                          for td in load_all_trial_descriptions()]
    misc_keys = ['anti_corr', 'accepted_coop0_invite', 'accepted_coop1_invite',
                 'declined_coop0_invite', 'declined_coop1_invite']
    for td in trial_descriptions:
        for y_key in y_keys + misc_keys:
            if y_key not in td.keys():
                td[y_key] = 0
        td['ahead'] = td['p0_ahead'] + td['p1_ahead']
        td['miscoordination'] = td['miscoordination'] + td['anti_corr']
        td['accepted_invite'] = td['accepted_coop0_invite'] + td['accepted_coop1_invite']
        td['declined_invite'] = td['declined_coop0_invite'] + td['declined_coop1_invite']
        del td['anti_corr']
        del td['p0_ahead']
        del td['p1_ahead']
        del td['accepted_coop0_invite']
        del td['accepted_coop1_invite']
        del td['declined_coop0_invite']
        del td['declined_coop1_invite']
    return pd.concat(trial_descriptions)


def get_efficiency_measure(measure=None):
    over_len_reasons = get_over_len(measure=measure)
    tds = load_all_trial_descriptions()
    over_len_reasons = {key: val.to_numpy() for key, val in over_len_reasons.items()}
    over_len_reasons['comp_fraction'] = np.array([get_comp_frac(td) for td in tds])
    return pd.DataFrame(over_len_reasons)


if __name__ == '__main__':
    matplotlib.use('TkAgg')
    ax_width = 3.9
    rows, cols = 5, 6
    fig, axes = plt.subplots(rows, cols, figsize=(cols * ax_width, rows * ax_width))

    measures = [get_class_count(),
                get_efficiency_measure('duration'),
                get_efficiency_measure('speed'),
                get_efficiency_measure('effort_ol'),
                get_efficiency_measure('distance_ol')]

    ylabels = ['Amount of motion type',
               'Fraction of mean trial duration',
               'Fraction of mean speed',
               'Amount of initial effort overlength',
               'Amount of distance overlength']

    scalings = [1, 1, game_width_in_cm, game_width_in_cm, game_width_in_cm]

    for args in zip(measures, axes, scalings,  ylabels):
        plot_row(*args)

    axes = axes.flatten()
    for key, ax in zip(list(range(len(axes))), axes):
        ax.legend(fontsize=6)
        ax.set_box_aspect(1)
        ax.text(-0.1, 1.1, key, transform=ax.transAxes, size=16, weight='bold')
    fig.tight_layout()
    save_fig()
