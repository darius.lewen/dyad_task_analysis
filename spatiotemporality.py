import numpy as np
import pandas as pd
import seaborn as sn
from matplotlib import pyplot as plt

from presentation.plotting import scatterplot
from rew_diff import get_skill_diff
from reward_decomposition import get_agent_rewards, get_mean_in_cm
from trial_classes import plot_aic_poly
from trial_descriptor import load_all_trial_descriptions, get_comp_frac


skill_diff = np.linspace(-.4, .4, 400)


def duration_to_target_amount(dur):
    return 20 * 60 / (dur + 1)
    # return .035 * 20 * 60 / (dur + 1)


def get_optimal_fst_for_skill_diff(target_amount_curve):
    global skill_diff
    target_amount_curve = target_amount_curve[:, None]
    single_target_diff = target_amount_curve @ skill_diff[None, :] * xspace[:, None]  # skill diff is on columns / x-axis
    # each column: skill diff, each row: fst, each value: expected payoff
    expected_payoffs = 0.07 * (single_target_diff + target_amount_curve) / 2
    optimal_fst_s = xspace[np.argmax(expected_payoffs, axis=0)]
    return optimal_fst_s, expected_payoffs


if __name__ == '__main__':
    fig, axes = plt.subplots(1, 3)
    axes = axes.flatten()
    tds = load_all_trial_descriptions()
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)
    y_reward_min, y_reward_max = min(agent_mean_reward), max(agent_mean_reward)
    trial_durations = np.array([td['duration'].to_numpy().mean() for td in tds])

    comp_fractions = [get_comp_frac(td) for td in tds]
    distance = get_mean_in_cm('distance', tds)
    speed = get_mean_in_cm('speed', tds)
    additional_effort = get_mean_in_cm('distance_ol', tds)
    effort_from_past_target = get_mean_in_cm('chosen_effort', tds) + get_mean_in_cm('path_shortening', tds)
    effort_reduction_by_ps = get_mean_in_cm('path_shortening', tds)

    xspace, speed_curve = plot_aic_poly(comp_fractions, speed, num=500)
    _, additional_effort_curve = plot_aic_poly(comp_fractions, additional_effort, num=500)
    _, effort_from_past_target_curve = plot_aic_poly(comp_fractions, effort_from_past_target, num=500)
    _, effort_reduction_by_ps_curve = plot_aic_poly(comp_fractions, effort_reduction_by_ps, num=500)
    distance_curve = effort_from_past_target_curve + additional_effort_curve - effort_reduction_by_ps_curve
    target_amount_curve = duration_to_target_amount(distance_curve / speed_curve)

    optimal_fst_with_st, expected_payoffs_with_st = get_optimal_fst_for_skill_diff(target_amount_curve)
    axes[0].plot(skill_diff, optimal_fst_with_st)

    distance_curve = effort_from_past_target_curve + additional_effort.mean() - effort_reduction_by_ps_curve
    target_amount_curve = duration_to_target_amount(distance_curve / speed.mean())

    optimal_fst_without_st, expected_payoffs_without_st = get_optimal_fst_for_skill_diff(target_amount_curve)
    axes[0].plot(skill_diff, optimal_fst_without_st)

    to_plot = dict(skill_diff=list(), fst=list(), expected_payoff_with_st=list(), expected_payoff_without_st=list())
    for fst_idx in range(len(xspace)):
        for skill_diff_idx in range(len(skill_diff)):
            to_plot['fst'].append(xspace[fst_idx])
            to_plot['skill_diff'].append(skill_diff[skill_diff_idx])
            to_plot['expected_payoff_with_st'].append(expected_payoffs_with_st[fst_idx, skill_diff_idx])
            to_plot['expected_payoff_without_st'].append(expected_payoffs_without_st[fst_idx, skill_diff_idx])
    to_plot = pd.DataFrame(to_plot)
    print(0)
    sn.lineplot(to_plot, x='fst', y='expected_payoff_with_st', hue='skill_diff', palette='vlag', ax=axes[1])
    sn.lineplot(to_plot, x='fst', y='expected_payoff_without_st', hue='skill_diff', palette='vlag', ax=axes[2])

    critical_point_with_st = to_plot[to_plot['skill_diff'] == skill_diff[225]]
    critical_point_without_st = to_plot[to_plot['skill_diff'] == skill_diff[270]]
    sn.lineplot(critical_point_with_st, x='fst', y='expected_payoff_with_st', ax=axes[1])
    sn.lineplot(critical_point_without_st, x='fst', y='expected_payoff_without_st', ax=axes[2])
    axes[1].get_legend().remove()
    axes[2].get_legend().remove()

    dyads_to_plot = pd.DataFrame(dict(
        skill_differences=np.array([get_skill_diff(td) for td in tds]),
        comp_fractions=comp_fractions,
    ))
    count_axes = axes[0].twinx()
    sn.histplot(dyads_to_plot, x='skill_differences', ax=count_axes)
    scatterplot(dyads_to_plot, x='skill_differences', y='comp_fractions', ax=axes[0])



    # for
    # to_plot = dict(skill_diff=skill_diff,
    #                optimal_fst_with_st=optimal_fst_with_st, optimal_fst_without_st=optimal_fst_without_st)


