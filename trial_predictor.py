import numpy as np

from simulations import get_weighted_efforts, get_sim_result_for_name


def get_dyad_weighting(comp_frac, sim_name='naive_0'):
    comp_fractions, _weightings = get_sim_result_for_name(sim_name)
    for w, prev_cf, next_cf in zip(_weightings, comp_fractions[:-1], comp_fractions[1:]):
        if prev_cf <= comp_frac <= next_cf:
            return w


def effort_based_prediction(*args, **kwargs):
    weighted_dists = get_weighted_dists(*args, **kwargs)
    return predict(weighted_dists)


def get_weighted_dists(efforts, weighting):
    weighted_efforts = get_weighted_efforts(np.array(list(efforts.values())), weighting)
    return dict(zip(efforts.keys(), weighted_efforts))


def neg_logit(w_distances):
    return 1 / (1 + np.exp(w_distances))


def softmin(w_distances):
    return np.exp(-1 * w_distances) / np.sum(np.exp(-1 * w_distances))


def predict(dists):
    probabilities = softmin(np.array(list(dists.values())))
    # probabilities = neg_logit(np.array(list(dists.values())))
    target_names = list(dists.keys())
    predicted_t_name = target_names[int(np.argmin(list(dists.values())))]
    return predicted_t_name, probabilities

