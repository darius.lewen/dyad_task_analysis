import random

import numpy as np
import scipy
from matplotlib import pyplot as plt

from trial_descriptor import load_all_trial_descriptions, get_comp_frac

if __name__ == '__main__':
    comp_fractions = [get_comp_frac(td) for td in load_all_trial_descriptions()]
    fig, axes = plt.subplots(2, 2)
    axes = axes.flatten()
    n_values = [30, 40, 50]
    kde_kws = dict(bw_adjust=.5)
    # sn.histplot(x=comp_fractions, bins=10, ax=axes[0], legend=False, kde=True, kde_kws=kde_kws)
    x = np.linspace(0, 1, 100)
    bw_method = .2
    density = scipy.stats.gaussian_kde(comp_fractions, bw_method=bw_method)
    axes[0].plot(x, density(x))
    # sn.kdeplot(x=comp_fractions, ax=axes[0], legend=False, bw_adjust=.5)
    for ax, n in zip(axes[1:], n_values):
        densities = list()
        for _ in range(100):
            # sn.kdeplot(x=random.sample(comp_fractions, n), ax=ax, legend=False, bw_adjust=.5)
            density = scipy.stats.gaussian_kde(random.sample(comp_fractions, n), bw_method=bw_method)
            densities.append(density(x))
            # res = scipy.stats.bootstrap(comp_fractions, density)
        densities = np.array(densities)
        mean, std = np.mean(densities, axis=0), np.std(densities, axis=0)
        for den in densities:
            ax.plot(x, den, color='gray', alpha=.2)
        ax.fill_between(x, mean - std, mean + std, color='red')
        ax.plot(x, mean, color='k')
    for ax in axes:
        ax.set_box_aspect(1)
