import io
import multiprocessing
import sys
import time
from multiprocessing import Process
from multiprocessing.shared_memory import SharedMemory

import numpy as np
import pyglet
import pyqtgraph as pg
import pyqtgraph.exporters
from PIL import Image
from PyQt5.QtCore import QBuffer
import plotly.express as px

# pyqtgraph.examples.run()
a = 0
mem_name = 'a'
a = SharedMemory(name=mem_name, create=True, size=10)


def _dummy():
    # global a
    b = SharedMemory(name=mem_name)
    while True:
        time.sleep(.1)
        b.buf[:] = b'hiiihelloo'
        print(bytes(b.buf))


if __name__ == '__main__':
    # multiprocessing.set_start_method('forkserver')
    p = Process(target=_dummy)
    p.start()
    print('hi')
    while True:
        time.sleep(.1)
        print(f'from main process: {bytes(a.buf)}')


    # df = px.data.gapminder().query("country=='Canada'")
    # t0 = time.time()
    # fig = px.line(df, x="year", y="lifeExp", title='Life expectancy in Canada')
    # # fig.show()
    # img_bytes = fig.to_image(format='png')
    # print(time.time() - t0)


#     app = pg.mkQApp()
#     win = pg.GraphicsLayoutWidget(show=False)
#     p2 = win.addPlot(title="Multiple curves")
#     img_exporter = pyqtgraph.exporters.ImageExporter(p2)
#     q_buffer = QBuffer()
#     q_buffer.open(q_buffer.ReadWrite)
#     t0 = time.time()
#     p2.plot(np.random.normal(size=110) + 5, pen=(0, 255, 0), name="Green curve")
#     print(time.time() - t0)
#     q_img = img_exporter.export(toBytes=True)
#     q_img.save(q_buffer, 'PNG')
#     # pil_img = Image.open(io.BytesIO(q_buffer.data()))
#     # print(pil_img.width, pil_img.height)
#     width, height = 622, 462
#     # pyglet_img = pyglet.image.ImageData(pil_img.width, pil_img.height, 'RGBA', pil_img.tobytes(), pitch=pil_img.width * -4)
#     pyglet_img = pyglet.image.ImageData(width, height, 'RGBA', io.BytesIO(q_buffer.data()).read(), pitch=width * -4)
#     print(time.time() - t0)
#
#     # x = np.arange(0, 10)
#     # y = np.arange(0, 10)
#     # pg.plot(x, y)
#     # graphics_layout = pg.GraphicsLayoutWidget()
#     # plot_item = graphics_layout.addPlot()
#     # plot_item.plot(x, y)
#     # app = QtWidgets.QApplication([])
#     # plot_widget = pg.PlotWidget()
#     # plot_widget.plot(x, y)
#     # # img_exporter = pyqtgraph.exporters.ImageExporter(plot_widget.plotItem)
#     # pg.exec()
#
# #     fig, axes = plt.subplots(2, 2)
# #     axes = axes.flatten()
# #
# #
# #     def x_change(x, y, r, w, dt):
# #         return (r * x - w * y - x * (x ** 2 + y ** 2)) * dt
# #
# #     def y_change(x, y, r, w, dt):
# #         return (r * y + w * x - y * (x ** 2 + y ** 2)) * dt
# #
# #
# #     # start = np.array([0.001, 0.001])
# #
# #     def plot_dynamics(ax, r, w, dt=.1, n=1000):
# #         for _ in range(100):
# #             start = np.random.rand(2) * .01
# #             start -= np.ones_like(start) * .01 / 2
# #             traj = np.zeros((n, 2))
# #             traj[0] = start
# #             for i in range(1, n):
# #                 x, y = traj[i - 1]
# #                 traj[i] = np.array([x + x_change(x, y, r, w, dt), y + y_change(x, y, r, w, dt)])
# #             ax.plot(traj[:, 0], traj[:, 1])
# #         res = 10
# #         x_coord = np.linspace(-1, 1, res)
# #         y_coord = np.linspace(-1, 1, res)
# #         xv, yv = np.meshgrid(x_coord, y_coord)
# #         ax.quiver(xv, yv, x_change(xv, yv, r, w, dt), y_change(xv, yv, r, w, dt))
# #
# #     plot_dynamics(axes[0], r=1, w=1)
# #     plot_dynamics(axes[1], r=2, w=1)
# #     plot_dynamics(axes[2], r=2, w=2)
# #     plot_dynamics(axes[3], r=-3, w=2)
# #     plt.show()
# #
# #     # fig, axes = plt.subplots(2, 2)
# #     # axes = axes.flatten()
# #     # xspace = np.linspace(-.07, .03)
# #     # # boltzmann = .0001
# #     # # boltzmann = 1.380649e-23
# #     # # body_temp_kelvin = 310.15
# #     # opening_rate = np.exp((xspace) / .1)
# #     # # opening_rate = np.exp(xspace / (boltzmann * body_temp_kelvin))
# #     # plt.plot(xspace, opening_rate)
# #     # plt.show()
# #     # xspace = np.linspace(0, 10)
# #     # y_lin = 2 * xspace + 1
# #     # y_exp = np.exp(xspace)
# #     # for ax in axes:
# #     #     ax.plot(xspace, y_lin)
# #     #     ax.plot(xspace, y_exp)
# #     # axes[1].set_yscale('log')
# #     # axes[2].set_xscale('log')
# #     # axes[3].set_yscale('log')
# #     # axes[3].set_xscale('log')
# #     # plt.show()
# #
# #
# #
# #
# #
# #
# #
# #
# #
# #
# #
# #
# #
# # # import numpy as np
# # # import pandas as pd
# # # import seaborn as sns
# # # import matplotlib.pyplot as plt
# # #
# # #
# # # def get_marker_path_objects(markers):
# # #     ax = plt.gca()
# # #     path_objects = list()
# # #     for m in markers:
# # #         collection = ax.scatter([0, 0], [1, 1], marker=m)
# # #         p_obj, = collection.get_paths()
# # #         path_objects.append(p_obj)
# # #     plt.close()
# # #     return path_objects
# # #
# # # path_objects = get_marker_path_objects(['s', '^'])
# # # square_mk = path_objects[0]
# # # triangle_up_mk = path_objects[1]
# # #
# # # tips = sns.load_dataset("tips")
# # #
# # # fig, ax = plt.subplots(1,1)
# # # ax = sns.swarmplot(x="day", y="total_bill", hue="sex",data=tips,size=8,ax=ax, dodge=False)
# # #
# # # collections = ax.collections
# # # unique_colors = np.unique(collections[0].get_facecolors(), axis=0)
# # # markers = [triangle_up_mk, square_mk]  # this array must be at least as large as the number of unique colors
# # # for collection in collections:
# # #     paths = []
# # #     for current_color in collection.get_facecolors():
# # #         for possible_marker,possible_color in zip(markers, unique_colors):
# # #             if np.array_equal(current_color, possible_color):
# # #                 # if possible_marker == markers[0]:
# # #                 #     facecolors = collection.get_facecolors()
# # #                 #     facecolors[1] = np.ones_like(facecolors[1]) * .5
# # #                 #     collection.set_facecolor(facecolors)
# # #                 paths.append(possible_marker)
# # #                 break
# # #     collection.set_paths(paths)
# # #
# # # ax.legend(collections[-2:], pd.unique(tips.sex))
# # # plt.show()
# # #
# # #
# # #
# # #
# # #
# # #
# # #
# # #
# # #
# # #
# # #
# # #
# # #
# # #
# # #
# # #
# # #
# # #
# # #
# # # # import seaborn as sns
# # # # import matplotlib.pyplot as plt
# # # #
# # # # ############## Begin hack ##############
# # # # from matplotlib.axes._axes import Axes
# # # # from matplotlib.markers import MarkerStyle
# # # # from seaborn import color_palette
# # # # from numpy import ndarray
# # # #
# # # # def GetColor2Marker(markers):
# # # #     palette = color_palette()
# # # #     mkcolors = [(palette[i]) for i in range(len(markers))]
# # # #     return dict(zip(mkcolors,markers))
# # # #
# # # # def fixlegend(ax,markers,markersize=8,**kwargs):
# # # #     # Fix Legend
# # # #     legtitle =  ax.get_legend().get_title().get_text()
# # # #     _,l = ax.get_legend_handles_labels()
# # # #     palette = color_palette()
# # # #     mkcolors = [(palette[i]) for i in range(len(markers))]
# # # #     newHandles = [plt.Line2D([0],[0], ls="none", marker=m, color=c, mec="none", markersize=markersize,**kwargs) \
# # # #                 for m,c in zip(markers, mkcolors)]
# # # #     ax.legend(newHandles,l)
# # # #     leg = ax.get_legend()
# # # #     leg.set_title(legtitle)
# # # #
# # # # old_scatter = Axes.scatter
# # # # def new_scatter(self, *args, **kwargs):
# # # #     colors = kwargs.get("c", None)
# # # #     co2mk = kwargs.pop("co2mk",None)
# # # #     print('colors', colors)
# # # #     print('co2mk', co2mk)
# # # #     FinalCollection = old_scatter(self, *args, **kwargs)
# # # #     old_set_facecolor = FinalCollection.set_facecolor
# # # #     def new_set_facecolor(c):
# # # #         old_set_facecolor(c)
# # # #         print('hi', c)
# # # #
# # # #     FinalCollection.set_facecolor = new_set_facecolor
# # # #
# # # #     if co2mk is not None and isinstance(colors, ndarray):
# # # #         Color2Marker = GetColor2Marker(co2mk)
# # # #         paths=[]
# # # #         for col in colors:
# # # #             mk=Color2Marker[tuple(col)]
# # # #             marker_obj = MarkerStyle(mk)
# # # #             paths.append(marker_obj.get_path().transformed(marker_obj.get_transform()))
# # # #         FinalCollection.set_paths(paths)
# # # #     return FinalCollection
# # # # Axes.scatter = new_scatter
# # # # ############## End hack. ##############
# # # #
# # # #
# # # # # Example Test
# # # # sns.set(style="whitegrid")
# # # # tips = sns.load_dataset("tips")
# # # #
# # # # # To test robustness
# # # # tips.loc[(tips['sex']=="Male") & (tips['day']=="Fri"),'sex']='Female'
# # # # tips.loc[(tips['sex']=="Female") & (tips['day']=="Sat"),'sex']='Male'
# # # #
# # # # Markers = ["o","P"]
# # # #
# # # # fig, axs = plt.subplots(1,2,figsize=(14,5))
# # # # axs[0] = sns.swarmplot(x="day", y="total_bill", hue="sex",data=tips,size=8,ax=axs[0])
# # # # axs[0].set_title("Original")
# # # # axs[1] = sns.swarmplot(x="day", y="total_bill", hue="sex",data=tips,size=8,ax=axs[1],co2mk=Markers,
# # # #                        palette=dict(Female='b', Male='k'))
# # # # axs[1].set_title("Hacked")
# # # # fixlegend(axs[1],Markers)
# # # #
# # # # plt.show()