import os.path

import numpy as np
import pandas as pd
import scipy.optimize

from glm.util import add_properties
from load.data_loader import pair_idx_to_ids, get_rec_num_path, load, recordings_folder


kernel_size = 10
speed_limit_per_frame = 0.007
# pred_target_frame = 12
pred_target_frame = 20
speed_limit = speed_limit_per_frame * pred_target_frame
# pair_idx = 38
pair_idx = 0
p_ids, rec_num = pair_idx_to_ids(pair_idx)
rec_path = get_rec_num_path(p_ids[1], rec_num[1], recordings_folder)


def get_velocity_mse(vel_prediction, velocity):
    return np.mean((velocity - vel_prediction) ** 2)


def get_kernel_depth(covariate, kernel_depth):
    if kernel_depth == 0:
        return covariate[kernel_size:]
    return covariate[kernel_size - kernel_depth: -kernel_depth]


def get_von_mises_pdf(x, mu, kappa):  # scipy.stats.vonmises.pdf throws inf values...
    return np.exp(kappa * np.cos(x - mu)) / (2 * np.pi * scipy.special.i0(kappa))


def sigmoid(x):
    return np.piecewise(  #https://stackoverflow.com/questions/51976461/optimal-way-of-defining-a-numerically-stable-sigmoid-function-for-a-list-in-pyth
        x,
        [x > 0],
        [lambda i: 1 / (1 + np.exp(-i)), lambda i: np.exp(i) / (1 + np.exp(i))],
    )


def get_covariate_matrix(covariates, covariate_keys, kernel_depth, kernel_size):
    selection = list()
    covariate_names = list()
    for key in covariate_keys:
        for d in range(kernel_depth):
            selection.append(get_kernel_depth(covariates[key], d))
            covariate_names.append(f'{key}_kernel_depth{d}')
    selection.append(np.ones_like(selection[-1]))
    covariate_names.append(f'intercept')
    return np.stack(selection), covariate_names


def von_mises_nll(w, model, pred_target, penalty=0):
    pred = model(w)
    kappa = np.min([80 * np.ones_like(pred[:, 1]), pred[:, 1]], axis=0)
    vonmises_pdf = get_von_mises_pdf(x=pred_target, mu=pred[:, 0], kappa=kappa)
    return -1 * np.sum(np.log(vonmises_pdf))


def beta_nll(w, model, pred_target, penalty=0):
    pred = model(w)
    # to_include = pred_target < 1  # excluding the jumps in the data, they cause infinite nll
    # pred_target = pred_target[to_include]
    # pred = pred[to_include]
    mu, phi_estimate = pred[:, 0], pred[:, 1]
    return -1 * np.sum(scipy.stats.beta.logpdf(x=pred_target, a=mu * phi_estimate, b=1 - mu))


def get_speed_glm_for_keys(kernel_depth, keys, rec, speed_limit, penalty=0):
    nll = lambda *args, **kwargs: beta_nll(penalty=penalty, *args, **kwargs)
    covariates = get_covariates(rec, speed_limit)
    train_covariate_matrix = get_covariate_matrix(covariates, keys, kernel_depth, kernel_size)[0]
    param_amount = len(keys) * kernel_depth * 2 + 2

    @add_properties(param_amount=param_amount, name=f'speed_glm_{"_".join(keys)}_kernel_depth={kernel_depth}', nll=nll)
    def glm(weights, covariates=None):
        if covariates is not None:
            covariate_matrix = get_covariate_matrix(covariates, keys, kernel_depth, kernel_size)[0]
        else:
            covariate_matrix = train_covariate_matrix
        speed_weights = weights[:param_amount // 2]
        speed_precision_weights = weights[param_amount // 2:]
        return np.stack([
            sigmoid(speed_weights @ covariate_matrix),
            np.exp(speed_precision_weights @ covariate_matrix),
        ]).T
    return glm


def get_angle_glm_for_keys(kernel_depth, keys, rec, speed_limit, speed_pred=None, penalty=0):  # todo remove penatly?
    nll = lambda *args, **kwargs: von_mises_nll(penalty=penalty, *args, **kwargs)
    covariates = get_covariates(rec, speed_limit, speed_pred=speed_pred)
    train_covariate_matrix = get_covariate_matrix(covariates, keys, kernel_depth, kernel_size)[0]
    param_amount = len(keys) * kernel_depth * 2 + 2

    @add_properties(param_amount=param_amount, name=f'angle_glm_{"_".join(keys)}_kernel_depth={kernel_depth}', nll=nll)
    def glm(w, covariates=None):
        if covariates is not None:
            covariate_matrix = get_covariate_matrix(covariates, keys, kernel_depth, kernel_size)[0]
        else:
            covariate_matrix = train_covariate_matrix
        weights = w[:param_amount // 2]
        kappa_weights = w[param_amount // 2:]
        return np.stack([
            weights @ covariate_matrix,
            # 80 / (1 + np.exp(kappa_weights @ covariate_matrix)),
            np.exp(kappa_weights @ covariate_matrix),
        ]).T
    return glm


def aic(w, args):
    model = args[0]
    return 2 * model.nll(w, *args) + 2 * len(w)


def zero_padding(covariate, kernel_size):
    return np.concatenate([np.zeros(kernel_size), covariate])


def get_velocity(rec):
    positions = rec[['p0_agent_x', 'p0_agent_y']].to_numpy()
    # sigma = 2
    # positions[:, 0] = scipy.ndimage.gaussian_filter1d(positions[:, 0], sigma=sigma)
    # positions[:, 1] = scipy.ndimage.gaussian_filter1d(positions[:, 1], sigma=sigma)
    filter_len = 2
    positions[:, 0] = np.convolve(np.concatenate([[0], positions[:, 0]]), np.ones(filter_len), 'valid') / filter_len
    positions[:, 1] = np.convolve(np.concatenate([[0], positions[:, 1]]), np.ones(filter_len), 'valid') / filter_len
    return np.diff(positions, axis=0)


def cartesian_to_polar(x, y):
    rho = np.sqrt(x ** 2 + y ** 2)
    phi = np.arctan2(y, x)
    return np.stack([rho, phi]).T


def polar_to_cartesian(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return np.stack([x, y]).T


def get_polar_velocity(rec, speed_limit, exclude_jumps=True):
    velocity = get_velocity(rec)
    return velocity_to_polar(velocity, speed_limit, exclude_jumps)


def velocity_to_polar(velocity, speed_limit, exclude_jumps=True, eps=1e-6):
    polar_velocity = cartesian_to_polar(velocity[:, 0], velocity[:, 1])
    polar_velocity[:, 0] /= speed_limit
    if exclude_jumps:
        jump_in_data = polar_velocity[:, 0] > 1
        polar_velocity = polar_velocity[~jump_in_data]
    speed_is_zero = polar_velocity[:, 0] == 0
    speed_is_one = polar_velocity[:, 0] == 1
    polar_velocity[:, 0][speed_is_zero] = eps  # for beta distribution, it's defined only for x>0
    polar_velocity[:, 0][speed_is_one] = 1 - eps  # for beta distribution, it's defined only for x<1
    # polar_velocity[:, 1] = get_backwards_filled_angles(angles=polar_velocity[:, 1], speed_is_zero=speed_is_zero)

    # polar_velocity[:, 1][speed_is_zero] = np.pi * np.random.random(size=speed_is_zero.astype(int).sum())
    # polar_velocity[:, 1][speed_is_zero] *= np.random.choice([-1, 1], size=speed_is_zero.astype(int).sum())

    speed_too_low = polar_velocity[:, 0] < 0.02
    # speed_too_low = polar_velocity[:, 0] < 0.005
    polar_velocity[:, 1][speed_too_low] = np.pi * np.random.random(size=speed_too_low.astype(int).sum())
    polar_velocity[:, 1][speed_too_low] *= np.random.choice([-1, 1], size=speed_too_low.astype(int).sum())
    angle_is_pi = polar_velocity[:, 1] == np.pi
    polar_velocity[:, 1][angle_is_pi] = np.pi - eps  # for vonmises distribution, it's defined only for x<pi
    return polar_velocity


def get_polar_rel_velocities(rec, speed_limit, exclude_jumps=True):
    rel_velocities = get_rel_velocities(rec)
    polar_rel_vel_single = velocity_to_polar(rel_velocities[0], speed_limit, exclude_jumps)
    polar_rel_vel_joint0 = velocity_to_polar(rel_velocities[1], speed_limit, exclude_jumps)
    polar_rel_vel_joint1 = velocity_to_polar(rel_velocities[2], speed_limit, exclude_jumps)
    return polar_rel_vel_single, polar_rel_vel_joint0, polar_rel_vel_joint1


def get_rel_velocities(rec):
    agent_positions = rec[['p0_agent_x', 'p0_agent_y']].to_numpy()
    single_positions = rec[['comp_x', 'comp_y']].to_numpy()
    joint0_positions = rec[['coop0_x', 'coop0_y']].to_numpy()
    joint1_positions = rec[['coop1_x', 'coop1_y']].to_numpy()
    rel_vel_single = np.diff(single_positions - agent_positions, axis=0)
    rel_vel_joint0 = np.diff(joint0_positions - agent_positions, axis=0)
    rel_vel_joint1 = np.diff(joint1_positions - agent_positions, axis=0)
    return rel_vel_single, rel_vel_joint0, rel_vel_joint1


def get_rel_accelerations(rel_velocities):
    return [np.diff(np.vstack([np.zeros((1, 2)), rel_vel]), axis=0) for rel_vel in rel_velocities]


def get_polar_acceleration(polar_velocity):
    # polar_velocity = get_polar_velocity(rec, speed_limit, exclude_jumps)
    polar_acceleration = np.diff(np.vstack([np.zeros((1, 2)), polar_velocity]), axis=0)
    # polar_acceleration[:, 1] %= 2 * np.pi  # such that all values are positive [0, 2pi)
    # polar_acceleration[polar_acceleration[:, 1] > np.pi][:, 1] -= 2 * np.pi
    # polar_acceleration[polar_acceleration[:, 1] < -np.pi][:, 1] += 2 * np.pi
    return polar_acceleration


def get_covariates(rec, speed_limit, speed_pred=None, exclude_jumps=True):
    polar_velocity = get_polar_velocity(rec, speed_limit, exclude_jumps)
    polar_acceleration = np.diff(np.vstack([np.zeros((1, 2)), polar_velocity]), axis=0)
    polar_jerk = np.diff(np.vstack([np.zeros((1, 2)), polar_acceleration]), axis=0)
    # polar_acceleration = get_polar_acceleration(rec, speed_limit, exclude_jumps)
    speed = polar_velocity[:, 0]
    angle = polar_velocity[:, 1]
    acceleration = polar_acceleration[:, 0]
    angle_change = polar_acceleration[:, 1]
    jerk = polar_jerk[:, 0]
    angle_change_change = polar_jerk[:, 1]
    # polar_rel_velocities = get_polar_rel_velocities(rec, speed_limit, exclude_jumps)  # todo reimplement with exclude jumps...
    if speed_pred is None:
        speed_mu = np.zeros_like(zero_padding(speed, kernel_size))
        speed_phi = np.zeros_like(zero_padding(speed, kernel_size))
    else:
        speed_mu = speed_pred[:, 0]
        speed_phi = speed_pred[:, 1]
    return dict(
        speed=zero_padding(speed, kernel_size),
        angle=zero_padding(angle, kernel_size),
        acceleration=zero_padding(acceleration, kernel_size),
        angle_change=zero_padding(angle_change, kernel_size),
        jerk=zero_padding(jerk, kernel_size),
        angle_change_change=zero_padding(angle_change_change, kernel_size),
        speed_mu=zero_padding(speed_mu, kernel_size),
        speed_phi=zero_padding(speed_phi, kernel_size),
        # rel_speed_single=zero_padding(polar_rel_velocities[0][:, 0], kernel_size),
        # rel_speed_joint0=zero_padding(polar_rel_velocities[1][:, 0], kernel_size),
        # rel_speed_joint1=zero_padding(polar_rel_velocities[2][:, 0], kernel_size),
        # rel_angle_single=zero_padding(polar_rel_velocities[0][:, 1], kernel_size),
        # rel_angle_joint0=zero_padding(polar_rel_velocities[1][:, 1], kernel_size),
        # rel_angle_joint1=zero_padding(polar_rel_velocities[2][:, 1], kernel_size),
    )


def get_model_results(models, pred_targets_s, rec, speed_limit):
    model_results = list()
    for model, pred_targets in zip(models, pred_targets_s):
        pred_targets = np.concatenate([1e-6 * np.ones(1), pred_targets[:-1]])
        print(model.name)
        args = (model, pred_targets)
        x0 = 1e-6 * np.ones(model.param_amount)
        niter = 2
        # niter = 1
        res = scipy.optimize.basinhopping(func=model.nll, x0=x0, niter=niter,
                                          minimizer_kwargs=dict(args=args, options=dict(disp=True, maxiter=1e6),
                                          method='Nelder-Mead'))
        predictions = model(res['x'])
        aic_val = aic(res['x'], args)
        print(np.min(predictions, axis=0), np.max(predictions, axis=0))
        model_results.append(dict(
            name=model.name,
            aic_val=aic_val,
            res=res,
            w=res['x'],
            predictions=predictions,
            pred_targets=pred_targets,
            residuals=pred_targets - predictions[:, 0],
        ))
    return model_results


def get_mov_pred_dir(rec_path):
    mov_pred_dir = f'{rec_path}/movement_prediction'
    if not os.path.exists(mov_pred_dir):
        os.makedirs(mov_pred_dir)
    return mov_pred_dir


def save_predictions(predictions, p_idx, rec):
    padded_predictions = np.vstack([1e-6 * np.ones_like(predictions[0]), predictions])
    org_sorted_predictions = np.zeros_like(padded_predictions)
    for idx, pred in zip(rec.index.to_numpy(), padded_predictions):
        org_sorted_predictions[idx] = pred
    mov_pred_dir = get_mov_pred_dir(rec_path)
    np.save(f'{mov_pred_dir}/p{p_idx}_pred.npy', org_sorted_predictions)


if __name__ == '__main__':
    org_rec = load(pair_idx, frames_per_sec=120)[1]
    # positions = org_rec[['p0_agent_x', 'p0_agent_y']]
    # velocity = np.diff(positions, axis=0)  # todo padding?
    # polar_velocity = velocity_to_polar(velocity, speed_limit)
    # polar_acceleration = np.diff(np.vstack([np.zeros((1, 2)), polar_velocity]), axis=0)
    # polar_jerk = np.diff(np.vstack([np.zeros((1, 2)), polar_acceleration]), axis=0)
    # org_data = pd.DataFrame(dict(
    #     speed=polar_velocity[:, 0],
    #     acceleration=polar_acceleration[:, 0],
    #     jerk=polar_jerk[:, 0],
    #     angle=polar_velocity[:, 1],
    #     angle_speed=polar_acceleration[:, 1],
    #     angle_acceleration=polar_jerk[:, 1],
    # ))
    # data = org_data.iloc[::pred_target_frame]
    # data = data[:5000]
    # full_data = pd.concat([org_data.iloc[i::pred_target_frame] for i in range(pred_target_frame)])

    # train_data = {key: d[::pred_target_frame] for key, d in data.items()}
    # train_data = {key: d[:5000] for key, d in train_data.items()}

    rec = org_rec.iloc[::pred_target_frame]
    # rec = rec[:1000]
    rec = rec[:5000]
    full_rec = pd.concat([org_rec.iloc[i::pred_target_frame] for i in range(pred_target_frame)])

    polar_velocity = get_polar_velocity(rec, speed_limit)
    speed = polar_velocity[:, 0]
    # speed = data['speed']
    speed_glms = [
        get_speed_glm_for_keys(kernel_depth=1, keys=[
            'speed',
            'acceleration',
            # 'jerk',
            'angle',
            'angle_change',
            # 'angle_change_change',
            # 'rel_speed_single', 'rel_speed_joint0', 'rel_speed_joint1',
            # 'rel_angle_single', 'rel_angle_joint0', 'rel_angle_joint1',
            ],
                               rec=rec, speed_limit=speed_limit, penalty=0),
    ]
    speed_glm_res = get_model_results(models=speed_glms, pred_targets_s=[speed for _ in speed_glms], rec=rec,
                                      speed_limit=speed_limit)
    angle = polar_velocity[:, 1]
    angle_glms = [
        get_angle_glm_for_keys(kernel_depth=1, keys=[
            'speed',
            # 'acceleration',
            # 'jerk',
            'angle',
            # 'angle_change',
            # 'angle_change_change',
            # 'rel_speed_single', 'rel_speed_joint0', 'rel_speed_joint1',
            # 'rel_angle_single', 'rel_angle_joint0', 'rel_angle_joint1',
            # 'speed_mu',
            # 'speed_phi',
        ],
                               rec=rec, speed_limit=speed_limit,
                               speed_pred=speed_glm_res[-1]['predictions'], penalty=10),
    ]
    angle_glm_res = get_model_results(models=angle_glms, pred_targets_s=[angle for _ in angle_glms], rec=rec,
                                      speed_limit=speed_limit)

    speed_predictions = speed_glms[-1](speed_glm_res[-1]['res']['x'], get_covariates(full_rec, speed_limit, exclude_jumps=False))
    angle_predictions = angle_glms[-1](angle_glm_res[-1]['res']['x'], get_covariates(full_rec, speed_limit, speed_predictions, exclude_jumps=False))

    predictions = np.hstack([speed_predictions, angle_predictions])

    vel_prediction = polar_to_cartesian(predictions[:, 0], predictions[:, 2]) * speed_limit
    print(f'MSE: {get_velocity_mse(vel_prediction, np.concatenate([1e-6 * np.ones(2)[None, :], get_velocity(full_rec)[:-1]]))}')

    save_predictions(predictions=predictions, p_idx=0, rec=full_rec)
