import cmasher
import numpy as np
import seaborn as sn
import pandas as pd
from matplotlib import pyplot as plt

from _util import save_fig


def triangle_plot(model_selection, ax):
    data = get_norm_performance(model_selection)
    p1 = np.array([0.0, (3.0 ** 0.5) - 1.0])
    p2 = np.array([-1.0, -1.0])
    p3 = np.array([1.0, -1.0])
    points = []
    for t1, t2, t3 in data:
        points.append(p1 * t1 + p2 * t2 + p3 * t3)
    points = np.array(points)
    for p, model_name, valign in zip([p1, p2, p3], model_selection, ['bottom', 'top', 'top']):
        ax.text(p[0], p[1], model_name, verticalalignment=valign)
    ax.plot([-1.0, 1.0, 0.0, -1.0], [-1.0, -1.0, (3.0 ** 0.5) - 1.0, -1.0], color='k')
    sn.scatterplot(x=points.T[0], y=points.T[1], hue=list(range(len(points.T[0]))), ax=ax,
                   palette=cmasher.gem, alpha=.6, s=4, legend=False)
    ax.axis('off')


def get_norm_performance(model_selection):
    performance = np.array([all_model_vals[model_name]['aic'] for model_name in model_selection]).T
    inv_performance = 1 / performance
    norm_performance = inv_performance / inv_performance.sum(axis=1)[:, None]
    return norm_performance


if __name__ == '__main__':
    fig, axes = plt.subplots(1, 3, figsize=(7.5, 2))
    axes = axes.flatten()
    all_model_vals = pd.read_pickle('performance.pkl')
    model_selection_0 = ['effort_2_prev_t_invite', 'effort_2_prev_t', 'bias']
    model_selection_1 = ['effort_invite', 'effort_4_prev_t', 'bias']
    model_selection_2 = ['effort_invite', 'effort_4_prev_t_invite', 'bias']
    triangle_plot(model_selection_0, axes[0])
    triangle_plot(model_selection_1, axes[1])
    triangle_plot(model_selection_2, axes[2])
    save_fig()

