import numpy as np

# order = ['1_prev_t',
#          'effort_3_prev_t',
#          'effort_2_prev_t',
#          'effort_1_prev_t',
#          'effort_4_prev_t_invite',
#          'effort_3_prev_t_invite',
#          'effort_2_prev_t_invite',
#          'effort_1_prev_t_invite',
#          'effort_invite',
#          'bias']


# def cut_first_half(rec=None, td=None, t=120*10*60):
#     if rec is not None:
#         rec = rec[t:]
#     for i, start_idx in enumerate(td['start'].to_numpy()):
#         if start_idx > t:
#             td = td.iloc[i:]
#             td['start'] -= t  # to use them as indices later on
#             td['end'] -= t   # todo substract start_idx instead of t, look also whether the warings can be resolved
#             return rec, td


def cut_first_half(rec=None, td=None, t=120*10*60):
    if rec is not None:
        rec = rec[t:]
    for i, start_idx in enumerate(td['start'].to_numpy()):
        if start_idx > t:
            td = td.iloc[i:].copy()
            td['start'] -= start_idx  # to use them as indices later on
            td['end'] -= start_idx   # todo check whether this causes an error: substract start_idx instead of t, look also whether the warings can be resolved
            return rec, td


order = [
    'effort_3_prev_t_invite',
    'effort_2_prev_t_invite',
    'effort_1_prev_t_invite',
    'effort_invite',
    'effort_3_prev_t',
    'effort_2_prev_t',
    'effort_1_prev_t',
    'bias',
    '1_prev_t',
]

# order = ['1_prev_t',
#          # '2_prev_t',
#          # 'effort_3_prev_t',
#          'effort_2_prev_t',
#          'effort_1_prev_t',
#          'effort_4_prev_t_invite',
#          'effort_3_prev_t_invite',
#          'effort_2_prev_t_invite',
#          'effort_1_prev_t_invite',
#          'effort_invite',
#          'bias']


def add_properties(param_amount=0, param_boundaries=None, linearity_names=None, name=None, nll=None, keys=None,
                   covariate_keys=None, pred_target_key=None, depth=None):
    def wrapper(f):
        f.name = name if name is not None else f.__name__[:-10]
        f.param_amount = param_amount
        f.param_boundaries = param_boundaries
        f.linearity_names = linearity_names
        f.nll = nll
        f.keys = keys
        f.covariate_keys = covariate_keys
        f.pred_target_key = pred_target_key
        f.depth = depth
        return f
    return wrapper


def to_hot_encoded(x, class_amount=3):
    x = x.astype(int)
    hot_encoded = np.zeros((x.size, class_amount))
    hot_encoded[np.arange(x.size), x] = 1
    return hot_encoded


