import matplotlib
import numpy as np
import pandas as pd
import seaborn as sn
from matplotlib import pyplot as plt

from _util import save_fig


def get_rel_likelihoods(dyad_idx, key='aic', filename='../performance.pkl', model_selection=None):
    all_model_vals = pd.read_pickle(filename)
    measure = all_model_vals.T[key]
    model_names = list()
    model_aics = list()
    for model_name, performance_array in dict(measure).items():
        model_names.append(model_name)
        model_aics.append(performance_array[dyad_idx])
    rel_likelihoods = [np.exp((min(model_aics) - aic) / 2) for aic in model_aics]
    return dict(zip(model_names, rel_likelihoods))


if __name__ == '__main__':
    matplotlib.rcParams.update({'figure.autolayout': True})
    for dyad_idx in range(58):
        # matplotlib.RcParams['figure.autolayout'] = True
        m = get_rel_likelihoods(dyad_idx)
        plt.figure()
        sn.barplot(y=list(m.keys()), x=list(m.values()))
        plt.savefig(f'relative_likelihoods/dyad{dyad_idx}.png', dpi=300)

