import time
from itertools import cycle

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.patches import FancyBboxPatch, Rectangle
from matplotlib.ticker import FixedFormatter, FixedLocator

from _util import save_fig, game_width_in_cm
from heatmap import plot_heatmap, heat_imgs
from path_shortening_visualization import plot_targets, img_comp, img_coop0, img_coop1, calc_extent
from presentation.plotting import scatterplot, get_comp_frac_color
from simulations import load_results, get_weightings

linewidth = 1.5

if __name__ == '__main__':
    fig_width = 7
    fig_ratio = 9 / 16
    fig = plt.figure(figsize=(fig_width, fig_width * fig_ratio))
    grid_spec = GridSpec(2, 3, width_ratios=[1, 1, 1.2], height_ratios=[1, 1],
                         right=.96, top=.94, bottom=.02, left=.06, wspace=.3, hspace=.7)
    axes = np.array([
        fig.add_subplot(grid_spec[0, 0]),
        fig.add_subplot(grid_spec[0, 1]),
        fig.add_subplot(grid_spec[0, 2]),
        fig.add_subplot(grid_spec[1, 0]),
        fig.add_subplot(grid_spec[1, 1]),
        fig.add_subplot(grid_spec[1, 2]),
    ])

    comp = np.array([.12, .45])
    # comp = np.array([.2, .15])
    # coop0 = np.array([.65, .3])
    coop0 = np.array([.55, .3])
    # coop0 = np.array([.7, .13])
    # coop0 = np.array([.6, .4])
    # coop0 = np.array([.5, .45])
    coop1 = np.array([.9, .9])
    targets = [comp, coop0, coop1]
    p0 = np.array([.18, .8])
    p1 = p0 + np.array([.01, .01])
    agents = (p0 + p1) / 2

    plot_targets(comp=comp, coop0=coop0, coop1=coop1, p0=p0, p1=p1, ax=axes[0], n=1, prev_t='coop0')
    relevant_distances_0 = list()
    for t in targets:
        axes[0].plot([t[0], agents[0]], [t[1], agents[1]], c='k', zorder=0, linewidth=linewidth)
        relevant_distances_0.append(np.linalg.norm(t - agents))

    axes[0].set_xlim(0, 1)
    axes[0].set_ylim(0, 1)

    p1 += np.array([.2, .05])
    p0 = comp

    alternative = False
    # alternative = True
    if alternative:
        tmp = p0.copy()
        p0 = p1.copy()
        p1 = tmp
        p1 += np.array([0, .1])
        # p1 += np.array([0, .1])

    comp = np.array([.2, .28])

    # comp = np.array([.25, .85])
    # p0 = np.array([.1, .36])
    if alternative:
        shift = -.1
        p0 += np.array([0, shift])
        p1 += np.array([0, shift])
        comp += np.array([0, shift])
        coop0 += np.array([0, shift])
        coop1 += np.array([0, shift])
    targets = [comp, coop0, coop1]
    agents = np.array([p0, p1])

    plot_targets(comp=comp, coop0=coop0, coop1=coop1, p0=p0, p1=p1, ax=axes[1], n=1, prev_t='comp')
    relevant_distances_1 = list()
    for i, t in enumerate(targets):
        relevant_selection_op = np.argmax if i != 0 else np.argmin
        distances = [np.linalg.norm(t - p0), np.linalg.norm(t - p1)]
        relevant_p_idx = relevant_selection_op(distances)
        relevant_distances_1.append(distances[relevant_p_idx])
        for i, p in enumerate(agents):
            if i == relevant_p_idx:
                axes[1].plot([t[0], p[0]], [t[1], p[1]], c='k', zorder=5, linewidth=linewidth)
            else:
                axes[1].plot([t[0], p[0]], [t[1], p[1]], c='k', zorder=5, linestyle='dotted', linewidth=linewidth * .7)

    axes[1].set_xlim(0, 1)
    axes[1].set_ylim(0, 1)

    weightings = get_weightings(40)
    weighting_idx = np.array([12, 20, 28])[[1, 0, 2]]  # remapping
    # weighting_idx = [5, 20, 35]
    sim_results = load_results()
    for key in sim_results.keys():
        sim_results[key][1] *= game_width_in_cm

    weights = weightings[weighting_idx]
    naive_dots = sim_results['naive_0'][:, weighting_idx]
    ps_dots = sim_results['path_shortening'][:, weighting_idx]

    naive_to_plot = pd.DataFrame(dict(comp_fractions=naive_dots[0], dist=naive_dots[1],
                                      style=np.zeros_like(naive_dots[0])))
    ps_to_plot = pd.DataFrame(dict(comp_fractions=ps_dots[0], dist=ps_dots[1],
                                   style=np.zeros_like(ps_dots[0])))
    scatterplot(naive_to_plot, x='comp_fractions', y='dist', ax=axes[2], size=10, dyad_plot=False, zorder=10)
    scatterplot(ps_to_plot, x='comp_fractions', y='dist', ax=axes[2], size=10, dyad_plot=False, zorder=10)

    axes[2].plot(sim_results['naive_0'][0], sim_results['naive_0'][1], c='gray', label='Always same\nstarting position')
    axes[2].plot(sim_results['path_shortening'][0], sim_results['path_shortening'][1], c='k',
                 label='With advantageous\nstarting position')

    comp_frac_locator = FixedLocator([0, 1/3, 2/3, 1])
    comp_frac_formatter = FixedFormatter(['0', '1/3', '2/3', '1'])
    axes[2].xaxis.set_major_locator(comp_frac_locator)
    axes[2].xaxis.set_major_formatter(comp_frac_formatter)
    axes[2].set_xlabel('Stable FST')
    # axes[2].set_xlabel('Fraction of single\ntargets $\\Phi$')
    # axes[2].set_ylabel('Mean distance\nto target (cm)')
    axes[2].set_ylabel('Mean distance\nto target (cm)')
    axes[2].set_ylim([12.475550740475837, 25.757501087688862])

    t0 = time.time()
    heatmap_res = 50
    plot_heatmap(comp=[.7, .3], coop0=[.8, .65], coop1=[.1, .2], ax=axes[4], plot_opti=True, res=heatmap_res,
                 heatmap_path=f'presentation/data/heatmap_{heatmap_res}.npy')
    # axes[4].text(.02, .98, '$\\Phi\\approx⅓$', transform=axes[4].transAxes, size=10, ha='left', va='top')
    print(f'calculation time: {time.time() - t0}')

    t0 = time.time()
    plot_heatmap(comp=[.7, .3], coop0=[.8, .65], coop1=[.1, .2], ax=axes[3], plot_opti=True, res=heatmap_res,
                 comp_mode=True,
                 heatmap_path=f'presentation/data/simple_heatmap_{heatmap_res}.npy')
    axes[3].text(.02, .98, '$\\Phi=1$', transform=axes[3].transAxes, size=10, ha='left', va='top')
    print(f'calculation time: {time.time() - t0}')
    cbar = fig.colorbar(heat_imgs[-1], ax=axes[[3, 4]].ravel().tolist())
    cbar.set_label('Expected distance\nto target (cm)',
                   rotation=-90, size=10, labelpad=25)
    cbar.ax.invert_yaxis()

    target_offset = .014
    target_radius = .02
    # block_offset = (1 - (target_offset * 6 + 9 * 2 * target_radius)) / 6
    block_offset = .05
    ax5_t_positions = list()
    ax5_t_y = .08
    prev_dist = 0
    for block in range(3):
        prev_dist += block_offset
        for target in range(3):
            prev_dist += target_radius
            ax5_t_positions.append(np.array([prev_dist, ax5_t_y]))
            prev_dist += target_radius
            if target != 2:
                prev_dist += target_offset
        # prev_dist += block_offset

    zorder = 20
    axes[-1].set_xlim([.23, 1])
    axes[-1].set_ylim([0, 1])
    axes[-1].axis('off')

    inter_block_offset = 0.
    # inter_block_offset = 0.01
    patch_width = target_offset * 2 + block_offset + target_radius * 2 * 3 - inter_block_offset
    patch_height = .3
    patch_x = inter_block_offset / 2
    patch_y = ax5_t_y - target_radius - block_offset

    def plot_distance_bars(relevant_distances, x_shift=.4, y_shift=0.):
        dist_y_base = ax5_t_y + target_radius + target_offset + y_shift
        scaled_distances = np.concatenate([np.array([
            relevant_distances[0] * (1 - w),
            relevant_distances[1] * w,
            relevant_distances[2] * w,
        ]) for w in weights])
        scaled_distances /= scaled_distances.max()
        scaled_distances *= (patch_height - 2 * block_offset - 2 * target_radius)
        new_t_positions = list()
        for pos, dist, img in zip(ax5_t_positions, scaled_distances, cycle([img_comp, img_coop0, img_coop1])):
            extent = calc_extent(pos + np.array([x_shift, y_shift]), 1, aspect_ratio=axes[-1]._get_aspect_ratio())
            new_t_positions.append((extent[0] + extent[1]) / 2)
            axes[-1].imshow(img, aspect='auto', extent=extent, zorder=zorder)
            plot_x = pos[0] - .025 + x_shift
            axes[-1].plot([plot_x, plot_x], [dist_y_base, dist_y_base + dist], c='k', linewidth=linewidth)
            # axes[-1].plot([pos[0] - .025, pos[0] - .025], [dist_y_base, dist_y_base + dist], c='k', linewidth=linewidth)
        labels = ['equal\nweighting', 'joint\npreferred', 'single\npreferred']
        # labels = ['equal\nweighting', 'towards\njoint', 'towards\nsingle']
        for i, text in enumerate(labels):
            text_height = .18
            bbox_x = patch_x + (patch_width + inter_block_offset) * i + x_shift
            bbox_y = patch_y + y_shift - text_height
            color_remapping = {0: 1, 1: 0, 2: 2}
            color = get_comp_frac_color(naive_dots[0][color_remapping[i]])
            if i == 1:
                color = get_comp_frac_color(0.)
            axes[-1].add_patch(FancyBboxPatch((bbox_x, bbox_y),
                                              patch_width, patch_height + text_height,
                                              boxstyle='round,pad=-0.0004,rounding_size=0.02',
                                              edgecolor='none',
                                              mutation_aspect=2,
                                              color=color,
                                              alpha=.55,
                                              # alpha=.8,
                                              # alpha=.7,
                                              ec='w'))
            text_x = new_t_positions[1 + i * 3]
            axes[-1].text(text_x, y_shift + ax5_t_y - target_radius - block_offset, text,
                          ha='center', va='top', fontdict=dict(size=7.4))

    plot_distance_bars(relevant_distances_0, y_shift=.68)
    plot_distance_bars(relevant_distances_1, y_shift=.18)

    # axes[0].text(1., 1.04, 'Same starting position', transform=axes[0].transAxes, ha='right')
    # axes[1].text(1., 1.04, 'Different starting position', transform=axes[1].transAxes, ha='right')
    axes[0].text(0.05, 1.05, 'Same starting position', transform=axes[0].transAxes, ha='left')
    if alternative:
        axes[1].text(0.05, 1.05, 'Three relevant distances', transform=axes[1].transAxes, ha='left')
    else:
        axes[1].text(0.05, 1.05, 'Different starting position', transform=axes[1].transAxes, ha='left')
    axes[4].text(0.05, 1.05, 'Advantageous starting position', transform=axes[4].transAxes, ha='left')
    axes[2].legend(loc='upper left', bbox_to_anchor=(-2., -.2), frameon=False, )

    for ax_label, ax in zip('abc d', axes):
        ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes, weight='bold')
        ax.set_box_aspect(1)

    save_fig(sub_folder='presentation/', format='svg')
    save_fig(sub_folder='presentation/', format='pdf')


