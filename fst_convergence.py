import cmasher
import matplotlib.pyplot as plt
import seaborn as sn
from matplotlib import ticker

from _util import fontdict, save_fig
from trial_descriptor import load_all_trial_descriptions, get_comp_frac


def split_tds_at_minute(tds=None, split_minute=10, fps=120):
    if tds is None:
        tds = load_all_trial_descriptions(cut_initial=False)
    last_frame = split_minute * 60 * fps
    first_minutes_tds = list()
    last_minutes_tds = list()
    for td in tds:
        for i, val in enumerate(td['start']):
            # for i, val in enumerate(td['end']):
            if val > last_frame:
                first_minutes_tds.append(td.head(i))
                last_minutes_tds.append(td.tail(len(td) - i))
                break
    return first_minutes_tds, last_minutes_tds


if __name__ == '__main__':
    figsize = 4
    fig = plt.figure(figsize=(figsize, figsize))
    fig.subplots_adjust(bottom=0.3, left=0.3, top=0.8, right=0.8)
    # fig.tight_layout()
    ax = fig.gca()

    tds = load_all_trial_descriptions(cut_initial=False)
    comp_fractions = [get_comp_frac(td) for td in tds]
    initial_tds, final_tds = split_tds_at_minute(tds)
    initial_comp_frac = [get_comp_frac(td) for td in initial_tds]
    final_comp_frac = [get_comp_frac(td) for td in final_tds]
    sn.scatterplot(x=final_comp_frac, y=initial_comp_frac, ax=ax, hue=list(range(len(tds))), palette=cmasher.gem,
                   alpha=.6, legend=False)
    sn.lineplot(x=[0, 1], y=[0, 1], c='k', ax=ax)

    ax.set_box_aspect(1)
    ax.set_xlabel('Final fraction of\nsingle targets collected $\\Phi$', fontdict=fontdict)
    ax.set_ylabel('Intial fraction of\nsingle targets collected $\\Phi$', fontdict=fontdict)
    comp_frac_locator = ticker.FixedLocator([0, 1/3, 2/3, 1])
    comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1'])
    ax.xaxis.set_major_locator(comp_frac_locator)
    ax.xaxis.set_major_formatter(comp_frac_formatter)
    ax.yaxis.set_major_locator(comp_frac_locator)
    ax.yaxis.set_major_formatter(comp_frac_formatter)
    ax.xaxis.set_tick_params(labelsize=14)
    ax.yaxis.set_tick_params(labelsize=14)

    # fig.tight_layout()
    save_fig()
