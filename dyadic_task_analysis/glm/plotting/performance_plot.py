import pandas as pd
import seaborn as sn
from matplotlib import pyplot as plt

from _util import fontdict, save_fig
from glm.util import order


def mean_barplot(vals, key, ylabel, ax):
    measure = vals.T[key]
    measure = {key: val.mean() for key, val in measure.items()}
    return _barplot(measure, ylabel, ax)


def dyad_barplot(dyad_idx, vals, key, ylabel, ax):
    measure = vals.T[key]
    measure = {key: val[dyad_idx] for key, val in measure.items()}
    return _barplot(measure, ylabel, ax)


def best_model_count_barplot(ax, ylabel='Min AIC Dyads'):
    best_model_count = get_best_model_count()
    best_model_count = {key: len(val) for key, val in best_model_count.items() if len(val) > 0}
    best_model_count = dict(sorted(best_model_count.items(), key=lambda x: x[1], reverse=True))
    return _barplot(best_model_count, ylabel, ax)


def _barplot(measure, ylabel, ax):
    model_names = list(measure.keys())
    accs = list(measure.values())
    bar_rectangles = ax.bar(x=model_names, height=accs)
    ax.xaxis.set_tick_params(labelsize=10)
    ax.yaxis.set_tick_params(labelsize=10)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=-30, horizontalalignment='left', fontdict=dict(size=10))
    ax.set_ylabel(ylabel, fontdict=fontdict)
    return bar_rectangles


def get_best_model_for_dyad(dyad_idx, key='aic', filename='performance.pkl', model_selection=None):
    all_model_vals = pd.read_pickle(filename)
    measure = all_model_vals.T[key]
    best_performance = None
    best_model_name = None
    for model_name, performance_array in dict(measure).items():
        if model_selection is None or model_name in model_selection:
            dyad_performance = performance_array[dyad_idx]
            if best_performance is None:
                best_performance = dyad_performance
                best_model_name = model_name
            elif key == 'acc' and best_performance < dyad_performance or key == 'aic' and best_performance > dyad_performance:  # lower is better
                best_performance = dyad_performance
                best_model_name = model_name
    return best_model_name


def get_best_model_count(dyad_amount=58):
    all_model_vals = pd.read_pickle('performance.pkl')
    best_model_count = {key: list() for key in all_model_vals.keys()}
    for dyad_idx in range(dyad_amount):
        best_model_count[get_best_model_for_dyad(dyad_idx)].append(dyad_idx)
    return best_model_count


def plot_best_model_count(bar_rectangles):
    best_model_count = get_best_model_count()
    for rect, dyad_list in zip(bar_rectangles, list(best_model_count.values())):
        y_pos = rect.get_height()
        x_pos = rect.get_x() + rect.get_width() / 2
        plt.text(x_pos, y_pos, f'{len(dyad_list)}', ha='center', va='bottom')

# todo create always comp target model for basline!

def dyad_wise_plot():
    rows = 30
    fig, axes = plt.subplots(rows, 1, figsize=(2, 1 * rows))
    axes = axes.flatten()
    all_model_vals = pd.read_pickle('performance.pkl')
    for dyad_idx, ax in enumerate(axes):
        _ = dyad_barplot(dyad_idx, all_model_vals, key='aic', ylabel='Accuracy', ax=ax)
    save_fig()


def mean_n_count_plot():
    fig, axes = plt.subplots(2, 1, figsize=(10, 5))
    axes = axes.flatten()
    all_model_vals = pd.read_pickle('performance.pkl')
    _ = mean_barplot(all_model_vals, key='acc', ylabel='Accuracy', ax=axes[0])
    bar_rectangles = mean_barplot(all_model_vals, key='aic', ylabel='AIC', ax=axes[1])
    plot_best_model_count(bar_rectangles)
    save_fig()


def best_model_count_plot():
    ax = plt.gca()
    best_model_count_barplot(ax)
    save_fig()


def get_dyad_model_heatmap(measure='aic'):
    best_model_count = get_best_model_count()
    model_selection = [key for key, val in best_model_count.items() if len(val) > 0]
    all_model_vals = pd.read_pickle('performance.pkl')
    aic_heatmap = {key: val for key, val in all_model_vals.T[measure].T.to_dict().items() if key in model_selection}
    for key in aic_heatmap.keys():
        print(key)
    aic_heatmap = {key: aic_heatmap[key] for key in order}
    # aic_heatmap = {key: val - np.min(val) for key, val in aic_heatmap.items()}
    aic_heatmap = pd.DataFrame(aic_heatmap).T
    for i in range(58):
        aic_heatmap[i] -= min(aic_heatmap[i])
    return aic_heatmap
    # sn.heatmap(aic_heatmap, ax=ax, vmin=0, vmax=20)


if __name__ == '__main__':
    # mean_n_count_plot()
    # best_model_count_plot()
    # dyad_model_heatmap()
    fig, axes = plt.subplots(2, 1, figsize=(11, 5))
    axes = axes.flatten()
    dyad_model_heatmap = get_dyad_model_heatmap(measure='aic')
    cbar_kws = dict(label='$AICc - AICc_{Dyad\\_best}$')
    sn.heatmap(dyad_model_heatmap, ax=axes[0], vmin=0, vmax=30, cmap='Reds_r', cbar_kws=dict(pad=.015))
    sn.heatmap(dyad_model_heatmap, ax=axes[1], vmin=0, vmax=300, cmap='Reds_r', cbar_kws=dict(pad=.015))
    for ax_label, ax in zip('ABCD', axes):
        ax.text(0, 1.04, ax_label, transform=ax.transAxes, size=14)
        ax.set_xlabel('Dyad')
        ax.set_ylabel('Model')
        ax.collections[0].colorbar.set_label('$AICc - AICc_{Dyad\\_best}$', rotation=-90, labelpad=15)
    plt.subplots_adjust(left=.18, right=1.05, hspace=.4, top=.93)
    save_fig()

