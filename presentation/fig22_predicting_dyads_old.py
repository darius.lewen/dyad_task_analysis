import numpy as np
import pandas as pd
import seaborn as sn
from matplotlib import pyplot as plt, dates, ticker
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import AutoMinorLocator
import scipy

from _util import save_fig
from conv_visualized import load_frame_count_corrected_tds, get_fst_ma_series, get_effort_prediction_mas, fps
from glm.plotting.type_hist import get_hist_plot_frame
from glm.swarm_parformance import model_selection
from load.loading_helper import unique_pairs
from pred_performance import generate_fst_bar
from presentation.plotting import violin_swarm, scatterplot
from trial_descriptor_2 import get_comp_frac, unsorted_load_all_trial_descriptions
from trial_descriptor_2 import load_all_trial_descriptions


window = 30  # second


def get_full_glm_prediction_mas(tds):
    prediction_mas = list()
    for td in tds:
        expected_values = td[['pred_prob_single', 'pred_prob_joint0', 'pred_prob_joint1']].to_numpy()
        fst_prediction_binary = (expected_values.argmax(axis=1) == 0).astype(int)
        prediction_mas.append(ma_base(fst_prediction_binary, td['start_time'].to_numpy(), start_time=10 * 60))
    return prediction_mas


def ma_base(single_t_binary, timings, start_time=0, w=None):
    if w is None:
        global window
        w = window
    ma_series = list()
    for i in range(start_time, 2 * 20 * 60 - w):
        a = (timings > i).astype('int64')
        b = (timings < i + w).astype('int64')
        in_window = a + b == 2
        ma_series.append(np.mean(single_t_binary[in_window]))
    return np.array(ma_series)


if __name__ == '__main__':
    fig_width = 7
    fig_ratio = 9 / 16
    fig = plt.figure(figsize=(fig_width, fig_width * fig_ratio))
    grid_spec = GridSpec(2, 3, width_ratios=[2.4, 1.4, 1.], height_ratios=[1, 1],
                         right=.96, top=.94, bottom=None, left=None, wspace=.35, hspace=.7)
    axes = np.array([
        fig.add_subplot(grid_spec[0]),
        fig.add_subplot(grid_spec[1: 3]),
        fig.add_subplot(grid_spec[3: 5]),
        fig.add_subplot(grid_spec[5]),
    ])

    # tds = load_frame_count_corrected_tds()
    tds = load_all_trial_descriptions()

    unsorted_comp_fractions = [get_comp_frac(td) for td in unsorted_load_all_trial_descriptions()]
    comp_frac_sorting = np.argsort(unsorted_comp_fractions)
    pair_labels = [f'replay_idx_{i}_pair_id_{unique_pairs[i][0]}_{unique_pairs[i][1]}' for i in comp_frac_sorting]

    ma_series = [get_fst_ma_series(td) for td in tds]

    # effort_prediction_ma = get_effort_prediction_mas(tds)
    # best_prediction_ma = get_effort_prediction_mas(tds, best_models=True)
    # best_prediction_ma = get_effort_prediction_mas(tds, full_glm=True)
    effort_prediction_ma = get_full_glm_prediction_mas(tds)  # todo add distance pred stuff..
    best_prediction_ma = get_full_glm_prediction_mas(tds)

    # effort_p_x_vals = np.array(list(range(10 * 60 + window, 2 * 20 * 60)))
    effort_p_x_vals = np.arange(10 * 60 + window, 2 * 20 * 60)
    # effort_p_x_vals = (effort_p_x_vals + window // 2) / fps  # to shift to mid and to seconds
    effort_x_dates = pd.to_datetime(effort_p_x_vals, unit='s')

    # effort_predictions = (effort_predictions == 0).astype(int)
    # x_values = np.array(list(range(window, 2 * 20 * 60 * fps, fps))) / fps
    x_values = np.arange(window, 2 * 20 * 60)
    # x_values = (x_values + window) / fps  # to shift to mid and to seconds
    # x_values = (x_values + window // 2) / fps  # to shift to mid and to seconds
    x_dates = pd.to_datetime(x_values, unit='s')

    dyad_idx = 28
    axes[2].plot(x_dates, ma_series[dyad_idx], linewidth=2.6, color='k', alpha=.4,
                 label=f'Truth')
    axes[2].plot(effort_x_dates, effort_prediction_ma[dyad_idx], linewidth=.8, alpha=1, c='tab:orange',
                 label=f'Distance weighing')
    # label = f'Moving average ({window // 120}s) over $\\Phi$ predicted by distance')
    axes[2].plot(effort_x_dates, best_prediction_ma[dyad_idx], linewidth=.8, alpha=1, c='tab:blue',
                 label=f'Full GLM')
    # label = f'Moving average ({window // 120}s) over $\\Phi$ predicted by best fitting model ({best_models[dyad_idx]})')

    axes[2].set_xlim([0, pd.to_datetime(2400, unit='s')])
    axes[2].set_ylim([-.02, 1.02])
    axes[3].set_ylim([-.02, 1.02])
    axes[2].xaxis.set_minor_locator(AutoMinorLocator(5))
    axes[2].xaxis.set_major_formatter(dates.DateFormatter('%M'))
    comp_frac_locator = ticker.FixedLocator([0, 1 / 3, 2 / 3, 1])
    comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1'])
    axes[2].yaxis.set_major_locator(comp_frac_locator)
    axes[2].yaxis.set_major_formatter(comp_frac_formatter)
    axes[3].yaxis.set_major_locator(comp_frac_locator)
    axes[3].yaxis.set_major_formatter(comp_frac_formatter)
    axes[2].set_xlabel('Time (min)')#, fontdict=fontdict)
    axes[2].set_ylabel('30-Second moving\naverage FST')#, fontdict=fontdict)
    axes[2].legend(loc='upper left', ncol=3, bbox_to_anchor=(-.01, 1.07), frameon=False, columnspacing=.6)#, fontsize=fontdict['size'])
    # axes[2].set_xlabel('Density')#, fontdict=fontdict)
    sn.histplot(y=ma_series[dyad_idx][10 * 60:], ax=axes[3], stat='density', kde=True, binwidth=.05, color='gray',
                kde_kws=dict(bw_method=.3))
    sn.kdeplot(y=effort_prediction_ma[dyad_idx], ax=axes[3], bw_method=.3, color='tab:orange')
    sn.kdeplot(y=best_prediction_ma[dyad_idx], ax=axes[3], bw_method=.3, color='tab:blue')
    comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1'])
    data = pd.read_pickle('glm/performance.pkl')
    model_selection = [  # full
        'effort',
        # 'effort_prev_t_inv',
        'effort_2_prev_t_invite',
        # 'effort_1_prev_t_invite',
    ]
    selected_model_accuracies = pd.DataFrame(data[model_selection].T['acc'].T.to_dict())
    size_scalar = 1.3

    w_dyad_acc = pd.read_pickle('data/acc_for_dyads.pkl')

    unsorted_best_model = get_hist_plot_frame('glm/unsorted_performance.pkl')
    best_model = unsorted_best_model.sort_values(by=['comp_frac']).set_index(pd.Series(list(range(len(unsorted_best_model)))))

    selected_model_accuracies = pd.concat([
                                           w_dyad_acc['$w=0.5$ '],  # todo perform this on test set!
                                           selected_model_accuracies], axis=1)
    col_names = ['Predict\ncloset\ntarget', 'Distance\nweighting\nGLM', 'Full\nGLM']
    selected_model_accuracies.rename(columns={key: col_name for key, col_name in
                                              zip(selected_model_accuracies.keys(), col_names)},
                                     inplace=True)
    violin_swarm(data=selected_model_accuracies, ax=axes[1], marker_size=3)
    axes[0].set_xlabel('')
    axes[1].set_xlabel('')
    axes[0].xaxis.set_ticks_position('none')
    axes[1].set_ylabel('Prediction accuracy')
    # print(axes[1].xaxis.label.get_size())
    # print(axes[1].xaxis.xtick)
    axes[1].set_xticklabels(axes[1].get_xticklabels(), fontdict=dict(size=10))

    generate_fst_bar(axes[0])
    axes[1].set_ylim([-.02, 1.02])

    for ax_label, ax in zip(' efg', axes):
            ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes, weight='bold')

    ## read out prediction accuracy
    def get_mean_model_accuracies(selected_model_accuracies, only_intermediate=False):
        if only_intermediate:
            in_intermediate_group = [.1 < get_comp_frac(td) < .9 for td in load_all_trial_descriptions()]
            selected_model_accuracies = selected_model_accuracies.iloc[in_intermediate_group]
        mean_model_accuracies = dict()
        ci_model_accuracies = dict()
        for key in selected_model_accuracies.keys():
            true_pred_count = selected_model_accuracies[key].to_numpy() * 100  # 100 is test set size
            prediction_binary = [np.concatenate([np.ones(int(count)), np.zeros(100 - int(count))]) for count in true_pred_count]
            prediction_binary = np.concatenate(prediction_binary)
            mean_model_accuracies[key] = np.mean(prediction_binary)
            ci_model_accuracies[key] = scipy.stats.t.interval(alpha=0.95, df=len(prediction_binary) - 1,
                                                              loc=np.mean(prediction_binary),
                                                              scale=scipy.stats.sem(prediction_binary))
        return mean_model_accuracies, ci_model_accuracies

    mean_model_accuracies, ci_model_accuracies = get_mean_model_accuracies(selected_model_accuracies)
    intermediate_mean_model_accuracies, intermediate_ci_model_accuracies = get_mean_model_accuracies(selected_model_accuracies, only_intermediate=True)

    save_fig(sub_folder='presentation/', format='svg')

    # showing that the full GLM grasps the standard deviation / fluctuations better
    # x_dates = effort_x_dates
    # weighed_dist_predicted_fst_ma_sd = effort_prediction_ma
    # full_glm_predicted_fst_ma_sd = best_prediction_ma
    true_fst_ma_sd = np.array([ma_s.std() for ma_s in ma_series])
    weighed_dist_predicted_fst_ma_sd = np.array([ma_s.std() for ma_s in effort_prediction_ma])
    full_glm_predicted_fst_ma_sd = np.array([ma_s.std() for ma_s in best_prediction_ma])
    in_intermediate_group = np.array([.1 < get_comp_frac(td) < .9 for td in tds])
    res = scipy.stats.wilcoxon(true_fst_ma_sd[in_intermediate_group],
                               weighed_dist_predicted_fst_ma_sd[in_intermediate_group])
    print(f'Wilcoxon signed-rank test: {res.statistic}, p-value: {res.pvalue}')
    res = scipy.stats.wilcoxon(true_fst_ma_sd[in_intermediate_group],
                               full_glm_predicted_fst_ma_sd[in_intermediate_group])
    print(f'Wilcoxon signed-rank test: {res.statistic}, p-value: {res.pvalue}')

    fig, axes = plt.subplots(2, 3)
    axes = axes.flatten()
    binwidth = .02
    sn.histplot(true_fst_ma_sd, ax=axes[0], binwidth=binwidth)
    sn.histplot(weighed_dist_predicted_fst_ma_sd, ax=axes[1], binwidth=binwidth)
    sn.histplot(full_glm_predicted_fst_ma_sd, ax=axes[2], binwidth=binwidth)
    sn.histplot(true_fst_ma_sd[in_intermediate_group], ax=axes[3], binwidth=binwidth)
    sn.histplot(weighed_dist_predicted_fst_ma_sd[in_intermediate_group], ax=axes[4], binwidth=binwidth)
    sn.histplot(full_glm_predicted_fst_ma_sd[in_intermediate_group], ax=axes[5], binwidth=binwidth)
    for ax in axes:
        ax.set_xlim(0, .5)
        ax.set_ylim(0, 30)
    # comp_fractions = [get_comp_frac(td) for td in tds]
    # to_plot = dict(sd=list(), kind=list())
    # for sd in true_fst_ma_sd:
    #     to_plot['sd'].append(sd)
    #     to_plot['kind'].append('true_fst')
    # for sd in weighed_dist_predicted_fst_ma_sd:
    #     to_plot['sd'].append(sd)
    #     to_plot['kind'].append('weighted_dist')
    # for sd in full_glm_predicted_fst_ma_sd:
    #     to_plot['sd'].append(sd)
    #     to_plot['kind'].append('full_glm')
    # sn.histplot(pd.DataFrame(to_plot), x='sd', hue='kind', multiple='layer', alpha=.3)
    # to_plot = pd.DataFrame(dict(true_fst=true_fst_ma_sd, weighted_dist=weighed_dist_predicted_fst_ma_sd,
    #                             full_glm=full_glm_predicted_fst_ma_sd, comp_fractions=comp_fractions))
    # scatterplot(to_plot, x='true_fst', y='weighted_dist', ax=axes[0])
    # scatterplot(to_plot, x='true_fst', y='full_glm', ax=axes[1])
    # for ax in axes:
    #     ax.set_xlim(0, .5)
    #     ax.set_ylim(0, .5)

