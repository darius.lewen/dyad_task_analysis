import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn

from _util import save_fig, convert_trial_classes
from inefficiencies_full import comp_frac_x_format
from trial_descriptor import load_all_trial_descriptions, get_comp_frac
from reward_decomposition import scatter, plot_corr_coef

classes = ['miscoordination', 'ahead', 'concurrent', 'anti_corr', 'declined_invite', 'accepted_invite']
colors = ['#164a64', '#d62628', '#fb4f2f', '#e4ae38', '#e277c1', '#9367bc']

fontdict = dict(size=12)


def get_class_amount(td, c):
    if 'anti_corr' in c:
        return get_class_amount_base(td, 'anti_corr')
    if 'miscoordination' in c:
        return get_class_amount_base(td, 'miscoordination')
    elif 'ahead' in c:
        return get_class_amount_base(td, 'p0_ahead') + get_class_amount_base(td, 'p1_ahead')
    elif 'invite' not in c:
        return get_class_amount_base(td, c)
    elif 'accepted' in c:
        return get_class_amount_base(td, 'accepted_coop0_invite') + get_class_amount_base(td, 'accepted_coop1_invite')
    else:
        return get_class_amount_base(td, 'declined_coop0_invite') + get_class_amount_base(td, 'declined_coop1_invite')


def get_class_amount_base(td, c):
    class_counts = td['trial_class'].value_counts()
    if c in class_counts:
        return class_counts[c] // 1.5  # due to 1.5 blocks
    return 0


def get_class_counts(tds, classes):
    return {c: np.array([get_class_amount(td, c) for td in tds]) for c in classes}


def plot_aic_poly(comp_fractions, param, ax=None, num=50):
    aic_for_degrees = list()  # https://vitalflux.com/aic-vs-bic-for-regression-models-formula-examples/
    n = len(comp_fractions)
    degrees_to_test = list(range(1, 10))
    for deg in degrees_to_test:
        _, residuals, _, _, _ = np.polyfit(comp_fractions, param, deg=deg, full=True)
        aic = n * np.log(residuals[0] / n) + 2 * deg
        small_sample_correction = (2 * deg * (deg + 1)) / (n - deg - 1)
        aic_for_degrees.append(aic + small_sample_correction)
    best_fitting_deg = degrees_to_test[np.argmin(aic_for_degrees)]
    print(best_fitting_deg)
    poly = np.polyfit(comp_fractions, param, deg=best_fitting_deg)
    xspace = np.linspace(0, 1, num=num)
    curve = np.polyval(poly, xspace)
    if ax is not None:
        sn.lineplot(x=xspace, y=curve, ax=ax, color='k', linewidth=7)
    return xspace, curve


if __name__ == '__main__':
    tds = load_all_trial_descriptions()
    comp_fractions = [get_comp_frac(td) for td in tds]
    scalar = .7
    fig, axes = plt.subplots(3, 4, figsize=(14 * scalar, 3.5 * 3 * scalar))
    axes = axes.flatten()
    class_counts = get_class_counts(tds, classes)
    class_counts = {key: val / len(td) for td, (key, val) in zip(tds, class_counts.items())}  # todo to make it to fractions
    curves = dict()

    for c, ax in zip(classes, axes):
        scatter(comp_fractions, class_counts[c], ax)
        plot_y = .73
        if c == 'ahead':
            plot_y = .06
        plot_corr_coef(comp_fractions, class_counts[c], ax, plot_x=.02, plot_y=plot_y, ha='left')
        _, curve = plot_aic_poly(comp_fractions, class_counts[c], ax)
        ax.set_ylabel(c)
        curves[c] = curve

    res = 50
    curve_x = np.linspace(0, 1, res)
    curves = np.array(list(curves.values()))
    class_labels = [convert_trial_classes[c] for c in classes]
    borders = np.cumsum(curves, axis=0)
    borders = np.vstack([np.zeros(res), borders])
    mean_trial_amount = np.mean([len(td) for td in tds])
    borders *= mean_trial_amount
    for prev_border, next_border, y_key, color, c_label in reversed(list(zip(borders[:-1], borders[1:], classes, colors, class_labels))):
        axes[-5].fill_between(curve_x, prev_border, next_border, label=c_label, color=color) #, label=c_label)
    borders /= mean_trial_amount
    borders = borders / borders[-1]  # normalize
    for prev_border, next_border, y_key, color, c_label in reversed(list(zip(borders[:-1], borders[1:], classes, colors, class_labels))):
        axes[-4].fill_between(curve_x, prev_border, next_border, label=c_label, color=color) #, label=c_label)

    total_class_counts = {key: sum(val) for key, val in class_counts.items()}
    # class_counts = dict(c=list(class_counts.keys()), vals=list(class_counts.values()))
    total_class_counts = pd.Series(total_class_counts)
    total_class_counts.plot.bar(ax=axes[-6], color=colors, width=.8)
    # class_counts.plot.bar(x='c', y='vals', ax=axes[-4])
    plt.subplots_adjust(left=.07, right=.98, bottom=.23, top=.9, wspace=1, hspace=1)
    axes[-4].legend(loc='upper left', bbox_to_anchor=(1.15, 1.15), frameon=False, fontsize=fontdict['size'])
    axes[-1].axis('off')
    axes[-2].axis('off')
    axes[-3].axis('off')

    # p0_ahead = np.array([get_class_amount_base(td, 'p0_ahead') for td in tds])
    # p1_ahead = np.array([get_class_amount_base(td, 'p1_ahead') for td in tds])
    # leader_follower = abs(p0_ahead - p1_ahead) / (p0_ahead + p1_ahead)
    # # leader_follower = [abs(get_class_amount_base(td, 'p0_ahead') - get_class_amount_base(td, 'p1_ahead')) / len(td)
    # #                    for td in tds]
    # scatter(comp_fractions, leader_follower, axes[-2])
    # plot_corr_coef(comp_fractions, leader_follower, axes[-2]) #, plot_x=.02, plot_y=plot_y, ha='left')

    ylabels = class_labels + ['Total amount\nover all dyads', 'Estimated trial\namount', 'Estimated fraction\nof trials']

    for key, ylabel, ax in zip('ABCDEFGHIJKL', ylabels, axes):
        ax.text(-0.1, 1.1, key, transform=ax.transAxes, size=fontdict['size'])
        ax.set_xlabel('Fraction of single\ntargets collected $\\Phi$', fontdict=fontdict)
        ax.set_ylabel(ylabel, fontdict=fontdict)
        ax.set_box_aspect(1)
        if key in 'ABCDEF':
            ax.set_ylim([0, .5])
        if key != 'G':
            comp_frac_x_format(ax)
        else:
            ax.set_xticklabels(class_labels, rotation=-40, horizontalalignment='left', fontdict=fontdict)

    axes[-6].set_xlabel('')
    axes[-6].set_xlim([-.7, len(classes) - .3])
    axes[-4].set_xlim([0, 1])
    axes[-4].set_ylim([0, 1])
    axes[-5].set_xlim([0, 1])
    axes[-5].set_ylim([0, 700])

    save_fig()

