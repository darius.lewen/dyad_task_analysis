import numpy as np
import pandas as pd
import seaborn as sn
from matplotlib import pyplot as plt
from scipy.stats import pearsonr, linregress

from _util import save_fig
from glm.plotting.entropy_ts import get_best_entropy_ts
from glm.plotting.entropy_vs_duration_2 import get_concat_td, remove_head
from pred_performance import fontdict
from trial_descriptor import load_all_trial_descriptions, get_comp_frac

if __name__ == '__main__':
    tds = [remove_head(get_concat_td(dyad_idx)) for dyad_idx in range(58)]
    durations = [td['duration'].to_numpy() for td in tds]
    distance_ol = [td['distance_ol'].to_numpy() for td in tds]
    mis_cord_measure = [td['mis_cord_measure'].to_numpy() for td in tds]
    distance = [td['distance'].to_numpy() for td in tds]
    effort = [td['chosen_effort'].to_numpy() for td in tds]
    speed = [td['speed'].to_numpy() for td in tds]
    best_entropy_ts = get_best_entropy_ts(filename='../unsorted_performance.pkl')

    error_inputs = dict()
    correlations = list()

    for dyad_idx in range(58):
        # measure = effort[dyad_idx] / durations[dyad_idx]
        measure = distance_ol[dyad_idx] / distance[dyad_idx]
        try:
            corr_coefficient, p_value = pearsonr(best_entropy_ts[dyad_idx], measure)
            if p_value < 0.05:
                correlations.append(corr_coefficient)
            print(dyad_idx, corr_coefficient, p_value)
        except ValueError as e:
            error_inputs[dyad_idx] = measure
            print(e)

    print('avg_corr:', sum(correlations) / len(correlations))

    # dyad_idx = 48
    dyad_idx = 8

    to_plot = pd.DataFrame(dict(
            durations=durations[dyad_idx],
            distance_ol=distance_ol[dyad_idx],
            entropy=best_entropy_ts[dyad_idx],
            mis_cord_measure=mis_cord_measure[dyad_idx],
            distance=distance[dyad_idx],
            effort=effort[dyad_idx],
            speed=speed[dyad_idx],
            effort_per_sec=effort[dyad_idx] / durations[dyad_idx],
            inefficiency=distance_ol[dyad_idx] / distance[dyad_idx],
        ))

    # sn.jointplot(data=to_plot, x='best_entropy_ts', y='a', kind='kde', fill=True)
    # sn.scatterplot(data=to_plot, x='best_entropy_ts', y='a', alpha=.2)
    # plt.tight_layout()
    fig = plt.figure(figsize=(2.3, 2.3))

    sn.scatterplot(data=to_plot, x='inefficiency', y='entropy', alpha=.2)
    slope, intercept, r, p, se = linregress(x=to_plot['inefficiency'], y=to_plot['entropy'])
    xlin = np.linspace(0, 1, 10)
    sn.lineplot(x=xlin, y=intercept + slope*xlin, c='k')

    corr_coefficient = "%.2f" % r
    p_value = "%.2f" % p
    axes = plt.gca()
    print(p_value)
    print('p_value is hardcoded!!!')
    axes.text(.98, .02, f'$r\\approx{corr_coefficient}$\n $p<1e-18$', transform=axes.transAxes, size=10,
              ha='right')
    axes.set_ylim([0, np.log2(3)])
    axes.set_xlim([0, 1])
    axes.set_xlabel('Inefficiency', fontdict=fontdict)
    axes.set_ylabel('Entropy', fontdict=fontdict)
    # axes.set_aspect(1)
    axes.set_box_aspect(1)
    fig.tight_layout()
    save_fig()

    # corr_coefficient, p_value = pearsonr(to_plot['best_entropy_ts'], to_plot['a'])

    # sn.jointplot(data=to_plot, x='best_entropy_ts', y='distance', kind='kde', fill=True)
    # corr_coefficient, p_value = pearsonr(to_plot['best_entropy_ts'], to_plot['distance'])

    # # sn.jointplot(data=to_plot, x='best_entropy_ts', y='mis_cord_measure', kind='kde', fill=True)
    # # corr_coefficient, p_value = pearsonr(to_plot['best_entropy_ts'], to_plot['mis_cord_measure'])
    #
    # # sn.jointplot(data=to_plot, x='best_entropy_ts', y='distance_ol', kind='kde', fill=True)
    # # corr_coefficient, p_value = pearsonr(to_plot['best_entropy_ts'], to_plot['distance_ol'])
    #
    #
    # # sn.jointplot(data=to_plot, x='best_entropy_ts', y='mis_cord_measure', kind='kde', fill=True)
    # # sn.jointplot(data=to_plot, x='best_entropy_ts', y='durations', kind='kde', fill=True)
    # # sn.jointplot(data=to_plot, x='best_entropy_ts', y='effort', kind='kde', fill=True)
    # # sn.jointplot(data=to_plot, x='best_entropy_ts', y='speed', kind='kde', fill=True)
    # # sn.jointplot(data=to_plot, x='best_entropy_ts', y='effort_per_sec', kind='kde', fill=True)


