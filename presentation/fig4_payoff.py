import pandas as pd
import seaborn as sn
import numpy as np
from matplotlib import pyplot as plt, ticker
from scipy.stats import pearsonr, linregress
from sklearn.metrics import r2_score

from _util import save_fig, game_width_in_cm
from presentation.plotting import scatterplot, reg_plot, r_and_p_text
from reward_decomposition import get_agent_rewards, get_mean_in_cm, reward_per_target, block_time, plot_strategies
from simulations import load_results
from trial_classes import plot_aic_poly
# from trial_descriptor import load_all_trial_descriptions, get_comp_frac
from trial_descriptor_2 import load_all_trial_descriptions, get_comp_frac

fontdict = dict(size=12)
xspace = np.linspace(0, 1)


def get_sim_results():
    sim_results = load_results()
    for key in sim_results.keys():
        sim_results[key][1] *= game_width_in_cm
    return sim_results


def get_strat_diff():
    sim_results = get_sim_results()
    naive_0 = sim_results['naive_0']
    path_shortening = sim_results['path_shortening']
    x_space = np.linspace(0, 1, 100)
    naive_0 = np.interp(x_space, naive_0[0], naive_0[1])
    path_shortening = np.interp(x_space, path_shortening[0], path_shortening[1])
    strat_diff = naive_0 - path_shortening
    return x_space, strat_diff


def get_optimal_effort_reduction():
    sim_results = get_sim_results()
    path_shortening = np.interp(xspace, sim_results['path_shortening'][0], sim_results['path_shortening'][1])
    naive_0 = np.interp(xspace, sim_results['naive_0'][0], sim_results['naive_0'][1])
    return naive_0 - path_shortening


def get_optimal_from_past_target_curve():
    sim_results = get_sim_results()
    naive_0 = np.interp(xspace, sim_results['naive_0'][0], sim_results['naive_0'][1])
    return naive_0


if __name__ == '__main__':
    rows, cols = 2, 3
    fig_width = 7
    fig_ratio = 9 / 16
    fig, axes = plt.subplots(rows, cols, figsize=(fig_width, fig_width * fig_ratio))
    plt.subplots_adjust(left=.1, right=.987, top=.95, bottom=.14, wspace=.8, hspace=.8)
    axes = axes.flatten()
    # trial_duration_axes = axes[0].twinx()

    tds = load_all_trial_descriptions()
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)
    y_reward_min, y_reward_max = min(agent_mean_reward), max(agent_mean_reward)

    trial_durations = np.array([td['duration'].to_numpy().mean() for td in tds])
    comp_fractions = [get_comp_frac(td) for td in tds]
    distance = get_mean_in_cm('path_len', tds)
    speed = get_mean_in_cm('speed', tds)
    additional_effort = get_mean_in_cm('path_len_due_to_curvature', tds)
    effort_from_past_target = get_mean_in_cm('limiting_agent_next_target_displacement', tds) + get_mean_in_cm('reduction_due_to_adv_placement', tds)
    effort_reduction_by_ps = get_mean_in_cm('reduction_due_to_adv_placement', tds)

    to_plot = pd.DataFrame(dict(
        comp_fractions=comp_fractions,
        agent_mean_reward=agent_mean_reward,
        joint_payoff=2 * agent_mean_reward,
        distance=distance,
        speed=speed,
        additional_effort=additional_effort,
        effort_from_past_target=effort_from_past_target,
        effort_reduction_by_ps=effort_reduction_by_ps,
    ))


    markers = None
    scatterplot(data=to_plot, x='comp_fractions', y='joint_payoff', ax=axes[0], markers=markers)
    scatterplot(data=to_plot, x='comp_fractions', y='distance', ax=axes[1], markers=markers)
    scatterplot(data=to_plot, x='comp_fractions', y='speed', ax=axes[2], markers=markers)
    scatterplot(data=to_plot, x='comp_fractions', y='effort_from_past_target', ax=axes[3], markers=markers)
    scatterplot(data=to_plot, x='comp_fractions', y='additional_effort', ax=axes[4], markers=markers)
    scatterplot(data=to_plot, x='comp_fractions', y='effort_reduction_by_ps', ax=axes[5], markers=markers)

    reg_plot(x=to_plot['comp_fractions'], y=to_plot['speed'], ax=axes[2])
    r_and_p_text(x=to_plot['comp_fractions'], y=to_plot['speed'], ax=axes[2])
    reg_plot(x=to_plot['comp_fractions'], y=to_plot['additional_effort'], ax=axes[4])
    r_and_p_text(x=to_plot['comp_fractions'], y=to_plot['additional_effort'], ax=axes[4], text_pos=(.985, .985), va='top')

    # calculate r-squared for joint payoff being well predicted by mean_traj_len and traj_speed
    joint_payoff = to_plot['joint_payoff'].to_numpy()
    joint_payoff_prediction = 20 * 60 / (distance / speed + 1) * .07
    r_and_p_text(x=joint_payoff_prediction, y=joint_payoff, ax=axes[0], text_pos=(1.5, .985), va='top')

    plot_strategies(axes[3], skip_ps=True, skip_second=False)
    axes[3].get_legend().remove()
    # axes[4].legend(loc='upper left', bbox_to_anchor=(-.55, -.8 + .2), frameon=False, )

    x_strat_diff, y_strat_diff = get_strat_diff()
    sn.lineplot(x=x_strat_diff, y=y_strat_diff, ax=axes[5], color='k')
    # sn.lineplot(x=x_strat_diff, y=y_strat_diff, ax=axes[5], color='k', label='Maximal distance reduction possible')

    # axes[0].set_ylabel('Joint payoff (€)')
    # axes[1].set_ylabel('Mean trajectory\nlength (cm)')
    # axes[2].set_ylabel('Mean speed (cm/s)', )
    # axes[3].set_ylabel('Mean distance from\nprevious to next target (cm)', )
    # axes[4].set_ylabel('Mean length due to\ncurvature (cm)')
    # axes[5].set_ylabel('Mean distance reduction\n due to split-up (cm)', )
    axes[0].set_ylabel('Joint payoff (€)')
    axes[1].set_ylabel('Mean trajectory\nlength (cm)')
    axes[2].set_ylabel('Mean speed\n(cm/s)')
    axes[3].set_ylabel('Mean distance\nfrom previous to\nnext target (cm)', )
    axes[4].set_ylabel('Mean length due\nto curvature (cm)')
    axes[5].set_ylabel('Mean distance\nreduction due to\nadv. placement (cm)', )

    # correction_term_min = 0
    # correction_term_max = 0
    # correction_term_max = -.12  # todo probably needed due to initial cut?
    # y_reward_min += -.5
    # y_reward_max += .5

    # trial_duration_axes.set_ylim(reward_per_target * block_time / (y_reward_max * 2) - 1 + correction_term_min,
    #                              reward_per_target * block_time / (y_reward_min * 2) - 1 + correction_term_max)
    # axes[0].set_ylim(y_reward_min, y_reward_max)

    # trial_duration_axes.yaxis.labelpad = 23
    # trial_duration_axes.invert_yaxis()

    comp_frac_locator = ticker.FixedLocator([0, 1/3, 2/3, 1])
    comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1'])

    xlim = axes[0].get_xlim()
    for ax_label, ax in zip('abcdef', axes):
        ax.set_xlim(xlim)
        ax.set_box_aspect(1)
        ax.set_xlabel('Stable FST')
        ax.xaxis.set_major_locator(comp_frac_locator)
        ax.xaxis.set_major_formatter(comp_frac_formatter)
        ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes, weight='bold')

    axes[1].set_ylim([18.5, 30.5])
    axes[-2].set_ylim(axes[-1].get_ylim())

    print(axes[3].get_ylim())

    # debugging for fig5...
    fig5_debug_flag = False
    if fig5_debug_flag:
        xspace, speed_curve = plot_aic_poly(comp_fractions, speed)
        _, additional_effort_curve = plot_aic_poly(comp_fractions, additional_effort)
        _, effort_from_past_target_curve = plot_aic_poly(comp_fractions, effort_from_past_target)
        _, effort_reduction_by_ps_curve = plot_aic_poly(comp_fractions, effort_reduction_by_ps)
        axes[2].plot(xspace, speed_curve)
        axes[3].plot(xspace, effort_from_past_target_curve)
        axes[4].plot(xspace, additional_effort_curve)
        axes[5].plot(xspace, effort_reduction_by_ps_curve)
        comp_distance_curve = effort_from_past_target_curve + additional_effort_curve - effort_reduction_by_ps_curve
        axes[1].plot(xspace, comp_distance_curve)
        comp_duration_curve = comp_distance_curve / speed_curve
        def duration_to_rew(dur):
            return .035 * 20 * 60 / (dur + 1)
        comp_reward_curve = duration_to_rew(comp_duration_curve)
        duration = np.array([td['duration'].to_numpy().mean() for td in tds])
        _, org_duration_curve = plot_aic_poly(comp_fractions, duration)
        org_reward_curve = duration_to_rew(org_duration_curve)
        axes[0].plot(xspace, org_reward_curve)
        axes[0].plot(xspace, comp_reward_curve)

    save_fig(sub_folder='presentation/', format='svg')


    # calculate r-squared for joint payoff being approximately proportional to the mean collection cycle duration
    joint_payoff = to_plot['joint_payoff'].to_numpy()  # todo check this!
    joint_payoff_prediction = 20 * 60 / (trial_durations + 1) * .07
    r, p = pearsonr(joint_payoff, joint_payoff_prediction)
    print('approximately proportional...', r, p)

    trial_durations_pred = distance / speed
    r, p = pearsonr(trial_durations, trial_durations_pred)
    print('approximately proportional... is the mean acquisition duration to the mean traj len divided by mean speed', r, p)

    # calculate r-squared for speed increases with fst
    r, p = pearsonr(speed, comp_fractions)
    print('fst - speed', r, p)
