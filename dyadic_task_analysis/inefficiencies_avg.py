import matplotlib
import numpy as np
from matplotlib import pyplot as plt

from inefficiencies_full import get_poly_fits, comp_frac_x_format, y_keys, get_class_count, get_efficiency_measure
from _util import save_fig, fontdict
from simulations import game_width_in_cm


def plot_contributions(measure, ax, res=100, deg=3, scaling=1, colors=None):
    if colors is None:
        colors = ['#164a64', '#fb4f2f', '#e4ae38', '#e277c1', '#9367bc', '#d62628']
    poly_fits = get_poly_fits(measure, deg)
    curve_x = np.linspace(0, 1, res)
    curves = [np.polyval(p, curve_x) for p, _ in poly_fits]
    borders = [np.sum(curves[:i], axis=0) for i in range(1, len(curves) + 1)]
    borders = [np.zeros(res)] + [border * scaling for border in borders]
    for prev_border, next_border, y_key, color in zip(borders[:-1], borders[1:], y_keys, colors):
        ax.fill_between(curve_x, prev_border, next_border, label=y_key, color=color)
    comp_frac_x_format(ax)
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1.1 * max(borders[-1]))
    ax.xaxis.set_tick_params(labelsize=14)
    ax.yaxis.set_tick_params(labelsize=14)


if __name__ == '__main__':
    matplotlib.use('TkAgg')
    matplotlib.rcParams['text.usetex'] = True
    ax_width = 2.7
    ax_height = ax_width
    rows, cols = 2, 3
    fig, axes = plt.subplots(rows, cols, figsize=(ax_width * cols, ax_height * rows))
    axes = axes.flatten()
    axes[3].remove()
    axes = axes[[0, 1, 2, 4, 5]]

    measure_names = ['duration', 'speed', 'effort_ol', 'distance_ol']
    scalings = [1] + [game_width_in_cm for _ in range(3)]
    class_count = get_class_count()
    plot_contributions(class_count, axes[0])

    for measure_name, scaling, ax in zip(measure_names, scalings, axes[1:]):
        measure = get_efficiency_measure(measure_name)
        plot_contributions(measure, ax, scaling=scaling)

    ylabels = ['Trail amount $A$',
               r'Mean trial duration $\langle t \rangle$ (s)',
               r'Mean speed $\langle s \rangle$ (cm/s)',
               r'Mean initial effort\newline overlength $\langle E_{ol} \rangle$ (cm)',
               r'Mean distance\newline overlength $\langle D_{ol} \rangle$ (cm)']

    for key, ylabel, ax in zip('ABCDE', ylabels, axes.flatten()):
        ax.set_box_aspect(1)
        ax.text(-0.1, 1.1, key, transform=ax.transAxes, size=16, weight='bold')
        ax.set_xlabel('Fraction of\nsingle targets collected', fontdict=fontdict)
        ax.set_ylabel(ylabel, fontdict=fontdict)

    axes[0].legend(loc='upper left', bbox_to_anchor=(-.55, -.55), frameon=False, fontsize=fontdict['size'])

    plt.subplots_adjust(top=.95, right=.95, wspace=.8, hspace=.35)
    save_fig()

