from collections import Counter

import numpy as np
import seaborn as sn
from matplotlib import pyplot as plt
from scipy.interpolate import splrep, splev

from _util import save_fig
from conv_pred import get_class_counts
from inefficiencies_full import comp_frac_x_format
from markov_traj import simplify_trial_class
from trial_descriptor_2 import load_all_trial_descriptions, get_comp_frac

label_translation = dict(
    anti_corr='Towards different\ntargets',
    declined_invite='Failed\ninvitation',
    accepted_invite='Invitation',
    miscoordination='Strongly\ncurved',
    ahead='One ahead to\nsame target',
    concurrent='Concurrent to\nsame target',
)


def get_class_scatter(trial_classes, probs=True):
    trial_probs = list()
    for t_classes in trial_classes:
        t_classes = [simplify_trial_class(tc) for tc in t_classes]
        class_count = Counter(t_classes)
        class_probs = {key: val / len(t_classes) for key, val in class_count.items()}
        trial_probs.append(class_probs)

    classes = list(set([simplify_trial_class(tc) for tc in np.concatenate(trial_classes)]))
    class_scatter = {key: list() for key in classes}

    for tp in trial_probs:
        for c in classes:
            class_scatter[c].append(tp[c] if c in tp.keys() else 0)
    return class_scatter


if __name__ == '__main__':
    tds = load_all_trial_descriptions()
    comp_fractions = np.array([get_comp_frac(td) for td in tds])

    trial_classes = [td['trial_class'].to_numpy() for td in tds]

    class_scatter = get_class_scatter(trial_classes)
    classes = list(set([simplify_trial_class(tc) for tc in np.concatenate(trial_classes)]))

    xspace = np.linspace(0, 1)
    fig, axes = plt.subplots(2, 3)
    plt.subplots_adjust(wspace=1, hspace=1)
    axes = axes.flatten()

    colorblind = sn.color_palette('colorblind')
    for ax_label, col, ax, c in zip('abcdef', colorblind, axes, classes):
        ax.scatter(comp_fractions, class_scatter[c], c=col, alpha=.2)
        ax.plot(xspace, splev(xspace, splrep(comp_fractions, class_scatter[c], s=1)), c=col, label=label_translation[c])
        comp_frac_x_format(ax)
        ax.set_ylim(0, 1)
        ax.legend(loc='upper left', bbox_to_anchor=(-.27, 1.45), frameon=False)
        ax.set_xlabel('Stable FST')
        ax.set_ylabel('Fraction of trials')
        ax.set_box_aspect(1)
        ax.text(-0.5, 1.14, ax_label, transform=ax.transAxes, weight='bold')

    boundary0 = 1/3/2
    boundary1 = 2/3
    group0 = comp_fractions < boundary0
    group1 = 2 == (comp_fractions > boundary0).astype(int) + (comp_fractions < boundary1).astype(int)

    a = np.mean(np.array(class_scatter['miscoordination'])[group0])
    b = np.mean(np.array(class_scatter['miscoordination'])[group1])
    c = np.mean(np.array(class_scatter['anti_corr'])[group1])
    mis_pred_prob = 1/3 * a + 1/3 * (b + c)

    trial_amounts = np.array([len(td) for td in tds])
    sel_trial_amounts = np.concatenate([trial_amounts[group0], trial_amounts[group1]])
    sel_trial_mis_pred_counts = sel_trial_amounts * mis_pred_prob
    mean_mis_pred_count = np.mean(sel_trial_mis_pred_counts)
    std_mis_pred_count = np.std(sel_trial_mis_pred_counts)

    _sel_trial_amounts = sel_trial_amounts + 2/3 * sel_trial_amounts
    _sel_trial_mis_pred_counts = _sel_trial_amounts * mis_pred_prob
    _mean_mis_pred_count = np.mean(_sel_trial_mis_pred_counts)
    _std_mis_pred_count = np.std(_sel_trial_mis_pred_counts)

    save_fig(format='svg')
