

if __name__ == '__main__':
    rows, cols = 2, 3
    fig_width = 7
    fig_ratio = 9 / 16
    fig, axes = plt.subplots(rows, cols, figsize=(fig_width, fig_width * fig_ratio))
    plt.subplots_adjust(left=.1, right=.987, top=.95, bottom=.14, wspace=.8, hspace=.8)
    axes = axes.flatten()
    # trial_duration_axes = axes[0].twinx()

    tds = load_all_trial_descriptions()
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)
    y_reward_min, y_reward_max = min(agent_mean_reward), max(agent_mean_reward)

    trial_durations = np.array([td['duration'].to_numpy().mean() for td in tds])
    comp_fractions = [get_comp_frac(td) for td in tds]
    distance = get_mean_in_cm('path_len', tds)
custom_bar_plot