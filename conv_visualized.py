import matplotlib
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import AutoMinorLocator

from glm.likelihood_2 import load_per_dyad_stats
from glm.plotting.performance_plot import get_best_model_for_dyad

matplotlib.use('Agg')
from matplotlib import pyplot as plt, ticker
from numba import jit
import numpy as np
import pandas as pd
import matplotlib.dates as dates
import seaborn as sn

from _util import save_fig
from load.loading_helper import unique_pairs
from trial_descriptor import load_all_trial_descriptions, get_comp_frac, unsorted_load_all_trial_descriptions

fps = 120
fontdict = dict(size=12)
colors = ['#35618f', '#68c3ef', '#6f2f5f']
window = 30 * 120
invite_trial_names = ['accepted_coop0_invite', 'declined_coop0_invite',
                      'accepted_coop1_invite', 'declined_coop1_invite']


def get_single_t_binary(td):
    p0_collections = np.array(td['coll_type'] == 'single_p0').astype(int)
    p1_collections = np.array(td['coll_type'] == 'single_p1').astype(int)
    return p0_collections + p1_collections


def get_invite_binary(td):
    accepted_coop0_invite = np.array(td['trial_class'] == 'accepted_coop0_invite').astype(int)
    accepted_coop1_invite = np.array(td['trial_class'] == 'accepted_coop1_invite').astype(int)
    declined_coop0_invite = np.array(td['trial_class'] == 'declined_coop0_invite').astype(int)
    declined_coop1_invite = np.array(td['trial_class'] == 'declined_coop1_invite').astype(int)
    return accepted_coop0_invite + accepted_coop1_invite + declined_coop0_invite + declined_coop1_invite


def get_only_after_single_invite(td):
    invite_binary = get_invite_binary(td)
    timings = td['end'].to_numpy() + 120
    return ma_base(invite_binary, timings)


def get_invite_ma_series(td):
    invite_binary = get_invite_binary(td)
    timings = td['end'].to_numpy() + 120
    return ma_base(invite_binary, timings)


def get_fst_ma_series(td, w=None):
    single_t_binary = get_single_t_binary(td)
    timings = td['end'].to_numpy() + 120
    return ma_base(single_t_binary, timings, w=w)


def get_only_after_single_invite_ma(td):
    single_t_binary = get_single_t_binary(td)
    invite_binary = get_invite_binary(td)
    after_single_binary = np.array([False] + list(single_t_binary[:-1].astype(bool)))
    timings = td['end'].to_numpy() + 120
    after_single_invite_binary = invite_binary[after_single_binary]
    after_single_timings = timings[after_single_binary]
    return ma_base(after_single_invite_binary, after_single_timings)


def ma_base(single_t_binary, timings, start_frame=0, w=None):
    if w is None:
        global window
        w = window
    ma_series = list()
    for i in range(start_frame, 2 * 20 * 60 * fps - w, fps):
        a = (timings > i).astype('int64')
        b = (timings < i + w).astype('int64')
        c = a + b == 2
        ma_series.append(np.mean(single_t_binary[c]))
    return np.array(ma_series)


def correct_frame_count(td, block_frame_amount=20 * 60 * 120):
    flag = False
    prev_start_frames = td[:-1]['start'].to_numpy()
    start_frames = td[1:]['start'].to_numpy()
    for i, (prev_start_f, start_f) in enumerate(zip(prev_start_frames, start_frames)):
        if not flag and prev_start_f > start_f:
            flag = True
        if flag:
            td['start'].iloc[i + 1] += block_frame_amount
            td['end'].iloc[i + 1] += block_frame_amount
    return td


def gen_tds():
    tds = [correct_frame_count(td) for td in load_all_trial_descriptions(cut_initial=False)]
    for dyad_idx, td in enumerate(tds):
        td.to_pickle(f'plots/conv_visualizations/tds/{dyad_idx}.pkl')


def load_frame_count_corrected_tds():
    tds = list()
    for dyad_idx in range(58):
        tds.append(pd.read_pickle(f'plots/conv_visualizations/tds/{dyad_idx}.pkl'))
    return tds


def special_cut_first_10(td, t=120 * 10 * 60):
    for i, start_idx in enumerate(td['start'].to_numpy()):
        if start_idx > t:
            return td.iloc[i + 4:].drop([0, 1, 2, 3])


best_models = [get_best_model_for_dyad(i, filename='glm/performance.pkl') for i in range(58)]


def get_best_models_expected_vals():
    performance = pd.read_pickle('glm/performance.pkl')
    expected_values = list()
    print('hi')
    for dyad_idx in range(58):
        expected_values.append(performance[best_models[dyad_idx]]['expected_vals'][dyad_idx])
    return expected_values


# def get_full_glm_expected_vals():
#     performance = pd.read_pickle('glm/performance.pkl')
#     expected_values = [performance['effort_prev_t_inv']['expected_vals'][dyad_idx] for dyad_idx in range(58)]
#     return expected_values


def get_effort_prediction_mas(tds, full_glm=False, best_models=False):
    performance = pd.read_pickle('glm/performance.pkl')
    fst_prediction_binary = performance['effort']['expected_vals']
    per_dyad_stats = load_per_dyad_stats()
    print(performance.keys())
    if full_glm:
        prediction_binary_old = performance['effort_2_prev_t_invite']['expected_vals']
        fst_prediction_binary = [dyad_stat['expected_values'] for dyad_stat in per_dyad_stats]
        comp_frac_sorting = np.argsort([get_comp_frac(td) for td in unsorted_load_all_trial_descriptions()])
        fst_prediction_binary = np.array(fst_prediction_binary)[comp_frac_sorting]

        # prediction_binary = performance['effort_prev_t_inv']['expected_vals']
        # prediction_binary = get_full_glm_expected_vals()
    if best_models:
        print('deprecated!')
        fst_prediction_binary = get_best_models_expected_vals()
    # cut_tds = [special_cut_first_10(td) for td in tds]
    # timings = [td['end'].to_numpy() + 120 for td in cut_tds]
    timings = [td['end'].to_numpy() + 120 for td in tds]
    fst_prediction_binary = [(p.argmax(axis=1) == 0).astype(int) for p in fst_prediction_binary]
    predictions_mas = list()
    for i, (p, t) in enumerate(zip(fst_prediction_binary, timings)):
        if full_glm:
            if len(p) != len(t):
                print(0)
        predictions_mas.append(ma_base(p, t, start_frame=10 * 60 * fps))
    return predictions_mas
    # return [ma_base(p, t, start_frame=10 * 60 * fps) for p, t in zip(fst_prediction_binary, timings)]


def plot_overview():
    fig, axes = plt.subplots(len(ma_series), 1, figsize=(2, 30), sharex='all', sharey='all')
    axes = axes.flatten()
    axes[0].set_ylim([0, 1])
    for series, ax in zip(ma_series, axes):
        ax.plot(x_dates, series)
    for i, ax in enumerate(axes):
        ax.text(1, .7, pair_labels[i], transform=ax.transAxes, size=4, ha='right')
    save_fig()


if __name__ == '__main__':
    # todo: create only single target invite ma by using only single target timings (timings[single_t_binary) and invites...

    # gen_tds()
    tds = load_frame_count_corrected_tds()

    unsorted_comp_fractions = [get_comp_frac(td) for td in unsorted_load_all_trial_descriptions()]
    comp_frac_sorting = np.argsort(unsorted_comp_fractions)
    pair_labels = [f'replay_idx_{i}_pair_id_{unique_pairs[i][0]}_{unique_pairs[i][1]}' for i in comp_frac_sorting]

    ma_series = [get_fst_ma_series(td) for td in tds]

    effort_prediction_ma = get_effort_prediction_mas(tds)
    best_prediction_ma = get_effort_prediction_mas(tds, full_glm=True)
    # best_prediction_ma = get_effort_prediction_mas(tds, best_models=True)

    effort_p_x_vals = np.array(list(range(10 * 60 * fps, 2 * 20 * 60 * fps - window, fps)))
    effort_p_x_vals = (effort_p_x_vals + window // 2) / fps  # to shift to mid and to seconds
    effort_x_dates = pd.to_datetime(effort_p_x_vals, unit='s')

    # effort_predictions = (effort_predictions == 0).astype(int)
    x_values = np.array(list(range(0, 2 * 20 * 60 * fps - window, fps)))
    x_values = (x_values + window // 2) / fps  # to shift to mid and to seconds
    x_dates = pd.to_datetime(x_values, unit='s')
    invite_trials = [td[td['trial_class'].isin(invite_trial_names)] for td in tds]
    invite_timings = [pd.to_datetime(td['start'].to_numpy() / fps, unit='s') for td in invite_trials]
    invite_mas = [get_invite_ma_series(td) for td in tds]
    # only_after_single_invites = [get_only_after_single_invites(td) for td in tds]
    only_after_single_invite_mas = [get_only_after_single_invite_ma(td) for td in tds]

    # plot_overview()
    for dyad_idx in range(58):
        # scalar = 1.6
        # scalar = 1.05
        scalar = 1
        # fig, axes = plt.subplots(1, 2, figsize=(8 * scalar, 2.5 * scalar))
        # ax, hist_ax = axes.flatten()
        fig = plt.figure(figsize=(8 * scalar, 2.5 * scalar))
        grid_spec = GridSpec(1, 2, width_ratios=[4.5, 1], height_ratios=[1], bottom=.28, right=.98, left=.12, top=.94)  # , wspace=1, hspace=1)
        ax = fig.add_subplot(grid_spec[0])
        hist_ax = fig.add_subplot(grid_spec[1])
        # after_single_ax = fig.add_subplot(grid_spec[0, 0])
        # invite_ax = fig.add_subplot(grid_spec[1, 0])
        # ax = fig.add_subplot(grid_spec[2, 0])
        # hist_ax = fig.add_subplot(grid_spec[2, 1])
        # after_single_ax.plot(x_dates, only_after_single_invite_mas[dyad_idx])
        # invite_ax.plot(x_dates, invite_mas[dyad_idx], linewidth=.8, alpha=1)
        # invite_ax.plot(x_dates, ma_series[dyad_idx], linewidth=.8, color='k', alpha=.4)
        ax.plot(x_dates, ma_series[dyad_idx], linewidth=2.6, color='k', alpha=.4,
                label=f'Moving average ({window // 120}s) over true $\\Phi$')
        ax.plot(effort_x_dates, best_prediction_ma[dyad_idx], linewidth=.8, alpha=1,
                label=f'Moving average ({window // 120}s) over $\\Phi$ predicted by best fitting model ({best_models[dyad_idx]})')
        ax.plot(effort_x_dates, effort_prediction_ma[dyad_idx], linewidth=.8, alpha=1,
                label=f'Moving average ({window // 120}s) over $\\Phi$ predicted by distance')
        # for invite_timing in invite_timings[dyad_idx]:
        #     ax.plot([invite_timing, invite_timing], [-.1, 1.1], linewidth=.4, alpha=.4, color='red')
        # for tmp_ax in [after_single_ax, invite_ax, ax]:
        #     tmp_ax.set_xlim([0, pd.to_datetime(2400, unit='s')])
        #     tmp_ax.set_ylim([-.02, 1.02])
        #     tmp_ax.xaxis.set_minor_locator(AutoMinorLocator(5))
        #     tmp_ax.xaxis.set_major_formatter(dates.DateFormatter('%M'))
        comp_frac_locator = ticker.FixedLocator([0, 1/3, 2/3, 1])
        comp_frac_formatter = ticker.FixedFormatter(['0', '1/3', '2/3', '1'])
        ax.yaxis.set_major_locator(comp_frac_locator)
        ax.yaxis.set_major_formatter(comp_frac_formatter)
        hist_ax.yaxis.set_major_locator(comp_frac_locator)
        hist_ax.yaxis.set_major_formatter(comp_frac_formatter)
        ax.set_xlabel('Time (min)', fontdict=fontdict)
        # after_single_ax.set_ylabel('Invite after single [%]', fontdict=fontdict)
        # invite_ax.set_ylabel('Invite trials [%]', fontdict=fontdict)
        ax.set_ylabel('Fraction of single\ntargets collected $\\Phi$', fontdict=fontdict)
        ax.legend(loc='upper left', bbox_to_anchor=(-.15, -.25), frameon=False, fontsize=fontdict['size'])
        hist_ax.set_xlabel('Density', fontdict=fontdict)
        sn.histplot(y=ma_series[dyad_idx][10 * 60:], ax=hist_ax, stat='density', kde=True, bins=10, color='gray',
                    kde_kws=dict(bw_method=.3))
        # sn.kdeplot(y=ma_series[dyad_idx][10 * 60:], ax=hist_ax) #, stat='percent') # , kde=True, bins=10)
        sn.kdeplot(y=best_prediction_ma[dyad_idx], ax=hist_ax, bw_method=.3) #, stat='percent') # , kde=True, bins=10)
        sn.kdeplot(y=effort_prediction_ma[dyad_idx], ax=hist_ax, bw_method=.3) #, stat='percent') # , kde=True, bins=10)
        fst_mean = "%.2f" % np.mean(ma_series[dyad_idx])
        fst_var = "%.2f" % np.var(ma_series[dyad_idx])
        hist_ax.text(.98, .8, f'Mean: {fst_mean}\nVar: {fst_var}',
                     transform=hist_ax.transAxes, size=10, ha='right')
        ax.plot([x_dates[10*60-15], x_dates[10*60-15]], [-.2, 1.2], color='k', linewidth=1)
        plt.savefig(f'plots/conv_visualizations/{pair_labels[dyad_idx]}.svg')

        #  convert svg to png in terminal: for file in *.svg; do inkscape "$file" -o "${file%svg}png"; done



