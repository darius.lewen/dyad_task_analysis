import numpy as np
import pandas as pd
import seaborn as sn
from matplotlib import pyplot as plt
from matplotlib.gridspec import GridSpec

from _util import save_fig, game_width_in_cm
from pred_performance import violin_swarm_mean_plot, generate_fst_bar, get_comp_frac_colors
from presentation.plotting import violin_swarm
from trial_descriptor import load_all_trial_descriptions, get_comp_frac

xticklabels = [
    'to optimal\nplacement\n($\\Phi=1$)',
    'to optimal\nplacement\n($\\Phi=⅓$)',
    'to collecting\nagent',
    'to next\njoint target',
]

if __name__ == '__main__':
    size_scalar = .85
    # fig, axes = plt.subplots(2, 2, figsize=(11 * size_scalar, 5 * size_scalar))
    fig = plt.figure(figsize=(7 * size_scalar, 5 * size_scalar))
    grid_spec = GridSpec(2, 2, width_ratios=[1, .2], height_ratios=[1, 1], bottom=.02, right=.98,
                         left=.12, top=.94, hspace=.6)
    axes = [
        fig.add_subplot(grid_spec[0, 0]),
        fig.add_subplot(grid_spec[0, 1]),
        fig.add_subplot(grid_spec[1, 0]),
        fig.add_subplot(grid_spec[1, 1]),
    ]

    tds = load_all_trial_descriptions()

    dists_from_free_agent = dict(
        distances_to_w10_opti=[td['asp_dist_to_w10_opti'].to_numpy(dtype=np.float64) * game_width_in_cm for td in tds],
        distances_to_w05_opti=[td['asp_dist_to_w05_opti'].to_numpy(dtype=np.float64) * game_width_in_cm for td in tds],
        distances_to_collecting=[td['asp_dist_to_other'].to_numpy(dtype=np.float64) * game_width_in_cm for td in tds],
        distances_to_coop=[td['asp_dist_to_coop'].to_numpy(dtype=np.float64) * game_width_in_cm for td in tds],
    )

    for distance_measure_name, measures in dists_from_free_agent.items():
        for i in range(len(measures)):
            measures[i] = measures[i][~np.isnan(measures[i])]
            if len(measures[i]) < 50:  # todo hyperparameter
                measures[i] = np.array([])
            measures[i] = measures[i].mean()

    dists_from_free_agent = pd.DataFrame(dists_from_free_agent)
    violin_swarm(data=dists_from_free_agent, ax=axes[0], show_mean=True)

    axes[1].axis('off')
    axes[0].set_xlabel('')
    axes[0].xaxis.set_ticks_position('none')
    axes[0].xaxis.set_tick_params()
    axes[0].yaxis.set_tick_params()
    axes[0].set_xticklabels(xticklabels)
    axes[0].set_ylabel('Mean distance form\nfree agent (cm)')
    axes[3].axis('off')
    generate_fst_bar(axes[2])
    save_fig()
