import cmasher
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sn

from glm.plotting.performance_plot import get_best_model_for_dyad
from trial_descriptor import load_all_trial_descriptions, get_comp_frac


matplotlib.use('TkAgg')


def get_best_entropy_ts(dyad_amount=58, filename='../performance.pkl'):
    all_model_vals = pd.read_pickle(filename)
    best_entropy_ts = list()
    for dyad_idx in range(dyad_amount):
        best_model = get_best_model_for_dyad(dyad_idx, filename=filename)
        best_entropy_ts.append(all_model_vals[best_model]['entropy_ts'][dyad_idx])
    return best_entropy_ts


if __name__ == '__main__':
    ax_width = 4
    rows = 1
    fig, axes = plt.subplots(rows, 1, figsize=(ax_width * rows, 1))
    # axes = axes.flatten()
    comp_fractions = [get_comp_frac(td) for td in load_all_trial_descriptions()]
    best_entropy_ts = get_best_entropy_ts()
    dyad_amount = 58
    for i, (comp_frac, entropy_ts) in enumerate(zip(comp_fractions, best_entropy_ts)):
        sn.kdeplot(x=entropy_ts, ax=axes, hue=0, palette=[cmasher.gem.colors[int(255 * i / dyad_amount)]],
                   alpha=.6)
    axes.set_ylim(0, 10)

# for entropy_ts, ax in zip(best_entropy_ts, axes):
    #     sn.kdeplot(x=entropy_ts, ax=axes[0])


