import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn
from scipy.stats import pearsonr

from _util import save_fig
from glm.plotting.acc_fst import get_best_model_acc
from glm.plotting.class_comparison import fontdict
from glm.plotting.entropy_ts import get_best_entropy_ts
from glm.plotting.entropy_vs_duration_2 import get_concat_td, remove_head
from glm.plotting.performance_plot import get_best_model_for_dyad
from inefficiencies_full import comp_frac_x_format
from trial_descriptor import get_comp_frac, unsorted_load_all_trial_descriptions

model_selection = [
    'bias',
    '1_prev_t',
    'effort_1_prev_t',
    'effort_2_prev_t',
    # 'effort_3_prev_t',
    'effort_1_prev_t_invite',
    'effort_2_prev_t_invite',
    'effort_3_prev_t_invite',
    # 'effort_4_prev_t_invite',
]

# model_selection = None

# model_selection = [
#     'bias',
#     '1_prev_t',
#     'effort_1_prev_t',
#     'effort_1_prev_t_invite',
#     'effort_2_prev_t_invite',
# ]

def key_func(x):
    return pd.Series([model_selection.index(model_name) for model_name in x])


def get_entropy_correlations(filename='../unsorted_performance.pkl'):
    tds = [remove_head(get_concat_td(dyad_idx)) for dyad_idx in range(58)]
    distance_ols = [td['distance_ol'].to_numpy() for td in tds]
    distances = [td['distance'].to_numpy() for td in tds]
    best_entropy_ts = get_best_entropy_ts(filename=filename)
    correlations = list()
    for dyad_idx in range(58):
        measure = distance_ols[dyad_idx] / distances[dyad_idx]
        measure[np.isinf(measure)] = 1  # todo hotfix
        measure[np.isnan(measure)] = 0
        corr_coefficient, p_value = pearsonr(best_entropy_ts[dyad_idx], measure)
        if np.min(best_entropy_ts[dyad_idx]) == np.max(best_entropy_ts[dyad_idx]):
            correlations.append('Constant entropy')
            # correlations.append(-0.1)
            # correlations.append(0)
        elif p_value > 0.05:
            correlations.append('No correlation')
            # correlations.append(-0.2)
            # correlations.append(0)
        else:
            correlations.append(corr_coefficient)
    return correlations


def get_hist_plot_frame(filename='../unsorted_performance.pkl'):
    dyad_amount = 58
    correlations = get_entropy_correlations(filename=filename)
    to_plot = pd.DataFrame(dict(
        best_model=[get_best_model_for_dyad(i, filename=filename, model_selection=model_selection) for i in range(dyad_amount)],
        best_model_acc=get_best_model_acc(filename=filename, model_selection=model_selection),
        comp_frac=[get_comp_frac(td) for td in unsorted_load_all_trial_descriptions()],
        entropy_correlation=correlations,
    ))
    if model_selection is not None:
        to_plot = to_plot.sort_values('best_model', key=key_func)
    return to_plot


if __name__ == '__main__':
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.style.use('default')

    to_plot = get_hist_plot_frame()

    fig, axes = plt.subplots(2, 2, figsize=(5, 5))
    axes = axes.flatten()
    axes[1].axis('off')
    colors = ['#fb4f2f', '#e4ae38', '#e277c1', '#9367bc', '#247a99', '#164a64', '#0a222e'] #081d27'] #071922'] #0b2634']  #  , '#d62628' ]
    # colors = ['#fb4f2f', '#e4ae38', '#e277c1', '#9367bc', '#247a99', '#164a64', '#d62628']
    # colors = None
    if model_selection is None:
        colors = None
    sn.histplot(data=to_plot, x='comp_frac', hue='best_model', multiple='stack', palette=colors, bins=10, ax=axes[0])
    sn.stripplot(data=to_plot, x='best_model', y='best_model_acc', color='k', alpha=.6, ax=axes[2])
    sn.barplot(data=to_plot, x='best_model', y='best_model_acc', palette=colors, ax=axes[2], errorbar=None)
    sn.histplot(data=to_plot, x='comp_frac', bins=10, ax=axes[3])
    plt.subplots_adjust(left=.15, bottom=.15, right=.9, top=.9, wspace=.6, hspace=.7) #.55)
    for ax_label, ax in zip('ABC', axes[[0, 2, 3]]):
        ax.text(-0.06, 1.04, ax_label, transform=ax.transAxes, size=10)
        ax.set_box_aspect(1)
    for ax in [axes[0], axes[3]]:
        ax.set_xlim(0, 1)
        ax.set_xlabel('Fraction of\nsingle targets collected $\\Phi$', fontdict=fontdict)
        ax.set_ylabel('Number of dyads', fontdict=fontdict)
        comp_frac_x_format(ax)
    axes[2].set_xlabel('Model', fontdict=fontdict)
    axes[2].set_ylabel('Test set accuracy', fontdict=fontdict)
    sn.move_legend(axes[0], loc='upper left', bbox_to_anchor=(1.2, 1.24), frameon=False)
    axes[2].set_xticks([])
    # axes[0].legend()
    # axes[0].legend(loc='upper left', bbox_to_anchor=(0, 0), frameon=False)
    save_fig()

