import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from _util import game_width_in_cm
from presentation.plotting import scatterplot, reg_plot, r_and_p_text
from trial_descriptor_2 import load_all_trial_descriptions, get_comp_frac

if __name__ == '__main__':
    tds = load_all_trial_descriptions()  # todo better would be looking at the data from rec # the trail descriptions include invites -> if we look per agent -> we have many inviting agents that do nothing -> bad value
    comp_fractions = np.array([get_comp_frac(td) for td in tds])
    agent_speeds = np.array([np.mean(np.concatenate([td['p0_speed'], td['p0_speed']])) for td in tds])
    agent_curvatures = np.array([np.mean(np.concatenate([td['p0_traj_len_due_to_curvature'],
                                                         td['p1_traj_len_due_to_curvature']])) for td in tds])
    agent_speeds *= game_width_in_cm
    agent_curvatures *= game_width_in_cm
    to_plot = pd.DataFrame(dict(
        comp_fractions=comp_fractions,
        agent_speeds=agent_speeds,
        agent_curvatures=agent_curvatures,
    ))
    fig, axes = plt.subplots(1, 2)
    axes = axes.flatten()
    scatterplot(to_plot, 'comp_fractions', 'agent_speeds', ax=axes[0])
    reg_plot(comp_fractions, agent_speeds, ax=axes[0])
    r_and_p_text(comp_fractions, agent_speeds, ax=axes[0])
    scatterplot(to_plot, 'comp_fractions', 'agent_curvatures', ax=axes[1])
    reg_plot(comp_fractions, agent_curvatures, ax=axes[1])
    r_and_p_text(comp_fractions, agent_curvatures, ax=axes[1])
