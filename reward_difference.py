import numpy as np

from reward_decomposition import get_mean_in_cm, get_agent_rewards
from trial_classes import plot_aic_poly
from trial_descriptor import load_all_trial_descriptions, get_comp_frac

if __name__ == '__main__':  # todo delete this file
    tds = load_all_trial_descriptions()
    agent_mean_reward, low_rewards, high_rewards = get_agent_rewards(tds)
    y_reward_min, y_reward_max = min(agent_mean_reward), max(agent_mean_reward)
    trial_durations = np.array([td['duration'].to_numpy().mean() for td in tds])
    comp_fractions = [get_comp_frac(td) for td in tds]
    distance = get_mean_in_cm('distance', tds)
    speed = get_mean_in_cm('speed', tds)
    additional_effort = get_mean_in_cm('distance_ol', tds)
    effort_from_past_target = get_mean_in_cm('chosen_effort', tds) + get_mean_in_cm('path_shortening', tds)
    effort_reduction_by_ps = get_mean_in_cm('path_shortening', tds)
    xspace, speed_curve = plot_aic_poly(comp_fractions, speed, None)
    _, additional_effort_curve = plot_aic_poly(comp_fractions, additional_effort, None)
    _, effort_from_past_target_curve = plot_aic_poly(comp_fractions, effort_from_past_target, None)
    _, effort_reduction_by_ps_curve = plot_aic_poly(comp_fractions, effort_reduction_by_ps, None)
    distance_curve = effort_from_past_target_curve + additional_effort_curve - effort_reduction_by_ps_curve
    duration_curve = distance_curve / speed_curve
