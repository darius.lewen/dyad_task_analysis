import numpy as np
from matplotlib import pyplot as plt

from _util import save_fig
from cmap_generator import get_cmap
from reward_decomposition import get_agent_rewards
from trial_descriptor import load_all_trial_descriptions, get_comp_frac

if __name__ == '__main__':
    plt.figure(figsize=(1.8, 1.8))
    # plt.figure(figsize=(2.2, 2.2))
    plt.subplots_adjust(bottom=.25, left=.3, top=.98, right=.95)
    tds = load_all_trial_descriptions()
    comp_fractions = [get_comp_frac(td) for td in tds]
    a, b, c = get_agent_rewards(tds)
    agent_mean_reward, low_rewards, high_rewards = np.array(a), np.array(b), np.array(c)
    width = .05
    arrow_len = .3
    color = 'k'
    alpha = 1.
    linewidth = 1.4
    comp_fractions = np.array(comp_fractions)
    cmap = get_cmap()
    special_markers = {2: 'd', 4: 'd', 7: 'd', 53: 'P'}
    for i, (comp_frac, mean, low, high) in enumerate(zip(comp_fractions, agent_mean_reward, low_rewards, high_rewards)):
        comp_frac_c = cmap.colors[int(min([comp_frac, 1]) * 255)]
        for c, scalar in zip(['k', comp_frac_c], [1.3, 1]):
            if high - low > .29:
                plt.plot([i, i], [low, high], c=c, alpha=alpha, linewidth=linewidth * scalar,
                         solid_capstyle='round', zorder=10)
            else:
                marker = 'o'
                s = linewidth * scalar
                if i in special_markers.keys():
                    marker = special_markers[i]
                    s *= 3
                if c == 'k':
                    s *= 1.05
                plt.scatter([i], [low + (high - low) / 2], c=c, s=s, marker=marker, zorder=20)
    ax = plt.gca()
    ax.set_xlabel('Dyad')
    ax.set_ylabel('Payoff (€)')
    ax.set_box_aspect(1)
    ax.set_xticks([0, 29, 58])
    save_fig()
