import cmasher
import pandas as pd
import seaborn as sn
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap
from matplotlib.gridspec import GridSpec


filename = 'colors.npy'


def get_cmap(path=None):
    def col_to_np(col):
        r = int(col[1:3], 16) / 256
        b = int(col[3:5], 16) / 256
        g = int(col[5:], 16) / 256
        return np.array([r, b, g])

    def gaussian(x, mu, sigma):
        return np.exp(-.5 * ((x - mu) / sigma) ** 2) / (sigma * np.sqrt(2 * np.pi))

    # c0 = col_to_np('#164A64')
    # c1 = col_to_np('#FB4F2F') * .8
    # c2 = col_to_np('#E277C1')

    c0 = col_to_np('#D62628') * .5
    c1 = col_to_np('#9367BC') * .8
    c2 = col_to_np('#E277C1')

    # filter_size = 9
    filter_0_size = 120
    filter_1_size = 250
    color_amount = 256
    pre_color_amount = color_amount + filter_0_size + filter_1_size - 2

    modality0 = 0
    modality1 = 1/3
    modality2 = 1
    modality1_weighting = .8
    border0 = int(((1 - modality1_weighting) * modality0 + modality1_weighting * modality1) * pre_color_amount)
    border1 = int(((1 - modality1_weighting) * modality2 + modality1_weighting * modality1) * pre_color_amount)
    border0 = int(border0 * .45)
    border1 = int(border1 * 1.55 * .9)
    # border1 = int(border1 * 1)
    # border0 = int((modality0 + modality1) / 2 * pre_color_amount) - 15
    # border1 = int((modality1 + modality2) / 2 * pre_color_amount)
    colors = [c0 for _ in range(border0)] + [c1 for _ in range(border0, border1)] + [c2 for _ in range(border1, pre_color_amount)]
    colors = np.array(colors)

    colors0 = colors[:int(len(colors) * 1/3)]
    colors1 = colors[int(len(colors) * 1/3):]

    def convolve_colors(colors, filter_size):
        gaussian_filter = gaussian(np.linspace(-5, 5, filter_size), 0, 2)
        gaussian_filter /= np.sum(gaussian_filter)
        r_values = colors[:, 0]
        g_values = colors[:, 1]
        b_values = colors[:, 2]
        r_values = np.convolve(r_values, gaussian_filter, 'valid')
        g_values = np.convolve(g_values, gaussian_filter, 'valid')
        b_values = np.convolve(b_values, gaussian_filter, 'valid')
        return np.stack([r_values, g_values, b_values]).T

    new_colors = np.concatenate([convolve_colors(colors0, filter_0_size),
                                 convolve_colors(colors1, filter_1_size)])

    # r_values[int(modality1 + filter_size - 1):] = np.convolve(r_values[int(modality1):], gaussian_filter, 'valid')
    # g_values[int(modality1 + filter_size - 1):] = np.convolve(g_values[int(modality1):], gaussian_filter, 'valid')
    # b_values[int(modality1 + filter_size - 1):] = np.convolve(b_values[int(modality1):], gaussian_filter, 'valid')

    new_colors /= np.max(new_colors)
    #return ListedColormap(new_colors)  todo disabled
    return cmasher.gem


if __name__ == '__main__':
    from _util import save_fig
    from pred_performance import violin_swarm_mean_plot
    from trial_descriptor import get_comp_frac, load_all_trial_descriptions

    dummy = np.linspace(0, 1, 100).reshape((10, 10))
    cbar_kws = dict(label='test', fraction=.3)
    tds = load_all_trial_descriptions()
    comp_fractions = [get_comp_frac(td) for td in tds]
    to_plot = pd.DataFrame(dict(comp_fractions=comp_fractions))
    # fig, axes = plt.subplots(1, 2)
    axes = list()


    def col_to_np(col):
        r = int(col[1:3], 16) / 256
        b = int(col[3:5], 16) / 256
        g = int(col[5:], 16) / 256
        return np.array([r, b, g])

    def gaussian(x, mu, sigma):
        return np.exp(-.5 * ((x - mu) / sigma) ** 2) / (sigma * np.sqrt(2 * np.pi))

    # c0 = col_to_np('#164A64')
    # c1 = col_to_np('#FB4F2F') * .8
    # c2 = col_to_np('#E277C1')

    c0 = col_to_np('#D62628') * .5
    c1 = col_to_np('#9367BC') * .8
    c2 = col_to_np('#E277C1')

    # filter_size = 9
    filter_0_size = 120
    filter_1_size = 250
    color_amount = 256
    pre_color_amount = color_amount + filter_0_size + filter_1_size - 2

    modality0 = 0
    modality1 = 1/3
    modality2 = 1
    modality1_weighting = .8
    border0 = int(((1 - modality1_weighting) * modality0 + modality1_weighting * modality1) * pre_color_amount)
    border1 = int(((1 - modality1_weighting) * modality2 + modality1_weighting * modality1) * pre_color_amount)
    border0 = int(border0 * .45)
    border1 = int(border1 * 1.55 * .9)
    # border1 = int(border1 * 1)
    # border0 = int((modality0 + modality1) / 2 * pre_color_amount) - 15
    # border1 = int((modality1 + modality2) / 2 * pre_color_amount)
    colors = [c0 for _ in range(border0)] + [c1 for _ in range(border0, border1)] + [c2 for _ in range(border1, pre_color_amount)]
    colors = np.array(colors)

    colors0 = colors[:int(len(colors) * 1/3)]
    colors1 = colors[int(len(colors) * 1/3):]

    def convolve_colors(colors, filter_size):
        gaussian_filter = gaussian(np.linspace(-5, 5, filter_size), 0, 2)
        gaussian_filter /= np.sum(gaussian_filter)
        r_values = colors[:, 0]
        g_values = colors[:, 1]
        b_values = colors[:, 2]
        r_values = np.convolve(r_values, gaussian_filter, 'valid')
        g_values = np.convolve(g_values, gaussian_filter, 'valid')
        b_values = np.convolve(b_values, gaussian_filter, 'valid')
        return np.stack([r_values, g_values, b_values]).T

    new_colors = np.concatenate([convolve_colors(colors0, filter_0_size),
                                 convolve_colors(colors1, filter_1_size)])

    # r_values[int(modality1 + filter_size - 1):] = np.convolve(r_values[int(modality1):], gaussian_filter, 'valid')
    # g_values[int(modality1 + filter_size - 1):] = np.convolve(g_values[int(modality1):], gaussian_filter, 'valid')
    # b_values[int(modality1 + filter_size - 1):] = np.convolve(b_values[int(modality1):], gaussian_filter, 'valid')

    new_colors /= np.max(new_colors)

    np.save(filename, new_colors)

    # gaussian_filter = gaussian_filter[:, None] * np.ones((1, 3))
    # print(colors.shape)
    # print(gaussian_filter.shape)
    # colors = np.convolve(colors, gaussian_filter.T, 'same')

    new_cmap = ListedColormap(new_colors)

    # sigma_scalar = 3
    # sigma_scalar_2 = 2
    # c0_distri = gaussian(xspace, 0., .0051 * sigma_scalar * sigma_scalar_2) * c0[:, None]
    # c1_distri = gaussian(xspace, 1/3, .03 * sigma_scalar * sigma_scalar_2) * c1[:, None]
    # c2_distri = gaussian(xspace, 1., .1 * sigma_scalar) * c2[:, None]
    # summed_distributions = c0_distri + c1_distri + c2_distri
    # normalized_distributions = summed_distributions / np.linalg.norm(summed_distributions, axis=0)
    # up_scaled_distributions = normalized_distributions * np.linspace(.4, 1, 256)
    # up_scaled_distributions /= np.max(up_scaled_distributions)
    # new_cmap = ListedColormap(up_scaled_distributions.T)

    cmaps = [cmasher.gem, new_cmap]
    scaler = .7
    fig = plt.figure(figsize=(6 * scaler, 3 * len(cmaps) * scaler))
    grid_spec = GridSpec(len(cmaps), 2, width_ratios=[2, 1], height_ratios=[1 for _ in range(len(cmaps))],
                         wspace=.7, hspace=.5)
    for row, cmap in enumerate(cmaps):
        axes.extend([fig.add_subplot(grid_spec[row, 0]), fig.add_subplot(grid_spec[row, 1])])
        sn.heatmap(dummy, ax=axes[-2], cmap=cmap, alpha=1., cbar_kws=cbar_kws)
        violin_swarm_mean_plot(to_plot, axes[-1], marker_size=3, alpha=1., cmap=cmap)
    save_fig()

